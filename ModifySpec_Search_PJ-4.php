<?php
/*
 * Copyright (C) 2015 Foxlink SCBG CAE Dept. All rights reserved
 * Project: Wanhao System
 * File Name: Spec Search-2
 * Function: Part Number List
 * Author: Bruce Huang
 * --------------------------------------------------
 * Rev: 1.2 Date: PM 04:34 2015/01/28 Modifier: Bruce Huang
 * --------------------------------------------------
 */
session_start();
include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
error_reporting(0);
mysqli_select_db($connect_spec, $database_spec);

$PartNumber = $_GET['PN'];
$substr1               = strrpos($PartNumber_MoldNumber, '_');

$Type                = substr($PartNumber, 0, 3);
$Type1       = substr($PartNumber, 5, 4);
$Type2       = substr($PartNumber, 5, 2);

if ($Type == '819' or $Type == '818') {
    $db_Name             = 'modify_spec_assembly';
} elseif ($Type == '810') {
    $db_Name             = 'modify_spec_molding';
} elseif ($Type == '812'){
    $db_Name             = 'modify_spec_iqc';
}
else{
    if($Type2 == 'GL' or $Type2 == '21' or $Type2 == '1H' or $Type2 == '43'){
        $db_Name             = 'modify_spec_plating';
    }
    else if($Type2 == '9W'){
        $db_Name             = 'modify_spec_welding';
    }
    else if($Type2 == '9U'){
        $db_Name             = 'modify_spec_blasting';
    }
    else if($Type2 == '90'){
        $db_Name = 'modify_spec_stamping';
    }
}
$_SESSION['db_Name'] = $db_Name;
$query_listout_S     = "SELECT * from " . $db_Name . " where PartNumber='$PartNumber' group by PartNumber,DimNO order by PartNumber";
$listout_S = mysqli_query($connect_spec, $query_listout_S) or die(mysqli_error());
?>

<!DOCTYPE HTML>
<head>
    <meta charset="utf-8">
    <title>Untitled Document</title>

    <style type="text/css">
        body {
          font: normal medium/1.4 sans-serif;
      }
      table {
          border-collapse: collapse;
      }
      th{
          padding: 0.25rem;
          text-align: center;
          border: 1px solid #ccc;
          background: #888888;
          font-size:15px;

      }
      td {
          padding: 0.25rem;
          text-align: center;
          border: 1px solid #ccc;
          font-size:13px;

      }
      B{
       font-family:"Arial Black", Gadget, sans-serif;
       color:#00000;
   }
   L{
    font-family:"Arial Black", Gadget, sans-serif;
    color:#cc6a08;
}
tbody tr:nth-child(odd) {
  background: #eee;
}

.ifile {
 position:absolute;opacity:0;filter:alpha(opacity=0);

}
</style>

</head>
<body>


    <table width='1750' method="post" style="table-layout:fixed">
        <thead>

            <?php
            echo "<table width='2000' method='post' style='table-layout:fixed'>";
            echo "<thead>";

            if ($Type == '810') {
                echo "<th width='35'>專案名稱</th>";
                echo "<th width='25'>專案描述</th>";
                echo "<th width='15'>版次</th>";
                echo "<th width='25'>模穴數</th>";
                echo "<th width='15'>模穴號</th>";
                echo "<th width='15'>FAI號</th>";
                echo "<th width='100'>FAI描述</th>";
                echo "<th width='25'>Spec值</th>";
                echo "<th width='25'>Spec上限值</th>";
                echo "<th width='25'>Spec下限值</th>";
                echo "<th width='25'>Spec PPK</th>";
                echo "<th width='25'>期望上限值</th>";
                echo "<th width='25'>期望下限值</th>";
                echo "<th width='40'>期望標準差上限值</th>";
                echo "<th width='40'>期望標準差下限值</th>";
                echo "<th width='25'>期望結果</th>";
                echo "<th width='25'>上傳者</th>";
                echo "<th width='35'>上傳時間</th>";
                echo "<th width='15'>備註</th>";
                echo "<th width='100'>Spec檔案名稱</th>";
            } elseif ($Type == '819' or $Type == '818') {
                echo "<th width='35'>專案名稱</th>";
                echo "<th width='25'>專案描述</th>";
                echo "<th width='15'>版次</th>";
                echo "<th width='15'>FAI號</th>";
                echo "<th width='100'>FAI描述</th>";
                echo "<th width='25'>Spec值</th>";
                echo "<th width='25'>Spec上限值</th>";
                echo "<th width='25'>Spec下限值</th>";
                echo "<th width='25'>Spec PPK</th>";
                echo "<th width='25'>期望上限值</th>";
                echo "<th width='25'>期望下限值</th>";
                echo "<th width='40'>期望標準差上限值</th>";
                echo "<th width='40'>期望標準差下限值</th>";
                echo "<th width='25'>期望結果</th>";
                echo "<th width='25'>上傳者</th>";
                echo "<th width='35'>上傳時間</th>";
                echo "<th width='35'>備註</th>";
                echo "<th width='100'>Spec檔案名稱</th>";
            } elseif ($Type == '812') {
                echo "<th width='35'>專案名稱</th>";
                echo "<th width='25'>專案描述</th>";
                echo "<th width='15'>版次</th>";
                echo "<th width='15'>FAI號</th>";
                echo "<th width='100'>FAI描述</th>";
                echo "<th width='25'>Spec值</th>";
                echo "<th width='25'>Spec上限值</th>";
                echo "<th width='25'>Spec下限值</th>";
                echo "<th width='25'>Spec PPK</th>";
                echo "<th width='25'>期望上限值</th>";
                echo "<th width='25'>期望下限值</th>";
                echo "<th width='40'>期望標準差上限值</th>";
                echo "<th width='40'>期望標準差下限值</th>";
                echo "<th width='25'>期望結果</th>";
                echo "<th width='25'>上傳者</th>";
                echo "<th width='35'>上傳時間</th>";
                echo "<th width='35'>備註</th>";
                echo "<th width='100'>Spec檔案名稱</th>";
            } else {
                echo "<th width='35'>專案名稱</th>";
                echo "<th width='25'>專案描述</th>";
                echo "<th width='15'>版次</th>";
                echo "<th width='15'>FAI號</th>";
                echo "<th width='100'>FAI描述</th>";
                echo "<th width='25'>Spec值</th>";
                echo "<th width='25'>Spec上限值</th>";
                echo "<th width='25'>Spec下限值</th>";
                echo "<th width='25'>Spec PPK</th>";
                echo "<th width='25'>期望上限值</th>";
                echo "<th width='25'>期望下限值</th>";
                echo "<th width='40'>期望標準差上限值</th>";
                echo "<th width='40'>期望標準差下限值</th>";
                echo "<th width='25'>期望結果</th>";
                echo "<th width='25'>上傳者</th>";
                echo "<th width='35'>上傳時間</th>";
                echo "<th width='15'>備註</th>";
                echo "<th width='100'>Spec檔案名稱</th>";
            }
            echo "</thead>";
            echo "<tbody>";
            ?>

            <?php
            $NGDim_array     = array();
            while ($listoutS = mysqli_fetch_assoc($listout_S)) {

                if ($Type == '810') {
                    if ($listoutS['Expect_Result'] == 'NG' or $listoutS['Expect_UpperData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperStdv'] < $listoutS['Expect_LowerStdv'] or $listoutS['Expect_LowerStdv'] > $listoutS['Expect_UpperStdv']) {
                        $color = "<font color='#EE0000'>";
                        $NGDim_array[] = $listoutS['DimNO'];
                    } else { $color = "<font color='#000000'>";}
                    echo "<tr>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['ProjectName'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['ProjectDescription'] . "</td>";
                    echo "<td align=center  width='15'>" . $color . $listoutS['Rev'] . "</td>";
                    echo "<td align=center  width='25'>" . $color . $listoutS['CavNums'] . "</td>";
                    echo "<td align=center  width='25'>" . $color . $listoutS['CavNumber'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimNO'] . "</td>";
                    echo "<td align=center  width='100'>" . $color . $listoutS['DimDesc'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimSpec'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimUpper'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimLower'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['PPKSpec'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['Expect_UpperData'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['Expect_LowerData'] . "</td>";
                    echo "<td align=center  width='40'>" . $color . $listoutS['Expect_UpperStdv'] . "</td>";
                    echo "<td align=center  width='40'>" . $color . $listoutS['Expect_LowerStdv'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['Expect_Result'] . "</td>";
                    echo "<td align=center  width='50'>" . $color . $listoutS['CreateName'] . "</td>";
                    echo "<td align=center  width='50'>" . $color . $listoutS['CreateDataTime'] . "</td>";
                    echo "<td align=center  width='15'>" . $color . $listoutS['Remark'] . "</td>";
                    echo "<td align=center  width='100'>" . $color . $listoutS['fileName'] . "</td>";
                    echo "</tr>";

                } elseif ($Type == '819' or $Type == '818') {
                    if ($listoutS['Expect_Result'] == 'NG' or $listoutS['Expect_UpperData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperStdv'] < $listoutS['Expect_LowerStdv'] or $listoutS['Expect_LowerStdv'] > $listoutS['Expect_UpperStdv']) {
                        $color = "<font color='#EE0000'>";
                        $NGDim_array[] = $listoutS['DimNO'];
                    } else { $color = "<font color='#000000'>";}
                    echo "<tr>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['ProjectName'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['ProjectDescription'] .  "</td>";
                    echo "<td align=center  width='15'>" . $color . $listoutS['Rev'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimNO'] . "</td>";
                    echo "<td align=center  width='100'>" . $color . $listoutS['DimDesc'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimSpec'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimUpper'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimLower'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['PPKSpec'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['Expect_UpperData'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['Expect_LowerData'] . "</td>";
                    echo "<td align=center  width='40'>" . $color . $listoutS['Expect_UpperStdv'] . "</td>";
                    echo "<td align=center  width='40'>" . $color . $listoutS['Expect_LowerStdv'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['Expect_Result'] . "</td>";
                    echo "<td align=center  width='50'>" . $color . $listoutS['CreateName'] . "</td>";
                    echo "<td align=center  width='50'>" . $color . $listoutS['CreateDataTime'] . "</td>";
                    echo "<td align=center  width='50'>" . $color . $listoutS['Remark'] . "</td>";
                    echo "<td align=center  width='100'>" . $color . $listoutS['fileName'] . "</td>";
                    echo "</tr>";
                } elseif ($Type == '812') {
                    if ($listoutS['Expect_Result'] == 'NG' or $listoutS['Expect_UpperData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperStdv'] < $listoutS['Expect_LowerStdv'] or $listoutS['Expect_LowerStdv'] > $listoutS['Expect_UpperStdv']) {
                        $color = "<font color='#EE0000'>";
                        $NGDim_array[] = $listoutS['DimNO'];
                    } else { $color = "<font color='#000000'>";}
                    echo "<tr>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['ProjectName'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['ProjectDescription'] .  "</td>";
                    echo "<td align=center  width='15'>" . $color . $listoutS['Rev'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimNO'] . "</td>";
                    echo "<td align=center  width='100'>" . $color . $listoutS['DimDesc'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimSpec'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimUpper'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimLower'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['PPKSpec'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['Expect_UpperData'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['Expect_LowerData'] . "</td>";
                    echo "<td align=center  width='40'>" . $color . $listoutS['Expect_UpperStdv'] . "</td>";
                    echo "<td align=center  width='40'>" . $color . $listoutS['Expect_LowerStdv'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['Expect_Result'] . "</td>";
                    echo "<td align=center  width='50'>" . $color . $listoutS['CreateName'] . "</td>";
                    echo "<td align=center  width='50'>" . $color . $listoutS['CreateDataTime'] . "</td>";
                    echo "<td align=center  width='50'>" . $color . $listoutS['Remark'] . "</td>";
                    echo "<td align=center  width='100'>" . $color . $listoutS['fileName'] . "</td>";
                    echo "</tr>";
                } else {
                    if ($listoutS['Expect_Result'] == 'NG' or $listoutS['Expect_UpperData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperStdv'] < $listoutS['Expect_LowerStdv'] or $listoutS['Expect_LowerStdv'] > $listoutS['Expect_UpperStdv']) {
                        $color = "<font color='#EE0000'>";
                        $NGDim_array[] = $listoutS['DimNO'];
                    } else { $color = "<font color='#000000'>";}
                    echo "<tr>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['ProjectName'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['ProjectDescription'] . "</td>";
                    echo "<td align=center  width='15'>" . $color . $listoutS['Rev'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimNO'] . "</td>";
                    echo "<td align=center  width='100'>" . $color . $listoutS['DimDesc'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimSpec'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimUpper'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['DimLower'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['PPKSpec'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['Expect_UpperData'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['Expect_LowerData'] . "</td>";
                    echo "<td align=center  width='40'>" . $color . $listoutS['Expect_UpperStdv'] . "</td>";
                    echo "<td align=center  width='40'>" . $color . $listoutS['Expect_LowerStdv'] . "</td>";
                    echo "<td align=center  width='35'>" . $color . $listoutS['Expect_Result'] . "</td>";
                    echo "<td align=center  width='50'>" . $color . $listoutS['CreateName'] . "</td>";
                    echo "<td align=center  width='50'>" . $color . $listoutS['CreateDataTime'] . "</td>";
                    echo "<td align=center  width='15'>" . $color . $listoutS['Remark'] . "</td>";
                    echo "<td align=center  width='100'>" . $color . $listoutS['fileName'] . "</td>";
                    echo "</tr>";
                }
            }
            $NGCount = count($NGDim_array);
            if ($NGCount > 0) {
                $NGDim = implode(",", $NGDim_array);
                echo "<L></br>尺寸:" . $NGDim . " 異常!請確認是否為以下項目!</L></br></br>  1.[期望上/下限值]超出[Spec上/下限值]</br>  2.[期望標準差上限值]低於[期望標準差下限值]</br>  3.[期望標準差下限值]高於[期望標準差上限值]</br>  4.確認[期望結果]是否設定NG</br>  ";
            }
            ?>
        </tbody>
    </table>
</body>
</html>


