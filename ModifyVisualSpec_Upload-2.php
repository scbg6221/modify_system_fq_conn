<!DOCTYPE HTML><head>
<meta charset="utf-8">
<title>Untitled Document</title></head>

<script>

</script>
<style type="text/css">
body {
  font: normal medium/1.4 sans-serif;
}
table {
  border-collapse: collapse;
}
th{
  padding: 0.25rem;
  text-align: center;
  border: 1px solid #ccc;
  background: #888888;
  font-size:15px;

}
td {
  padding: 0.25rem;
  text-align: center;
  border: 1px solid #ccc;
  font-size:13px;

}
B{
    font-family:"Arial Black", Gadget, sans-serif;
    color:#00000;
    }
tbody tr:nth-child(odd) {
  background: #eee;
}

.ifile {
    position:absolute;opacity:0;filter:alpha(opacity=0);

}
</style>
</head>
<body>

<?php
/*
 * Copyright (C) 2015 Foxlink SCBG CAE Dept. All rights reserved
 * Project: Wanhao System
 * File Name: Spec Upload-2
 * Function: Spec Insert Data Base & Display
 * Author: Bruce Huang
 * --------------------------------------------------
 * Rev: 1.2 Date: PM 04:34 2015/01/28 Modifier: Bruce Huang
 * --------------------------------------------------
 */
session_start();
include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';
error_reporting(0);

$DateTime   = date("Y-m-d H:i:s");
$CreateName = $_SESSION['user'];
//$Type       = substr($_SESSION['PartNumber'], 0, 3);
//$Type = $_SESSION['Type'];
//$allPNRow = $_SESSION['allPNRow'];

$db_Name             = 'modify_visual_spec_assembly';
$_SESSION['db_Name'] = $db_Name;
$ProjectName_array   = $_SESSION['ProjectName_array'];
$ProjectDesc_array   = $_SESSION['ProjectDesc_array'];
$PartNumber_array    = $_SESSION['PartNumber_array'];
$Rev_array           = $_SESSION['Rev_array'];

mysqli_select_db($connect_spec, $database_spec);
$unique_PN   = array_values(array_unique($PartNumber_array));
$uniquePNRow = count($unique_PN);

for ($delPNrow = 0; $delPNrow < $uniquePNRow; $delPNrow++) {
    $deletePN = "delete FROM  " . $db_Name . " WHERE `PartNumber` = '$unique_PN[$delPNrow]' AND Rev='$Rev_array[$delPNrow]' ";
    //echo $deletePN;
    $deletePN_query = mysqli_query($connect_spec, $deletePN) or die("警告 ： 刪除modify_visual_spec失敗");
}
//print_r($PartNumber_array);
//echo $allPNRow;
/////check this part number of upload file is exist or not on data base/////

/////insert data base & display/////
//else {
$PartNumber_array_ok = array_filter($PartNumber_array);
$allPNRow            = count($PartNumber_array_ok);

for ($PNrow = 0; $PNrow < $allPNRow; $PNrow++) {
    $uploaddir = "..\..\Spec\Modify_System\FQ_Conn\VisualSpec/";
    $fileName  = $_SESSION['upload_file_name'];
    $Error1    = $_SESSION['Error'];
    $tmpfile   = $_SESSION['tmpfile'];
    $file2     = $_SESSION['file2'];
    $fileExt   = substr($fileName, strrpos($fileName, '.') + 1);

    if ($Error1 == 0 & $fileExt == "xlsx") {
        //設定&開啟Spec Excel
        $phpexcel_spec = PHPExcel_IOFactory::load($uploaddir . $file2);
        //$phpexcel_spec->setActiveSheetIndex(0);
        $phpexcel_spec->setActiveSheetIndexByName('Spec');
        $xlsx_spec = $phpexcel_spec->getActiveSheet();
        $allRow    = $xlsx_spec->getHighestRow(); //取得最大行數

        $x = 1;
        for ($row = 2; $row <= $allRow; $row++) {
            $OKData_array = array();

            //抓取Spec資料
            $checkstop          = $xlsx_spec->getCell('A' . $row)->getValue();
            $sort               = $xlsx_spec->getCell('B' . $row)->getValue();
            $Part_Number_V      = $xlsx_spec->getCell('C' . $row)->getValue();
            $checkitem          = $xlsx_spec->getCell('D' . $row)->getValue();
            $up_lim             = $xlsx_spec->getCell('E' . $row)->getValue();
            $down_lim           = $xlsx_spec->getCell('F' . $row)->getValue();
            $Remark             = $xlsx_spec->getCell('G' . $row)->getValue();
            $ProjectName        = $ProjectName_array[$PNrow];
            $ProjectDescription = $ProjectDesc_array[$PNrow];
            $PartNumber         = $PartNumber_array[$PNrow];
            $Rev                = $Rev_array[$PNrow];

            $sql = "INSERT INTO " . $db_Name . " (`id`,`PartNumber`,`ProjectName`,`Rev`,`checkstop`,`sort`,`Part_Number_V`,`checkitem`,`up_lim`,`down_lim`, `CreateName`, `CreateDataTime`, `Remark`)VALUES('$x','$PartNumber','$ProjectName','$Rev','$checkstop','$sort','$Part_Number_V','$checkitem','$up_lim','$down_lim','$CreateName','$DateTime','$Remark')";

            if ($checkstop != '') {
                //echo $sql;
                $result = mysqli_query($connect_spec, $sql) or die("警告 ： 輸入modify_visual_spec失敗");
            }
            $x++;
        }
    }

//echo $sql;
    $delete1       = "delete FROM " . $db_Name . "  WHERE id = '0'";
    $delete1_query = mysqli_query($connect_spec, $delete1) or die("警告 ： 刪除modify_visual_spec失敗-1");
    $delete2       = "delete FROM " . $db_Name . "  WHERE checkstop = ''";
    $delete2_query = mysqli_query($connect_spec, $delete2) or die("警告 ： 刪除modify_visual_spec失敗-2");
    echo "<B>料號:" . $PartNumber_array[$PNrow] . "輸入資料庫成功!!</B></BR>";
}

if ($fileExt != "xlsx") {
    echo "<L>請上傳 .xlsx 格式的檔案!!</L>";
}

?>
<?php
echo "<table width='1400' method='post' style='table-layout:fixed'>";
echo "<thead>";
echo "<th width='35'>專案名稱</th>";
echo "<th width='15'>版次</th>";
echo "<th width='15'>檢查工站</th>";
echo "<th width='25'>物料</th>";
echo "<th width='25'>物料料號</th>";
echo "<th width='25'>檢查項目</th>";
echo "<th width='25'>上限</th>";
echo "<th width='25'>下限</th>";
echo "<th width='40'>上傳者</th>";
echo "<th width='40'>上傳時間</th>";
echo "<th width='25'>備註</th>";
echo "</thead>";
echo "<tbody>";
?>

<?php
$query_listout_S = "SELECT * FROM " . $db_Name . " WHERE PartNumber = '$PartNumber' AND Rev='$Rev' order by id "; // 需修改項6
$listout_S       = mysqli_query($connect_spec, $query_listout_S) or die("警告 ： 搜尋Spec失敗");
while ($listoutS = mysqli_fetch_assoc($listout_S)) {
    echo "<tr>";
    echo "<td align=center  width='35'>" . $ProjectName . "</td>";
    echo "<td align=center  width='15'>" . $listoutS['Rev'] . "</td>";
    echo "<td align=center  width='35'>" . $listoutS['checkstop'] . "</td>";
    echo "<td align=center  width='25'>" . $listoutS['sort'] . "</td>";
    echo "<td align=center  width='35'>" . $listoutS['Part_Number_V'] . "</td>";
    echo "<td align=center  width='35'>" . $listoutS['checkitem'] . "</td>";
    echo "<td align=center  width='25'>" . $listoutS['up_lim'] . "</td>";
    echo "<td align=center  width='25'>" . $listoutS['down_lim'] . "</td>";
    echo "<td align=center  width='50'>" . $listoutS['CreateName'] . "</td>";
    echo "<td align=center  width='50'>" . $listoutS['CreateDataTime'] . "</td>";
    echo "<td align=center  width='15'>" . $listoutS['Remark'] . "</td>";
    echo "</tr>";
}
?>
</tbody>
</table>
</body>
</html>


