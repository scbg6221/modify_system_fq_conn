<?php
/*
 * Copyright (C) 2015 Foxlink SCBG CAE Dept. All rights reserved
 * Project: Wanhao System
 * File Name: Spec Search-2
 * Function: Part Number List
 * Author: Bruce Huang
 * --------------------------------------------------
 * Rev: 1.2 Date: PM 04:34 2015/01/28 Modifier: Bruce Huang
 * --------------------------------------------------
 */
session_start();
include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/Connections/sfc_system_fq_icbu.php';
error_reporting(0);
mysqli_select_db($connect_spec, $database_spec);
mysqli_select_db($connect_sfc, $database_sfc);

$ProjectName = $_SESSION['ProjectName'];

$LN_Search_sql   = "SELECT REAL_LINE FROM project_with_real_line WHERE PROJECT_CODE='" . $ProjectName . "'";
$LN_Search_query = mysqli_query($connect_sfc, $LN_Search_sql) or die("警告 ： 搜尋LINE_NO失敗");
$num_LineNumber = mysqli_num_rows($LN_Search_query);

while ($LN_Search = mysqli_fetch_assoc($LN_Search_query)) {
    $LineNumber[]           = $LN_Search['REAL_LINE'];
}
$LineNumber=implode(" , ",$LineNumber);
/*
echo $num_LineNumber;
$LineNumber=implode(",",$LineNumber);
echo $LineNumber;
*/

$query_listout_asm = "SELECT ProjectName,PartNumber FROM modify_spec_assembly WHERE ProjectName='$ProjectName' group by ProjectName,PartNumber";
$listoutF_asm       = mysqli_query($connect_spec, $query_listout_asm);
$num_asm = mysqli_num_rows($listoutF_asm);

$query_listoutF_mold = "SELECT ProjectName,PartNumber,MoldNumber,CavNums FROM modify_spec_molding WHERE ProjectName='$ProjectName' group by ProjectName,PartNumber,MoldNumber";
$listoutF_mold       = mysqli_query($connect_spec, $query_listoutF_mold);
$num_mold = mysqli_num_rows($listoutF_mold);

$query_listoutF_stmp = "SELECT ProjectName,PartNumber,MoldNumber FROM modify_spec_stamping WHERE ProjectName='$ProjectName' group by ProjectName,PartNumber,MoldNumber";
$listoutF_stmp       = mysqli_query($connect_spec, $query_listoutF_stmp);
$num_stmp = mysqli_num_rows($listoutF_stmp);

$query_listoutF_plating = "SELECT ProjectName,PartNumber FROM modify_spec_plating WHERE ProjectName='$ProjectName' group by ProjectName,PartNumber";
$listoutF_plating      = mysqli_query($connect_spec, $query_listoutF_plating);
$num_plating = mysqli_num_rows($listoutF_plating);

$query_listoutF_welding = "SELECT ProjectName,PartNumber FROM modify_spec_welding WHERE ProjectName='$ProjectName' group by ProjectName,PartNumber";
$listoutF_welding     = mysqli_query($connect_spec, $query_listoutF_welding);
$num_welding = mysqli_num_rows($listoutF_welding);

$query_listoutF_blasting = "SELECT ProjectName,PartNumber FROM modify_spec_blasting WHERE ProjectName='$ProjectName' group by ProjectName,PartNumber";
$listoutF_blasting      = mysqli_query($connect_spec, $query_listoutF_blasting);
$num_blasting = mysqli_num_rows($listoutF_blasting);

$query_listoutF_iqc = "SELECT ProjectName,PartNumber FROM modify_spec_iqc WHERE ProjectName='$ProjectName' group by ProjectName,PartNumber";
$listoutF_iqc      = mysqli_query($connect_spec, $query_listoutF_iqc);
$num_iqc = mysqli_num_rows($listoutF_iqc);
?>

<!DOCTYPE HTML>
<head>
    <meta charset="utf-8">
    <title>Untitled Document</title>

    <style type="text/css">
        body {
          font: normal medium/1.4 sans-serif;
      }
      table {
          border-collapse: collapse;
      }
      th{
          padding: 0.25rem;
          text-align: center;
          border: 1px solid #ccc;
          background: #888888;
          font-size:15px;

      }
      td {
          padding: 0.25rem;
          text-align: center;
          border: 1px solid #ccc;
          font-size:13px;

      }
      B{
       font-family:"Arial Black", Gadget, sans-serif;
       color:#00000;
   }
   L{
    font-family:"Arial Black", Gadget, sans-serif;
    color:#cc6a08;
}
tbody tr:nth-child(odd) {
  background: #eee;
}

.ifile {
 position:absolute;opacity:0;filter:alpha(opacity=0);

}
</style>

</head>
<body>
    <form id="form1" name="form1" method="post" >

        <div class="Measure-Data-table-2">
            <table id="Measure-Data-table-2" class="sortable">
                <thead>
                    <BIG><B>##直接點選料號可查規格內容##</B></BIG></BR>
                    <?php
                    if ($num_asm != 0) {

                        echo"<br>
                        組裝:
                        <div class='Measure-Data-table-2'>
                        <table id='Measure-Data-table-2' class='sortable'>
                        <thead>
                        <th width='auto'><div align='center'>專案名稱</div></th>
                        <th width='auto'><div align='center'>料號</div></th>
                        <th width='auto'><div align='center'>線別</div></th>
                        </thead>
                        <div align='center'></div>
                        <tbody>";

                        while ($listout_asm = mysqli_fetch_assoc($listoutF_asm)) {

                            $PartNumber       = $listout_asm['PartNumber'];
                            $ProjectName         = $listout_asm['ProjectName'];

                            echo "<tr>";
                            echo "<td>" . $ProjectName . "</td>";
                            echo "<td onclick=javascript:location.href='ModifySpec_Search_PJ-4.php?PN=$PartNumber'><div align='left'><u><font color='blue'>" . $PartNumber . "</font></u></div></td>";
                            echo "<td>" . $LineNumber . "</td>";
                            echo "</tr>";
                        }
                    }
                    ?>
                </tbody>
            </table>

            <?php
            if ($num_stmp != 0) {

                echo"<br>
                沖壓:
                <div class='Measure-Data-table-2'>
                <table id='Measure-Data-table-2' class='sortable'>
                <thead>
                <th width='auto'><div align='center'>專案名稱</div></th>
                <th width='auto'><div align='center'>料號</div></th>
                <th width='auto'><div align='center'>模號</div></th>
                </thead>
                <div align='center'></div>
                <tbody>";

                while ($listout_stmp = mysqli_fetch_assoc($listoutF_stmp)) {

                    $PartNumber       = $listout_stmp['PartNumber'];
                    $MoldNumber         = $listout_stmp['MoldNumber'];

                    echo "<tr>";
                    echo "<td>" . $ProjectName . "</td>";
                    echo "<td onclick=javascript:location.href='ModifySpec_Search_PJ-4.php?PN=$PartNumber'><div align='left'><u><font color='blue'>" . $PartNumber . "</font></u></div></td>";
                    echo "<td>" . $MoldNumber . "</td>";
                    echo "</tr>";
                }
            }
            ?>
        </tbody>
    </table>

    <?php
    if ($num_mold != 0) {

        echo"<br>
        成型:
        <div class='Measure-Data-table-2'>
        <table id='Measure-Data-table-2' class='sortable'>
        <thead>
        <th width='auto'><div align='center'>專案名稱</div></th>
        <th width='auto'><div align='center'>料號</div></th>
        <th width='auto'><div align='center'>模號</div></th>
        <th width='auto'><div align='center'>穴數</div></th>
        </thead>
        <div align='center'></div>
        <tbody>";

        while ($listout_mold = mysqli_fetch_assoc($listoutF_mold)) {

            $PartNumber       = $listout_mold['PartNumber'];
            $MoldNumber         = $listout_mold['MoldNumber'];
            $CavNums         = $listout_mold['CavNums'];

            echo "<tr>";
            echo "<td>" . $ProjectName . "</td>";
            echo "<td onclick=javascript:location.href='ModifySpec_Search_PJ-4.php?PN=$PartNumber'><div align='left'><u><font color='blue'>" . $PartNumber . "</font></u></div></td>";
            echo "<td>" . $MoldNumber . "</td>";
            echo "<td>" . $CavNums . "</td>";
            echo "</tr>";
        }
    }
    ?>
</tbody>
</table>

<?php
if ($num_plating != 0) {

    echo"<br>
    電鍍:
    <div class='Measure-Data-table-2'>
    <table id='Measure-Data-table-2' class='sortable'>
    <thead>
    <th width='auto'><div align='center'>專案名稱</div></th>
    <th width='auto'><div align='center'>料號</div></th>
    </thead>
    <div align='center'></div>
    <tbody>";

    while ($listout_plating= mysqli_fetch_assoc($listoutF_plating)) {

        $PartNumber       = $listout_plating['PartNumber'];

        echo "<tr>";
        echo "<td>" . $ProjectName . "</td>";
        echo "<td onclick=javascript:location.href='ModifySpec_Search_PJ-4.php?PN=$PartNumber'><div align='left'><u><font color='blue'>" . $PartNumber . "</font></u></div></td>";
        echo "</tr>";
    }
}
?>
</tbody>
</table>

<?php
if ($num_welding != 0) {

    echo"<br>
    鐳射:
    <div class='Measure-Data-table-2'>
    <table id='Measure-Data-table-2' class='sortable'>
    <thead>
    <th width='auto'><div align='center'>專案名稱</div></th>
    <th width='auto'><div align='center'>料號</div></th>
    </thead>
    <div align='center'></div>
    <tbody>";

    while ($listout_welding= mysqli_fetch_assoc($listoutF_welding)) {

        $PartNumber       = $listout_welding['PartNumber'];
        $LineNumber         = $listout_welding['LineNumber'];

        echo "<tr>";
        echo "<td>" . $ProjectName . "</td>";
        echo "<td onclick=javascript:location.href='ModifySpec_Search_PJ-4.php?PN=$PartNumber'><div align='left'><u><font color='blue'>" . $PartNumber . "</font></u></div></td>";
        echo "</tr>";
    }
}
?>
</tbody>
</table>

<?php
if ($num_blasting != 0) {

    echo"<br>
    噴砂:
    <div class='Measure-Data-table-2'>
    <table id='Measure-Data-table-2' class='sortable'>
    <thead>
    <th width='auto'><div align='center'>專案名稱</div></th>
    <th width='auto'><div align='center'>料號</div></th>
    </thead>
    <div align='center'></div>
    <tbody>";

    while ($listout_blasting= mysqli_fetch_assoc($listoutF_blasting)) {

        $PartNumber       = $listout_blasting['PartNumber'];
        $LineNumber         = $listout_blasting['LineNumber'];

        echo "<tr>";
        echo "<td>" . $ProjectName . "</td>";
        echo "<td onclick=javascript:location.href='ModifySpec_Search_PJ-4.php?PN=$PartNumber'><div align='left'><u><font color='blue'>" . $PartNumber . "</font></u></div></td>";
        echo "</tr>";
    }
}
?>
</tbody>
</table>

<?php
if ($num_iqc != 0) {

    echo"<br>
    IQC:
    <div class='Measure-Data-table-2'>
    <table id='Measure-Data-table-2' class='sortable'>
    <thead>
    <th width='auto'><div align='center'>專案名稱</div></th>
    <th width='auto'><div align='center'>料號</div></th>
    </thead>
    <div align='center'></div>
    <tbody>";

    while ($listout_iqc= mysqli_fetch_assoc($listoutF_iqc)) {

        $PartNumber       = $listout_iqc['PartNumber'];

        echo "<tr>";
        echo "<td>" . $ProjectName . "</td>";
        echo "<td onclick=javascript:location.href='ModifySpec_Search_PJ-4.php?PN=$PartNumber'><div align='left'><u><font color='blue'>" . $PartNumber . "</font></u></div></td>";
        echo "</tr>";
    }
}
?>
</tbody>
</table>
</form>
</body>
</html>