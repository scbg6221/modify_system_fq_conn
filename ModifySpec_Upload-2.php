<!DOCTYPE HTML><head>
    <meta charset="utf-8">
    <title>Untitled Document</title></head>

    <script>

        function check(){

            if(confirm ("此料號規格已經存在!!是否要複寫?"))
             { location.href="ModifySpec_Upload-3.php?A=T"; }
         else { location.href="ModifySpec_Upload-3.php?A=F"; }

     }

 </script>
 <style type="text/css">
    body {
      font: normal medium/1.4 sans-serif;
  }
  table {
      border-collapse: collapse;
  }
  th{
      padding: 0.25rem;
      text-align: center;
      border: 1px solid #ccc;
      background: #888888;
      font-size:15px;

  }
  td {
      padding: 0.25rem;
      text-align: center;
      border: 1px solid #ccc;
      font-size:13px;

  }
  B{
    font-family:"Arial Black", Gadget, sans-serif;
    color:#00000;
}
L{
    font-family:"Arial Black", Gadget, sans-serif;
    color:#cc6a08;
}
tbody tr:nth-child(odd) {
  background: #eee;
}

.ifile {
    position:absolute;opacity:0;filter:alpha(opacity=0);

}
</style>
</head>
<body>

    <?php
/*
 * Copyright (C) 2015 Foxlink SCBG CAE Dept. All rights reserved
 * Project: Wanhao System
 * File Name: Spec Upload-2
 * Function: Spec Insert Data Base & Display
 * Author: Bruce Huang
 * --------------------------------------------------
 * Rev: 1.2 Date: PM 04:34 2015/01/28 Modifier: Bruce Huang
 * --------------------------------------------------
 */
session_start();
include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';
error_reporting(0);

$DateTime   = date("Y-m-d H:i:s");
$CreateName = $_SESSION['user'];
//$Type       = substr($_SESSION['PartNumber'], 0, 3);
$Type = $_SESSION['Type'];
$Type1 = $_SESSION['Type1'];
$Type2 = $_SESSION['Type2'];
//$allPNRow = $_SESSION['allPNRow']; 

if ($Type == '819' or $Type == '818') {
    $db_Name             = 'modify_spec_assembly';
    $_SESSION['db_Name'] = $db_Name;
    $ProjectName_array   = $_SESSION['ProjectName_array'];
    $ProjectDesc_array   = $_SESSION['ProjectDesc_array'];
    $PartNumber_array    = $_SESSION['PartNumber_array'];
    $Rev_array           = $_SESSION['Rev_array'];
} elseif ($Type == '811') {
    $ProjectName_array   = $_SESSION['ProjectName_array'];
    $ProjectDesc_array   = $_SESSION['ProjectDesc_array'];
    $PartNumber_array    = $_SESSION['PartNumber_array'];

    if($Type1              == '9000' or $Type1              == '9001' or $Type1              == '9002'){
        $db_Name             = 'modify_spec_stamping';
        $_SESSION['db_Name'] = $db_Name;
        $MoldNumber_array    = $_SESSION['MoldNumber_array'];
        $Rev_array           = $_SESSION['Rev_array'];
    }
    else{
        /*
        if($Type2              == 'GL' or $Type2              == '21' or $Type2              == '1H'){
            $db_Name             = 'modify_spec_plating';
            $_SESSION['db_Name'] = $db_Name;
        }
        */
        if($Type2              == '9W'){
            $db_Name             = 'modify_spec_welding';
            $_SESSION['db_Name'] = $db_Name;
        }
        else if($Type2              == '9U'){
            $db_Name             = 'modify_spec_blasting';
            $_SESSION['db_Name'] = $db_Name;
        }
        else{
            $db_Name             = 'modify_spec_plating';
            $_SESSION['db_Name'] = $db_Name;
        }
        $MoldNumber_array    = $_SESSION['MoldNumber_array'];
        $Rev_array           = $_SESSION['Rev_array'];
    }

} elseif ($Type == '812') {
    $ProjectName_array   = $_SESSION['ProjectName_array'];
    $ProjectDesc_array   = $_SESSION['ProjectDesc_array'];
    $PartNumber_array    = $_SESSION['PartNumber_array'];
    $db_Name             = 'modify_spec_iqc';
    $_SESSION['db_Name'] = $db_Name;
    $Rev_array           = $_SESSION['Rev_array'];
    
}else {
    $db_Name             = 'modify_spec_molding';
    $_SESSION['db_Name'] = $db_Name;
    $ProjectName_array   = $_SESSION['ProjectName_array'];
    $ProjectDesc_array   = $_SESSION['ProjectDesc_array'];
    $PartNumber_array    = $_SESSION['PartNumber_array'];
    $MoldNumber_array    = $_SESSION['MoldNumber_array'];
    $Rev_array           = $_SESSION['Rev_array'];
    $CavNums_array       = $_SESSION['CavNums_array'];
}

mysqli_select_db($connect_spec, $database_spec);
$unique_PN   = array_values(array_unique($PartNumber_array));
$uniquePNRow = count($unique_PN);

for ($delPNrow = 0; $delPNrow < $uniquePNRow; $delPNrow++) {
    $deletePN       = "delete FROM  " . $db_Name . " WHERE `PartNumber` = '$unique_PN[$delPNrow]'";
    //echo $deletePN ;
    $deletePN_query = mysqli_query($connect_spec, $deletePN) or die("警告 ： 刪除modify_spec失敗");
}
//print_r($PartNumber_array);
//echo $allPNRow;
/////check this part number of upload file is exist or not on data base/////

/////insert data base & display/////
//else {
$PartNumber_array_ok = array_filter($PartNumber_array);
$allPNRow            = count($PartNumber_array_ok);

for ($PNrow = 0; $PNrow < $allPNRow; $PNrow++) {
    $uploaddir = "..\..\Spec\Modify_System\FQ_Conn\DimSpec/";
    $fileName  = $_SESSION['upload_file_name'];
    $Error1    = $_SESSION['Error'];
    $tmpfile   = $_SESSION['tmpfile'];
    $file2     = $_SESSION['file2'];
    $fileExt   = substr($fileName, strrpos($fileName, '.') + 1);

    if ($Error1 == 0 & $fileExt == "xlsx") {
        //設定&開啟Spec Excel
        $phpexcel_spec = PHPExcel_IOFactory::load($uploaddir . $file2);
        //$phpexcel_spec->setActiveSheetIndex(0);
        $phpexcel_spec->setActiveSheetIndexByName('尺寸 Spec');
        $xlsx_spec = $phpexcel_spec->getActiveSheet();
        $allRow    = $xlsx_spec->getHighestRow(); //取得最大行數
        $Check              = $xlsx_spec->getCell('I2')->getValue();

        if($Check !=''){
            $x = 1;
            for ($row = 2; $row <= $allRow; $row++) {
                $OKData_array = array();

                if ($Type == '810') {
                    $CavNums         = $CavNums_array[$PNrow];
                    $CavNumber_array = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P');

                    for ($Cav_i = 0; $Cav_i < $CavNums; $Cav_i++) {

                    //抓取Spec資料
                        $DimNO              = $xlsx_spec->getCell('A' . $row)->getValue();
                        $CavNumber          = $CavNumber_array[$Cav_i];
                        $DimSpec            = $xlsx_spec->getCell('B' . $row)->getValue();
                        $DimUpper           = $xlsx_spec->getCell('C' . $row)->getValue();
                        $DimLower           = $xlsx_spec->getCell('D' . $row)->getValue();
                        $Expect_UpperData   = $xlsx_spec->getCell('E' . $row)->getValue();
                        $Expect_LowerData   = $xlsx_spec->getCell('F' . $row)->getValue();
                        $Expect_UpperStdv   = $xlsx_spec->getCell('G' . $row)->getValue();
                        $Expect_LowerStdv   = $xlsx_spec->getCell('H' . $row)->getValue();
                        $Expect_Result      = $xlsx_spec->getCell('I' . $row)->getValue();
                        $Remark             = $xlsx_spec->getCell('J' . $row)->getValue();
                        $PPKSpec            = $xlsx_spec->getCell('K' . $row)->getValue();
                        $DimDesc            = $xlsx_spec->getCell('L' . $row)->getValue();
                        $ProjectName        = $ProjectName_array[$PNrow];
                        $ProjectDescription = $ProjectDesc_array[$PNrow];
                        $PartNumber         = $PartNumber_array[$PNrow];
                        $MoldNumber         = $MoldNumber_array[$PNrow];
                    //$CavNums    = $CavNums_array[$PNrow];
                    //
                        $Rev = $Rev_array[$PNrow];

                        $Expect_R = strtoupper($Expect_Result);
                        $sql      = "INSERT INTO " . $db_Name . " (`id`, `ProjectName`, `ProjectDescription`,`PartNumber`,`MoldNumber`,`CavNums`,`CavNumber`,`Rev`,`DimNO`, `DimNOOrder`, `DimSpec`, `DimUpper`, `DimLower`, `PPKSpec`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `CreateName`, `CreateDataTime`, `Remark`, `DimDesc`, `fileName`)VALUES('$x','$ProjectName','$ProjectDescription','$PartNumber','$MoldNumber','$CavNums','$CavNumber','$Rev','$DimNO','$DimNO','$DimSpec','$DimUpper','$DimLower','$PPKSpec','$Expect_UpperData','$Expect_LowerData','$Expect_UpperStdv','$Expect_LowerStdv','$Expect_R','$CreateName','$DateTime','$Remark','$DimDesc','$fileName')";
                        if ($DimNO != '') {
                            $result = mysqli_query($connect_spec, $sql) or die("警告 ： 輸入molding modify_spec失敗");
                        }
                        $x++;
                    }
                } elseif ($Type == '819' or $Type == '818') {
//抓取Spec資料
                    $DimNO              = $xlsx_spec->getCell('A' . $row)->getValue();
                    $DimSpec            = $xlsx_spec->getCell('B' . $row)->getValue();
                    $DimUpper           = $xlsx_spec->getCell('C' . $row)->getValue();
                    $DimLower           = $xlsx_spec->getCell('D' . $row)->getValue();
                    $Expect_UpperData   = $xlsx_spec->getCell('E' . $row)->getValue();
                    $Expect_LowerData   = $xlsx_spec->getCell('F' . $row)->getValue();
                    $Expect_UpperStdv   = $xlsx_spec->getCell('G' . $row)->getValue();
                    $Expect_LowerStdv   = $xlsx_spec->getCell('H' . $row)->getValue();
                    $Expect_Result      = $xlsx_spec->getCell('I' . $row)->getValue();
                    $PPKSpec            = $xlsx_spec->getCell('J' . $row)->getValue();
                    $DimDesc            = $xlsx_spec->getCell('K' . $row)->getValue();
                    $Remark             = '尺寸';
                    $ProjectName        = $ProjectName_array[$PNrow];
                    $ProjectDescription = $ProjectDesc_array[$PNrow];
                    $PartNumber         = $PartNumber_array[$PNrow];
                    $Rev                = $Rev_array[$PNrow];

                    $Expect_R = strtoupper($Expect_Result);
                    $sql      = "INSERT INTO " . $db_Name . "  (`id`, `ProjectName`, `ProjectDescription`, `PartNumber`, `Rev`, `DimNO`, `DimNOOrder`, `DimSpec`, `DimUpper`, `DimLower`, `PPKSpec`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `CreateName`, `CreateDataTime`, `Remark`, `DimDesc`, `fileName`) VALUES ('$x','$ProjectName','$ProjectDescription','$PartNumber','$Rev','$DimNO','$DimNO','$DimSpec','$DimUpper','$DimLower','$PPKSpec','$Expect_UpperData','$Expect_LowerData','$Expect_UpperStdv','$Expect_LowerStdv','$Expect_R','$CreateName','$DateTime','$Remark','$DimDesc','$fileName')";
                    if ($DimNO != '') {
                    //echo $sql;
                        $result = mysqli_query($connect_spec, $sql) or die("警告 ： 輸入asm modify_spec失敗");
                    }
                    $x++;

                } elseif ($Type == '812') {
//抓取Spec資料
                    $DimNO              = $xlsx_spec->getCell('A' . $row)->getValue();
                    $DimSpec            = $xlsx_spec->getCell('B' . $row)->getValue();
                    $DimUpper           = $xlsx_spec->getCell('C' . $row)->getValue();
                    $DimLower           = $xlsx_spec->getCell('D' . $row)->getValue();
                    $Expect_UpperData   = $xlsx_spec->getCell('E' . $row)->getValue();
                    $Expect_LowerData   = $xlsx_spec->getCell('F' . $row)->getValue();
                    $Expect_UpperStdv   = $xlsx_spec->getCell('G' . $row)->getValue();
                    $Expect_LowerStdv   = $xlsx_spec->getCell('H' . $row)->getValue();
                    $Expect_Result      = $xlsx_spec->getCell('I' . $row)->getValue();
                    $Remark             = $xlsx_spec->getCell('J' . $row)->getValue();
                    $PPKSpec            = $xlsx_spec->getCell('K' . $row)->getValue();
                    $DimDesc            = $xlsx_spec->getCell('L' . $row)->getValue();
                    $ProjectName        = $ProjectName_array[$PNrow];
                    $ProjectDescription = $ProjectDesc_array[$PNrow];
                    $PartNumber         = $PartNumber_array[$PNrow];
                    $Rev                = $Rev_array[$PNrow];

                    $Expect_R = strtoupper($Expect_Result);
                    $sql      = "INSERT INTO " . $db_Name . "  (`id`, `ProjectName`, `ProjectDescription`, `PartNumber`, `Rev`, `DimNO`, `DimNOOrder`, `DimSpec`, `DimUpper`, `DimLower`, `PPKSpec`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `CreateName`, `CreateDataTime`, `Remark`, `DimDesc`, `fileName`) VALUES ('$x','$ProjectName','$ProjectDescription','$PartNumber','$Rev','$DimNO','$DimNO','$DimSpec','$DimUpper','$DimLower','$PPKSpec','$Expect_UpperData','$Expect_LowerData','$Expect_UpperStdv','$Expect_LowerStdv','$Expect_R','$CreateName','$DateTime','$Remark','$DimDesc','$fileName')";

                    if ($DimNO != '') {
                    //echo $sql;
                        $result = mysqli_query($connect_spec, $sql) or die("警告 ： 輸入iqc modify_spec失敗");
                    }
                    $x++;

                } else {
                    //抓取Spec資料
                    $DimNO              = $xlsx_spec->getCell('A' . $row)->getValue();
                    $DimSpec            = $xlsx_spec->getCell('B' . $row)->getValue();
                    $DimUpper           = $xlsx_spec->getCell('C' . $row)->getValue();
                    $DimLower           = $xlsx_spec->getCell('D' . $row)->getValue();
                    $Expect_UpperData   = $xlsx_spec->getCell('E' . $row)->getValue();
                    $Expect_LowerData   = $xlsx_spec->getCell('F' . $row)->getValue();
                    $Expect_UpperStdv   = $xlsx_spec->getCell('G' . $row)->getValue();
                    $Expect_LowerStdv   = $xlsx_spec->getCell('H' . $row)->getValue();
                    $Expect_Result      = $xlsx_spec->getCell('I' . $row)->getValue();
                    $Remark             = $xlsx_spec->getCell('J' . $row)->getValue();
                    $PPKSpec            = $xlsx_spec->getCell('K' . $row)->getValue();
                    $DimDesc            = $xlsx_spec->getCell('L' . $row)->getValue();
                    $ProjectName        = $ProjectName_array[$PNrow];
                    $ProjectDescription = $ProjectDesc_array[$PNrow];
                    $PartNumber         = $PartNumber_array[$PNrow];
                    $MoldNumber         = $MoldNumber_array[$PNrow];
                    $Rev                = $Rev_array[$PNrow];
                    $Expect_R = strtoupper($Expect_Result);

                    if($DimNO !=''){
                        $sql      = "INSERT INTO " . $db_Name . " (`id`, `ProjectName`, `ProjectDescription`, `PartNumber`, `MoldNumber`, `Rev`, `DimNO`, `DimNOOrder`, `DimSpec`, `DimUpper`, `DimLower`, `PPKSpec`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `CreateName`, `CreateDataTime`, `Remark`, `DimDesc`, `fileName`) VALUES ('$x','$ProjectName','$ProjectDescription','$PartNumber','$MoldNumber','$Rev','$DimNO','$DimNO','$DimSpec','$DimUpper','$DimLower','$PPKSpec','$Expect_UpperData','$Expect_LowerData','$Expect_UpperStdv','$Expect_LowerStdv','$Expect_R','$CreateName','$DateTime','$Remark','$DimDesc','$fileName')";
                    //echo $sql;
                //if ($DimNO != '') {

                        if($Type1              == '9000' or $Type1              == '9001' or $Type1              == '9002'){
                            $result = mysqli_query($connect_spec, $sql) or die("警告 ： 輸入stamping modify_spec失敗");
                        }
                        else{
                            /*
                            if($Type2              == 'GL' or $Type2              == '21' or $Type2              == '1H'){
                                $result = mysqli_query($connect_spec, $sql) or die("警告 ： 輸入plating modify_spec失敗");
                            }
                            */
                            if($Type2              == '9W'){
                                $result = mysqli_query($connect_spec, $sql) or die("警告 ： 輸入welding modify_spec失敗");
                            }
                            else if($Type2              == '9U'){
                                $result = mysqli_query($connect_spec, $sql) or die("警告 ： 輸入blasting modify_spec失敗");
                            }
                            else{
                               $result = mysqli_query($connect_spec, $sql) or die("警告 ： 輸入plating modify_spec失敗"); 
                            }
                        }
                }//Check DimNO !=''結束

                $x++;
            }
        }

    }

    else{

        $sql      = "INSERT INTO " . $db_Name . " (`id`, `ProjectName`, `ProjectDescription`, `PartNumber`, `MoldNumber`, `Rev`, `DimNO`, `DimNOOrder`, `DimSpec`, `DimUpper`, `DimLower`, `PPKSpec`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `CreateName`, `CreateDataTime`, `Remark`, `DimDesc`, `fileName`) VALUES ('1','$ProjectName_array[$PNrow]','$ProjectDesc_array[$PNrow]','$PartNumber_array[$PNrow]','$MoldNumber_array[$PNrow]','','','','','','','','','','','','','','','','','')";
            //echo $sql ;
        $result = mysqli_query($connect_spec, $sql) or die("警告 ： 空值輸入失敗");
    }

    if ($Type == '819' or $Type == '818') {
            //設定&開啟Spec Excel
        $phpexcel_spec_ort = PHPExcel_IOFactory::load($uploaddir . $file2);
            //$phpexcel_spec_ort->setActiveSheetIndex(1);
        $phpexcel_spec_ort->setActiveSheetIndexByName('ORT Spec');
        $xlsx_spec_ort = $phpexcel_spec_ort->getActiveSheet();
            $allRow_ort    = $xlsx_spec_ort->getHighestRow(); //取得最大行數
            //echo $allRow_ort;
            $y = 1;
            for ($row_ort = 2; $row_ort <= $allRow_ort; $row_ort++) {

                //抓取Spec資料
                $DimNO_ort            = 'ORT_' . $y;
                $DimSpec_ort          = $xlsx_spec_ort->getCell('B' . $row_ort)->getValue();
                $DimUpper_ort         = $xlsx_spec_ort->getCell('C' . $row_ort)->getValue();
                $DimLower_ort         = $xlsx_spec_ort->getCell('D' . $row_ort)->getValue();
                $Expect_UpperData_ort = $xlsx_spec_ort->getCell('E' . $row_ort)->getValue();
                $Expect_LowerData_ort = $xlsx_spec_ort->getCell('F' . $row_ort)->getValue();
                $Expect_UpperStdv_ort = $xlsx_spec_ort->getCell('G' . $row_ort)->getValue();
                $Expect_LowerStdv_ort = $xlsx_spec_ort->getCell('H' . $row_ort)->getValue();
                $Expect_Result_ort    = $xlsx_spec_ort->getCell('I' . $row_ort)->getValue();
                $PPKSpec_ort          = $xlsx_spec_ort->getCell('J' . $row_ort)->getValue();
                $DimDesc_ort          = $xlsx_spec_ort->getCell('K' . $row_ort)->getValue();
                $RemarkNO_ort         = $xlsx_spec_ort->getCell('A' . $row_ort)->getValue();

                if ($RemarkNO_ort == '2') {
                    $Remark_ort = '插拔力_插入力';
                    $DimDesc_ort = '插拔力';
                } else if ($RemarkNO_ort == '3') {
                    $Remark_ort = '插拔力_拔出力';
                    $DimDesc_ort = '插拔力';
                } else if ($RemarkNO_ort == '4') {
                    $Remark_ort = '氣密_爐前';
                    $DimDesc_ort = '氣密_爐前';
                } else if ($RemarkNO_ort == '5') {
                    $Remark_ort = '氣密_爐後1';
                    $DimDesc_ort = '氣密_爐後1';
                } else if ($RemarkNO_ort == '6') {
                    $Remark_ort = '氣密_爐後2';
                    $DimDesc_ort = '氣密_爐後2';
                } else if ($RemarkNO_ort == '7') {
                    $Remark_ort = '氣密_爐後3';
                    $DimDesc_ort = '氣密_爐後3';
                } else if ($RemarkNO_ort == '8') {
                    $Remark_ort = '氣密_爐後4';
                    $DimDesc_ort = '氣密_爐後4';
                } else if ($RemarkNO_ort == '9') {
                    $Remark_ort = '拉力';
                } else if ($RemarkNO_ort == '10') {
                    $Remark_ort = '端子保持力_爐前';
                } else if ($RemarkNO_ort == '11') {
                    $Remark_ort = '端子保持力_爐後';
                } else if ($RemarkNO_ort == '12') {
                    $Remark_ort = '鐵件保持力_爐前';
                } else if ($RemarkNO_ort == '13') {
                    $Remark_ort = '鐵件保持力_爐後';
                } else if ($RemarkNO_ort == '14') {
                    $Remark_ort = '後蓋保持力_爐前';
                } else if ($RemarkNO_ort == '15') {
                    $Remark_ort = '後蓋保持力_爐後';
                } else if ($RemarkNO_ort == '16') {
                    $Remark_ort = '推壓力_推力';
                }  else if ($RemarkNO_ort == '17') {
                    $Remark_ort = '推壓力_壓力';
                }

                $ProjectName        = $ProjectName_array[$PNrow];
                $ProjectDescription = $ProjectDesc_array[$PNrow];
                $PartNumber         = $PartNumber_array[$PNrow];
                $Rev                = $Rev_array[$PNrow];

                if ($RemarkNO_ort != '1') {
                    $Expect_R_ort = strtoupper($Expect_Result_ort);
                    $sql_ort      = "INSERT INTO " . $db_Name . "  (`id`, `ProjectName`, `ProjectDescription`, `PartNumber`, `Rev`, `DimNO`, `DimNOOrder`, `DimSpec`, `DimUpper`, `DimLower`, `PPKSpec`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `CreateName`, `CreateDataTime`, `Remark`, `DimDesc`, `fileName`) VALUES ('$y','$ProjectName','$ProjectDescription','$PartNumber','$Rev','$DimNO_ort','$DimNO_ort','$DimSpec_ort','$DimUpper_ort','$DimLower_ort','$PPKSpec_ort','$Expect_UpperData_ort','$Expect_LowerData_ort','$Expect_UpperStdv_ort','$Expect_LowerStdv_ort','$Expect_R_ort','$CreateName','$DateTime','$Remark_ort','$DimDesc_ort','$fileName')";
//echo $sql_ort;
                    if ($DimNO_ort != '') {
                        $result_ort = mysqli_query($connect_spec, $sql_ort) or die("警告 ： 輸入modify_spec_ort失敗");
                        //echo $sql_ort;
                    }
                    $y++;
                }
            }
        }

        else if ($Type == '810') {
            //設定&開啟Spec Excel
            $phpexcel_spec_ort = PHPExcel_IOFactory::load($uploaddir . $file2);
            //$phpexcel_spec_ort->setActiveSheetIndex(1);
            $phpexcel_spec_ort->setActiveSheetIndexByName('ORT Spec');
            $xlsx_spec_ort = $phpexcel_spec_ort->getActiveSheet();
            $allRow_ort    = $xlsx_spec_ort->getHighestRow(); //取得最大行數
            //echo $allRow_ort;

            $y = 1;
            for ($row_ort = 2; $row_ort <= $allRow_ort; $row_ort++) {

                $CavNums         = $CavNums_array[$PNrow];
                $CavNumber_array = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P');
                $DimNO_ort            = 'ORT_' . $y;

                for ($Cav_i = 0; $Cav_i < $CavNums; $Cav_i++) {

                    $CavNumber          = $CavNumber_array[$Cav_i];

                //抓取Spec資料
                    $DimSpec_ort          = $xlsx_spec_ort->getCell('B' . $row_ort)->getValue();
                    $DimUpper_ort         = $xlsx_spec_ort->getCell('C' . $row_ort)->getValue();
                    $DimLower_ort         = $xlsx_spec_ort->getCell('D' . $row_ort)->getValue();
                    $Expect_UpperData_ort = $xlsx_spec_ort->getCell('E' . $row_ort)->getValue();
                    $Expect_LowerData_ort = $xlsx_spec_ort->getCell('F' . $row_ort)->getValue();
                    $Expect_UpperStdv_ort = $xlsx_spec_ort->getCell('G' . $row_ort)->getValue();
                    $Expect_LowerStdv_ort = $xlsx_spec_ort->getCell('H' . $row_ort)->getValue();
                    $Expect_Result_ort    = $xlsx_spec_ort->getCell('I' . $row_ort)->getValue();
                    $PPKSpec_ort          = $xlsx_spec_ort->getCell('J' . $row_ort)->getValue();
                    $DimDesc_ort          = $xlsx_spec_ort->getCell('K' . $row_ort)->getValue();
                    $RemarkNO_ort         = $xlsx_spec_ort->getCell('A' . $row_ort)->getValue();

                    if ($RemarkNO_ort == '2') {
                        $Remark_ort = '插拔力_插入力';
                    } else if ($RemarkNO_ort == '3') {
                        $Remark_ort = '插拔力_拔出力';
                    } else if ($RemarkNO_ort == '4') {
                        $Remark_ort = '氣密_爐前';
                    } else if ($RemarkNO_ort == '5') {
                        $Remark_ort = '氣密_爐後1';
                    } else if ($RemarkNO_ort == '6') {
                        $Remark_ort = '氣密_爐後2';
                    } else if ($RemarkNO_ort == '7') {
                        $Remark_ort = '拉力';
                    } else if ($RemarkNO_ort == '8') {
                        $Remark_ort = '端子保持力_爐前';
                    } else if ($RemarkNO_ort == '9') {
                        $Remark_ort = '端子保持力_爐後';
                    } else if ($RemarkNO_ort == '10') {
                        $Remark_ort = '鐵件保持力_爐前';
                    } else if ($RemarkNO_ort == '11') {
                        $Remark_ort = '鐵件保持力_爐後';
                    } else if ($RemarkNO_ort == '12') {
                        $Remark_ort = '後蓋保持力_爐前';
                    } else if ($RemarkNO_ort == '13') {
                        $Remark_ort = '後蓋保持力_爐後';
                    } else if ($RemarkNO_ort == '14') {
                        $Remark_ort = '推壓力_推力';
                    }  else if ($RemarkNO_ort == '15') {
                        $Remark_ort = '推壓力_壓力';
                    }
                    $ProjectName        = $ProjectName_array[$PNrow];
                    $ProjectDescription = $ProjectDesc_array[$PNrow];
                    $PartNumber         = $PartNumber_array[$PNrow];
                    $Rev                = $Rev_array[$PNrow];

                    if ($RemarkNO_ort != '1') {
                        $Expect_R_ort = strtoupper($Expect_Result_ort);
                        $sql_ort      = "INSERT INTO " . $db_Name . "  (`id`, `ProjectName`, `ProjectDescription`, `PartNumber`, `MoldNumber`, `CavNums`, `CavNumber`, `Rev`, `DimNO`, `DimNOOrder`, `DimSpec`, `DimUpper`, `DimLower`, `PPKSpec`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `CreateName`, `CreateDataTime`, `Remark`, `DimDesc`, `fileName`) VALUES ('$y','$ProjectName','$ProjectDescription','$PartNumber','$MoldNumber','$CavNums','$CavNumber','$Rev','$DimNO_ort','$DimNO_ort','$DimSpec_ort','$DimUpper_ort','$DimLower_ort','$PPKSpec_ort','$Expect_UpperData_ort','$Expect_LowerData_ort','$Expect_UpperStdv_ort','$Expect_LowerStdv_ort','$Expect_R_ort','$CreateName','$DateTime','$Remark_ort','$DimDesc_ort','$fileName')";
//echo $sql_ort;
                        if ($DimNO_ort != '') {
                            $result_ort = mysqli_query($connect_spec, $sql_ort) or die("警告 ： 輸入modify_spec_molding ort失敗");
                        //echo $sql_ort;
                        }
                    }
                }
                $y++;
            }
        }
    }
    //}
    //echo $sql;
    $delete1       = "delete FROM " . $db_Name . "  WHERE id = '0'";
    $delete1_query = mysqli_query($connect_spec, $delete1) or die("警告 ： 刪除modify_spec失敗-1");
    $delete2       = "delete FROM " . $db_Name . "  WHERE DimSpec = '' AND DimUpper = '' AND DimLower = '' ";
    $delete2_query = mysqli_query($connect_spec, $delete2) or die("警告 ： 刪除modify_spec失敗-2");
    //echo $delete1;
    if ($Type == '819' or $Type == '818') {
        echo "<B>料號:" . $PartNumber_array[$PNrow] . "輸入資料庫成功!!</B></BR>";
    } else {
        echo "<B>料號:" . $PartNumber_array[$PNrow] . "模號:" . $MoldNumber_array[$PNrow] . "輸入資料庫成功!!</B></BR>";
    }
}

if ($fileExt != "xlsx") {
    echo "<L>請上傳 .xlsx 格式的檔案!!</L>";
}

?>

<?php
echo "<table width='2000' method='post' style='table-layout:fixed'>";
echo "<thead>";

if ($Type == '810') {
    echo "<th width='35'>專案名稱</th>";
    echo "<th width='25'>專案描述</th>";
    echo "<th width='15'>版次</th>";
    echo "<th width='25'>模穴數</th>";
    echo "<th width='15'>FAI號</th>";
    echo "<th width='100'>FAI描述</th>";
    echo "<th width='25'>Spec值</th>";
    echo "<th width='25'>Spec上限值</th>";
    echo "<th width='25'>Spec下限值</th>";
    echo "<th width='25'>Spec PPK</th>";
    echo "<th width='25'>期望中心值上限</th>";
    echo "<th width='25'>期望中心值下限</th>";
    echo "<th width='40'>期望標準差上限值</th>";
    echo "<th width='40'>期望標準差下限值</th>";
    echo "<th width='25'>期望結果</th>";
    echo "<th width='25'>上傳者</th>";
    echo "<th width='35'>上傳時間</th>";
    echo "<th width='15'>備註</th>";
    echo "<th width='100'>Spec檔案名稱</th>";
} elseif ($Type == '819' or $Type == '818') {
    echo "<th width='35'>專案名稱</th>";
    echo "<th width='25'>專案描述</th>";
    echo "<th width='15'>版次</th>";
    echo "<th width='15'>FAI號</th>";
    echo "<th width='100'>FAI描述</th>";
    echo "<th width='25'>Spec值</th>";
    echo "<th width='25'>Spec上限值</th>";
    echo "<th width='25'>Spec下限值</th>";
    echo "<th width='25'>Spec PPK</th>";
    echo "<th width='25'>期望中心值上限</th>";
    echo "<th width='25'>期望中心值下限</th>";
    echo "<th width='40'>期望標準差上限值</th>";
    echo "<th width='40'>期望標準差下限值</th>";
    echo "<th width='25'>期望結果</th>";
    echo "<th width='25'>上傳者</th>";
    echo "<th width='35'>上傳時間</th>";
    echo "<th width='35'>備註</th>";
    echo "<th width='100'>Spec檔案名稱</th>";
} elseif ($Type == '812') {
    echo "<th width='35'>專案名稱</th>";
    echo "<th width='25'>專案描述</th>";
    echo "<th width='15'>版次</th>";
    echo "<th width='15'>FAI號</th>";
    echo "<th width='100'>FAI描述</th>";
    echo "<th width='25'>Spec值</th>";
    echo "<th width='25'>Spec上限值</th>";
    echo "<th width='25'>Spec下限值</th>";
    echo "<th width='25'>Spec PPK</th>";
    echo "<th width='25'>期望中心值上限</th>";
    echo "<th width='25'>期望中心值下限</th>";;
    echo "<th width='40'>期望標準差上限值</th>";
    echo "<th width='40'>期望標準差下限值</th>";
    echo "<th width='25'>期望結果</th>";
    echo "<th width='25'>上傳者</th>";
    echo "<th width='35'>上傳時間</th>";
    echo "<th width='35'>備註</th>";
    echo "<th width='100'>Spec檔案名稱</th>";
} else {
    echo "<th width='35'>專案名稱</th>";
    echo "<th width='25'>專案描述</th>";
    echo "<th width='15'>版次</th>";
    echo "<th width='15'>FAI號</th>";
    echo "<th width='100'>FAI描述</th>";
    echo "<th width='25'>Spec值</th>";
    echo "<th width='25'>Spec上限值</th>";
    echo "<th width='25'>Spec下限值</th>";
    echo "<th width='25'>Spec PPK</th>";
    echo "<th width='25'>期望中心值上限</th>";
    echo "<th width='25'>期望中心值下限</th>";
    echo "<th width='40'>期望標準差上限值</th>";
    echo "<th width='40'>期望標準差下限值</th>";
    echo "<th width='25'>期望結果</th>";
    echo "<th width='25'>上傳者</th>";
    echo "<th width='35'>上傳時間</th>";
    echo "<th width='15'>備註</th>";
    echo "<th width='100'>Spec檔案名稱</th>";
}
echo "</thead>";
echo "<tbody>";
?>

<?php
mysqli_select_db($connect2, $database2);
$query_listout_S = "SELECT * FROM " . $db_Name . " WHERE `PartNumber` = '$PartNumber' AND ProjectName = '$ProjectName' AND ProjectDescription = '$ProjectDescription' group by DimNO order by DimNO ";
$listout_S       = mysqli_query($connect_spec, $query_listout_S) or die("警告 ： 搜尋modify_spec失敗");
$NGDim_array     = array();

while ($listoutS = mysqli_fetch_assoc($listout_S)) {
    if ($Type == '810') {
        if ($listoutS['Expect_Result'] == 'NG' or $listoutS['Expect_UpperData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperStdv'] < $listoutS['Expect_LowerStdv']) {
            $color         = "<font color='#EE0000'>";
            $NGDim_array[] = $listoutS['DimNO'];
        } else { $color = "<font color='#000000'>";}
        echo "<tr>";
        echo "<td align=center  width='35'>" . $color . $listoutS['ProjectName'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['ProjectDescription'] . "</td>";
        echo "<td align=center  width='15'>" . $color . $listoutS['Rev'] . "</td>";
        echo "<td align=center  width='25'>" . $color . $listoutS['CavNums'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimNO'] . "</td>";
        echo "<td align=center  width='100'>" . $color . $listoutS['DimDesc'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimSpec'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimUpper'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimLower'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['PPKSpec'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['Expect_UpperData'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['Expect_LowerData'] . "</td>";
        echo "<td align=center  width='40'>" . $color . $listoutS['Expect_UpperStdv'] . "</td>";
        echo "<td align=center  width='40'>" . $color . $listoutS['Expect_LowerStdv'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['Expect_Result'] . "</td>";
        echo "<td align=center  width='50'>" . $color . $listoutS['CreateName'] . "</td>";
        echo "<td align=center  width='50'>" . $color . $listoutS['CreateDataTime'] . "</td>";
        echo "<td align=center  width='15'>" . $color . $listoutS['Remark'] . "</td>";
        echo "<td align=center  width='100'>" . $color . $listoutS['fileName'] . "</td>";
        echo "</tr>";

    } elseif ($Type == '819' or $Type == '818') {
        if ($listoutS['Expect_Result'] == 'NG' or $listoutS['Expect_UpperData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperStdv'] < $listoutS['Expect_LowerStdv']) {
            $color         = "<font color='#EE0000'>";
            $NGDim_array[] = $listoutS['DimNO'];
        } else { $color = "<font color='#000000'>";}

        if($Type == '819' ){$ProjectDesc = '成品';}
        else{$ProjectDesc = '半成品';}

        echo "<tr>";
        echo "<td align=center  width='35'>" . $color . $listoutS['ProjectName'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $ProjectDesc . "</td>";
        echo "<td align=center  width='15'>" . $color . $listoutS['Rev'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimNO'] . "</td>";
        echo "<td align=center  width='100'>" . $color . $listoutS['DimDesc'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimSpec'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimUpper'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimLower'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['PPKSpec'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['Expect_UpperData'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['Expect_LowerData'] . "</td>";
        echo "<td align=center  width='40'>" . $color . $listoutS['Expect_UpperStdv'] . "</td>";
        echo "<td align=center  width='40'>" . $color . $listoutS['Expect_LowerStdv'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['Expect_Result'] . "</td>";
        echo "<td align=center  width='50'>" . $color . $listoutS['CreateName'] . "</td>";
        echo "<td align=center  width='50'>" . $color . $listoutS['CreateDataTime'] . "</td>";
        echo "<td align=center  width='50'>" . $color . $listoutS['Remark'] . "</td>";
        echo "<td align=center  width='100'>" . $color . $listoutS['fileName'] . "</td>";
        echo "</tr>";
    } elseif ($Type == '812') {
        if ($listoutS['Expect_Result'] == 'NG' or $listoutS['Expect_UpperData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperStdv'] < $listoutS['Expect_LowerStdv']) {
            $color         = "<font color='#EE0000'>";
            $NGDim_array[] = $listoutS['DimNO'];
        } else { $color = "<font color='#000000'>";}

        $ProjectDesc = '外購件';

        echo "<tr>";
        echo "<td align=center  width='35'>" . $color . $listoutS['ProjectName'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $ProjectDesc . "</td>";
        echo "<td align=center  width='15'>" . $color . $listoutS['Rev'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimNO'] . "</td>";
        echo "<td align=center  width='100'>" . $color . $listoutS['DimDesc'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimSpec'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimUpper'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimLower'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['PPKSpec'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['Expect_UpperData'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['Expect_LowerData'] . "</td>";
        echo "<td align=center  width='40'>" . $color . $listoutS['Expect_UpperStdv'] . "</td>";
        echo "<td align=center  width='40'>" . $color . $listoutS['Expect_LowerStdv'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['Expect_Result'] . "</td>";
        echo "<td align=center  width='50'>" . $color . $listoutS['CreateName'] . "</td>";
        echo "<td align=center  width='50'>" . $color . $listoutS['CreateDataTime'] . "</td>";
        echo "<td align=center  width='50'>" . $color . $listoutS['Remark'] . "</td>";
        echo "<td align=center  width='100'>" . $color . $listoutS['fileName'] . "</td>";
        echo "</tr>";
    } else {
        if ($listoutS['Expect_Result'] == 'NG' or $listoutS['Expect_UpperData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] < $listoutS['DimLower'] or $listoutS['Expect_LowerData'] > $listoutS['DimUpper'] or $listoutS['Expect_UpperStdv'] < $listoutS['Expect_LowerStdv']) {
            $color         = "<font color='#EE0000'>";
            $NGDim_array[] = $listoutS['DimNO'];
        } else { $color = "<font color='#000000'>";}
        echo "<tr>";
        echo "<td align=center  width='35'>" . $color . $listoutS['ProjectName'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['ProjectDescription'] . "</td>";
        echo "<td align=center  width='15'>" . $color . $listoutS['Rev'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimNO'] . "</td>";
        echo "<td align=center  width='100'>" . $color . $listoutS['DimDesc'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimSpec'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimUpper'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['DimLower'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['PPKSpec'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['Expect_UpperData'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['Expect_LowerData'] . "</td>";
        echo "<td align=center  width='40'>" . $color . $listoutS['Expect_UpperStdv'] . "</td>";
        echo "<td align=center  width='40'>" . $color . $listoutS['Expect_LowerStdv'] . "</td>";
        echo "<td align=center  width='35'>" . $color . $listoutS['Expect_Result'] . "</td>";
        echo "<td align=center  width='50'>" . $color . $listoutS['CreateName'] . "</td>";
        echo "<td align=center  width='50'>" . $color . $listoutS['CreateDataTime'] . "</td>";
        echo "<td align=center  width='15'>" . $color . $listoutS['Remark'] . "</td>";
        echo "<td align=center  width='100'>" . $color . $listoutS['fileName'] . "</td>";
        echo "</tr>";
    }
}
$NGCount = count($NGDim_array);
if ($NGCount > 0) {
    $NGDim = implode(",", $NGDim_array);
    echo "<L></br>尺寸:" . $NGDim . " 異常!請確認是否為以下項目!</L></br></br>  1.[期望上/下限值]超出[Spec上/下限值]</br>  2.[期望標準差上限值]低於[期望標準差下限值]</br>  3.[期望標準差下限值]高於[期望標準差上限值]</br>  4.確認[期望結果]是否設定NG</br>  ";
}
?>
</tbody>
</table>
</body>
</html>


