<?php
if (!isset($_SESSION)) {
    session_start();
} //ob_start();
// session_destroy();
include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';
error_reporting(0);

$uploaddir                    = "..\..\Spec\Modify_System\FQ_Conn\VisualSpec/"; 
$fileName                     = $_FILES["upload_file"]["name"];
$fileExt                      = substr($fileName, strrpos($fileName, '.') + 1);
$Error1                       = $_FILES['upload_file']['error'];
$tmpfile                      = $_FILES["upload_file"]["tmp_name"];
$file2                        = mb_convert_encoding($fileName, "big5", "utf8");
$_SESSION['upfile']           = $_POST['upfile'];
$_SESSION['upload_file_name'] = $fileName;
$_SESSION['fileExt']          = $fileExt;
$_SESSION['Error']            = $Error1;
$_SESSION['tmpfile']          = $tmpfile;
$_SESSION['file2']            = $file2;

/////check moving the file success or not /////
if (isset($_POST['Submit_Upload'])) {
    if (move_uploaded_file($tmpfile, $uploaddir . $file2)) {

        //抓取excel內Page2中的產品資訊
        if ($Error1 == 0 & $fileExt == "xlsx") {
            $phpexcel = new PHPExcel;
            $phpexcel = PHPExcel_IOFactory::load($uploaddir . $file2);
            //$xlsx             = $phpexcel->setActiveSheetIndex(1);
            $xlsx     = $phpexcel->setActiveSheetIndexByName('Information');
            $allRow   = $xlsx->getHighestRow(); //取得最大行數
            $allPNRow = $allRow - 1;

            $ProjectName_array = $ProjectDesc_array = $PartNumber_array = $Rev_array = $MoldNumber_array = $CavNums_array = array();

            $row = 2;
            for ($PNrow = 0; $PNrow < $allPNRow; $PNrow++) {
                    $ProjectName                   = $xlsx->getCell('A' . $row)->getValue();
                    $ProjectDesc                   = $xlsx->getCell('B' . $row)->getValue();
                    $PartNumber                    = $xlsx->getCell('C' . $row)->getValue();
                    $Rev                           = $xlsx->getCell('E' . $row)->getValue();
                    $ProjectName_array[]           = $ProjectName;
                    $ProjectDesc_array[]           = $ProjectDesc;
                    $PartNumber_array[]            = $PartNumber;
                    $Rev_array[]                   = $Rev;
                    $_SESSION['ProjectName_array'] = $ProjectName_array;
                    $_SESSION['ProjectDesc_array'] = $ProjectDesc_array;
                    $_SESSION['PartNumber_array']  = $PartNumber_array;
                    $_SESSION['Rev_array']         = $Rev_array;
                    $row++;
            }
        }
        header('Location:ModifyVisualSpec_Upload-2.php');
    } else {
        header('Location:ModifyVisualSpec_Upload-3.php');
    }
}

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>無標題文件</title>
<link rel="stylesheet" type="text/css" href="CSS/ALL_CSS.css?id='sja'">
<script>

function result()
{
    document['form1'].action = "ModifyVisualSpec_Upload-1.php";
    document['form1'].target = 'Index_Content';
}
</script>

<style>
</style>
</head>

<body background="Images/loginb.png">
  <form name="form1" enctype="multipart/form-data" method="post">
  <table width="800"" cellpadding="5" cellspacing="5" frame="void" rules="groups" align="left">
 <tr>
 <td width=350 height=30 align=left colspan="2">
  <VisualL>製程巡檢規格書上傳</VisualL></td>
 </tr>

 <tr>
  <td width=200 height=30 align=left>
  <VisualL>檔案名稱:</VisualL></td>

  <td width=330 height=30 align=left>
  <input type="text" name="upfile" size="20"  class="SpecUpload-1" readonly></td>

  <td width=110 height=30 align=left>
  <input type="button" value="選擇檔案" class="SpecUpload-BT" onclick="this.form.file.click();"></td>

  <td width=110 height=30 align=left>
  <input type="Submit" name="Submit_Upload" id="Submit_Upload" value="上傳" class="SpecUpload-BT" onclick="result()"></td>

  <td width=220 height=30 align=left>
  <input type="file" name="upload_file" id="file" class="ifile"
     onchange="this.form.upfile.value=this.value.substr(this.value.lastIndexOf('\\')+1);"></td>
  </tr>



</table>
</form>
</body>
</html>
