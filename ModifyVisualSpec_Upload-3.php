<?php
/*
* Copyright (C) 2015 Foxlink SCBG CAE Dept. All rights reserved
* Project: Wanhao System
* File Name: Spec Upload-4
* Function: Fail message
* Author: Bruce Huang
* --------------------------------------------------
* Rev: 1.2 Date: PM 04:34 2015/01/28 Modifier: Bruce Huang
* --------------------------------------------------
*/
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>無標題文件</title>
<style type="text/css">

L{
	font-family:"Arial Black", Gadget, sans-serif;
	color:#cc6a08;
	}
	
</style>
</head>

<body>
<?php
echo "<L>規格書上傳失敗 !</L>"; 
?>
</body>
</html>