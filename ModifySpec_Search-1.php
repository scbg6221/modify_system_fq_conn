<?php
if (!isset($_SESSION)) {
    session_start();
} //ob_start();
// session_destroy();
include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/library/Other/Fork.php';

//ob_end_clean();
error_reporting(0);

if (isset($_POST['PartNumber_MoldNumber'])) {
    $_SESSION['PartNumber_MoldNumber'] = trim($_POST['PartNumber_MoldNumber']);
}

//執行轉跳
if (isset($_POST['Submit_Search'])) {
    header('Location: ModifySpec_Search-3.php');
}

if (isset($_POST['Submit_Delete'])) {
    echo $_POST['PartNumber_MoldNumber'];
    if ($_POST['PartNumber_MoldNumber'] != "") {

        $PartNumber_MoldNumber = trim($_POST['PartNumber_MoldNumber']);

        mysqli_select_db($connect_spec, $database_spec);
        $DB_namestr = substr($PartNumber_MoldNumber, 0, 3);
        $Type1 = substr($PartNumber_MoldNumber, 5, 4);
        $Type2 = substr($PartNumber_MoldNumber, 5, 2);
        $PartNumber = substr($PartNumber_MoldNumber, 0, 14);
        $MoldNumber = substr($PartNumber_MoldNumber, 15);

        if ($DB_namestr == '810') {
            $db_Name             = 'modify_spec_molding';
            $select_deletePN = "DELETE FROM " . $db_Name . " WHERE PartNumber='" . $PartNumber . "' AND MoldNumber='" . $MoldNumber . "' ";
            $select_searchFN     = "SELECT * from " . $db_Name . " where PartNumber='$PartNumber' and MoldNumber='$MoldNumber' order by PartNumber,MoldNumber";

        } elseif ($DB_namestr == '811') {
            if($Type1 == '9000'){
                $db_Name             = 'modify_spec_stamping';
                $select_deletePN = "DELETE FROM " . $db_Name . " WHERE PartNumber='" . $PartNumber . "' AND MoldNumber='" . $MoldNumber . "' ";
                $select_searchFN     = "SELECT * from " . $db_Name . " where PartNumber='$PartNumber' and MoldNumber='$MoldNumber' order by PartNumber,MoldNumber";
            } 
            else{
                if($Type2              == 'GL' or $Type2              == '21' or $Type2              == '1H' or $Type2              == '43'){
                    $db_Name             = 'modify_spec_plating';
                }
                else if($Type2              == '9W'){
                    $db_Name             = 'modify_spec_welding';
                }
                else if($Type2              == '9U'){
                    $db_Name             = 'modify_spec_blasting';
                }
                $select_deletePN = "DELETE FROM " . $db_Name . " WHERE PartNumber='" . $PartNumber . "' ";
                $select_searchFN     = "SELECT * from " . $db_Name . " where PartNumber='$PartNumber' order by PartNumber,MoldNumber";
            }

        } elseif ($DB_namestr == '812') {
            $db_Name             = 'modify_spec_iqc';
            $select_deletePN = "DELETE FROM " . $db_Name . " WHERE PartNumber='" . $PartNumber . "' ";
            $select_searchFN     = "SELECT * from " . $db_Name . " where PartNumber='$PartNumber' order by PartNumber";
        } else {
            $db_Name             = 'modify_spec_assembly';
            $select_deletePN = "DELETE FROM " . $db_Name . " WHERE PartNumber='" . $PartNumber . "' ";
            $select_searchFN     = "SELECT * from " . $db_Name . " where PartNumber='$PartNumber' order by PartNumber";
        }
        $query_searchFN = mysqli_query($connect_spec, $select_searchFN) or die(mysqli_error());

        $searchFN = mysqli_fetch_assoc($query_searchFN);
        $FileName=$searchFN['fileName'];


        $fileroute = "C:\wamp\www\MainWebsite\Spec\Modify_System\FQ_Conn\DimSpec/" . $FileName;
        unlink($fileroute);
        $query_deletePN = mysqli_query($connect_spec, $select_deletePN) or die(mysqli_error());
        
        header('Location: ModifySpec_Search-2.php');

    } else {
        echo "<script> alert('請輸入欲刪除的料號');self.location.href='ModifySpec_Search-2.php'; </script>";}

    }

    if (isset($_POST['Submit_Download'])) {

        if ($_POST['PartNumber_MoldNumber'] != "") {
            $PartNumber_MoldNumber = trim($_POST['PartNumber_MoldNumber']);

            mysqli_select_db($connect_spec, $database_spec);
            $DB_namestr = substr($PartNumber_MoldNumber, 0, 3);
            $Type1 = substr($PartNumber_MoldNumber, 5, 4);
            $Type2 = substr($PartNumber_MoldNumber, 5, 2);
            $PartNumber = substr($PartNumber_MoldNumber, 0, 14);
            $MoldNumber = substr($PartNumber_MoldNumber, 15);

            if ($DB_namestr == '810') {
                $db_Name             = 'modify_spec_molding';
                $select_searchFN     = "SELECT * from " . $db_Name . " where PartNumber='$PartNumber' and MoldNumber='$MoldNumber' order by PartNumber,MoldNumber";
            } elseif ($DB_namestr == '811') {
                if($Type1 == '9000'){
                    $db_Name             = 'modify_spec_stamping';
                    $_SESSION['db_Name'] = $db_Name;
                    $select_searchFN     = "SELECT * from " . $db_Name . " where PartNumber='$PartNumber' and MoldNumber='$MoldNumber' order by PartNumber,MoldNumber";
                }
                else{
                    if($Type2              == 'GL' or $Type2              == '21' or $Type2              == '1H' or $Type2              == '43' ){
                        $db_Name             = 'modify_spec_plating';
                    }
                    else if($Type2              == '9W'){
                        $db_Name             = 'modify_spec_welding';
                    }
                    else if($Type2              == '9U'){
                        $db_Name             = 'modify_spec_blasting';
                    }
                    $_SESSION['db_Name'] = $db_Name;
                    $select_searchFN     = "SELECT * from " . $db_Name . " where PartNumber='$PartNumber' order by PartNumber";
                }
            } elseif ($DB_namestr == '812') {
                $db_Name             = 'modify_spec_iqc';
                $_SESSION['db_Name'] = $db_Name;
                $select_searchFN     = "SELECT * from " . $db_Name . " where PartNumber='$PartNumber' order by PartNumber";
            } else {
                $db_Name             = 'modify_spec_assembly';
                $_SESSION['db_Name'] = $db_Name;
                $select_searchFN     = "SELECT * from " . $db_Name . " where PartNumber='$PartNumber' order by PartNumber";
            }

            $query_searchFN = mysqli_query($connect_spec, $select_searchFN) or die(mysqli_error());
            $searchFN = mysqli_fetch_assoc($query_searchFN);
            $FileName=$searchFN['fileName'];

            $fileroute0 = "../../Spec\Modify_System\FQ_Conn\DimSpec/" . $FileName;
    $fileroute  = iconv("UTF-8", "BIG5", $fileroute0); //UTF-8傳換成BIG5

    header("Content-type:application");
    header("Content-Length: " . (string) (filesize($fileroute)));
    header("Content-Disposition: attachment; filename=" . $FileName);
    readfile($fileroute);

    header('Location: ModifySpec_Search-2.php');

} else {
    echo "<script> alert('請輸入欲下載的料號');self.location.href='ModifySpec_Search-2.php'; </script>";}

}
?>

<!DOCTYPE HTML>
<head>
    <meta charset="utf-8">
    <title>Untitled Document</title>
    <link rel="stylesheet" type="text/css" href="CSS/ALL_CSS.css?id='ssaaa'">
    <script>
var $j = jQuery.noConflict(); //解決不同版本問題

$j(document).ready(function(){
    $("#Part_Number_VF").autocomplete("ModifySpec_Search-3.php", {
        selectFirst: true
    });
})

</script>

<script type="text/javascript">
    function result()
    {
        document['form1'].action = "ModifySpec_Search-1.php";
        document['form1'].target = 'Index_Content';
    }

    function specdelete()
    {
        document['form1'].action = "ModifySpec_Search-1.php";
        document['form1'].target = 'Index_Content';
    }

    function specdownload(PN)
    {
        document['form1'].action = "ModifySpec_Search-1.php";
        document['form1'].target = 'Index_Content';
    }
</script>

<style>
</style>
</head>

<body background="Images/loginb.png">
  <form name="form1" enctype="multipart/form-data" method="post">
      <table width="700" cellpadding="5" cellspacing="5" frame="void" rules="groups" align="left">
          <tr>
           <td width=700 height=30 align=left colspan="2">
              <VisualL>尺寸優化規格書搜尋</VisualL></td>
          </tr>

          <tr>
              <td width=130 height=30 align=left>
                  <VisualL>料號(*):</VisualL></td>

                  <td width=225 height=30 align=left>
                    <input type="text" name="PartNumber_MoldNumber" id="PartNumber_MoldNumber" class="SpecSearch-1" value="<?php echo $_POST['PartNumber_MoldNumber'] ?>"></td>


                    <td width=100 height=30 align=left>
                        <input type="Submit" name="Submit_Search" id="Submit_Search" value="搜尋" class="SpecSearch-BT" onclick="result()"></td>


                        <td width=100 height=30 align=left>

                          <input type="Submit" name="Submit_Download" id="Submit_Download" value="下載" class="SpecSearch-BT" onclick="specdownload()"></td>

                          <td width=100 height=30 align=left>
                            <input type="Submit" name="Submit_Delete" id="Submit_Delete" value="刪除" class="SpecSearch-BT" onclick="specdelete()"></td>
                        </tr>



                    </table>
                </form>
            </body>
            </html>

