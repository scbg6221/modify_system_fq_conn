<?php
/*
 * Copyright (C) 2015 Foxlink SCBG CAE Dept. All rights reserved
 * Project: Wanhao System
 * File Name: Spec Search-3
 * Function: Display all SPEC in data base
 * Author: Bruce Huang
 * --------------------------------------------------
 * Rev: 1.2 Date: PM 04:34 2015/01/28 Modifier: Bruce Huang
 * --------------------------------------------------
 */
session_start();
include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/library/PHPExcel/php-excel-reader-2.21/simplexlsx.class.php';
error_reporting(0);

//搜尋IQC DB內SPEC專案數量
mysqli_select_db($connect2, $database2);
$iqc_query_listout_U = "SELECT PartNumber FROM modify_spec_iqc group by PartNumber ";
$iqc_listout_U       = mysqli_query($connect_spec, $iqc_query_listout_U) or die(mysqli_error());
$iqc_total_num       = mysqli_num_rows($iqc_listout_U);
$iqc_total_row1      = $iqc_total_num / 6;

//重新排列SPEC專案顯示(一排顯示六個SPEC專案)
if (is_int($iqc_total_row1)) {
    $i = 0;
    $j = 6;

    for ($k = 1; $k <= $iqc_total_row1; $k++) {
        ${'A' . $k} = "SELECT PartNumber FROM modify_spec_iqc  group by PartNumber order by PartNumber limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

            ${'data_iqc' . $k}[] = ${'C' . $k}['PartNumber'];
        }
        $i += $j;
    }
    $iqc_total_row = $iqc_total_row1;
} else {
    $iqc_total_row2 = floor($iqc_total_row1);
    $iqc_total_row3 = ($iqc_total_row2) + 1;
    $i               = 0;
    $j               = 6;
    for ($k = 1; $k <= $iqc_total_row2; $k++) {
        ${'A' . $k} = "SELECT PartNumber FROM modify_spec_iqc group by PartNumber order by PartNumber limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

            ${'data_iqc' . $k}[] = ${'C' . $k}['PartNumber'];
        }
        $i += $j;
    }

    $i          = ($iqc_total_row2) * 6;
    $j          = 6;
    $k          = $iqc_total_row3;
    ${'A' . $k} = "SELECT PartNumber FROM modify_spec_iqc  group by PartNumber order by PartNumber limit $i,$j";
    ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
    while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

        ${'data_iqc' . $k}[] = ${'C' . $k}['PartNumber'];
    }
    $iqc_total_row = $iqc_total_row3;
}

//搜尋電鍍DB內SPEC專案數量
mysqli_select_db($connect2, $database2);
$plating_query_listout_U = "SELECT PartNumber FROM modify_spec_plating group by PartNumber ";
$plating_listout_U       = mysqli_query($connect_spec, $plating_query_listout_U) or die(mysqli_error());
$plating_total_num       = mysqli_num_rows($plating_listout_U);
$plating_total_row1      = $plating_total_num / 6;

//重新排列SPEC專案顯示(一排顯示六個SPEC專案)
if (is_int($plating_total_row1)) {
    $i = 0;
    $j = 6;

    for ($k = 1; $k <= $plating_total_row1; $k++) {
        ${'A' . $k} = "SELECT PartNumber FROM modify_spec_plating  group by PartNumber order by PartNumber limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

            ${'data_plating' . $k}[] = ${'C' . $k}['PartNumber'];
        }
        $i += $j;
    }
    $plating_total_row = $plating_total_row1;
} else {
    $plating_total_row2 = floor($plating_total_row1);
    $plating_total_row3 = ($plating_total_row2) + 1;
    $i               = 0;
    $j               = 6;
    for ($k = 1; $k <= $plating_total_row2; $k++) {
        ${'A' . $k} = "SELECT PartNumber FROM modify_spec_plating group by PartNumber order by PartNumber limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

            ${'data_plating' . $k}[] = ${'C' . $k}['PartNumber'];
        }
        $i += $j;
    }

    $i          = ($plating_total_row2) * 6;
    $j          = 6;
    $k          = $plating_total_row3;
    ${'A' . $k} = "SELECT PartNumber FROM modify_spec_plating  group by PartNumber order by PartNumber limit $i,$j";
    ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
    while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

        ${'data_plating' . $k}[] = ${'C' . $k}['PartNumber'];
    }
    $plating_total_row = $plating_total_row3;
}

//搜尋鐳射DB內SPEC專案數量
mysqli_select_db($connect2, $database2);
$welding_query_listout_U = "SELECT PartNumber FROM modify_spec_welding group by PartNumber ";
$welding_listout_U       = mysqli_query($connect_spec, $welding_query_listout_U) or die(mysqli_error());
$welding_total_num       = mysqli_num_rows($welding_listout_U);
$welding_total_row1      = $welding_total_num / 6;

//重新排列SPEC專案顯示(一排顯示六個SPEC專案)
if (is_int($welding_total_row1)) {
    $i = 0;
    $j = 6;

    for ($k = 1; $k <= $welding_total_row1; $k++) {
        ${'A' . $k} = "SELECT PartNumber FROM modify_spec_welding group by PartNumber order by PartNumber limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

            ${'data_welding' . $k}[] = ${'C' . $k}['PartNumber'];
        }
        $i += $j;
    }
    $welding_total_row = $welding_total_row1;
} else {
    $welding_total_row2 = floor($welding_total_row1);
    $welding_total_row3 = ($welding_total_row2) + 1;
    $i               = 0;
    $j               = 6;
    for ($k = 1; $k <= $welding_total_row2; $k++) {
        ${'A' . $k} = "SELECT PartNumber FROM modify_spec_welding group by PartNumber order by PartNumber limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

            ${'data_welding' . $k}[] = ${'C' . $k}['PartNumber'];
        }
        $i += $j;
    }

    $i          = ($welding_total_row2) * 6;
    $j          = 6;
    $k          = $welding_total_row3;
    ${'A' . $k} = "SELECT PartNumber FROM modify_spec_welding  group by PartNumber order by PartNumber limit $i,$j";
    ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
    while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

        ${'data_welding' . $k}[] = ${'C' . $k}['PartNumber'];
    }
    $welding_total_row = $welding_total_row3;
}

//搜尋噴砂DB內SPEC專案數量
mysqli_select_db($connect2, $database2);
$blasting_query_listout_U = "SELECT PartNumber FROM modify_spec_blasting group by PartNumber ";
$blasting_listout_U       = mysqli_query($connect_spec, $blasting_query_listout_U) or die(mysqli_error());
$blasting_total_num       = mysqli_num_rows($blasting_listout_U);
$blasting_total_row1      = $blasting_total_num / 6;

//重新排列SPEC專案顯示(一排顯示六個SPEC專案)
if (is_int($blasting_total_row1)) {
    $i = 0;
    $j = 6;

    for ($k = 1; $k <= $blasting_total_row1; $k++) {
        ${'A' . $k} = "SELECT PartNumber FROM modify_spec_blasting  group by PartNumber order by PartNumber limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

            ${'data_blasting' . $k}[] = ${'C' . $k}['PartNumber'];
        }
        $i += $j;
    }
    $blasting_total_row = $blasting_total_row1;
} else {
    $blasting_total_row2 = floor($blasting_total_row1);
    $blasting_total_row3 = ($blasting_total_row2) + 1;
    $i               = 0;
    $j               = 6;
    for ($k = 1; $k <= $blasting_total_row2; $k++) {
        ${'A' . $k} = "SELECT PartNumber FROM modify_spec_blasting group by PartNumber order by PartNumber limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

            ${'data_blasting' . $k}[] = ${'C' . $k}['PartNumber'];
        }
        $i += $j;
    }

    $i          = ($blasting_total_row2) * 6;
    $j          = 6;
    $k          = $blasting_total_row3;
    ${'A' . $k} = "SELECT PartNumber FROM modify_spec_blasting  group by PartNumber order by PartNumber limit $i,$j";
    ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
    while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

        ${'data_blasting' . $k}[] = ${'C' . $k}['PartNumber'];
    }
    $blasting_total_row = $blasting_total_row3;
}
//搜尋沖壓DB內SPEC專案數量
mysqli_select_db($connect2, $database2);
$stmp_query_listout_U = "SELECT PartNumber,MoldNumber FROM modify_spec_stamping group by PartNumber,MoldNumber ";
$stmp_listout_U       = mysqli_query($connect_spec, $stmp_query_listout_U) or die(mysqli_error());
$stmp_total_num       = mysqli_num_rows($stmp_listout_U);
$stmp_total_row1      = $stmp_total_num / 6;

//重新排列SPEC專案顯示(一排顯示六個SPEC專案)
if (is_int($stmp_total_row1)) {
    $i = 0;
    $j = 6;

    for ($k = 1; $k <= $stmp_total_row1; $k++) {
        ${'A' . $k} = "SELECT PartNumber,MoldNumber FROM modify_spec_stamping  group by PartNumber,MoldNumber order by PartNumber limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

            if (${'C' . $k}['MoldNumber'] != '') {
                ${'data_stmp' . $k}[] = ${'C' . $k}['PartNumber'] . '_' . ${'C' . $k}['MoldNumber'];
            } else {
                ${'data_stmp' . $k}[] = ${'C' . $k}['PartNumber'];
            }
        }
        $i += $j;
    }
    $stmp_total_row = $stmp_total_row1;
} else {
    $stmp_total_row2 = floor($stmp_total_row1);
    $stmp_total_row3 = ($stmp_total_row2) + 1;
    $i               = 0;
    $j               = 6;
    for ($k = 1; $k <= $stmp_total_row2; $k++) {
        ${'A' . $k} = "SELECT PartNumber,MoldNumber FROM modify_spec_stamping  group by PartNumber,MoldNumber order by PartNumber limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

            if (${'C' . $k}['MoldNumber'] != '') {
                ${'data_stmp' . $k}[] = ${'C' . $k}['PartNumber'] . '_' . ${'C' . $k}['MoldNumber'];
            } else {
                ${'data_stmp' . $k}[] = ${'C' . $k}['PartNumber'];
            }
        }
        $i += $j;
    }

    $i          = ($stmp_total_row2) * 6;
    $j          = 6;
    $k          = $stmp_total_row3;
    ${'A' . $k} = "SELECT PartNumber,MoldNumber FROM modify_spec_stamping  group by PartNumber,MoldNumber order by PartNumber limit $i,$j";
    ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
    while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

        if (${'C' . $k}['MoldNumber'] != '') {
            ${'data_stmp' . $k}[] = ${'C' . $k}['PartNumber'] . '_' . ${'C' . $k}['MoldNumber'];
        } else {
            ${'data_stmp' . $k}[] = ${'C' . $k}['PartNumber'];
        }

    }
    $stmp_total_row = $stmp_total_row3;
}

//搜尋成型DB內SPEC專案數量
mysqli_select_db($connect2, $database2);
$mold_query_listout_U = "SELECT PartNumber,MoldNumber FROM modify_spec_molding  group by PartNumber,MoldNumber ";
$mold_listout_U       = mysqli_query($connect_spec, $mold_query_listout_U) or die(mysqli_error());
$mold_total_num       = mysqli_num_rows($mold_listout_U);
$mold_total_row1      = $mold_total_num / 6;

//重新排列SPEC專案顯示(一排顯示六個SPEC專案)
if (is_int($mold_total_row1)) {
    $i = 0;
    $j = 6;
    for ($k = 1; $k <= $mold_total_row1; $k++) {
        ${'A' . $k} = "SELECT PartNumber,MoldNumber FROM modify_spec_molding  group by PartNumber,MoldNumber order by PartNumber limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

            if (${'C' . $k}['MoldNumber'] != '') {
                ${'data_mold' . $k}[] = ${'C' . $k}['PartNumber'] . '_' . ${'C' . $k}['MoldNumber'];
            } else {
                ${'data_mold' . $k}[] = ${'C' . $k}['PartNumber'];
            }
        }
        $i += $j;
    }
    $mold_total_row = $mold_total_row1;
} else {
    $mold_total_row2 = floor($mold_total_row1);
    $mold_total_row3 = ($mold_total_row2) + 1;
    $i               = 0;
    $j               = 6;
    for ($k = 1; $k <= $mold_total_row2; $k++) {
        ${'A' . $k} = "SELECT PartNumber,MoldNumber FROM modify_spec_molding  group by PartNumber,MoldNumber order by PartNumber limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

            if (${'C' . $k}['MoldNumber'] != '') {
                ${'data_mold' . $k}[] = ${'C' . $k}['PartNumber'] . '_' . ${'C' . $k}['MoldNumber'];
            } else {
                ${'data_mold' . $k}[] = ${'C' . $k}['PartNumber'];
            }
        }
        $i += $j;
    }

    $i          = ($mold_total_row2) * 6;
    $j          = 6;
    $k          = $mold_total_row3;
    ${'A' . $k} = "SELECT PartNumber,MoldNumber FROM modify_spec_molding  group by PartNumber,MoldNumber order by PartNumber limit $i,$j";
    ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
    while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

        if (${'C' . $k}['MoldNumber'] != '') {
            ${'data_mold' . $k}[] = ${'C' . $k}['PartNumber'] . '_' . ${'C' . $k}['MoldNumber'];
        } else {
            ${'data_mold' . $k}[] = ${'C' . $k}['PartNumber'];
        }

    }
    $mold_total_row = $mold_total_row3;
}

//搜尋組裝DB內SPEC專案數量
mysqli_select_db($connect2, $database2);
$asm_query_listout_U = "SELECT PartNumber FROM modify_spec_assembly  group by PartNumber ";
$asm_listout_U       = mysqli_query($connect_spec, $asm_query_listout_U) or die(mysqli_error());
$asm_total_num       = mysqli_num_rows($asm_listout_U);
$asm_total_row1      = $asm_total_num / 6;

//重新排列SPEC專案顯示(一排顯示六個SPEC專案)
if (is_int($asm_total_row1)) {
    $i = 0;
    $j = 6;
    for ($k = 1; $k <= $asm_total_row1; $k++) {
        ${'A' . $k} = "SELECT PartNumber FROM modify_spec_assembly  group by PartNumber order by PartNumber limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {
            ${'data_asm' . $k}[] = ${'C' . $k}['PartNumber'];
        }
        $i += $j;
    }
    $asm_total_row = $asm_total_row1;
} else {
    $asm_total_row2 = floor($asm_total_row1);
    $asm_total_row3 = ($asm_total_row2) + 1;
    $i              = 0;
    $j              = 6;
    for ($k = 1; $k <= $asm_total_row2; $k++) {
        ${'A' . $k} = "SELECT PartNumber FROM modify_spec_assembly  group by PartNumber order by PartNumber limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {
            ${'data_asm' . $k}[] = ${'C' . $k}['PartNumber'];
        }
        $i += $j;
    }

    $i          = ($asm_total_row2) * 6;
    $j          = 6;
    $k          = $asm_total_row3;
    ${'A' . $k} = "SELECT PartNumber FROM modify_spec_assembly  group by PartNumber order by PartNumber limit $i,$j";
    ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
    while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

        ${'data_asm' . $k}[] = ${'C' . $k}['PartNumber'];

    }
    $asm_total_row = $asm_total_row3;
}

?>

<!DOCTYPE HTML><head>
    <meta charset="utf-8">
    <title>Untitled Document</title></head>

    <style type="text/css">
        body {
          font: normal medium/1.4 sans-serif;
      }
      table {
          border-collapse: collapse;
      }
      th{
          padding: 0.25rem;
          text-align: center;
          border: 1px solid #ccc;
          background: #888888;
          font-size:15px;

      }
      td {
          padding: 0.25rem;
          text-align: center;
          border: 1px solid #ccc;
          font-size:13px;

      }
      B{
       font-family:"Arial Black", Gadget, sans-serif;
       color:#00000;
   }
   tbody tr:nth-child(odd) {
      background: #eee;
  }

  .ifile {
   position:absolute;opacity:0;filter:alpha(opacity=0);

}
</style>
</head>

<body background="Images/loginb.png">
    <table width='1400' method="post" style="table-layout:fixed">
        <thead>
            <tr>
                <th width='100' colspan=6>外購件:已上傳料號</th>
            </tr>
        </thead>

        <tbody>
            <?php
            echo "<tr>";
            for ($k = 1; $k <= $iqc_total_row; $k++) {
                for ($a = 0; $a < 6; $a++) {
                    echo "<td width=100% align=right>" . ${'data_iqc' . $k}[$a] . "</td>";
                }
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>

    <table width='1400' method="post" style="table-layout:fixed">
        <thead>
            <tr>
                <th width='100' colspan=6>零件電鍍:已上傳料號</th>
            </tr>
        </thead>

        <tbody>
            <?php
            echo "<tr>";
            for ($k = 1; $k <= $plating_total_row; $k++) {
                for ($a = 0; $a < 6; $a++) {
                    echo "<td width=100% align=right>" . ${'data_plating' . $k}[$a] . "</td>";
                }
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>

<table width='1400' method="post" style="table-layout:fixed">
        <thead>
            <tr>
                <th width='100' colspan=6>零件鐳射:已上傳料號</th>
            </tr>
        </thead>

        <tbody>
            <?php
            echo "<tr>";
            for ($k = 1; $k <= $welding_total_row; $k++) {
                for ($a = 0; $a < 6; $a++) {
                    echo "<td width=100% align=right>" . ${'data_welding' . $k}[$a] . "</td>";
                }
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>

    <table width='1400' method="post" style="table-layout:fixed">
        <thead>
            <tr>
                <th width='100' colspan=6>零件噴砂:已上傳料號</th>
            </tr>
        </thead>

        <tbody>
            <?php
            echo "<tr>";
            for ($k = 1; $k <= $blasting_total_row; $k++) {
                for ($a = 0; $a < 6; $a++) {
                    echo "<td width=100% align=right>" . ${'data_blasting' . $k}[$a] . "</td>";
                }
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>
    <table width='1400' method="post" style="table-layout:fixed">
        <thead>
            <tr>
                <th width='100' colspan=6>沖壓:已上傳料號_模號</th>
            </tr>
        </thead>

        <tbody>
            <?php
            echo "<tr>";
            for ($k = 1; $k <= $stmp_total_row; $k++) {
                for ($a = 0; $a < 6; $a++) {
                    echo "<td width=100% align=right>" . ${'data_stmp' . $k}[$a] . "</td>";
                }
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>

    <table width='1400' method="post" style="table-layout:fixed">
        <thead>
            <tr>
                <th width='100' colspan=6>成型:已上傳料號_模號</th>
            </tr>
        </thead>

        <tbody>
            <?php
            echo "<tr>";
            for ($k = 1; $k <= $mold_total_row; $k++) {
                for ($a = 0; $a < 6; $a++) {
                    echo "<td width=100% align=right>" . ${'data_mold' . $k}[$a] . "</td>";
                }
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>

    <table width='1400' method="post" style="table-layout:fixed">
        <thead>
            <tr>
                <th width='100' colspan=6>成品 or 半成品:已上傳料號</th>
            </tr>
        </thead>

        <tbody>
            <?php
            echo "<tr>";
            for ($k = 1; $k <= $asm_total_row; $k++) {
                for ($a = 0; $a < 6; $a++) {
                    echo "<td width=100% align=right>" . ${'data_asm' . $k}[$a] . "</td>";
                }
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>
</body>
</html>


