<?php
set_time_limit(0);
session_start();
error_reporting(0);
$start_time = date("H")*3600+date("i")*60+date("s");

require_once '../../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

function nrand($mean, $sd)
{
  $x = mt_rand() / mt_getrandmax();
  $y = mt_rand() / mt_getrandmax();
  return sqrt(-2 * log($x)) * cos(2 * pi() * $y) * $sd + $mean;
}

if (!isset($_SESSION)) {session_start();}
$Ticket_Number_array0 = $Ticket_Number_array1 = $PN_MN_array = array();
$NowDate              = date('Y-m-d');
$d1                   = date('Y-m-d', strtotime($NowDate) - 60 * 60 * 24 * 1) . " 08:00:00";
$d2                   = $NowDate . " 07:59:59";

mysqli_select_db($connect_spec, $database_spec);
mysqli_select_db($connect_plating, $database_plating);
mysqli_select_db($connect_sq, $database_sq);

//Select part Number from Spec db
$PNSearch_sql = "SELECT * FROM modify_spec_blasting WHERE 1=1 group by PartNumber order by PartNumber";

$PNSearch_query = mysqli_query($connect_spec, $PNSearch_sql) or die("警告 ： 搜尋Part Number失敗");
while ($PNSearch = mysqli_fetch_assoc($PNSearch_query)) {
  $PartNumber    = $PNSearch['PartNumber'];
  $M_Type    = substr($PartNumber,5,2);

  if($M_Type=='9U'){
    $Search_db_name='blasting_visual_data';
    $Insert_db_name='modify_blasting_dim_data';
      $Type='Blasting';//噴砂

      $AllTNSearch_sql   = "SELECT * FROM $Search_db_name WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' GROUP BY Ticket_Number ORDER BY DateTime";
          //echo $AllTNSearch_sql."</br>" ;

      $AllTNSearch_query = mysqli_query($connect_plating, $AllTNSearch_sql) or die("警告 ： 搜尋Ticket Number失敗");
      while ($AllTNSearch = mysqli_fetch_assoc($AllTNSearch_query)) {
        $End_DateTime           = $AllTNSearch['DateTime'];
        $End_Time           = substr($AllTNSearch['DateTime'],11,8);
        $Date               = $AllTNSearch['Date'];
        $DateTime               = $AllTNSearch['DateTime'];
        $PartNumber         = $AllTNSearch['PartNumber'];
        $Ticket_Number      = $AllTNSearch['Ticket_Number'];
        $Productline      = $AllTNSearch['Productline'];
        $day_night      = $AllTNSearch['day_night'];
        $Personnel_ID      = $AllTNSearch['Personnel_ID'];
    //$sections_array[]      = $AllTNSearch['sections'];

        //for Insert sq system
        if ($End_Time >= '08:00:00' && $End_Time <= '09:59:59'){
         $PhaseNumber='D-1';
       }
       else if ($End_Time >= '10:00:00' && $End_Time <= '11:59:59'){
         $PhaseNumber='D-2';
       }
       else if ($End_Time >= '12:00:00' && $End_Time <= '13:59:59'){
         $PhaseNumber='D-3';
       }
       else if ($End_Time >= '14:00:00' && $End_Time <= '15:59:59'){
         $PhaseNumber='D-4';
       }
       else if ($End_Time >= '16:00:00' && $End_Time <= '17:59:59'){
         $PhaseNumber='D-5';
       }
       else if ($End_Time >= '18:00:00' && $End_Time <= '19:59:59'){
         $PhaseNumber='D-6';
       }
       else if ($End_Time >= '20:00:00' && $End_Time <= '21:59:59'){
         $PhaseNumber='N-1';
       }
       else if ($End_Time >= '22:00:00' && $End_Time <= '23:59:59'){
         $PhaseNumber='N-2';
       }
       else if ($End_Time >= '00:00:00' && $End_Time <= '01:59:59'){
         $PhaseNumber='N-3';
       }
       else if ($End_Time >= '02:00:00' && $End_Time <= '03:59:59'){
         $PhaseNumber='N-4';
       }
       else if ($End_Time >= '04:00:00' && $End_Time <= '05:59:59'){
         $PhaseNumber='N-5';
       }
       else if ($End_Time >= '06:00:00' && $End_Time <= '07:59:59'){
         $PhaseNumber='N-6';
       }

       $SpecSearch_sql   = "SELECT * FROM modify_spec_blasting WHERE PartNumber = '" . $PartNumber . "' ";
         //echo $SpecSearch_sql."</br>";
       $SpecSearch_query = mysqli_query($connect_spec, $SpecSearch_sql) or die("警告 ： 搜尋Modify Spec失敗");
       $id=0;
       while ($AllInfo = mysqli_fetch_array($SpecSearch_query)) {
        $Data_array       = array();
        $Project_Name       = $AllInfo['ProjectName'];
        $PartNumber       = $AllInfo['PartNumber'];
        $Rev              = $AllInfo['Rev'];
        $DimNO            = $AllInfo['DimNO'];
        $Remark           = strtoupper($AllInfo['Remark']);
        $DimSpec          = $AllInfo['DimSpec'];
        $DimUpper         = $AllInfo['DimUpper'];
        $DimLower         = $AllInfo['DimLower'];
        $Expect_UpperData = $AllInfo['Expect_UpperData'];
        $Expect_LowerData = $AllInfo['Expect_LowerData'];
        $Expect_UpperStdv = ($AllInfo['Expect_UpperStdv']);
        $Expect_LowerStdv = ($AllInfo['Expect_LowerStdv']);
        $Expect_Result    = $AllInfo['Expect_Result'];
        $Expect_Data      = rand($Expect_LowerData * 1000, $Expect_UpperData * 1000) / 1000;
        $Expect_Stdv      = rand($Expect_LowerStdv * 1000, $Expect_UpperStdv * 1000) / 1000;
//echo "PartNumber : ".$PartNumber."</br>";
//echo "DimNO : ".$DimNO."</br>";
//echo "Remark : ".$Remark."</br>";

        $A = 0;
        while ($A < 5) {
          $val = round(nrand($Expect_Data, $Expect_Stdv), 4);

          if ($Expect_Result == 'OK') {
            if ($val <= $DimUpper && $val >= $DimLower) {
              $Data_array[] = $val;
              $A++;
            }

          } else {
            $Data_array[] = $val;
            $A++;
          }
        }

//Insert優化數據
        $Measure_Data = $Data_array[0];
        if ($Measure_Data > $DimUpper or $Measure_Data < $DimLow) {
          $Measure_Result = 'NG';
        } else {
          $Measure_Result = 'OK';
        }

    //Insert sq system
        $InsertSQModifyData_sql   = "REPLACE INTO `ipqc_modify_datas` (`MeasureDataTime`,`DateTime`, `PartNumber`, `PartDescription`, `LineNumber`, `PhaseNumber`, `TicketNumber`, `DimNO`, `MoldNumber`, `Type`, `CavNumber`, `Sample1`, `Sample2`, `Sample3`, `Sample4`, `Sample5`) VALUES ('" . $End_DateTime . "','" . $Date . "', '" . $PartNumber . "', '" . $Project_Name . "', '". $Productline ."', '" . $PhaseNumber . "', '" . $Ticket_Number . "', '" . $DimNO . "', 'NA', '" . $Type . "', 'NA', '" . $Data_array[0] . "', '" . $Data_array[1] . "', '" . $Data_array[2] . "', '" . $Data_array[3] . "', '" . $Data_array[4] . "')";
            //echo $InsertSQModifyData_sql;
        $InsertSQModifyData_query = mysqli_query($connect_sq, $InsertSQModifyData_sql) or die("警告 " . $Ticket_Number . " : Blasting SQModifydatas數據上傳資料庫失敗");

    //Insert Modify System
        $InsertModifyData_sql   = "REPLACE INTO $Insert_db_name (`id`,`PartNumber`, `Type`, `day_night`, `Productline`, `Ticket_Number`, `Personnel_ID`, `Date`, `DateTime`, `FAI_Number`, `spec`, `up_lim`, `down_lim`, `Measure_Value-1`, `Measure_Value-2`, `Measure_Value-3`, `Measure_Value-4`, `Measure_Value-5`) VALUES ('" . $id . "','" . $PartNumber . "', '" . $Type . "', '" . $day_night . "', '" . $Productline . "', '" . $Ticket_Number . "', '" . $Personnel_ID . "', '" . $Date . "', '" . $DateTime . "', '" . $DimNO . "', '" . $DimSpec . "', '" . $DimUpper . "', '" . $DimLower . "', '" . $Data_array[0] . "', '" . $Data_array[1] . "', '" . $Data_array[2] . "', '" . $Data_array[3] . "', '" . $Data_array[4] . "')";
              //echo $InsertModifyData_sql;
        $InsertModifyData_query = mysqli_query($connect_plating, $InsertModifyData_sql) or die("警告 " . $Ticket_Number . " : Modifydatas噴砂數據上傳資料庫失敗");
        $id++;

      }
    }
//Insert modify_laser_visual_data
    $VisualInsert_sql   = "REPLACE INTO modify_blasting_visual_data SELECT * from blasting_visual_data WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $VisualInsert_sql;
    $VisualInsert_query = mysqli_query($connect_plating, $VisualInsert_sql) or die("警告 ： Insert modify_blasting_visual_data");

      //Insert modify_laser_roughness_data
    $RoughnessInsert_sql   = "REPLACE INTO modify_blasting_roughness_data SELECT * from blasting_roughness_data WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $RoughnessInsert_sql;
    $RoughnessInsert_query = mysqli_query($connect_plating, $RoughnessInsert_sql) or die("警告 ： Insert modify_blasting_roughness_data");  
  }
}

echo "【 優化數據上傳資料庫成功 】</br>";
$end_time = date("H")*3600+date("i")*60+date("s");

//echo "start_time：".$start_time."秒<br/>";
//echo "end_time：".$end_time."秒<br/>";
echo "報告生成日期:".$NowDate."<br/>";
$time_total = $end_time - $start_time;
echo "執行了：".$time_total."秒<br/>";
