<?php
set_time_limit(0);
session_start();
error_reporting(0);
$start_time = date("H")*3600+date("i")*60+date("s");

require_once '../../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

function nrand($mean, $sd)
{
    $x = mt_rand() / mt_getrandmax();
    $y = mt_rand() / mt_getrandmax();
    return sqrt(-2 * log($x)) * cos(2 * pi() * $y) * $sd + $mean;
}

if (!isset($_SESSION)) {session_start();}
$Ticket_Number_array0 = $Ticket_Number_array1 = $PN_MN_array = array();
$NowDate              = date('Y-m-d');
$d1                   = date('Y-m-d', strtotime($NowDate) - 60 * 60 * 24 * 1) . "-08:00:00";
$d2                   = $NowDate . "-07:59:59";

mysqli_select_db($connect_spec, $database_spec);
mysqli_select_db($connect_stmp, $database_stmp);
mysqli_select_db($connect_sq, $database_sq);

//Select part Number from Spec db
$PNSearch_sql = "SELECT * FROM modify_spec_stamping WHERE 1=1 group by PartNumber,MoldNumber order by PartNumber,MoldNumber";

$PNSearch_query = mysqli_query($connect_spec, $PNSearch_sql) or die("警告 ： 搜尋Part Number失敗");
while ($PNSearch = mysqli_fetch_assoc($PNSearch_query)) {
    $ProjectName_S    = $PNSearch['ProjectName'];
    $PartNumber    = $PNSearch['PartNumber'];
    $MoldNumber    = $PNSearch['MoldNumber'];
    $PN_MN_array[] = ($PNSearch['PartNumber'] . "_" . $PNSearch['MoldNumber']);

//delect 既有的數據
    $AllCTDelete_sql   = "SELECT Ticket_Number FROM modify_main_table WHERE 1=1 AND Part_Number_V = '$PartNumber' AND Mold_Number = '$MoldNumber' AND concat(End_Date,'-',End_Time) between '$d1' AND '$d2' ORDER BY End_Date,End_Time";
    $AllCTDelete_query = mysqli_query($connect_stmp, $AllCTDelete_sql) or die("警告 ： 搜尋main table數據失敗");

    $DeleteVisuals_sql   = "DELETE FROM modify_visual_inspection WHERE 1=1 AND Part_Number = '$PartNumber' AND Mold_Number = '$MoldNumber' AND concat(Date,'-',Time) between '$d1' AND '$d2' ";
    $DeleteVisuals_query = mysqli_query($connect_stmp, $DeleteVisuals_sql) or die("警告 ： 刪除Visual數據失敗");
    $DeleteSQ_sql   = "DELETE FROM ipqc_modify_datas WHERE 1=1 AND PartNumber = '$PartNumber' AND MoldNumber = '$MoldNumber' AND MeasureDataTime between '$d1' AND '$d2' ";
    $DeleteSQ_query = mysqli_query($connect_sq, $DeleteSQ_sql) or die("警告 ： 刪除SQ數據失敗");

    while ($AllCTDelete = mysqli_fetch_assoc($AllCTDelete_query)) {
        $Ticket_Number = $AllCTDelete['Ticket_Number'];

        $DeleteContents_sql   = "DELETE FROM modify_main_table WHERE 1=1 AND Ticket_Number = '$Ticket_Number'";
        $DeleteContents_query = mysqli_query($connect_stmp, $DeleteContents_sql) or die("警告 ： 刪除main table數據失敗");
        $DeleteDatas_sql      = "DELETE FROM modify_measure_data_1cav WHERE 1=1 AND Ticket_Number = '$Ticket_Number'";
        $DeleteDatas_query    = mysqli_query($connect_stmp, $DeleteDatas_sql) or die("警告 ： 刪除measuredatas數據失敗");
    }

//Insert Visual Datas
    $VisaulSearch_sql = "SELECT * FROM visual_inspection WHERE 1=1 AND Part_Number = '$PartNumber' AND Mold_Number = '$MoldNumber' AND concat(Date,'-',Time) between '$d1' AND '$d2' GROUP BY   Ticket_Number ORDER BY concat(Date,'-',Time)";
    //echo $VisaulSearch_sql;
    $VisualSearch_query = mysqli_query($connect_stmp, $VisaulSearch_sql) or die("警告 ： 搜尋Visual Datas失敗");
    while ($VisualSearch = mysqli_fetch_assoc($VisualSearch_query)) {
        $Inspector_ID           = $VisualSearch['Inspector_ID'];
        $Ticket_Number          = $VisualSearch['Ticket_Number'];
        $Ticket_Number_array0[] = $VisualSearch['Ticket_Number'];
        $Part_Number            = $VisualSearch['Part_Number'];
        $Version                = $VisualSearch['Version'];
        $Mold_Number            = $VisualSearch['Mold_Number'];
        $Date                   = $VisualSearch['Date'];
        $Time                   = $VisualSearch['Time'];
        $Inspect_Status         = $VisualSearch['Inspect_Status'];
        $Component_Item_Desc    = $VisualSearch['Component_Item_Desc'];
        $Component_Item_NO      = $VisualSearch['Component_Item_NO'];
        $Material_Num           = $VisualSearch['Material_Num'];
        $Prod_Line_Code         = $VisualSearch['Prod_Line_Code'];
        $I1                     = $VisualSearch['I1'];
        $I2                     = $VisualSearch['I2'];
        $I3                     = $VisualSearch['I3'];
        $I4                     = $VisualSearch['I4'];
        $I5                     = $VisualSearch['I5'];
        $I6                     = $VisualSearch['I6'];
        $I7                     = $VisualSearch['I7'];
        $I8                     = $VisualSearch['I8'];
        $I9                     = $VisualSearch['I9'];
        $I10                    = $VisualSearch['I10'];
        $I11                    = $VisualSearch['I11'];
        $I12                    = $VisualSearch['I12'];
        $I13                    = $VisualSearch['I13'];
        $I14                    = $VisualSearch['I14'];
        $I15                    = $VisualSearch['I15'];
        $HSF                    = $VisualSearch['HSF'];
        $Result                 = $VisualSearch['Result'];
        $Remark                 = $VisualSearch['Remark'];
        $InsertTime             = $VisualSearch['InsertTime'];

        $Insert_Visualtable_sql = "REPLACE INTO `modify_visual_inspection` (`Inspector_ID`, `Ticket_Number`, `Part_Number`, `Version`, `Mold_Number`, `Date`, `Time`, `Inspect_Status`, `Component_Item_Desc`, `Component_Item_NO`, `Material_Num`, `Prod_Line_Code`, `I1`, `I2`, `I3`, `I4`, `I5`, `I6`, `I7`, `I8`, `I9`, `I10`, `I11`, `I12`, `I13`, `I14`, `I15`, `HSF`, `Result`, `Remark`, `InsertTime`) VALUES ('" . $Inspector_ID . "','" . $Ticket_Number . "','" . $PartNumber . "','" . $Version . "','" . $Mold_Number . "','" . $Date . "','" . $Time . "','" . $Inspect_Status . "','" . $Component_Item_Desc . "','" . $Component_Item_NO . "','" . $Material_Num . "','" . $Prod_Line_Code . "','" . $I1 . "','" . $I2 . "','" . $I3 . "','" . $I4 . "','" . $I5 . "','" . $I6 . "','" . $I7 . "','" . $I8 . "','" . $I9 . "','" . $I10 . "','" . $I11 . "','" . $I12 . "','" . $I13 . "','" . $I14 . "','" . $I15 . "','" . $HSF . "','" . $Result . "','" . $Remark . "','" . $InsertTime . "')";
        //echo $Insert_Visualtable_sql;
        $Insert_Visualtable_query = mysqli_query($connect_stmp, $Insert_Visualtable_sql) or die("警告 " . $Ticket_Number . " : 輸入modify_visual_inspection資料庫失敗");
    }

//Insert Measure Datas
    $AllTNSearch_sql   = "SELECT * FROM main_table WHERE 1=1 AND Part_Number_V LIKE '$PartNumber%' AND Mold_Number = '$MoldNumber' AND concat(End_Date,'-',End_Time) between '$d1' AND '$d2' ORDER BY concat(End_Date,'-',End_Time)";
    $AllTNSearch_query = mysqli_query($connect_stmp, $AllTNSearch_sql) or die("警告 ： 搜尋Ticket Number失敗");
    while ($AllTNSearch = mysqli_fetch_assoc($AllTNSearch_query)) {
        //$Project_Name_m           = $AllTNSearch['Project_Name'];
        $Part_Number_V          = $AllTNSearch['Part_Number_V'];
        $Mold_Number            = $AllTNSearch['Mold_Number'];
        $Ticket_Number          = $AllTNSearch['Ticket_Number'];
        $Ticket_Number_array1[] = $VisualSearch['Ticket_Number'];
        $Start_Date             = $AllTNSearch['Start_Date'];
        $Start_Time             = $AllTNSearch['Start_Time'];
        $End_Date               = $AllTNSearch['End_Date'];
        $End_Time               = $AllTNSearch['End_Time'];
        $End_DateTime           = $End_Date . " " . $End_Time;
        $Status                 = $AllTNSearch['Status'];
        $Remark                 = $AllTNSearch['Remark'];
        $Spec_Status            = $AllTNSearch['Spec_Status'];
        $Cavity                 = $AllTNSearch['Cavity'];
        $Re                     = $AllTNSearch['Re'];

        //for Insert sq system
        if ($End_Time >= '00:00:00' && $End_Time <= '07:59:59') {
            $DateTime = date('Y-m-d', strtotime($End_Date) - 60 * 60 * 24);
        } else {
            $DateTime = $End_Date;
        }
        if ($End_Time >= '08:00:00' && $End_Time <= '09:59:59'){
           $PhaseNumber='D-1';
       }
       else if ($End_Time >= '10:00:00' && $End_Time <= '11:59:59'){
           $PhaseNumber='D-2';
       }
       else if ($End_Time >= '12:00:00' && $End_Time <= '13:59:59'){
           $PhaseNumber='D-3';
       }
       else if ($End_Time >= '14:00:00' && $End_Time <= '15:59:59'){
           $PhaseNumber='D-4';
       }
       else if ($End_Time >= '16:00:00' && $End_Time <= '17:59:59'){
           $PhaseNumber='D-5';
       }
       else if ($End_Time >= '18:00:00' && $End_Time <= '19:59:59'){
           $PhaseNumber='D-6';
       }
       else if ($End_Time >= '20:00:00' && $End_Time <= '21:59:59'){
           $PhaseNumber='N-1';
       }
       else if ($End_Time >= '22:00:00' && $End_Time <= '23:59:59'){
           $PhaseNumber='N-2';
       }
       else if ($End_Time >= '00:00:00' && $End_Time <= '01:59:59'){
           $PhaseNumber='N-3';
       }
       else if ($End_Time >= '02:00:00' && $End_Time <= '03:59:59'){
           $PhaseNumber='N-4';
       }
       else if ($End_Time >= '04:00:00' && $End_Time <= '05:59:59'){
           $PhaseNumber='N-5';
       }
       else if ($End_Time >= '06:00:00' && $End_Time <= '07:59:59'){
           $PhaseNumber='N-6';
       }

//Insert Main Table數據
       $Insert_maintable = "REPLACE INTO `modify_main_table` (`Project_Name`, `Part_Number_V`, `Mold_Number`, `Ticket_Number`, `Start_Date`, `Start_Time`, `End_Date`, `End_Time`, `Status`, `Remark`, `Spec_Status`, `Cavity`, `Re`) VALUES ('" . $ProjectName_S . "', '" . $PartNumber . "', '" . $Mold_Number . "', '" . $Ticket_Number . "', '" . $Start_Date . "', '" . $Start_Time . "', '" . $End_Date . "', '" . $End_Time . "', '" . $Status . "', '" . $Remark . "', '" . $Spec_Status . "', '" . $Cavity . "', '" . $Re . "')";
       $query            = mysqli_query($connect_stmp, $Insert_maintable) or die("警告 " . $Ticket_Number . " : Modify_Main_table上傳資料庫失敗");

       $SpecSearch_sql   = "SELECT * FROM modify_spec_stamping WHERE PartNumber = '" . $PartNumber . "' AND MoldNumber = '$MoldNumber' ";
       $SpecSearch_query = mysqli_query($connect_spec, $SpecSearch_sql) or die("警告 ： 搜尋Modify Spec失敗");
       while ($AllInfo = mysqli_fetch_array($SpecSearch_query)) {
        $Data_array       = array();
        $Spec_Project_Name       = $AllInfo['ProjectName'];
        $PartNumber       = $AllInfo['PartNumber'];
        $Rev              = $AllInfo['Rev'];
        $DimNO            = $AllInfo['DimNO'];
        $Remark           = strtoupper($AllInfo['Remark']);
        $DimSpec          = $AllInfo['DimSpec'];
        $DimUpper         = $AllInfo['DimUpper'];
        $DimLower         = $AllInfo['DimLower'];
        $Expect_UpperData = $AllInfo['Expect_UpperData'];
        $Expect_LowerData = $AllInfo['Expect_LowerData'];
        $Expect_UpperStdv = ($AllInfo['Expect_UpperStdv']);
        $Expect_LowerStdv = ($AllInfo['Expect_LowerStdv']);
        $Expect_Result    = $AllInfo['Expect_Result'];
        $Expect_Data      = rand($Expect_LowerData * 1000, $Expect_UpperData * 1000) / 1000;
        $Expect_Stdv      = rand($Expect_LowerStdv * 1000, $Expect_UpperStdv * 1000) / 1000;

        $A = 0;
        while ($A < 6) {
            $val = round(nrand($Expect_Data, $Expect_Stdv), 4);

            if ($Expect_Result == 'OK') {
                if ($val <= $DimUpper && $val >= $DimLower) {
                    $Data_array[] = $val;
                    $A++;
                }

            } else {
                $Data_array[] = $val;
                $A++;
            }
        }

            //Insert CPK優化數據
        if ($Remark == 'CPK') {
            $B = 0;
            for ($i = 1; $i <= 5; $i++) {
                $DimNO_CPK    = $DimNO . "_CPK" . $i;
                $Measure_Data = $Data_array[$B];
                if ($Measure_Data > $DimUpper or $Measure_Data < $DimLow) {
                    $Measure_Result = 'NG';
                } else {
                    $Measure_Result = 'OK';
                }

                $InsertModifyData_sql   = "REPLACE INTO `modify_measure_data_1cav` (`Ticket_Number`, `FAI_Number`, `Nominal_Dim`, `Upper_Dim`, `Lower_Dim`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `Expect_Data`, `Expect_Stdv`, `Measure_Value`, `Measure_Datetime`, `Measure_Result`) VALUES ('" . $Ticket_Number . "', '" . $DimNO_CPK . "', '" . $DimSpec . "', '" . $DimUpper . "', '" . $DimLower . "', '" . $Expect_UpperData . "', '" . $Expect_LowerData . "', '" . $Expect_UpperStdv . "', '" . $Expect_LowerStdv . "', '" . $Expect_Result . "', '" . $Expect_Data . "', '" . $Expect_Stdv . "', '" . $Measure_Data . "', '" . $End_DateTime . "', '" . $Measure_Result . "')";
                $InsertModifyData_query = mysqli_query($connect_stmp, $InsertModifyData_sql) or die("警告 " . $Ticket_Number . " : ModifyCPKdatas數據上傳資料庫失敗");
                $B++;
            }
                //Insert sq system
            $InsertSQModifyData_sql   = "REPLACE INTO `ipqc_modify_datas` (`MeasureDataTime`,`DateTime`, `PartNumber`, `PartDescription`, `LineNumber`, `PhaseNumber`, `TicketNumber`, `DimNO`, `MoldNumber`, `Type`, `CavNumber`, `Sample1`, `Sample2`, `Sample3`, `Sample4`, `Sample5`) VALUES ('" . $End_DateTime . "','" . $DateTime . "', '" . $PartNumber . "', '" . $ProjectName_S . "', 'NA', '" . $PhaseNumber . "', '" . $Ticket_Number . "', '" . $DimNO . "', '" . $Mold_Number . "', 'Stamping', 'NA', '" . $Data_array[0] . "', '" . $Data_array[1] . "', '" . $Data_array[2] . "', '" . $Data_array[3] . "', '" . $Data_array[4] . "')";
            $InsertSQModifyData_query = mysqli_query($connect_sq, $InsertSQModifyData_sql) or die("警告 " . $Ticket_Number . " : SQModifyCPKdatas數據上傳資料庫失敗");
        } else {
//Insert優化數據
            $Measure_Data = $Data_array[0];
            if ($Measure_Data > $DimUpper or $Measure_Data < $DimLow) {
                $Measure_Result = 'NG';
            } else {
                $Measure_Result = 'OK';
            }

            $InsertModifyData_sql   = "REPLACE INTO `modify_measure_data_1cav` (`Ticket_Number`, `FAI_Number`, `Nominal_Dim`, `Upper_Dim`, `Lower_Dim`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Data`, `Expect_Stdv`, `Measure_Value`, `Measure_Datetime`, `Measure_Result`) VALUES ('" . $Ticket_Number . "', '" . $DimNO . "', '" . $DimSpec . "', '" . $DimUpper . "', '" . $DimLower . "', '" . $Expect_UpperData . "', '" . $Expect_LowerData . "', '" . $Expect_UpperStdv . "', '" . $Expect_LowerStdv . "', '" . $Expect_Data . "', '" . $Expect_Stdv . "', '" . $Measure_Data . "', '" . $End_DateTime . "', '" . $Measure_Result . "')";
            $InsertModifyData_query = mysqli_query($connect_stmp, $InsertModifyData_sql) or die("警告 " . $Ticket_Number . " : Modifydatas數據上傳資料庫失敗");
            
                //Insert sq system
            $InsertSQModifyData_sql   = "REPLACE INTO `ipqc_modify_datas` (`MeasureDataTime`,`DateTime`, `PartNumber`, `PartDescription`, `LineNumber`, `PhaseNumber`, `TicketNumber`, `DimNO`, `MoldNumber`, `Type`, `CavNumber`, `Sample1`, `Sample2`, `Sample3`, `Sample4`, `Sample5`) VALUES ('" . $End_DateTime . "','" . $DateTime . "', '" . $PartNumber . "', '" . $ProjectName_S . "', 'NA', '" . $PhaseNumber . "', '" . $Ticket_Number . "', '" . $DimNO . "', '" . $Mold_Number . "', 'Stamping', 'NA', '" . $Data_array[0] . "', '', '', '', '')";
            $InsertSQModifyData_query = mysqli_query($connect_sq, $InsertSQModifyData_sql) or die("警告 " . $Ticket_Number . " : SQModifyCPKdatas數據上傳資料庫失敗");
        }
    }
}
    //echo "【 優化數據上傳資料庫成功 】";
}
//print_r($PN_MN_array);
echo "【 優化數據上傳資料庫成功 】</br>";
$end_time = date("H")*3600+date("i")*60+date("s");

//echo "start_time：".$start_time."秒<br/>";
//echo "end_time：".$end_time."秒<br/>";
echo "報告生成日期:".$NowDate."<br/>";
$time_total = $end_time - $start_time;
echo "執行了：".$time_total."秒<br/>";
