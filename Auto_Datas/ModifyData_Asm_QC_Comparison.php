<?php
session_start();

error_reporting(0);
require_once '../../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../../Public/Connections/sfc_system_fq_icbu.php';
require_once '../../../Public/Connections/projector_system_Oracle.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

mysqli_select_db($connect_Oracle, $database_Oracle);
mysqli_select_db($connect_spec, $database_spec);
mysqli_select_db($connect_asm, $database_asm);
mysqli_select_db($connect_sq, $database_sq);
mysqli_select_db($connect_sfc, $database_sfc);
mysqli_select_db($connect_aoi, $database_aoi);
mysqli_select_db($connect_aoi_sudo, $database_aoi_sudo);

function nrand($mean, $sd)
{
    $x = mt_rand() / mt_getrandmax();
    $y = mt_rand() / mt_getrandmax();
    return sqrt(-2 * log($x)) * cos(2 * pi() * $y) * $sd + $mean;
}

if (!isset($_SESSION)) {session_start();}
$PN_array = array();
$NowDate  = date('Y-m-d');
$NowTime  = date('H:i:s');
$NowDate  = '2021-07-05';
$NowTime  = '22:00:00';

if ($NowTime >= '20:00:00' && $NowTime <= '23:59:59') {
    $d1          = $NowDate . " 08:00:00";
    $d2          = $NowDate . " 19:59:59";
    $MeasureDate = $NowDate;
    $NowShift    = 'D';
    $day_night   = '白班';
} else if ($NowTime >= '08:00:00' && $NowTime <= '19:59:59') {
    $d1          = date('Y-m-d', strtotime($NowDate) - 60 * 60 * 24 * 1) . " 20:00:00";
    $d2          = $NowDate . " 07:59:59";
    $MeasureDate = date('Y-m-d', strtotime($NowDate) - 60 * 60 * 24 * 1);
    $NowShift    = 'N';
    $day_night   = '夜班';
}
echo "MeasureDate:".$MeasureDate."</br>";
echo "NowShift:".$NowShift."</br>";
/*
$d1  = '2021-05-10 20:00:00';
$d2     ='2021-05-11 07:59:59';
$MeasureDate='2021-05-10';
$NowShift='N';
$day_night='夜班';
$PartNumber='8195-3510-3U71';
$PartDescription='';
 */
$OracleTicketNumber_array = $DBTicketNumber_array = $TicketNumber_array = $LineNumber_array = $LineName_array = $Activation_array = $enable_out_array = $Partno_array = $DeviceName_array = $MaterialNum_array = $PartDescription_array = array();

//$PartDescription_array = array('D52 E75'=>'1','D53 E75'=>'1','D54 E75'=>'1','X61 E75'=>'1','B688 NEMO'=>'0','B635 CONTACT CARRIER L'=>'0','B635 CONTACT CARRIER R'=>'0');
$PartDescription_array = array('B688 NEMO'=>'0');

foreach($PartDescription_array as $PartDescription => $key){
    echo "PartDescription:".$PartDescription."</br></br>";

    $LN_Search_sql   = "SELECT LINE_NO,REAL_LINE FROM project_with_real_line WHERE PROJECT_CODE='" . $PartDescription . "'";
    $LN_Search_query = mysqli_query($connect_sfc, $LN_Search_sql) or die("警告 ： 搜尋LINE_NO失敗");

    while ($LN_Search = mysqli_fetch_assoc($LN_Search_query)) {
        unset($OracleTicketNumber_array);
        unset($DBTicketNumber_array);
        unset($TicketNumber_array);
        unset($LineNumber_array);
        unset($LineName_array);
        unset($Activation_array);
        unset($enable_out_array);
        unset($Partno_array);
        unset($DeviceName_array);
        unset($MaterialNum_array);

        $LineNumber_array[] = $LN_Search['LINE_NO'];
        $LineNumber         = $LN_Search['LINE_NO'];
        $LineName_array[]   = $LN_Search['REAL_LINE'];
        $LineName           = $LN_Search['REAL_LINE'];

        echo "LineNumber :".$LineNumber."</br>";
        echo "LineName :".$LineName."</br>";

        //查詢是否對外顯示
        $enableout_Search_sql = "SELECT enable_out, module FROM enable_calendar_test WHERE project = '" . $PartDescription . "' and work_date = '" . $MeasureDate . "' and shift = '" . $NowShift . "' and line ='" . $LineNumber . "' ";
        //echo $enableout_Search_sql."</br>";
        $enableout_Search_query = mysqli_query($connect_sfc, $enableout_Search_sql) or die("警告 ： 搜尋enable_out失敗");

        while ($enableout_Search = mysqli_fetch_assoc($enableout_Search_query)) {
            $enable_out_array[] = $enableout_Search['enable_out'];
            $enable_out         = $enableout_Search['enable_out'];
        }

        //if (end($Activation_array) > 0 && $enable_out == 1) {
        if ($enable_out == 1) {
            if ($key =='1') {//$key=1 成品有鐳雕條碼;
            //查詢是否指定料號(Partno==''時表示不指定料號)
                $Partno_Search_sql = "SELECT Partno FROM yield_target_relax WHERE Project='" . $PartDescription . "' AND Date='" . $MeasureDate . "' AND Shift='" . $NowShift . "' AND Line='" . $LineNumber . "' GROUP BY Partno ";
            //echo $Partno_Search_sql."</br>";
                $Partno_Search_query = mysqli_query($connect_sfc, $Partno_Search_sql) or die("警告 ： 搜尋Partno失敗");

                while ($Partno_Search = mysqli_fetch_assoc($Partno_Search_query)) {
                    $Partno_array[] = $Partno_Search['Partno'];
                    $Partno         = $Partno_Search['Partno'];
                }

            //不指定料號(依當下生產料號造數據)
                if ($Partno == '') {

                //抓取生產線最後一個工站名稱
                    $DeviceName_Search_sql   = "SELECT Device_Name,Measure_Workno FROM measure_info where Project='" . $PartDescription . "' and Measure_Workno not like 'Re_%' AND SF_Ref
                    ='1' ORDER BY CAST(SUBSTRING(Device_Name FROM 8) AS UNSIGNED) ASC";
                //echo $DeviceName_Search_sql."</br>";
                    $DeviceName_Search_query = mysqli_query($connect_aoi, $DeviceName_Search_sql) or die("警告 ： 搜尋DeviceName失敗");

                    while ($DeviceName_Search = mysqli_fetch_assoc($DeviceName_Search_query)) {
                        $DeviceName_array[]     = $DeviceName_Search['Device_Name'];
                        $DeviceName             = $DeviceName_Search['Device_Name'];
                        $Measure_Workno_array[] = $DeviceName_Search['Measure_Workno'];
                        $Measure_Workno         = $DeviceName_Search['Measure_Workno'];
                    }
                    $Station = end($Measure_Workno_array);
                //echo $Station."</br>";
                //$Station='Package';
//print_r($DeviceName_array)."</br></br>";
//print_r($Measure_Workno_array)."</br>";
                //不指定料號時抓取當下生產的料號
                    $MaterialNum_Search_sql   = "SELECT MaterialNum FROM sq_presave where project = '" . $PartDescription . "' and Line = '" . $LineNumber . "' and Date = '" . $MeasureDate . "' and Shift = '" . $NowShift . "' AND Station='" . $Station . "' ";
                //echo $MaterialNum_Search_sql."</br>";
                    $MaterialNum_Search_query = mysqli_query($connect_aoi_sudo, $MaterialNum_Search_sql) or die("警告 ： 搜尋MaterialNum失敗");

                    while ($MaterialNum_Search = mysqli_fetch_assoc($MaterialNum_Search_query)) {
                        $MaterialNum_array[] = $MaterialNum_Search['MaterialNum'];
                        $MaterialNum         = $MaterialNum_Search['MaterialNum'];
                    }
                //$PartNumber = end($MaterialNum_array);
                //print_r($MaterialNum_array)."</br>";
                //echo $PartNumber."</br>";
                } else {
                //$PartNumber = $Partno;
                    $MaterialNum_array[] = $Partno;
                }
            //echo "PartNumber:".$PartNumber."</br></br>";
                $PN_array = array_unique($MaterialNum_array);
                //Print_r($PN_array);      
            }
            else{ //$key=0 成品沒有鐳雕條碼;
                $PN_Search_sql   = "SELECT * FROM modify_spec_assembly WHERE ProjectName='" . $PartDescription . "'";
            //echo $PN_Search_sql;
                $PN_Search_query = mysqli_query($connect_spec, $PN_Search_sql) or die("警告 ： 搜尋LINE_NO失敗");

                while ($PN_Search = mysqli_fetch_assoc($PN_Search_query)) {
                    $PartNumber           = $PN_Search['PartNumber'];
                    $MaterialNum_array[] = $PartNumber;
                    
                }
                $PN_array = array_unique($MaterialNum_array);
                //Print_r($PN_array); 
            }
            //Print_r($PN_array);
            foreach ($PN_array as $PartNumber) {
            //Select measurecontent數據(尺寸)
                echo "PartNumber :".$PartNumber."</br></br>";
                $QCSearch_sql = "SELECT * FROM measurecontent WHERE 1=1 AND PartNumber = '".$PartNumber."' AND DateTime = '" . $MeasureDate . "' AND LineNumber='" . $LineName . "' AND PhaseNumber LIKE '" . $NowShift . "%' union SELECT * FROM 3f_measurecontent WHERE 1=1 AND PartNumber = '".$PartNumber."' AND DateTime = '" . $MeasureDate . "' AND LineNumber='" . $LineName . "' AND PhaseNumber LIKE '" . $NowShift . "%' ORDER BY MeasureEndTime";

                //$QCSearch_sql = "SELECT * FROM measurecontent WHERE 1=1 AND PartDescription='" .$PartDescription. "' AND DateTime = '" . $MeasureDate . "' UNION SELECT * FROM 3f_measurecontent WHERE 1=1 AND PartDescription='" .$PartDescription. "' AND DateTime = '" . $MeasureDate . "' ORDER BY MeasureEndTime";
                //echo $QCSearch_sql;
                $QCSearch_query = mysqli_query($connect_asm, $QCSearch_sql) or die("警告 ： 搜尋measurecontent數據失敗");
                $num             = mysqli_num_rows($QCSearch_query);
                while ($QCSearch = mysqli_fetch_assoc($QCSearch_query)) {
                    $QC_DateTime[]=$QCSearch['DateTime'];
                    $QC_PhaseNumber[]=$QCSearch['PhaseNumber'];
                    $QC_PartNumber[]=$QCSearch['PartNumber'];
                    $QC_LineNumber[]=$QCSearch['LineNumber'];
                }


            }
        }
    }
}

?>

<!DOCTYPE HTML>
<head>
    <meta charset="utf-8">
    <title>Untitled Document</title>

    <style type="text/css">
        body {
          font: normal medium/1.4 sans-serif;
      }
      table {
          border-collapse: collapse;
          width: 900px;
      }
      th{
          padding: 0.25rem;
          text-align: center;
          border: 1px solid #ccc;
          background: #888888;
          font-size:15px;

      }
      td {
          padding: 0.25rem;
          text-align: center;
          border: 1px solid #ccc;
          font-size:13px;

      }
      B{
        font-family:"Arial Black", Gadget, sans-serif;
        color:#00000;
    }
    tbody tr:nth-child(odd) {
      background: #eee;
  }

  .ifile {
    position:absolute;opacity:0;filter:alpha(opacity=0);

}
</style>

</head>
<body>
    生產料號/線別 & QC量測比對:<br>
    <div class="Measure-Data-table-2">
        <table id="Measure-Data-table-2" class="sortable">
            <thead>
                <th width='60px'><div align="center">專案名稱</div></th>
                <th width='60px'><div align="center">量測日期</div></th>
                <th width='60px'><div align="center">量測班別</div></th>
                <th width='60px'><div align="center">QC量測料號</div></th>
                <th width='60px'><div align="center">QC量測線別</div></th>
            </thead>
            <div align="center"></div>
            <tbody>

                <?php

                if($num>0){
                    for($i=0;$i<$num;$i++) {
                        echo "<tr>";
                        echo "<td>" . $PartDescription . "</td>";
                        echo "<td>" . $MeasureDate . "</td>";
                        echo "<td>" . $NowShift . "</td>";
                        echo "<td>" . $QC_PartNumber[$i] . "</td>";
                        echo "<td>" . $QC_LineNumber[$i] . "</td>";
                        echo "</tr>";
                    }
                }
                else{
                    echo "<tr>";
                    echo "<td>" . $PartDescription . "</td>";
                    echo "<td>" . $MeasureDate . "</td>";
                    echo "<td>" . $NowShift . "</td>";
                    echo "<td>QC無檢驗此料號/線別</td>";
                    echo "<td>QC無檢驗此料號/線別</td>";
                    echo "</tr>";

                }

                ?>
            </tbody>
        </table>

    </body>
    </html>


