<?php
set_time_limit(0);
session_start();
error_reporting(0);
$start_time = date("H")*3600+date("i")*60+date("s");

require_once '../../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

function nrand($mean, $sd)
{
  $x = mt_rand() / mt_getrandmax();
  $y = mt_rand() / mt_getrandmax();
  return sqrt(-2 * log($x)) * cos(2 * pi() * $y) * $sd + $mean;
}

if (!isset($_SESSION)) {session_start();}
$Ticket_Number_array0 = $Ticket_Number_array1 = $PN_MN_array = array();
$NowDate              = date('Y-m-d');
$d1                   = date('Y-m-d', strtotime($NowDate) - 60 * 60 * 24 * 1) . " 08:00:00";
$d2                   = $NowDate . " 07:59:59";

mysqli_select_db($connect_spec, $database_spec);
mysqli_select_db($connect_plating, $database_plating);
mysqli_select_db($connect_sq, $database_sq);

//Select part Number from Spec db
$PNSearch_sql = "SELECT * FROM modify_spec_plating WHERE 1=1 group by PartNumber order by PartNumber";

$PNSearch_query = mysqli_query($connect_spec, $PNSearch_sql) or die("警告 ： 搜尋Part Number失敗");
while ($PNSearch = mysqli_fetch_assoc($PNSearch_query)) {
  $PartNumber    = $PNSearch['PartNumber'];
  $M_Type    = substr($PartNumber,3,1);

  if($M_Type=='1'){
    $Search_db_name1='pin_visual_data';
    $Search_db_name2='pin_visual_data_4f';
    $Search_db_name3='metal_visual_data';
    $Insert_db_name1='modify_pin_dim_data';
    $Insert_db_name2='modify_pin_bin_dim_data';
    $Type='Plating';//電鍍

    $AllTNSearch_sql   = "SELECT DateTime,Date,PartNumber,Ticket_Number,Productline,day_night,Personnel_ID FROM $Search_db_name1 WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' GROUP BY Ticket_Number UNION SELECT DateTime,Date,PartNumber,Ticket_Number,Productline,day_night,Personnel_ID FROM $Search_db_name2 WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' GROUP BY Ticket_Number UNION SELECT DateTime,Date,PartNumber,Ticket_Number,Productline,day_night,Personnel_ID FROM $Search_db_name3 WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' GROUP BY Ticket_Number ORDER BY DateTime";
//echo $AllTNSearch_sql ;

    $AllTNSearch_query = mysqli_query($connect_plating, $AllTNSearch_sql) or die("警告 ： 搜尋Ticket Number失敗");
    while ($AllTNSearch = mysqli_fetch_assoc($AllTNSearch_query)) {
      $End_DateTime           = $AllTNSearch['DateTime'];
      $End_Time           = substr($AllTNSearch['DateTime'],11,8);
      $Date               = $AllTNSearch['Date'];
      $DateTime               = $AllTNSearch['DateTime'];
      $PartNumber         = $AllTNSearch['PartNumber'];
      $Ticket_Number      = $AllTNSearch['Ticket_Number'];
      $Productline      = $AllTNSearch['Productline'];
      $day_night      = $AllTNSearch['day_night'];
      $Personnel_ID      = $AllTNSearch['Personnel_ID'];
    //$sections_array[]      = $AllTNSearch['sections'];

        //for Insert sq system
      if ($End_Time >= '08:00:00' && $End_Time <= '09:59:59'){
       $PhaseNumber='D-1';
     }
     else if ($End_Time >= '10:00:00' && $End_Time <= '11:59:59'){
       $PhaseNumber='D-2';
     }
     else if ($End_Time >= '12:00:00' && $End_Time <= '13:59:59'){
       $PhaseNumber='D-3';
     }
     else if ($End_Time >= '14:00:00' && $End_Time <= '15:59:59'){
       $PhaseNumber='D-4';
     }
     else if ($End_Time >= '16:00:00' && $End_Time <= '17:59:59'){
       $PhaseNumber='D-5';
     }
     else if ($End_Time >= '18:00:00' && $End_Time <= '19:59:59'){
       $PhaseNumber='D-6';
     }
     else if ($End_Time >= '20:00:00' && $End_Time <= '21:59:59'){
       $PhaseNumber='N-1';
     }
     else if ($End_Time >= '22:00:00' && $End_Time <= '23:59:59'){
       $PhaseNumber='N-2';
     }
     else if ($End_Time >= '00:00:00' && $End_Time <= '01:59:59'){
       $PhaseNumber='N-3';
     }
     else if ($End_Time >= '02:00:00' && $End_Time <= '03:59:59'){
       $PhaseNumber='N-4';
     }
     else if ($End_Time >= '04:00:00' && $End_Time <= '05:59:59'){
       $PhaseNumber='N-5';
     }
     else if ($End_Time >= '06:00:00' && $End_Time <= '07:59:59'){
       $PhaseNumber='N-6';
     }

     $SpecSearch_sql   = "SELECT * FROM modify_spec_plating WHERE PartNumber = '" . $PartNumber . "' ";
         //echo $SpecSearch_sql."</br>";
     $SpecSearch_query = mysqli_query($connect_spec, $SpecSearch_sql) or die("警告 ： 搜尋Modify Spec失敗");
     $id=0;
     while ($AllInfo = mysqli_fetch_array($SpecSearch_query)) {
      $Data_array       = array();
      $Project_Name       = $AllInfo['ProjectName'];
      $PartNumber       = $AllInfo['PartNumber'];
      $Rev              = $AllInfo['Rev'];
      $DimNO            = $AllInfo['DimNO'];
      $Remark           = strtoupper($AllInfo['Remark']);
      $DimSpec          = $AllInfo['DimSpec'];
      $DimUpper         = $AllInfo['DimUpper'];
      $DimLower         = $AllInfo['DimLower'];
      $Expect_UpperData = $AllInfo['Expect_UpperData'];
      $Expect_LowerData = $AllInfo['Expect_LowerData'];
      $Expect_UpperStdv = ($AllInfo['Expect_UpperStdv']);
      $Expect_LowerStdv = ($AllInfo['Expect_LowerStdv']);
      $Expect_Result    = $AllInfo['Expect_Result'];
      $Expect_Data      = rand($Expect_LowerData * 1000, $Expect_UpperData * 1000) / 1000;
      $Expect_Stdv      = rand($Expect_LowerStdv * 1000, $Expect_UpperStdv * 1000) / 1000;
//echo "PartNumber : ".$PartNumber."</br>";
//echo "DimNO : ".$DimNO."</br>";
//echo "Remark : ".$Remark."</br>";

      $A = 0;
      while ($A < 10) {
        $val = round(nrand($Expect_Data, $Expect_Stdv), 4);

        if ($Expect_Result == 'OK') {
          if ($val <= $DimUpper && $val >= $DimLower) {
            $Data_array[] = $val;
            $A++;
          }

        } else {
          $Data_array[] = $val;
          $A++;
        }
      }

//Insert優化數據
      $Measure_Data = $Data_array[0];
      if ($Measure_Data > $DimUpper or $Measure_Data < $DimLow) {
        $Measure_Result = 'NG';
      } else {
        $Measure_Result = 'OK';
      }

      if($Remark!='BIN'){
//Insert Modify System(Pin Dim)
      //Insert modify_pin_dim_data
        $InsertModifyData_sql   = "REPLACE INTO $Insert_db_name1 (`id`,`PartNumber`, `Type`, `day_night`, `Productline`, `Ticket_Number`, `Personnel_ID`, `Date`, `DateTime`, `FAINumber`, `spec`, `up_lim`, `down_lim`, `Measure_Value-1`, `Measure_Value-2`, `Measure_Value-3`) VALUES ('" . $id . "','" . $PartNumber . "', '" . $Type . "', '" . $day_night . "', '" . $Productline . "', '" . $Ticket_Number . "', '" . $Personnel_ID . "', '" . $Date . "', '" . $DateTime . "', '" . $DimNO . "', '" . $DimSpec . "', '" . $DimUpper . "', '" . $DimLower . "', '" . $Data_array[0] . "', '" . $Data_array[1] . "', '" . $Data_array[2] . "')";
              //echo $InsertModifyData_sql;
        $InsertModifyData_query = mysqli_query($connect_plating, $InsertModifyData_sql) or die("警告 " . $Ticket_Number . " : Modifydatas Plating(Pin Dim)數據上傳資料庫失敗");
        $id++;

       //Insert sq system(Pin Dim)
        $InsertSQModifyData_sql   = "REPLACE INTO `ipqc_modify_datas` (`MeasureDataTime`,`DateTime`, `PartNumber`, `PartDescription`, `LineNumber`, `PhaseNumber`, `TicketNumber`, `DimNO`, `MoldNumber`, `Type`, `CavNumber`, `Sample1`, `Sample2`, `Sample3`) VALUES ('" . $End_DateTime . "','" . $Date . "', '" . $PartNumber . "', '" . $Project_Name . "', '". $Productline ."', '" . $PhaseNumber . "', '" . $Ticket_Number . "', '" . $DimNO . "', 'NA', '" . $Type . "', 'NA', '" . $Data_array[0] . "', '" . $Data_array[1] . "', '" . $Data_array[2] . "')";
            //echo $InsertSQModifyData_sql;
        $InsertSQModifyData_query = mysqli_query($connect_sq, $InsertSQModifyData_sql) or die("警告 " . $Ticket_Number . " : Plating(Pin Dim) SQModifydatas數據上傳資料庫失敗");
      }
    }

  }

//Insert modify_pin_visual_data
  $VisualInsert_sql   = "REPLACE INTO modify_pin_visual_data SELECT * from pin_visual_data WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $SemInsert_sql;
  $VisualInsert_query = mysqli_query($connect_plating, $VisualInsert_sql) or die("警告 ： Insert modify_pin_visual_data");

  //Insert modify_pin_visual_data_4f
  $VisualInsert_4f_sql   = "REPLACE INTO modify_pin_visual_data_4f SELECT * from pin_visual_data_4f WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $SemInsert_sql;
  $VisualInsert_4f_query = mysqli_query($connect_plating, $VisualInsert_4f_sql) or die("警告 ： Insert modify_pin_visual_data_4f");

//Insert modify_pin_xray_data_4f
  $XRayInsert_4f_sql   = "REPLACE INTO modify_pin_xray_data_4f SELECT * from pin_xray_data_4f WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $SemInsert_sql;
  $XRayInsert_4f_query = mysqli_query($connect_plating, $XRayInsert_4f_sql) or die("警告 ： Insert modify_pin_xray_data_4f");

//Insert modify_pin_xray_data
  $XRayInsert_sql   = "REPLACE INTO modify_pin_xray_data SELECT * from pin_xray_data WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $SemInsert_sql;
  $XRayInsert_query = mysqli_query($connect_plating, $XRayInsert_sql) or die("警告 ： Insert modify_pin_xray_data");

//Insert modify_pin_sem_data
  $SemInsert_sql   = "REPLACE INTO modify_pin_sem_data SELECT * from pin_sem_data WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $filmetricsInsert_sql;
  $SemInsert_query = mysqli_query($connect_plating, $SemInsert_sql) or die("警告 ： Insert modify_pin_sem_data");  

//Insert modify_pin_roughness_data
  $RoughnessInsert_sql   = "REPLACE INTO modify_pin_roughness_data SELECT * from pin_roughness_data WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $filmetricsInsert_sql;
  $RoughnessInsert_query = mysqli_query($connect_plating, $RoughnessInsert_sql) or die("警告 ： Insert modify_pin_roughness_data"); 

//Insert modify_pin_roughness_data
  $PinDimInsert_sql   = "REPLACE INTO modify_pin_bin_dim_data SELECT * from pin_bin_dim_data WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $filmetricsInsert_sql;
  $PinDimInsert_query = mysqli_query($connect_plating, $PinDimInsert_sql) or die("警告 ： Insert modify_pin_bin_dim_data"); 

//Insert sq system(BIN Dim)
  $query_listout_S = "SELECT * FROM pin_bin_dim_data WHERE 1=1 AND PartNumber = '" . $PartNumber . "' order by id";
             //echo $query_listout_S;
  $listout_S       = mysqli_query($connect_plating, $query_listout_S) or die(mysqli_error());
  $id=0;
  while ($listoutS = mysqli_fetch_array($listout_S)) {
    $DateTime  = $listoutS['DateTime'];
    $Date = $listoutS['Date'];
    $Productline  = $listoutS['Productline'];
    $Ticket_Number = $listoutS['Ticket_Number'];
    $DimNO = $listoutS['FAI_Number'];
    $up_lim  = $listoutS['up_lim'];
    $down_lim = $listoutS['down_lim'];
    $up_lim_bin1_1   = $listoutS['up_lim_bin1-1'];
    $down_lim_bin1_1 = $listoutS['down_lim_bin1-1'];
    $up_lim_bin1_2  = $listoutS['up_lim_bin1-2'];
    $down_lim_bin1_2 = $listoutS['down_lim_bin1-2'];
    $up_lim_bin2_3   = $listoutS['up_lim_bin2-3'];
    $down_lim_bin2_3 = $listoutS['down_lim_bin2-3'];
    $up_lim_bin2_4  = $listoutS['up_lim_bin2-4'];
    $down_lim_bin2_4 = $listoutS['down_lim_bin2-4'];
    $up_lim_bin3_5   = $listoutS['up_lim_bin3-5'];
    $down_lim_bin3_5 = $listoutS['down_lim_bin3-5'];
    $up_lim_bin3_6  = $listoutS['up_lim_bin3-6'];
    $down_lim_bin3_6 = $listoutS['down_lim_bin3-6'];
    $Measure_Value1 = $listoutS['Measure_Value-1'];
    $Measure_Value2 = $listoutS['Measure_Value-2'];
    $Measure_Value3 = $listoutS['Measure_Value-3'];
    $Measure_Value4 = $listoutS['Measure_Value-4'];
    $Measure_Value5 = $listoutS['Measure_Value-5'];
    $Measure_Value6 = $listoutS['Measure_Value-6'];
    $Measure_Value7 = $listoutS['Measure_Value-7'];
    $Measure_Value8 = $listoutS['Measure_Value-8'];
    $Measure_Value9 = $listoutS['Measure_Value-9'];
    $Measure_Value10 = $listoutS['Measure_Value-10'];
    $Bin_Number = $listoutS['Bin_Number'];
    $Level_Number = $listoutS['Level_Number'];

    $End_Time=substr($DateTime,11,8);
    if ($End_Time >= '08:00:00' && $End_Time <= '09:59:59'){
     $PhaseNumber='D-1';
   }
   else if ($End_Time >= '10:00:00' && $End_Time <= '11:59:59'){
     $PhaseNumber='D-2';
   }
   else if ($End_Time >= '12:00:00' && $End_Time <= '13:59:59'){
     $PhaseNumber='D-3';
   }
   else if ($End_Time >= '14:00:00' && $End_Time <= '15:59:59'){
     $PhaseNumber='D-4';
   }
   else if ($End_Time >= '16:00:00' && $End_Time <= '17:59:59'){
     $PhaseNumber='D-5';
   }
   else if ($End_Time >= '18:00:00' && $End_Time <= '19:59:59'){
     $PhaseNumber='D-6';
   }
   else if ($End_Time >= '20:00:00' && $End_Time <= '21:59:59'){
     $PhaseNumber='N-1';
   }
   else if ($End_Time >= '22:00:00' && $End_Time <= '23:59:59'){
     $PhaseNumber='N-2';
   }
   else if ($End_Time >= '00:00:00' && $End_Time <= '01:59:59'){
     $PhaseNumber='N-3';
   }
   else if ($End_Time >= '02:00:00' && $End_Time <= '03:59:59'){
     $PhaseNumber='N-4';
   }
   else if ($End_Time >= '04:00:00' && $End_Time <= '05:59:59'){
     $PhaseNumber='N-5';
   }
   else if ($End_Time >= '06:00:00' && $End_Time <= '07:59:59'){
     $PhaseNumber='N-6';
   }


   $InsertSQModifyData_sql   = "REPLACE INTO `ipqc_modify_datas` (`MeasureDataTime`,`DateTime`, `PartNumber`, `PartDescription`, `LineNumber`, `PhaseNumber`, `TicketNumber`, `DimNO`, `MoldNumber`, `Type`, `CavNumber`, `Sample1`, `Sample2`, `Sample3`, `Sample4`, `Sample5`, `Sample6`, `Sample7`, `Sample8`, `Sample9`, `Sample10`, `Bin_Number`, `Level_Number`) VALUES ('" . $DateTime . "','" . $Date . "', '" . $PartNumber . "', '" . $Project_Name . "', '". $Productline ."', '" . $PhaseNumber . "', '" . $Ticket_Number . "', '" . $DimNO . "', 'NA', '" . $Type . "', 'NA', '" . $Measure_Value1 . "', '" . $Measure_Value2 . "', '" . $Measure_Value3 . "', '" . $Measure_Value4 . "', '" . $Measure_Value5 . "', '" . $Measure_Value6 . "', '" . $Measure_Value7 . "', '" . $Measure_Value8 . "', '" . $Measure_Value9 . "', '" . $Measure_Value10 . "', '" . $Bin_Number . "', '" . $Level_Number . "')";
   //echo $InsertSQModifyData_sql;
   $InsertSQModifyData_query = mysqli_query($connect_sq, $InsertSQModifyData_sql) or die("警告 " . $Ticket_Number . " : Plating(Pin Bin Dim) SQModifydatas數據上傳資料庫失敗");
 } 
}
}
echo "【 優化數據上傳資料庫成功 】</br>";
$end_time = date("H")*3600+date("i")*60+date("s");

//echo "start_time：".$start_time."秒<br/>";
//echo "end_time：".$end_time."秒<br/>";
echo "報告生成日期:".$NowDate."<br/>";
$time_total = $end_time - $start_time;
echo "執行了：".$time_total."秒<br/>";
