<?php
set_time_limit(0);
session_start();
error_reporting(0);
$start_time = date("H")*3600+date("i")*60+date("s");

require_once '../../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

function nrand($mean, $sd)
{
  $x = mt_rand() / mt_getrandmax();
  $y = mt_rand() / mt_getrandmax();
  return sqrt(-2 * log($x)) * cos(2 * pi() * $y) * $sd + $mean;
}

if (!isset($_SESSION)) {session_start();}
$Ticket_Number_array0 = $Ticket_Number_array1 = $PN_MN_array = array();
$NowDate              = date('Y-m-d');
$NowDate              = '2021-09-03';
$d1                   = date('Y-m-d', strtotime($NowDate) - 60 * 60 * 24 * 1) . " 08:00:00";
$d2                   = $NowDate . " 07:59:59";

mysqli_select_db($connect_spec, $database_spec);
mysqli_select_db($connect_plating, $database_plating);
mysqli_select_db($connect_sq, $database_sq);

//Select part Number from Spec db
$PNSearch_sql = "SELECT * FROM modify_spec_plating WHERE 1=1 group by PartNumber order by PartNumber";

$PNSearch_query = mysqli_query($connect_spec, $PNSearch_sql) or die("警告 ： 搜尋Part Number失敗");
while ($PNSearch = mysqli_fetch_assoc($PNSearch_query)) {
  $PartNumber    = $PNSearch['PartNumber'];
  $M_Type    = substr($PartNumber,3,1);

  if($M_Type=='2' or $M_Type=='3' or $M_Type=='4'){
    $Search_db_name='metal_visual_data';
    $Insert_db_name='modify_metal_plating_data';
    $Type='Plating';//電鍍

//Insert modify_metal_plating_data
    $PlatingInsert_sql   = "REPLACE INTO modify_metal_plating_data SELECT * from metal_plating_data WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $SemInsert_sql;
    $PlatingInsert_query = mysqli_query($connect_plating, $PlatingInsert_sql) or die("警告 ： Insert modify_metal_plating_data");

//Insert modify_metal_visual_data
    $VisualInsert_sql   = "REPLACE INTO modify_metal_visual_data SELECT * from metal_visual_data WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $SemInsert_sql;
    $VisualInsert_query = mysqli_query($connect_plating, $VisualInsert_sql) or die("警告 ： Insert modify_pin_visual_data");

//Insert modify_metal_roughness_data
    $RoughnessInsert_sql   = "REPLACE INTO modify_metal_roughness_data SELECT * from metal_roughness_data WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' "; 
//echo $filmetricsInsert_sql;
    $RoughnessInsert_query = mysqli_query($connect_plating, $RoughnessInsert_sql) or die("警告 ： Insert modify_pin_roughness_data"); 

  }
}
echo "【 優化數據上傳資料庫成功 】</br>";
$end_time = date("H")*3600+date("i")*60+date("s");

//echo "start_time：".$start_time."秒<br/>";
//echo "end_time：".$end_time."秒<br/>";
echo "報告生成日期:".$NowDate."<br/>";
$time_total = $end_time - $start_time;
echo "執行了：".$time_total."秒<br/>";
