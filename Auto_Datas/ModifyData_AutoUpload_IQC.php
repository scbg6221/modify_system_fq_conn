<?php
set_time_limit(0);
session_start();
error_reporting(0);
$start_time = date("H")*3600+date("i")*60+date("s");

require_once '../../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

function nrand($mean, $sd)
{
  $x = mt_rand() / mt_getrandmax();
  $y = mt_rand() / mt_getrandmax();
  return sqrt(-2 * log($x)) * cos(2 * pi() * $y) * $sd + $mean;
}

if (!isset($_SESSION)) {session_start();}
$Ticket_Number_array0 = $Ticket_Number_array1 = $PN_MN_array = array();
$NowDate              = date('Y-m-d');
$d1                   = date('Y-m-d', strtotime($NowDate) - 60 * 60 * 24 * 1) . "-08:00:00";
$d2                   = $NowDate . "-07:59:59";

mysqli_select_db($connect_spec, $database_spec);
mysqli_select_db($connect_iqc, $database_iqc);
mysqli_select_db($connect_sq, $database_sq);

//Select part Number from Spec db
$PNSearch_sql = "SELECT * FROM modify_spec_iqc WHERE 1=1 group by PartNumber order by PartNumber";

$PNSearch_query = mysqli_query($connect_spec, $PNSearch_sql) or die("警告 ： 搜尋Part Number失敗");
while ($PNSearch = mysqli_fetch_assoc($PNSearch_query)) {
  $ProjectName_S    = $PNSearch['ProjectName'];
  $PartNumber    = $PNSearch['PartNumber'];

//delect 既有的數據
  $AllCTDelete_sql   = "SELECT Ticket_Number FROM modify_main_table WHERE 1=1 AND Part_Number = '$PartNumber' AND Date_Time between '$d1' AND '$d2' ORDER BY Date_Time";
  $AllCTDelete_query = mysqli_query($connect_iqc, $AllCTDelete_sql) or die("警告 ： 搜尋modify main table數據失敗");

  while ($AllCTDelete = mysqli_fetch_assoc($AllCTDelete_query)) {
    $Ticket_Number = $AllCTDelete['Ticket_Number'];

    $DeleteContents_sql   = "DELETE FROM modify_main_table WHERE 1=1 AND Ticket_Number = '$Ticket_Number'";
    $DeleteContents_query = mysqli_query($connect_iqc, $DeleteContents_sql) or die("警告 ： 刪除main table數據失敗");
    $DeleteDatas_sql      = "DELETE FROM modify_measure_data WHERE 1=1 AND Ticket_Number = '$Ticket_Number'";
    $DeleteDatas_query    = mysqli_query($connect_iqc, $DeleteDatas_sql) or die("警告 ： 刪除measuredatas數據失敗");
  }

//Insert Measure Datas
  $AllTNSearch_sql   = "SELECT * FROM visual_inspection WHERE 1=1 AND Part_Number = '$PartNumber' AND `DateTime` between '$d1' AND '$d2' ORDER BY `DateTime`";
  $AllTNSearch_query = mysqli_query($connect_iqc, $AllTNSearch_sql) or die("警告 ： 搜尋Ticket Number失敗");

  while ($AllTNSearch = mysqli_fetch_assoc($AllTNSearch_query)) {
        //$Project_Name_m           = $AllTNSearch['Project_Name'];
    $Part_Number          = $AllTNSearch['Part_Number'];
    $Mold_Number            = $AllTNSearch['Mold_Number'];
    $Ticket_Number          = $AllTNSearch['Ticket_Number'];
    $Date             = $AllTNSearch['Date'];
    $Time             = $AllTNSearch['Time'];
    $MeasureDataTime               = $AllTNSearch['DateTime'];
    $Remark                 = $AllTNSearch['Remark'];

        //for Insert sq system
    if ($Time >= '00:00:00' && $Time <= '07:59:59') {
      $DateTime = date('Y-m-d', strtotime($Date) - 60 * 60 * 24);
    } else {
      $DateTime = $Date;
    }
    if ($Time >= '08:00:00' && $Time <= '09:59:59'){
     $PhaseNumber='D-1';
   }
   else if ($Time >= '10:00:00' && $Time <= '11:59:59'){
     $PhaseNumber='D-2';
   }
   else if ($Time >= '12:00:00' && $Time <= '13:59:59'){
     $PhaseNumber='D-3';
   }
   else if ($Time >= '14:00:00' && $Time <= '15:59:59'){
     $PhaseNumber='D-4';
   }
   else if ($Time >= '16:00:00' && $Time <= '17:59:59'){
     $PhaseNumber='D-5';
   }
   else if ($Time >= '18:00:00' && $Time <= '19:59:59'){
     $PhaseNumber='D-6';
   }
   else if ($Time >= '20:00:00' && $Time <= '21:59:59'){
     $PhaseNumber='N-1';
   }
   else if ($Time >= '22:00:00' && $Time <= '23:59:59'){
     $PhaseNumber='N-2';
   }
   else if ($Time >= '00:00:00' && $Time <= '01:59:59'){
     $PhaseNumber='N-3';
   }
   else if ($Time >= '02:00:00' && $Time <= '03:59:59'){
     $PhaseNumber='N-4';
   }
   else if ($Time >= '04:00:00' && $Time <= '05:59:59'){
     $PhaseNumber='N-5';
   }
   else if ($Time >= '06:00:00' && $Time <= '07:59:59'){
     $PhaseNumber='N-6';
   }

//Insert Main Table數據
   $Insert_maintable = "REPLACE INTO `modify_main_table` (`Project_Name`, `Part_Number`, `Mold_Number`, `Ticket_Number`, `Date`, `Time`, `Date_Time`) VALUES ('" . $ProjectName_S . "', '" . $Part_Number . "', '" . $Mold_Number . "', '" . $Ticket_Number . "', '" . $Date . "', '" . $Time . "', '" . $MeasureDataTime . "')";
   $query            = mysqli_query($connect_iqc, $Insert_maintable) or die("警告 " . $Ticket_Number . " : Modify_Main_table上傳資料庫失敗");

   $SpecSearch_sql   = "SELECT * FROM modify_spec_iqc WHERE PartNumber = '" . $PartNumber . "' ";
   $SpecSearch_query = mysqli_query($connect_spec, $SpecSearch_sql) or die("警告 ： 搜尋Modify Spec失敗");
   while ($AllInfo = mysqli_fetch_array($SpecSearch_query)) {
    $Data_array       = array();
            //$Spec_Project_Name       = $AllInfo['ProjectName'];
    $PartNumber       = $AllInfo['PartNumber'];
    $Rev              = $AllInfo['Rev'];
    $DimNO            = $AllInfo['DimNO'];
    $Remark           = strtoupper($AllInfo['Remark']);
    $DimSpec          = $AllInfo['DimSpec'];
    $DimUpper         = $AllInfo['DimUpper'];
    $DimLower         = $AllInfo['DimLower'];
    $PPKSpec         = $AllInfo['PPKSpec'];
    $Expect_UpperData = $AllInfo['Expect_UpperData'];
    $Expect_LowerData = $AllInfo['Expect_LowerData'];
    $Expect_UpperStdv = ($AllInfo['Expect_UpperStdv']);
    $Expect_LowerStdv = ($AllInfo['Expect_LowerStdv']);
    $Expect_Result    = $AllInfo['Expect_Result'];
    $Expect_Data      = rand($Expect_LowerData * 1000, $Expect_UpperData * 1000) / 1000;
    $Expect_Stdv      = rand($Expect_LowerStdv * 1000, $Expect_UpperStdv * 1000) / 1000;

    $A = 0;
    while ($A < 32) {
      $val = round(nrand($Expect_Data, $Expect_Stdv), 4);

      if ($Expect_Result == 'OK') {
        if ($val <= $DimUpper && $val >= $DimLower) {
          $Data_array[] = $val;
          $A++;
        }

      } else {
        $Data_array[] = $val;
        $A++;
      }
    }

    //Insert CPK優化數據
    if ($Remark == 'CPK') {
      $B = 0;
      for ($i = 1; $i <= 32; $i++) {
        $Measure_Data = $Data_array[$B];
        if ($Measure_Data > $DimUpper or $Measure_Data < $DimLow) {
          $Measure_Result = 'NG';
        } else {
          $Measure_Result = 'OK';
        }

        $InsertModifyData_sql   = "REPLACE INTO `modify_measure_data` (`Ticket_Number`, `DimNO`, `DimNOOrder`, `DimSpec`, `DimUpper`, `DimLow`, `PPKSpec`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `Sample1`, `Sample2`, `Sample3`, `Sample4`, `Sample5`, `Sample6`, `Sample7`, `Sample8`, `Sample9`, `Sample10`, `Sample11`, `Sample12`, `Sample13`, `Sample14`, `Sample15`, `Sample16`, `Sample17`, `Sample18`, `Sample19`, `Sample20`, `Sample21`, `Sample22`, `Sample23`, `Sample24`, `Sample25`, `Sample26`, `Sample27`, `Sample28`, `Sample29`, `Sample30`, `Sample31`, `Sample32`, `Measure_Result`, `Remark`) VALUES ('" . $Ticket_Number . "', '" . $DimNO . "', '" . $DimNO . "', '" . $DimSpec . "', '" . $DimUpper . "', '" . $DimLower . "', '" . $PPKSpec . "', '" . $Expect_UpperData . "','" . $Expect_LowerData . "', '" . $Expect_UpperStdv . "', '" . $Expect_LowerStdv . "', '" . $Expect_Result . "', '" . $Data_array[0] . "', '" . $Data_array[1] . "', '" . $Data_array[2] . "', '" . $Data_array[3] . "', '" . $Data_array[4] . "', '" . $Data_array[5] . "', '" . $Data_array[6] . "', '" . $Data_array[7] . "', '" . $Data_array[8] . "', '" . $Data_array[9] . "', '" . $Data_array[10] . "', '" . $Data_array[11] . "', '" . $Data_array[12] . "', '" . $Data_array[13] . "', '" . $Data_array[14] . "', '" . $Data_array[15] . "', '" . $Data_array[16] . "', '" . $Data_array[17] . "', '" . $Data_array[18] . "', '" . $Data_array[19] . "', '" . $Data_array[20] . "', '" . $Data_array[21] . "', '" . $Data_array[22] . "', '" . $Data_array[23] . "', '" . $Data_array[24] . "', '" . $Data_array[25] . "', '" . $Data_array[26] . "', '" . $Data_array[27] . "', '" . $Data_array[28] . "', '" . $Data_array[29] . "', '" . $Data_array[30] . "', '" . $Data_array[31] . "', '" . $Measure_Result . "', '" . $Remark . "')";
        //echo $InsertModifyData_sql;
        $InsertModifyData_query = mysqli_query($connect_iqc, $InsertModifyData_sql) or die("警告 " . $Ticket_Number . " : ModifydCPKatas數據上傳資料庫失敗");
        $B++;
      }
      
      //Insert sq system
      $InsertSQModifyData_sql   = "REPLACE INTO `ipqc_modify_datas` (`MeasureDataTime`,`DateTime`, `PartNumber`, `PartDescription`, `LineNumber`, `PhaseNumber`, `TicketNumber`, `DimNO`, `MoldNumber`, `Type`, `CavNumber`, `Sample1`, `Sample2`, `Sample3`, `Sample4`, `Sample5`, `Sample6`, `Sample7`, `Sample8`, `Sample9`, `Sample10`, `Sample11`, `Sample12`, `Sample13`, `Sample14`, `Sample15`, `Sample16`, `Sample17`, `Sample18`, `Sample19`, `Sample20`, `Sample21`, `Sample22`, `Sample23`, `Sample24`, `Sample25`, `Sample26`, `Sample27`, `Sample28`, `Sample29`, `Sample30`, `Sample31`, `Sample32`, `Bin_Number`, `Level_Number`) VALUES ('" . $MeasureDataTime . "','" . $DateTime . "', '" . $PartNumber . "', '" . $ProjectName_S . "', 'NA', '" . $PhaseNumber . "', '" . $Ticket_Number . "', '" . $DimNO . "', '" . $Mold_Number . "', 'IQC', 'NA', '" . $Data_array[0] . "', '" . $Data_array[1] . "', '" . $Data_array[2] . "', '" . $Data_array[3] . "', '" . $Data_array[4] . "', '" . $Data_array[5] . "', '" . $Data_array[6] . "', '" . $Data_array[7] . "', '" . $Data_array[8] . "', '" . $Data_array[9] . "', '" . $Data_array[10] . "', '" . $Data_array[11] . "', '" . $Data_array[12] . "', '" . $Data_array[13] . "', '" . $Data_array[14] . "', '" . $Data_array[15] . "', '" . $Data_array[16] . "', '" . $Data_array[17] . "', '" . $Data_array[18] . "', '" . $Data_array[19] . "', '" . $Data_array[20] . "', '" . $Data_array[21] . "', '" . $Data_array[22] . "', '" . $Data_array[23] . "', '" . $Data_array[24] . "', '" . $Data_array[25] . "', '" . $Data_array[26] . "', '" . $Data_array[27] . "', '" . $Data_array[28] . "', '" . $Data_array[29] . "', '" . $Data_array[30] . "', '" . $Data_array[31] . "','NA','NA')";
      $InsertSQModifyData_query = mysqli_query($connect_sq, $InsertSQModifyData_sql) or die("警告 " . $Ticket_Number . " : SQModifyCPKdatas數據上傳資料庫失敗");
    } 
    else {

//Insert優化數據
      $B = 0;
      for ($i = 1; $i <= 3; $i++) {
        $Measure_Data = $Data_array[$B];
        if ($Measure_Data > $DimUpper or $Measure_Data < $DimLow) {
          $Measure_Result = 'NG';
        } else {
          $Measure_Result = 'OK';
        }

        $InsertModifyData_sql   = "REPLACE INTO `modify_measure_data` (`Ticket_Number`, `DimNO`, `DimNOOrder`, `DimSpec`, `DimUpper`, `DimLow`, `PPKSpec`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `Sample1`, `Sample2`, `Sample3`) VALUES ('" . $Ticket_Number . "', '" . $DimNO . "', '" . $DimNO . "', '" . $DimSpec . "', '" . $DimUpper . "', '" . $DimLower . "', '" . $PPKSpec . "', '" . $Expect_UpperData . "','" . $Expect_LowerData . "', '" . $Expect_UpperStdv . "', '" . $Expect_LowerStdv . "', '" . $Expect_Result . "', '" . $Data_array[0] . "', '" . $Data_array[1] . "', '" . $Data_array[2] . "', '" . $Measure_Result . "', '" . $Remark . "')";

        $InsertModifyData_query = mysqli_query($connect_iqc, $InsertModifyData_sql) or die("警告 " . $Ticket_Number . " : Modifydatas數據上傳資料庫失敗");
        $B++;
      }
      
      //Insert sq system
      $InsertSQModifyData_sql   = "REPLACE INTO `ipqc_modify_datas` (`MeasureDataTime`,`DateTime`, `PartNumber`, `PartDescription`, `LineNumber`, `PhaseNumber`, `TicketNumber`, `DimNO`, `MoldNumber`, `Type`, `CavNumber`, `Sample1`, `Sample2`, `Sample3`, `Bin_Number`, `Level_Number`) VALUES ('" . $MeasureDataTime . "','" . $DateTime . "', '" . $PartNumber . "', '" . $ProjectName_S . "', 'NA', '" . $PhaseNumber . "', '" . $Ticket_Number . "', '" . $DimNO . "', '" . $Mold_Number . "', 'IQC', 'NA', '" . $Data_array[0] . "', '" . $Data_array[1] . "', '" . $Data_array[2] . "','NA','NA')";
      $InsertSQModifyData_query = mysqli_query($connect_sq, $InsertSQModifyData_sql) or die("警告 " . $Ticket_Number . " : SQModifydatas數據上傳資料庫失敗");
    }
  }
}

//Insert modify_other_measuredata
    $OtherDataInsert_sql   = "REPLACE INTO modify_other_measuredata SELECT * from other_measuredata WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $OtherDataInsert_sql;
    $OtherDataInsert_query = mysqli_query($connect_iqc, $OtherDataInsert_sql) or die("警告 ： Insert modify_other_measuredata失敗");

    //Insert modify_visual_inspection
    $VisualDataInsert_sql   = "REPLACE INTO modify_visual_inspection SELECT * from visual_inspection WHERE 1=1 AND Part_Number = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $VisualDataInsert_sql;
    $VisualDataInsert_query = mysqli_query($connect_iqc, $VisualDataInsert_sql) or die("警告 ： Insert modify_visual_inspection失敗");
    
}

echo "【 優化數據上傳資料庫成功 】</br>";
$end_time = date("H")*3600+date("i")*60+date("s");

//echo "start_time：".$start_time."秒<br/>";
//echo "end_time：".$end_time."秒<br/>";
echo "報告生成日期:".$NowDate."<br/>";
$time_total = $end_time - $start_time;
echo "執行了：".$time_total."秒<br/>";
