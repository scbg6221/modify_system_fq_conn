<?php
set_time_limit(0);
session_start();
error_reporting(0);
$start_time = date("H")*3600+date("i")*60+date("s");

require_once '../../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

function nrand($mean, $sd)
{
    $x = mt_rand() / mt_getrandmax();
    $y = mt_rand() / mt_getrandmax();
    return sqrt(-2 * log($x)) * cos(2 * pi() * $y) * $sd + $mean;
}

if (!isset($_SESSION)) {session_start();}
$Ticket_Number_array0 = $Ticket_Number_array1 = $PN_MN_array = array();

$NowDate              = date('Y-m-d');
$MeasureDate              = date('Y-m-d', strtotime($NowDate) - 60 * 60 * 24 * 1) ;
$d1                   = date('Y-m-d', strtotime($NowDate) - 60 * 60 * 24 * 1) . "-08:00:00";
$d2                   = $NowDate . "-07:59:59";

mysqli_select_db($connect_spec, $database_spec);
mysqli_select_db($connect_mold1, $database_mold1);
mysqli_select_db($connect_mold2, $database_mold2);
mysqli_select_db($connect_sq, $database_sq);

//Select part Number from Spec db
$PNSearch_sql = "SELECT * FROM modify_spec_molding WHERE 1=1 group by PartNumber,MoldNumber order by PartNumber,MoldNumber";

$PNSearch_query = mysqli_query($connect_spec, $PNSearch_sql) or die("警告 ： 搜尋Part Number失敗");
while ($PNSearch = mysqli_fetch_assoc($PNSearch_query)) {
    $CavNums           = $PNSearch['CavNums'];
    $ProjectName_S    = $PNSearch['ProjectName'];
    $PartNumber        = $PNSearch['PartNumber'];
    $MoldNumber        = $PNSearch['MoldNumber'];
    $Measuredata_Table = "modify_m_measure_data_" . $CavNums . "cav";

    //delect 既有的數據
    $AllCTDelete_sql   = "SELECT * FROM modify_m_main_table WHERE 1=1 AND Part_Number_V = '$PartNumber' AND Mold_Number = '$MoldNumber' AND concat(End_Date,'-',End_Time) between '$d1' AND '$d2' ORDER BY End_Date,End_Time";
    $AllCTDelete_query = mysqli_query($connect_mold1, $AllCTDelete_sql) or die("警告 ： 搜尋main table數據失敗");

    $DeleteVisuals_sql   = "DELETE FROM modify_visual_inspection WHERE 1=1 AND Part_Number = '$PartNumber' AND Mold_Number = '$MoldNumber' AND concat(Date,'-',Time) between '$d1' AND '$d2' ";
    $DeleteVisuals_query = mysqli_query($connect_mold1, $DeleteVisuals_sql) or die("警告 ： 刪除Visual數據失敗");
    $DeleteSQ_sql   = "DELETE FROM ipqc_modify_datas WHERE 1=1 AND PartNumber = '$PartNumber' AND MoldNumber = '$MoldNumber' AND MeasureDataTime between '$d1' AND '$d2' ";
    $DeleteSQ_query = mysqli_query($connect_sq, $DeleteSQ_sql) or die("警告 ： 刪除SQ數據失敗");

    while ($AllCTDelete = mysqli_fetch_assoc($AllCTDelete_query)) {
        $Ticket_Number = $AllCTDelete['Ticket_Number'];

        $DeleteContents_sql   = "DELETE FROM modify_m_main_table WHERE 1=1 AND Ticket_Number = '$Ticket_Number'";
        $DeleteContents_query = mysqli_query($connect_mold1, $DeleteContents_sql) or die("警告 ： 刪除main table數據失敗");
        $DeleteDatas_sql      = "DELETE FROM " . $Measuredata_Table . " WHERE 1=1 AND Ticket_Number = '$Ticket_Number'";
        $DeleteDatas_query    = mysqli_query($connect_mold1, $DeleteDatas_sql) or die("警告 ： 刪除measuredatas數據失敗");
    }

//Insert Visual Datas
    $VisaulSearch_sql   = "SELECT * FROM visual_inspection WHERE 1=1 AND Part_Number = '$PartNumber' AND Mold_Number = '$MoldNumber' AND concat(Date,'-',Time) between '$d1' AND '$d2' group by Part_Number,Mold_Number,Ticket_Number ORDER BY concat(Date,'-',Time)";
    $VisualSearch_query = mysqli_query($connect_mold1, $VisaulSearch_sql) or die("警告 ： 搜尋Visual Datas失敗");
    while ($VisualSearch = mysqli_fetch_assoc($VisualSearch_query)) {

        $Inspector_ID           = $VisualSearch['Inspector_ID'];
        $Ticket_Number          = $VisualSearch['Ticket_Number'];
        $Ticket_Number_array0[] = $VisualSearch['Ticket_Number'];
        $Part_Number            = $VisualSearch['Part_Number'];
        $Version                = $VisualSearch['Version'];
        $Mold_Number            = $VisualSearch['Mold_Number'];
        $Date                   = $VisualSearch['Date'];
        $Time                   = $VisualSearch['Time'];
        $Inspect_Status         = $VisualSearch['Inspect_Status'];
        $Component_Item_Desc    = $VisualSearch['Component_Item_Desc'];
        $Component_Item_NO      = $VisualSearch['Component_Item_NO'];
        $Material_Num           = $VisualSearch['Material_Num'];
        $Prod_Line_Code         = $VisualSearch['Prod_Line_Code'];
        $I1                     = $VisualSearch['I1'];
        $I2                     = $VisualSearch['I2'];
        $I3                     = $VisualSearch['I3'];
        $I4                     = $VisualSearch['I4'];
        $I5                     = $VisualSearch['I5'];
        $I6                     = $VisualSearch['I6'];
        $I7                     = $VisualSearch['I7'];
        $I8                     = $VisualSearch['I8'];
        $I9                     = $VisualSearch['I9'];
        $I10                    = $VisualSearch['I10'];
        $I11                    = $VisualSearch['I11'];
        $I12                    = $VisualSearch['I12'];
        $I13                    = $VisualSearch['I13'];
        $I14                    = $VisualSearch['I14'];
        $I15                    = $VisualSearch['I15'];
        $I16                    = $VisualSearch['I16'];
        $I17                    = $VisualSearch['I17'];
        $I18                    = $VisualSearch['I18'];
        $I19                    = $VisualSearch['I19'];
        $I20                    = $VisualSearch['I20'];
        $I21                    = $VisualSearch['I21'];
        $I22                    = $VisualSearch['I22'];
        $I23                    = $VisualSearch['I23'];
        $I24                    = $VisualSearch['I24'];
        $I25                    = $VisualSearch['I25'];
        $Result                 = $VisualSearch['Result'];
        $Remark                 = $VisualSearch['Remark'];
        $InsertTime             = $VisualSearch['InsertTime'];

        $Insert_Visualtable_sql   = "REPLACE INTO `modify_visual_inspection` (`Inspector_ID`, `Ticket_Number`, `Part_Number`, `Version`, `Mold_Number`, `Date`, `Time`, `Inspect_Status`, `Component_Item_Desc`, `Component_Item_NO`, `Material_Num`, `Prod_Line_Code`, `I1`, `I2`, `I3`, `I4`, `I5`, `I6`, `I7`, `I8`, `I9`, `I10`, `I11`, `I12`, `I13`, `I14`, `I15`, `I16`, `I17`, `I18`, `I19`, `I20`, `I21`, `I22`, `I23`, `I24`, `I25`, `Result`, `Remark`, `InsertTime`) VALUES ('" . $Inspector_ID . "','" . $Ticket_Number . "','" . $Part_Number . "','" . $Version . "','" . $Mold_Number . "','" . $Date . "','" . $Time . "','" . $Inspect_Status . "','" . $Component_Item_Desc . "','" . $Component_Item_NO . "','" . $Material_Num . "','" . $Prod_Line_Code . "','" . $I1 . "','" . $I2 . "','" . $I3 . "','" . $I4 . "','" . $I5 . "','" . $I6 . "','" . $I7 . "','" . $I8 . "','" . $I9 . "','" . $I10 . "','" . $I11 . "','" . $I12 . "','" . $I13 . "','" . $I14 . "','" . $I15 . "','" . $I16 . "','" . $I17 . "','" . $I18 . "','" . $I19 . "','" . $I20 . "','" . $I21 . "','" . $I22 . "','" . $I23 . "','" . $I24 . "','" . $I25 . "','" . $Result . "','" . $Remark . "','" . $InsertTime . "')";
        $Insert_Visualtable_query = mysqli_query($connect_mold1, $Insert_Visualtable_sql) or die("警告 " . $Ticket_Number . " :  輸入modify_visual_inspection資料庫失敗");

    }

//Insert Measure Datas
    $AllTNSearch_sql = "SELECT * FROM m_main_table WHERE 1=1 AND Part_Number_V LIKE '$PartNumber%' AND Mold_Number = '$MoldNumber' AND concat(End_Date,'-',End_Time) between '$d1' AND '$d2' ORDER BY concat(End_Date,'-',End_Time)";
//echo $AllTNSearch_sql;
    $AllTNSearch_query = mysqli_query($connect_mold1, $AllTNSearch_sql) or die("警告 ： 搜尋Ticket Number失敗");
    while ($AllTNSearch = mysqli_fetch_assoc($AllTNSearch_query)) {

        $Project_Name_m           = $AllTNSearch['Project_Name'];
        $Mold_Number            = $AllTNSearch['Mold_Number'];
        $Ticket_Number          = $AllTNSearch['Ticket_Number'];
        $Ticket_Number_array1[] = $AllTNSearch['Ticket_Number'];
        $Start_Date             = $AllTNSearch['Start_Date'];
        $Start_Time             = $AllTNSearch['Start_Time'];
        $End_Date               = $AllTNSearch['End_Date'];
        $End_Time               = $AllTNSearch['End_Time'];
        $End_DateTime           = $End_Date . " " . $End_Time;
        $Status                 = $AllTNSearch['Status'];
        $Remark                 = $AllTNSearch['Remark'];
        $Spec_Status            = $AllTNSearch['Spec_Status'];
        $magazine               = $AllTNSearch['magazine'];
        $Quantity               = $AllTNSearch['Quantity'];
        $Sample_Amount          = $AllTNSearch['Sample_Amount'];

        if ($End_Time >= '00:00:00' && $End_Time <= '07:59:59') {
            $DateTime = date('Y-m-d', strtotime($End_Date) - 60 * 60 * 24);
        } else {
            $DateTime = $End_Date;
        }
        if ($End_Time >= '08:00:00' && $End_Time <= '09:59:59') {
            $PhaseNumber = 'D-1';
        } else if ($End_Time >= '10:00:00' && $End_Time <= '11:59:59') {
            $PhaseNumber = 'D-2';
        } else if ($End_Time >= '12:00:00' && $End_Time <= '13:59:59') {
            $PhaseNumber = 'D-3';
        } else if ($End_Time >= '14:00:00' && $End_Time <= '15:59:59') {
            $PhaseNumber = 'D-4';
        } else if ($End_Time >= '16:00:00' && $End_Time <= '17:59:59') {
            $PhaseNumber = 'D-5';
        } else if ($End_Time >= '18:00:00' && $End_Time <= '19:59:59') {
            $PhaseNumber = 'D-6';
        } else if ($End_Time >= '20:00:00' && $End_Time <= '21:59:59') {
            $PhaseNumber = 'N-1';
        } else if ($End_Time >= '22:00:00' && $End_Time <= '23:59:59') {
            $PhaseNumber = 'N-2';
        } else if ($End_Time >= '00:00:00' && $End_Time <= '01:59:59') {
            $PhaseNumber = 'N-3';
        } else if ($End_Time >= '02:00:00' && $End_Time <= '03:59:59') {
            $PhaseNumber = 'N-4';
        } else if ($End_Time >= '04:00:00' && $End_Time <= '05:59:59') {
            $PhaseNumber = 'N-5';
        } else if ($End_Time >= '06:00:00' && $End_Time <= '07:59:59') {
            $PhaseNumber = 'N-6';
        }

//Insert Main Table數據
        $Insert_maintable = "REPLACE INTO `modify_m_main_table` (`Project_Name`, `Part_Number_V`, `Mold_Number`, `Ticket_Number`, `Cavity`, `Start_Date`, `Start_Time`, `End_Date`, `End_Time`, `Status`, `Remark`, `Spec_Status`, `magazine`, `Quantity`, `Sample_Amount`) VALUES ('" . $ProjectName_S . "', '" . $PartNumber . "', '" . $Mold_Number . "', '" . $Ticket_Number . "', '" . $CavNums . "', '" . $Start_Date . "', '" . $Start_Time . "', '" . $End_Date . "', '" . $End_Time . "', '" . $Status . "', '" . $Remark . "', '" . $Spec_Status . "', '" . $magazine . "', '" . $Quantity . "', '" . $Sample_Amount . "')";
        $query            = mysqli_query($connect_mold1, $Insert_maintable) or die("警告 " . $Ticket_Number . " Modify_Main_table上傳資料庫失敗");

        $SpecSearch_sql   = "SELECT * FROM modify_spec_molding WHERE PartNumber = '" . $PartNumber . "' AND MoldNumber = '$MoldNumber' ";
        $SpecSearch_query = mysqli_query($connect_spec, $SpecSearch_sql) or die("警告 ： 搜尋Modify Spec失敗");
        while ($AllInfo = mysqli_fetch_array($SpecSearch_query)) {
            $Data_array       = array();
            $Spec_Project_Name        = $AllInfo['ProjectName'];
            $CavNumber        = $AllInfo['CavNumber'];
            $Rev              = $AllInfo['Rev'];
            $DimNO            = $AllInfo['DimNO'];
            $CavNums          = $AllInfo['CavNums'];
            $Remark           = strtoupper($AllInfo['Remark']);
            $DimSpec          = $AllInfo['DimSpec'];
            $DimUpper         = $AllInfo['DimUpper'];
            $DimLower         = $AllInfo['DimLower'];
            $Expect_UpperData = $AllInfo['Expect_UpperData'];
            $Expect_LowerData = $AllInfo['Expect_LowerData'];
            $Expect_UpperStdv = ($AllInfo['Expect_UpperStdv']);
            $Expect_LowerStdv = ($AllInfo['Expect_LowerStdv']);
            $Expect_Result    = $AllInfo['Expect_Result'];
            $Measuredata_Table = "modify_m_measure_data_" . $CavNums . "cav";
            $Expect_Data = rand($Expect_LowerData * 10000, $Expect_UpperData * 10000) / 10000;
            $Expect_Stdv = rand($Expect_LowerStdv * 10000, $Expect_UpperStdv * 10000) / 10000;

            $A = 0;
            while ($A < 6) {
                $val = round(nrand($Expect_Data, $Expect_Stdv), 4);

                if ($Expect_Result == 'OK') {
                    if ($val <= $DimUpper && $val >= $DimLower) {
                        $Data_array[] = $val;
                        $A++;
                    }

                } else {
                    $Data_array[] = $val;
                    $A++;
                }
            }

//Insert CPK優化數據
            if ($Remark == 'CPK') {
                $B = 0;
                for ($j = 1; $j <= 5; $j++) {
                    $DimNO_CPK    = $DimNO . "_CPK" . $j;
                    $Measure_Data = $Data_array[$B];

                    if ($Measure_Data > $DimUpper or $Measure_Data < $DimLow) {
                        $Measure_Result = 'NG';
                    } else {
                        $Measure_Result = 'OK';
                    }

                    $InsertModifyData_sql = "REPLACE INTO " . $Measuredata_Table . " (`Ticket_Number`, `Cavity_Number`, `FAI_Number`, `DimSpec`, `DimUpper`, `DimLower`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `Expect_Data`, `Expect_Stdv`, `Measure_Value`, `Measure_Datetime`, `Measure_Result`) VALUES ('" . $Ticket_Number . "', '" . $CavNumber . "', '" . $DimNO_CPK . "', '" . $DimSpec . "', '" . $DimUpper . "', '" . $DimLower . "', '" . $Expect_UpperData . "', '" . $Expect_LowerData . "', '" . $Expect_UpperStdv . "', '" . $Expect_LowerStdv . "', '" . $Expect_Result . "', '" . $Expect_Data . "', '" . $Expect_Stdv . "', '" . $Measure_Data . "', '" . $End_DateTime . "', '" . $Measure_Result . "')";
//echo $InsertModifyData_sql;
                    $InsertModifyData_query = mysqli_query($connect_mold1, $InsertModifyData_sql) or die("警告 " . $Ticket_Number . "  ModifyCPKdatas數據上傳資料庫失敗");
                    $B++;
                }
                //Insert sq system
                $InsertSQModifyData_sql   = "REPLACE INTO `ipqc_modify_datas` (`MeasureDataTime`,`DateTime`, `PartNumber`, `PartDescription`, `LineNumber`, `PhaseNumber`, `TicketNumber`, `DimNO`, `MoldNumber`, `Type`, `CavNumber`, `Sample1`, `Sample2`, `Sample3`, `Sample4`, `Sample5`) VALUES ('" . $End_DateTime . "','" . $DateTime . "', '" . $PartNumber . "', '" . $Spec_Project_Name . "', 'NA', '" . $PhaseNumber . "', '" . $Ticket_Number . "', '" . $DimNO . "', '" . $Mold_Number . "', 'Molding', '" . $CavNumber . "', '" . $Data_array[0] . "', '" . $Data_array[1] . "', '" . $Data_array[2] . "', '" . $Data_array[3] . "', '" . $Data_array[4] . "')";
                $InsertSQModifyData_query = mysqli_query($connect_sq, $InsertSQModifyData_sql) or die("警告 " . $Ticket_Number . " : SQModifyCPKdatas數據上傳資料庫失敗");
            } else {
//Insert優化數據
                $Measure_Data = $Data_array[0];
                if ($Measure_Data > $DimUpper or $Measure_Data < $DimLow) {
                    $Measure_Result = 'NG';
                } else {
                    $Measure_Result = 'OK';
                }

                $InsertModifyData_sql   = "REPLACE INTO " . $Measuredata_Table . " (`Ticket_Number`, `Cavity_Number`, `FAI_Number`, `DimSpec`, `DimUpper`, `DimLower`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `Expect_Data`, `Expect_Stdv`, `Measure_Value`, `Measure_Datetime`, `Measure_Result`) VALUES ('" . $Ticket_Number . "', '" . $CavNumber . "', '" . $DimNO . "', '" . $DimSpec . "', '" . $DimUpper . "', '" . $DimLower . "', '" . $Expect_UpperData . "', '" . $Expect_LowerData . "', '" . $Expect_UpperStdv . "', '" . $Expect_LowerStdv . "', '" . $Expect_Result . "', '" . $Expect_Data . "', '" . $Expect_Stdv . "', '" . $Measure_Data . "', '" . $End_DateTime . "', '" . $Measure_Result . "')";
                $InsertModifyData_query = mysqli_query($connect_mold1, $InsertModifyData_sql) or die("警告 " . $Ticket_Number . " : Modifydatas數據上傳資料庫失敗");
                
                //Insert sq system
                $InsertSQModifyData_sql   = "REPLACE INTO `ipqc_modify_datas` (`MeasureDataTime`,`DateTime`, `PartNumber`, `PartDescription`, `LineNumber`, `PhaseNumber`, `TicketNumber`, `DimNO`, `MoldNumber`, `Type`, `CavNumber`, `Sample1`, `Sample2`, `Sample3`, `Sample4`, `Sample5`) VALUES ('" . $End_DateTime . "','" . $DateTime . "', '" . $PartNumber . "', '" . $Spec_Project_Name . "', 'NA', '" . $PhaseNumber . "', '" . $Ticket_Number . "', '" . $DimNO . "', '" . $Mold_Number . "', 'Molding', '" . $CavNumber . "', '" . $Data_array[0] . "', '', '', '', '')";
                $InsertSQModifyData_query = mysqli_query($connect_sq, $InsertSQModifyData_sql) or die("警告 " . $Ticket_Number . " : SQModifyCPKdatas數據上傳資料庫失敗");
//echo $InsertModifyData_sql;

            }
        }
    }

//GV//
    //Select measurecontent數據
    $AllCTSearch_sql_GV = "SELECT * FROM measurecontent WHERE 1=1 AND PartNumber = '$PartNumber' AND PartMold = '$MoldNumber' AND Status='9' AND MeasureEndTime between '$d1' AND '$d2' ORDER BY MeasureEndTime";
//echo $AllCTSearch_sql_GV;
    $AllCTSearch_query_GV = mysqli_query($connect_mold2, $AllCTSearch_sql_GV) or die("警告 ： 搜尋 GV measurecontent數據失敗");

    while ($AllCTSearch_GV = mysqli_fetch_assoc($AllCTSearch_query_GV)) {
        $ProjectName_m_GV         = $AllCTSearch_GV['PartDescription'];
        $ServiceNumber_GV       = $AllCTSearch_GV['ServiceNumber'];
        $ServiceNumber_array1[] = $AllCTSearch_GV['ServiceNumber'];
        $Status_GV              = $AllCTSearch_GV['Status'];
        $TicketNumber_GV        = $AllCTSearch_GV['TicketNumber'];
        $Ticket_Number_array1[] = $AllCTSearch_GV['Ticket_Number'];
        $PartNumber_GV          = $AllCTSearch_GV['PartNumber'];
        $PartMold_GV            = $AllCTSearch_GV['PartMold'];
        //$FormingMachine_GV        = $AllCTSearch_GV['FormingMachine'];
        $PartMold_GV          = $AllCTSearch_GV['PartMold'];
        $MachineNumber_GV     = $AllCTSearch_GV['MachineNumber'];
        $GV_Inspector_GV      = $AllCTSearch_GV['GV_Inspector'];
        $IPQC_Inspector_GV    = $AllCTSearch_GV['IPQC_Inspector'];
        $MeasureStartTime_GV  = $AllCTSearch_GV['MeasureStartTime'];
        $StartDate_GV         = substr($AllCTSearch_GV['MeasureStartTime'], 0, 10);
        $StartTime_GV         = substr($AllCTSearch_GV['MeasureStartTime'], 11, 18);
        $MeasureEndTime_GV    = $AllCTSearch_GV['MeasureEndTime'];
        $MeasureDate_GV    = $AllCTSearch_GV['DateTime'];
        $EndDate_GV           = substr($AllCTSearch_GV['MeasureEndTime'], 0, 10);
        $EndTime_GV           = substr($AllCTSearch_GV['MeasureEndTime'], 11, 18);
        //$Measuredata_Table_GV = "modify_m_measure_data_" . $CavityNumber_GV . "cav";
//echo $EndDate_GV."</br>" ;

        //for Insert sq system
        if ($EndTime_GV >= '00:00:00' && $EndTime_GV <= '07:59:59') {
            $MeasureDate_GV = date('Y-m-d', strtotime($EndDate_GV) - 60 * 60 * 24);
        } else {
            $MeasureDate_GV = $EndDate_GV;
        }
        if ($EndTime_GV >= '08:00:00' && $EndTime_GV <= '09:59:59') {
            $PhaseNumber_GV = 'D-1';
        } else if ($EndTime_GV >= '10:00:00' && $EndTime_GV <= '11:59:59') {
            $PhaseNumber_GV = 'D-2';
        } else if ($EndTime_GV >= '12:00:00' && $EndTime_GV <= '13:59:59') {
            $PhaseNumber_GV = 'D-3';
        } else if ($EndTime_GV >= '14:00:00' && $EndTime_GV <= '15:59:59') {
            $PhaseNumber_GV = 'D-4';
        } else if ($EndTime_GV >= '16:00:00' && $EndTime_GV <= '17:59:59') {
            $PhaseNumber_GV = 'D-5';
        } else if ($EndTime_GV >= '18:00:00' && $EndTime_GV <= '19:59:59') {
            $PhaseNumber_GV = 'D-6';
        } else if ($EndTime_GV >= '20:00:00' && $EndTime_GV <= '21:59:59') {
            $PhaseNumber_GV = 'N-1';
        } else if ($EndTime_GV >= '22:00:00' && $EndTime_GV <= '23:59:59') {
            $PhaseNumber_GV = 'N-2';
        } else if ($EndTime_GV >= '00:00:00' && $EndTime_GV <= '01:59:59') {
            $PhaseNumber_GV = 'N-3';
        } else if ($EndTime_GV >= '02:00:00' && $EndTime_GV <= '03:59:59') {
            $PhaseNumber_GV = 'N-4';
        } else if ($EndTime_GV >= '04:00:00' && $EndTime_GV <= '05:59:59') {
            $PhaseNumber_GV = 'N-5';
        } else if ($EndTime_GV >= '06:00:00' && $EndTime_GV <= '07:59:59') {
            $PhaseNumber_GV = 'N-6';
        }

        $TKSearch_sql = "SELECT * FROM modify_m_main_table WHERE 1=1 AND Ticket_Number = '$TicketNumber_GV'";
        $TKSearch_query = mysqli_query($connect_mold1, $TKSearch_sql) or die("警告 ： 搜尋 GV Ticket是否在modify_m_main_table重複  失敗");

        if (empty(mysqli_fetch_assoc($TKSearch_query))) {
//Insert Main Table數據
            $Insert_maintable_GV = "REPLACE INTO `modify_m_main_table` (`Project_Name`, `Part_Number_V`, `Mold_Number`, `Ticket_Number`, `Cavity`, `Start_Date`, `Start_Time`, `End_Date`, `End_Time`, `Status`, `Remark`, `Spec_Status`, `magazine`, `Quantity`, `Sample_Amount`) VALUES ('" . $ProjectName_S . "', '" . $PartNumber_GV . "', '" . $PartMold_GV . "', '" . $TicketNumber_GV . "', '" . $CavNums . "', '" . $StartDate_GV . "', '" . $StartTime_GV . "', '" . $EndDate_GV . "', '" . $EndTime_GV . "', '" . $Status_GV . "', '', '', '" . $MachineNumber_GV . "', '', '')";
            $query_GV            = mysqli_query($connect_mold1, $Insert_maintable_GV) or die("警告 " . $TicketNumber_GV . " : Modify_Main_table上傳資料庫失敗");
//echo $Insert_maintable_GV;
            //Select Spec
            $SpecSearch_sql_GV = "SELECT * FROM modify_spec_molding WHERE PartNumber = '" . $PartNumber . "' AND MoldNumber = '" . $MoldNumber . "' ";
//echo $SpecSearch_sql_GV ;
            $SpecSearch_query_GV = mysqli_query($connect_spec, $SpecSearch_sql_GV) or die("警告 ： 搜尋GV Modify Spec失敗");
            while ($AllInfo_GV = mysqli_fetch_array($SpecSearch_query_GV)) {
                $Data_array_GV        = array();
                $SpecProjectName_GV         = $AllInfo_GV['ProjectName'];
                $CavNumber_GV         = $AllInfo_GV['CavNumber'];
                $CavNums_GV           = $AllInfo_GV['CavNums'];
                $PartNumber_GV        = $AllInfo_GV['PartNumber'];
                $Rev_GV               = $AllInfo_GV['Rev'];
                $DimNO_GV             = $AllInfo_GV['DimNO'];
                $Remark_GV            = strtoupper($AllInfo_GV['Remark']);
                $DimSpec_GV           = $AllInfo_GV['DimSpec'];
                $DimUpper_GV          = $AllInfo_GV['DimUpper'];
                $DimLower_GV          = $AllInfo_GV['DimLower'];
                $Expect_UpperData_GV  = $AllInfo_GV['Expect_UpperData'];
                $Expect_LowerData_GV  = $AllInfo_GV['Expect_LowerData'];
                $Expect_UpperStdv_GV  = ($AllInfo_GV['Expect_UpperStdv']);
                $Expect_LowerStdv_GV  = ($AllInfo_GV['Expect_LowerStdv']);
                $Expect_Result_GV     = $AllInfo_GV['Expect_Result'];
                $Measuredata_Table_GV = "modify_m_measure_data_" . $CavNums_GV . "cav";

                $Expect_Data_GV = rand($Expect_LowerData_GV * 10000, $Expect_UpperData_GV * 10000) / 10000;
                $Expect_Stdv_GV = rand($Expect_LowerStdv_GV * 10000, $Expect_UpperStdv_GV * 10000) / 10000;

                $A = 0;
                while ($A < 6) {
                    $val_GV = round(nrand($Expect_Data_GV, $Expect_Stdv_GV), 4);

                    if ($Expect_Result_GV == 'OK') {
                        if ($val_GV <= $DimUpper_GV && $val_GV >= $DimLower_GV) {
                            $Data_array_GV[] = $val_GV;
                            $A++;
                        }

                    } else {
                        $Data_array_GV[] = $val_GV;
                        $A++;
                    }
                }
//Insert CPK優化數據
                if ($Remark_GV == 'CPK') {
                    $B = 0;
                    for ($j = 1; $j <= 5; $j++) {
                        $DimNO_CPK_GV    = $DimNO_GV . "_CPK" . $j;
                        $Measure_Data_GV = $Data_array_GV[$B];
                        if ($Measure_Data_GV > $DimUpper_GV or $Measure_Data_GV < $DimLow_GV) {
                            $Measure_Result_GV = 'NG';
                        } else {
                            $Measure_Result_GV = 'OK';
                        }
                        $InsertModifyData_sql_GV = "REPLACE INTO " . $Measuredata_Table_GV . "  (`Ticket_Number`, `Cavity_Number`, `FAI_Number`, `DimSpec`, `DimUpper`, `DimLower`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `Expect_Data`, `Expect_Stdv`, `Measure_Value`, `Measure_Datetime`, `Measure_Result`) VALUES ('" . $TicketNumber_GV . "', '" . $CavNumber_GV . "', '" . $DimNO_CPK_GV . "', '" . $DimSpec_GV . "', '" . $DimUpper_GV . "', '" . $DimLower_GV . "', '" . $Expect_UpperData_GV . "', '" . $Expect_LowerData_GV . "', '" . $Expect_UpperStdv_GV . "', '" . $Expect_LowerStdv_GV . "', '" . $Expect_Result_GV . "', '" . $Expect_Data_GV . "', '" . $Expect_Stdv_GV . "', '" . $Measure_Data_GV . "', '" . $MeasureEndTime_GV . "', '" . $Measure_Result_GV . "')";

                        $InsertModifyData_query_GV = mysqli_query($connect_mold1, $InsertModifyData_sql_GV) or die("警告 " . $TicketNumber_GV . " : GV ModifyCPKdatas數據上傳資料庫失敗");
                        $B++;
//echo $InsertModifyData_sql_GV;
                    }
                    //Insert sq system
                    $InsertSQModifyData_sql_GV   = "REPLACE INTO `ipqc_modify_datas` (`MeasureDataTime`,`DateTime`, `PartNumber`, `PartDescription`, `LineNumber`, `PhaseNumber`, `TicketNumber`, `DimNO`, `MoldNumber`, `Type`, `CavNumber`, `Sample1`, `Sample2`, `Sample3`, `Sample4`, `Sample5`) VALUES ('" . $MeasureEndTime_GV . "','" . $MeasureDate_GV . "', '" . $PartNumber_GV . "', '" . $SpecProjectName_GV . "', 'NA', '" . $PhaseNumber_GV . "', '" . $TicketNumber_GV . "', '" . $DimNO_GV . "', '" . $PartMold_GV . "', 'Molding', '" . $CavNumber_GV . "', '" . $Data_array_GV[0] . "', '" . $Data_array_GV[1] . "', '" . $Data_array_GV[2] . "', '" . $Data_array_GV[3] . "', '" . $Data_array_GV[4] . "')";
                    $InsertSQModifyData_query_GV = mysqli_query($connect_sq, $InsertSQModifyData_sql_GV) or die("警告 " . $$TicketNumber_GV . " : SQModifydatas數據上傳資料庫失敗");
                } else {
//Insert優化數據
                    $Measure_Data_GV = $Data_array_GV[0];
                    if ($Measure_Data_GV > $DimUpper_GV or $Measure_Data_GV < $DimLow_GV) {
                        $Measure_Result_GV = 'NG';
                    } else {
                        $Measure_Result_GV = 'OK';
                    }

                    $InsertModifyData_sql_GV = "REPLACE INTO " . $Measuredata_Table_GV . "  (`Ticket_Number`, `Cavity_Number`, `FAI_Number`, `DimSpec`, `DimUpper`, `DimLower`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `Expect_Data`, `Expect_Stdv`, `Measure_Value`, `Measure_Datetime`, `Measure_Result`) VALUES ('" . $TicketNumber_GV . "', '" . $CavNumber_GV . "', '" . $DimNO_GV . "', '" . $DimSpec_GV . "', '" . $DimUpper_GV . "', '" . $DimLower_GV . "', '" . $Expect_UpperData_GV . "', '" . $Expect_LowerData_GV . "', '" . $Expect_UpperStdv_GV . "', '" . $Expect_LowerStdv_GV . "', '" . $Expect_Result_GV . "', '" . $Expect_Data_GV . "', '" . $Expect_Stdv_GV . "', '" . $Measure_Data_GV . "', '" . $MeasureEndTime_GV . "', '" . $Measure_Result_GV . "')";
//echo $InsertModifyData_sql_GV;
                    $InsertModifyData_query_GV = mysqli_query($connect_mold1, $InsertModifyData_sql_GV) or die("警告 ：" . $TicketNumber_GV . " : GV Modifydatas數據上傳資料庫失敗");
                    
                   //Insert sq system
                    $InsertSQModifyData_sql_GV   = "REPLACE INTO `ipqc_modify_datas` (`MeasureDataTime`,`DateTime`, `PartNumber`, `PartDescription`, `LineNumber`, `PhaseNumber`, `TicketNumber`, `DimNO`, `MoldNumber`, `Type`, `CavNumber`, `Sample1`, `Sample2`, `Sample3`, `Sample4`, `Sample5`) VALUES ('" . $MeasureEndTime_GV . "','" . $MeasureDate_GV . "', '" . $PartNumber_GV . "', '" . $SpecProjectName_GV . "', 'NA', '" . $PhaseNumber_GV . "', '" . $TicketNumber_GV . "', '" . $DimNO_GV . "', '" . $PartMold_GV . "', 'Molding', '" . $CavNumber_GV . "', '" . $Data_array_GV[0] . "', '', '', '', '')";
                    $InsertSQModifyData_query_GV = mysqli_query($connect_sq, $InsertSQModifyData_sql_GV) or die("警告 " . $TicketNumber_GV . " : SQModifydatas數據上傳資料庫失敗");
                }
            }
        }
    }

//Select measurecontent數據(ORT)
     //搜尋並輸入request_measure的週期性測試數據
    $PeriodicTestSearch_sql_ort = "INSERT INTO modify_measurecontent SELECT * FROM measurecontent WHERE 1=1 AND PartNumber = '" . $PartNumber . "' AND LineNumber='" . $MoldNumber . "' AND MachineName ='週期性送測' AND Status='9' AND RequestDate = '" . $MeasureDate . "' ";
//echo $PeriodicTestSearch_sql_ort;
    $PeriodicTestSearch_query_ort = mysqli_query($connect_ort, $PeriodicTestSearch_sql_ort) or die("警告 ： 搜尋並輸入request_measure的週期性測試數據失敗");

    //搜尋request_measure的非週期性測試數據
    $AllCTSearch_sql_ort = "SELECT * FROM request_measure WHERE 1=1 AND PartNumber = '" . $PartNumber . "' AND LineNumber='" . $MoldNumber . "' AND MachineName !='週期性送測' AND RequestDate = '" . $MeasureDate . "' AND Status='9' ORDER BY RequestDate,Phase";
//echo $AllCTSearch_sql_ort;
    $AllCTSearch_query_ort = mysqli_query($connect_ort, $AllCTSearch_sql_ort) or die("警告 ： 搜尋request_measure非週期性測試數據失敗");

    while ($AllCTSearch_ort = mysqli_fetch_assoc($AllCTSearch_query_ort)) {
        $ProjectName_ort     = $AllCTSearch_ort['ProjectName'];
        $TicketNumber_ort     = $AllCTSearch_ort['TicketNumber'];
        $ServiceNumber_ort    = $AllCTSearch_ort['ServiceNumber'];
        $RequestStatus_ort    = $AllCTSearch_ort['RequestStatus'];
        $Status_ort           = $AllCTSearch_ort['Status'];
        $PartNumber_ort       = $AllCTSearch_ort['PartNumber'];
        $ProduceDate_ort      = $AllCTSearch_ort['ProduceDate'];
        $LineNumber_ort       = $AllCTSearch_ort['LineNumber'];
        $Phase_ort            = $AllCTSearch_ort['Phase'];
        $MachineName_ort      = $AllCTSearch_ort['MachineName'];
        $MachineNumber_ort    = $AllCTSearch_ort['MachineNumber'];
        $RequestTestItem_ort  = $AllCTSearch_ort['RequestTestItem'];
        $RequestTestFAI_ort   = $AllCTSearch_ort['RequestTestFAI'];
        $SampleNums_ort       = $AllCTSearch_ort['SampleNums'];
        $TestCondition_ort    = $AllCTSearch_ort['TestCondition'];
        $Test_Inspector_ort   = $AllCTSearch_ort['Test_Inspector'];
        $IPQC_Inspector_ort   = $AllCTSearch_ort['IPQC_Inspector'];
        $RawFileName_ort      = $AllCTSearch_ort['RawFileName'];
        $RequestDate_ort      = $AllCTSearch_ort['RequestDate'];
        $MeasureStartTime_ort = $AllCTSearch_ort['MeasureStartTime'];
        $MeasureEndTime_ort   = $AllCTSearch_ort['MeasureEndTime'];

//Insert measurecontent數據
        $Insert_maintable_ort = "REPLACE INTO `modify_measurecontent` (`ProjectName`,`TicketNumber`, `ServiceNumber`, `RequestStatus`, `Status`, `PartNumber`, `ProduceDate`, `LineNumber`, `Phase`, `MachineName`, `MachineNumber`, `RequestTestItem`,`RequestTestFAI`, `SampleNums`, `TestCondition`, `Test_Inspector`, `IPQC_Inspector`, `RawFileName`, `RequestDate`, `MeasureStartTime`, `MeasureEndTime`) VALUES ('" . $ProjectName_ort . "','" . $TicketNumber_ort . "', '" . $ServiceNumber_ort . "', '" . $RequestStatus_ort . "', '" . $Status_ort . "', '" . $PartNumber_ort . "', '" . $ProduceDate_ort . "', '" . $LineNumber_ort . "', '" . $Phase_ort . "', '" . $MachineName_ort . "', '" . $MachineNumber_ort . "', '" . $RequestTestItem_ort . "', '" . $RequestTestFAI_ort . "', '" . $SampleNums_ort . "', '" . $TestCondition_ort . "', '" . $Test_Inspector_ort . "', '" . $IPQC_Inspector_ort . "', '" . $RawFileName_ort . "', '" . $RequestDate_ort . "', '" . $MeasureStartTime_ort . "', '" . $MeasureEndTime_ort . "')";
        //echo $Insert_maintable_ort."</br>";
        $query_ort = mysqli_query($connect_ort, $Insert_maintable_ort) or die("警告 ：" . $ServiceNumber_ort . " : modify_measurecontent_ort數據上傳資料庫失敗");

        if (strpos($RequestTestItem_ort, '_') != '') {
            $ort_type = substr($RequestTestItem_ort, 0, strpos($RequestTestItem_ort, '_'));
        } else {
            $ort_type = $RequestTestItem_ort;
        }
                //echo $ort_type;
        $SpecSearch_sql_ort = "SELECT * FROM modify_spec_molding WHERE PartNumber = '" . $PartNumber . "' AND Remark LIKE '" . $ort_type . "%' ";
        //echo $SpecSearch_sql_ort."</br>";
        $SpecSearch_query_ort = mysqli_query($connect_spec, $SpecSearch_sql_ort) or die("警告 ： 搜尋Modify Spec失敗");

        $InsertData_array_ort = $InsertUpper_array_ort = $InsertLower_array_ort = $WithdrawData_array_ort = $WithdrawUpper_array_ort = $WithdrawLower_array_ort = $PushData_array_ort = $PushUpper_array_ort = $PushLower_array_ort = $PressData_array_ort = $PressUpper_array_ort = $PressLower_array_ort = $PullData_array_ort = $PullUpper_array_ort = $PullLower_array_ort = $RetentionData_array_ort1 = $RetentionUpper_array_ort1 = $RetentionLower_array_ort1 = $RetentionData_array_ort2 = $RetentionUpper_array_ort2 = $RetentionLower_array_ort2 = $LeakageData_array_ort1 = $LeakageUpper_array_ort1 = $LeakageLower_array_ort1 = $LeakageData_array_ort2 = $LeakageUpper_array_ort2 = $LeakageLower_array_ort2 = $LeakageData_array_ort3 = $LeakageUpper_array_ort3 = $LeakageLower_array_ort3 = array();

        while ($AllInfo_ort = mysqli_fetch_array($SpecSearch_query_ort)) {
            $DimNO_ort            = $AllInfo_ort['DimNO'];
            $PartNumber_ort       = $AllInfo_ort['PartNumber'];
            $Rev_ort              = $AllInfo_ort['Rev'];
            $DimSpec_ort          = $AllInfo_ort['DimSpec'];
            $DimUpper_ort         = $AllInfo_ort['DimUpper'];
            $DimLower_ort         = $AllInfo_ort['DimLower'];
            $Expect_UpperData_ort = $AllInfo_ort['Expect_UpperData'];
            $Expect_LowerData_ort = $AllInfo_ort['Expect_LowerData'];
            $Expect_UpperStdv_ort = ($AllInfo_ort['Expect_UpperStdv']);
            $Expect_LowerStdv_ort = ($AllInfo_ort['Expect_LowerStdv']);
            $Expect_Result_ort    = $AllInfo_ort['Expect_Result'];
            $Remark_ort           = $AllInfo_ort['Remark'];
            $DimDesc_ort           = $AllInfo_ort['DimDesc'];
            $Expect_Data_ort      = rand($Expect_LowerData_ort * 10000, $Expect_UpperData_ort * 10000) / 10000;
            $Expect_Stdv_ort      = rand($Expect_LowerStdv_ort * 10000, $Expect_UpperStdv_ort * 10000) / 10000;
            $A                    = 0;

            if (strpos($Remark_ort, '_') != '') {
                $Remarkort_check = substr($Remark_ort, 0, strpos($Remark_ort, '_'));
            } else {
                $Remarkort_check = $Remark_ort;
            }

            if ($Remark_ort == '插拔力_插入力') {
                if ($DimDesc_ort==$RequestTestFAI_ort or $RequestTestFAI_ort=='N/A' or $RequestTestFAI_ort=='') {
                    while ($A < $SampleNums_ort) {
                        $InsertUpper_array_ort[] = $AllInfo_ort['DimUpper'];
                        $InsertLower_array_ort[] = $AllInfo_ort['DimLower'];
                        $Insertval_ort           = round(nrand($Expect_Data_ort, $Expect_Stdv_ort), 2);

                        if ($Expect_Result_ort == 'OK') {
                            if ($Insertval_ort <= $DimUpper_ort && $Insertval_ort >= $DimLower_ort) {
                                $InsertData_array_ort[] = $Insertval_ort;
                                $A++;
                            }
                        } else {
                            $InsertData_array_ort[] = $Insertval_ort;
                            $A++;
                        }
                    }
                }
            } else if ($Remark_ort == '插拔力_拔出力') {
                if ($DimDesc_ort==$RequestTestFAI_ort or $RequestTestFAI_ort=='N/A' or $RequestTestFAI_ort=='') {
                    while ($A < $SampleNums_ort) {
                        $WithdrawUpper_array_ort[] = $AllInfo_ort['DimUpper'];
                        $WithdrawLower_array_ort[] = $AllInfo_ort['DimLower'];
                        $Withdrawval_ort           = round(nrand($Expect_Data_ort, $Expect_Stdv_ort), 2);

                        if ($Expect_Result_ort == 'OK') {
                            if ($Withdrawval_ort <= $DimUpper_ort && $Withdrawval_ort >= $DimLower_ort) {
                                $WithdrawData_array_ort[] = $Withdrawval_ort;
                                $A++;
                            }
                        } else {
                            $WithdrawData_array_ort[] = $Withdrawval_ort;
                            $A++;
                        }
                    }
                }
            } else if ($Remark_ort == '推壓力_推力') {
                if ($DimDesc_ort==$RequestTestFAI_ort or $RequestTestFAI_ort=='N/A' or $RequestTestFAI_ort=='') {
                    while ($A < $SampleNums_ort) {
                        $PushUpper_array_ort[] = $AllInfo_ort['DimUpper'];
                        $PushLower_array_ort[] = $AllInfo_ort['DimLower'];
                        $Pushval_ort           = round(nrand($Expect_Data_ort, $Expect_Stdv_ort), 2);

                        if ($Expect_Result_ort == 'OK') {
                            if ($Pushval_ort <= $DimUpper_ort && $Pushval_ort >= $DimLower_ort) {
                                $PushData_array_ort[] = $Pushval_ort;
                                $A++;
                            }
                        } else {
                            $PushData_array_ort[] = $Pushval_ort;
                            $A++;
                        }
                    }
                }
            } else if ($Remark_ort == '推壓力_壓力') {
                if ($DimDesc_ort==$RequestTestFAI_ort or $RequestTestFAI_ort=='N/A' or $RequestTestFAI_ort=='') {
                    while ($A < $SampleNums_ort) {
                        $PressUpper_array_ort[] = $AllInfo_ort['DimUpper'];
                        $PressLower_array_ort[] = $AllInfo_ort['DimLower'];
                        $Pressval_ort           = round(nrand($Expect_Data_ort, $Expect_Stdv_ort), 2);

                        if ($Expect_Result_ort == 'OK') {
                            if ($Pressval_ort <= $DimUpper_ort && $Pressval_ort >= $DimLower_ort) {
                                $PressData_array_ort[] = $Pressval_ort;
                                $A++;
                            }
                        } else {
                            $PressData_array_ort[] = $Pressval_ort;
                            $A++;
                        }
                    }
                }
            } else if ($Remark_ort == '拉力') {
                if ($DimDesc_ort==$RequestTestFAI_ort or $RequestTestFAI_ort=='N/A' or $RequestTestFAI_ort=='') {
                    while ($A < $SampleNums_ort) {
                        $PullUpper_array_ort[] = $AllInfo_ort['DimUpper'];
                        $PullLower_array_ort[] = $AllInfo_ort['DimLower'];
                        $Pullval_ort           = round(nrand($Expect_Data_ort, $Expect_Stdv_ort), 2);

                        if ($Expect_Result_ort == 'OK') {
                            if ($Pullval_ort <= $DimUpper_ort && $Pullval_ort >= $DimLower_ort) {
                                $PullData_array_ort[] = $Pullval_ort;
                                $A++;
                            }
                        } else {
                            $PullData_array_ort[] = $Pullval_ort;
                            $A++;
                        }
                    }
                }
            } else if ($Remark_ort == '端子保持力_爐前' or $Remark_ort == '鐵件保持力_爐前' or $Remark_ort == '後蓋保持力_爐前') {
                if ($DimDesc_ort==$RequestTestFAI_ort or $RequestTestFAI_ort=='N/A' or $RequestTestFAI_ort=='') {
                    while ($A < $SampleNums_ort) {
                        $RetentionUpper_array_ort1[] = $AllInfo_ort['DimUpper'];
                        $RetentionLower_array_ort1[] = $AllInfo_ort['DimLower'];
                        $Retentionval_ort1           = round(nrand($Expect_Data_ort, $Expect_Stdv_ort), 2);

                        if ($Expect_Result_ort == 'OK') {
                            if ($Retentionval_ort1 <= $DimUpper_ort && $Retentionval_ort1 >= $DimLower_ort) {
                                $RetentionData_array_ort1[] = $Retentionval_ort1;
                                $A++;
                            }
                        } else {
                            $RetentionData_array_ort1[] = $Retentionval_ort1;
                            $A++;
                        }
                    }
                }
            } else if ($Remark_ort == '端子保持力_爐後' or $Remark_ort == '鐵件保持力_爐後' or $Remark_ort == '後蓋保持力_爐後') {
                if ($DimDesc_ort==$RequestTestFAI_ort or $RequestTestFAI_ort=='N/A' or $RequestTestFAI_ort=='') {
                    while ($A < $SampleNums_ort) {
                        $RetentionUpper_array_ort2[] = $AllInfo_ort['DimUpper'];
                        $RetentionLower_array_ort2[] = $AllInfo_ort['DimLower'];
                        $Retentionval_ort2           = round(nrand($Expect_Data_ort, $Expect_Stdv_ort), 2);

                        if ($Expect_Result_ort == 'OK') {
                            if ($Retentionval_ort2 <= $DimUpper_ort && $Retentionval_ort2 >= $DimLower_ort) {
                                $RetentionData_array_ort2[] = $Retentionval_ort2;
                                $A++;
                            }
                        } else {
                            $RetentionData_array_ort2[] = $Retentionval_ort2;
                            $A++;
                        }
                    }
                }
            } else if ($Remark_ort == '氣密_爐前') {
                while ($A < $SampleNums_ort) {
                    $LeakageUpper_array_ort1[] = $AllInfo_ort['DimUpper'];
                    $LeakageLower_array_ort1[] = $AllInfo_ort['DimLower'];
                    $Leakageval_ort1           = round(nrand($Expect_Data_ort, $Expect_Stdv_ort), 4);
                            //print_r($LeakageUpper_array_ort1);
                    if ($Expect_Result_ort == 'OK') {
                        if ($Leakageval_ort1 <= $DimUpper_ort && $Leakageval_ort1 >= $DimLower_ort) {
                            $LeakageData_array_ort1[] = $Leakageval_ort1;
                            $A++;
                        }
                    } else {
                        $LeakageData_array_ort1[] = $Leakageval_ort1;
                        $A++;
                    }
                }
            } else if ($Remark_ort == '氣密_爐後1') {
                while ($A < $SampleNums_ort) {
                    $LeakageUpper_array_ort2[] = $AllInfo_ort['DimUpper'];
                    $LeakageLower_array_ort2[] = $AllInfo_ort['DimLower'];
                    $Leakageval_ort2           = round(nrand($Expect_Data_ort, $Expect_Stdv_ort), 4);
                            //print_r($LeakageUpper_array_ort2);
                    if ($Expect_Result_ort == 'OK') {
                        if ($Leakageval_ort2 <= $DimUpper_ort && $Leakageval_ort2 >= $DimLower_ort) {
                            $LeakageData_array_ort2[] = $Leakageval_ort2;
                            $A++;
                        }
                    } else {
                        $LeakageData_array_ort2[] = $Leakageval_ort2;
                        $A++;
                    }
                }
            } else if ($Remark_ort == '氣密_爐後2') {
                while ($A < $SampleNums_ort) {
                    $LeakageUpper_array_ort3[] = $AllInfo_ort['DimUpper'];
                    $LeakageLower_array_ort3[] = $AllInfo_ort['DimLower'];
                    $Leakageval_ort3           = round(nrand($Expect_Data_ort, $Expect_Stdv_ort), 4);
                            //print_r($LeakageUpper_array_ort2);
                    if ($Expect_Result_ort == 'OK') {
                        if ($Leakageval_ort3 <= $DimUpper_ort && $Leakageval_ort3 >= $DimLower_ort) {
                            $LeakageData_array_ort3[] = $Leakageval_ort3;
                            $A++;
                        }
                    } else {
                        $LeakageData_array_ort3[] = $Leakageval_ort3;
                        $A++;
                    }
                }
            }
        }

        if ($MachineName_ort == '插拔力機_順瀅') {
            $db_name        = 'measuredata_ip1';
            $db_name_modify = 'modify_measuredata_ip1';} else if ($MachineName_ort == '插拔力機_海達') {
                $db_name        = 'measuredata_ip2';
                $db_name_modify = 'modify_measuredata_ip2';} else if ($MachineName_ort == '拉力機') {
                    $db_name        = 'measuredata_pull';
                    $db_name_modify = 'modify_measuredata_pull';} else if ($MachineName_ort == '氣密機') {
                        $db_name        = 'measuredata_airtight';
                        $db_name_modify = 'modify_measuredata_airtight';}
/*
echo "MachineName:" . $MachineName_ort . "</br>";
echo "RequestTestItem:" . $RequestTestItem_ort . "</br>";
echo "Remark:" . $Remark_ort . "</br>";
echo "ort_type:" . $ort_type . "</br>";
echo "db_name:" . $db_name . "</br>";
echo "db_name_modify:" . $db_name_modify . "</br></br></br>";
 */
//確認Spec中是否有此檢測項目,如果沒有就不執行
if ($Remarkort_check == $ort_type) {
    if ($ort_type == '插拔力') {
        $IWInfoSearch_sql_ort = "SELECT * FROM " . $db_name . " WHERE 1=1 AND ServiceNumber = '$ServiceNumber_ort' ORDER BY DimNOOrder";
                        //echo $Ip1InfoSearch_sql_ort;
        $IWInfoSearch_query_ort = mysqli_query($connect_ort, $IWInfoSearch_sql_ort) or die("警告 ： 搜尋measuredata_airtight數據失敗");
        $i                       = 0;
        while ($IWInfo_ort = mysqli_fetch_assoc($IWInfoSearch_query_ort)) {
            $DimNO_ort      = $IWInfo_ort['DimNO'];
            $DimNOOrder_ort = $IWInfo_ort['DimNOOrder'];

            $IW_InsertModifyip1_sql_ort   = "REPLACE INTO " . $db_name_modify . " (`TicketNumber`, `ServiceNumber`, `DimNO`, `DimNOOrder`, `UpperSpec_In`, `LowerSpec_In`, `UpperSpec_Out`, `LowerSpec_Out`, `MeasureData_In`, `MeasureData_Out`) VALUES ('" . $TicketNumber_ort . "', '" . $ServiceNumber_ort . "', '" . $DimNO_ort . "', '" . $DimNOOrder_ort . "', '" . $InsertUpper_array_ort[$i] . "', '" . $InsertLower_array_ort[$i] . "', '" . $WithdrawUpper_array_ort[$i] . "','" . $WithdrawLower_array_ort[$i] . "', '" . $InsertData_array_ort[$i] . "', '" . $WithdrawData_array_ort[$i] . "')";
            $IW_InsertModifyip1_query_ort = mysqli_query($connect_ort, $IW_InsertModifyip1_sql_ort) or die("警告 ：" . $ServiceNumber_ort . " : modify_measuredata_ip1數據上傳資料庫失敗");
            //echo $IW_InsertModifyip1_sql_ort;
            $i++;
        }
    } else if ($ort_type == '推壓力') {
        $PPInfoSearch_sql_ort = "SELECT * FROM " . $db_name . " WHERE 1=1 AND ServiceNumber = '$ServiceNumber_ort' ORDER BY DimNOOrder";
                        //echo $PPInfoSearch_sql_ort;
        $PPInfoSearch_query_ort = mysqli_query($connect_ort, $PPInfoSearch_sql_ort) or die("警告 ： 搜尋measuredata_airtight數據失敗");
        $i                       = 0;
        while ($PPInfo_ort = mysqli_fetch_assoc($PPInfoSearch_query_ort)) {
            $DimNO_ort      = $PPInfo_ort['DimNO'];
            $DimNOOrder_ort = $PPInfo_ort['DimNOOrder'];

            $PP_InsertModifyip1_sql_ort   = "REPLACE INTO " . $db_name_modify . " (`TicketNumber`, `ServiceNumber`, `DimNO`, `DimNOOrder`, `UpperSpec_In`, `LowerSpec_In`, `UpperSpec_Out`, `LowerSpec_Out`, `MeasureData_In`, `MeasureData_Out`) VALUES ('" . $TicketNumber_ort . "', '" . $ServiceNumber_ort . "', '" . $DimNO_ort . "', '" . $DimNOOrder_ort . "', '" . $PushUpper_array_ort[$i] . "', '" . $PushLower_array_ort[$i] . "', '" . $PressUpper_array_ort[$i] . "','" . $PressLower_array_ort[$i] . "', '" . $PushData_array_ort[$i] . "', '" . $PressData_array_ort[$i] . "')";
            $PP_InsertModifyip1_query_ort = mysqli_query($connect_ort, $PP_InsertModifyip1_sql_ort) or die("警告 ：" . $ServiceNumber_ort . " : modify_measuredata_ip1數據上傳資料庫失敗");
            //echo $PP_InsertModifyip1_sql_ort ;
            $i++;
        }
    } else if ($ort_type == '拉力') {
        $PullInfoSearch_sql_ort = "SELECT * FROM " . $db_name . " WHERE 1=1 AND ServiceNumber = '$ServiceNumber_ort' ORDER BY DimNOOrder";
                        //echo $PullInfoSearch_sql_ort;
        $PullInfoSearch_query_ort = mysqli_query($connect_ort, $PullInfoSearch_sql_ort) or die("警告 ： 搜尋measuredata_airtight數據失敗");
        $i                        = 0;
        while ($PullInfo_ort = mysqli_fetch_assoc($PullInfoSearch_query_ort)) {
            $DimNO_ort      = $PullInfo_ort['DimNO'];
            $DimNOOrder_ort = $PullInfo_ort['DimNOOrder'];

            if ($MachineName_ort == '插拔力機_順瀅') {
                $InsertModifypull_sql_ort = "REPLACE INTO " . $db_name_modify . " (`TicketNumber`, `ServiceNumber`, `DimNO`, `DimNOOrder`, `UpperSpec_In`, `LowerSpec_In`, `UpperSpec_Out`, `LowerSpec_Out`, `MeasureData_In`, `MeasureData_Out`) VALUES ('" . $TicketNumber_ort . "', '" . $ServiceNumber_ort . "', '" . $DimNO_ort . "', '" . $DimNOOrder_ort . "', '" . $PullUpper_array_ort[$i] . "', '" . $PullLower_array_ort[$i] . "', 'N/A','N/A', '" . $PullData_array_ort[$i] . "', 'N/A')";
            } else {
                $InsertModifypull_sql_ort = "REPLACE INTO " . $db_name_modify . " (`TicketNumber`, `ServiceNumber`, `DimNO`, `DimNOOrder`, `UpperSpec_In`, `LowerSpec_In`, `UpperSpec_Out`, `LowerSpec_Out`, `MeasureData1`, `MeasureData2`, `MeasureData3`)  VALUES ('" . $TicketNumber_ort . "', '" . $ServiceNumber_ort . "', '" . $DimNO_ort . "', '" . $DimNOOrder_ort . "', '" . $PullUpper_array_ort[$i] . "', '" . $PullLower_array_ort[$i] . "','N/A', 'N/A', '" . $PullData_array_ort[$i] . "','N/A', 'N/A')";
            }
            $InsertModifypull_query_ort = mysqli_query($connect_ort, $InsertModifypull_sql_ort) or die("警告 ：" . $ServiceNumber_ort . " : modify_measuredata_pull拉力數據上傳資料庫失敗");
            $i++;
        }
    } else if ($ort_type == '端子保持力' or $ort_type == '鐵件保持力' or $ort_type == '後蓋保持力') {
        $RetentionInfoSearch_sql_ort   = "SELECT * FROM " . $db_name . " WHERE 1=1 AND ServiceNumber = '$ServiceNumber_ort' ORDER BY DimNOOrder";
        $RetentionInfoSearch_query_ort = mysqli_query($connect_ort, $RetentionInfoSearch_sql_ort) or die("警告 ： 搜尋measuredata_airtight數據失敗");
        $i                             = 0;
        while ($RetentionInfo_ort = mysqli_fetch_assoc($RetentionInfoSearch_query_ort)) {
            $DimNO_ort      = $RetentionInfo_ort['DimNO'];
            $DimNOOrder_ort = $RetentionInfo_ort['DimNOOrder'];

            if ($MachineName_ort == '插拔力機_順瀅') {
                $InsertModifyRetention_sql_ort = "REPLACE INTO " . $db_name_modify . " (`TicketNumber`, `ServiceNumber`, `DimNO`, `DimNOOrder`, `UpperSpec_In`, `LowerSpec_In`, `UpperSpec_Out`, `LowerSpec_Out`, `MeasureData_In`, `MeasureData_Out`) VALUES ('" . $TicketNumber_ort . "', '" . $ServiceNumber_ort . "', '" . $DimNO_ort . "', '" . $DimNOOrder_ort . "', '" . $RetentionUpper_array_ort1[$i] . "', '" . $RetentionLower_array_ort1[$i] . "', '" . $RetentionUpper_array_ort2[$i] . "','" . $RetentionLower_array_ort2[$i] . "', '" . $RetentionData_array_ort1[$i] . "', '" . $RetentionData_array_ort2[$i] . "')";
            } else {
                $InsertModifyRetention_sql_ort = "REPLACE INTO " . $db_name_modify . " (`TicketNumber`, `ServiceNumber`, `DimNO`, `DimNOOrder`, `UpperSpec_In`, `LowerSpec_In`, `UpperSpec_Out`, `LowerSpec_Out`, `MeasureData1`, `MeasureData2`, `MeasureData3`)  VALUES ('" . $TicketNumber_ort . "', '" . $ServiceNumber_ort . "', '" . $DimNO_ort . "', '" . $DimNOOrder_ort . "', '" . $RetentionUpper_array_ort1[$i] . "', '" . $RetentionLower_array_ort1[$i] . "', '" . $RetentionUpper_array_ort2[$i] . "','" . $RetentionLower_array_ort2[$i] . "', '" . $RetentionData_array_ort1[$i] . "','" . $RetentionData_array_ort2[$i] . "', 'N/A')";
            }
            $InsertModifyRetention_query_ort = mysqli_query($connect_ort, $InsertModifyRetention_sql_ort) or die("警告 ：" . $ServiceNumber_ort . " : modify_measuredata_pull保持力數據上傳資料庫失敗");
            $i++;
        }
    } else if ($ort_type == '氣密') {
        $AirtightInfoSearch_sql_ort   = "SELECT * FROM " . $db_name . " WHERE 1=1 AND ServiceNumber = '$ServiceNumber_ort' ORDER BY DimNOOrder";
        $AirtightInfoSearch_query_ort = mysqli_query($connect_ort, $AirtightInfoSearch_sql_ort) or die("警告 ： 搜尋measuredata_airtight數據失敗");
        $j                            = 0;

        while ($AirtightInfo_ort = mysqli_fetch_assoc($AirtightInfoSearch_query_ort)) {
            $TicketNumber_ort  = $AirtightInfo_ort['TicketNumber'];
            $ServiceNumber_ort = $AirtightInfo_ort['ServiceNumber'];
            $DimNO_ort         = $AirtightInfo_ort['DimNO'];
            $DimNOOrder_ort    = $AirtightInfo_ort['DimNOOrder'];
            $AirPressure_ort   = $AirtightInfo_ort['AirPressure'];
            $Temperature_ort   = $AirtightInfo_ort['Temperature'];
            $TestDate_ort      = $AirtightInfo_ort['TestDate'];
            $TestTime_ort      = $AirtightInfo_ort['TestTime'];
            $TestResult_ort    = 'OK';

            if ($RequestStatus_ort == '爐前') {
                $InsertModifyAirtight_sql_ort = "REPLACE INTO " . $db_name_modify . "  (`TicketNumber`, `ServiceNumber`, `DimNO`, `DimNOOrder`, `UpperSpec`, `LowerSpec`, `AirPressure`, `Leakage`, `Temperature`, `TestResult`, `TestDate`, `TestTime`)  VALUES ('" . $TicketNumber_ort . "', '" . $ServiceNumber_ort . "', '" . $DimNO_ort . "', '" . $DimNOOrder_ort . "', '" . $LeakageUpper_array_ort1[$j] . "', '" . $LeakageLower_array_ort1[$j] . "', '" . $AirPressure_ort . "', '" . $LeakageData_array_ort1[$j] . "', '" . $Temperature_ort . "', '" . $TestResult_ort . "' , '" . $TestDate_ort . "', '" . $TestTime_ort . "')";
            } else if ($RequestStatus_ort == '爐後1') {
                $InsertModifyAirtight_sql_ort = "REPLACE INTO " . $db_name_modify . "  (`TicketNumber`, `ServiceNumber`, `DimNO`, `DimNOOrder`, `UpperSpec`, `LowerSpec`, `AirPressure`, `Leakage`, `Temperature`, `TestResult`, `TestDate`, `TestTime`)  VALUES ('" . $TicketNumber_ort . "', '" . $ServiceNumber_ort . "', '" . $DimNO_ort . "', '" . $DimNOOrder_ort . "', '" . $LeakageUpper_array_ort2[$j] . "', '" . $LeakageLower_array_ort2[$j] . "', '" . $AirPressure_ort . "', '" . $LeakageData_array_ort2[$j] . "', '" . $Temperature_ort . "', '" . $TestResult_ort . "' , '" . $TestDate_ort . "', '" . $TestTime_ort . "')";
            } else {
                $InsertModifyAirtight_sql_ort = "REPLACE INTO " . $db_name_modify . "  (`TicketNumber`, `ServiceNumber`, `DimNO`, `DimNOOrder`, `UpperSpec`, `LowerSpec`, `AirPressure`, `Leakage`, `Temperature`, `TestResult`, `TestDate`, `TestTime`)  VALUES ('" . $TicketNumber_ort . "', '" . $ServiceNumber_ort . "', '" . $DimNO_ort . "', '" . $DimNOOrder_ort . "', '" . $LeakageUpper_array_ort3[$j] . "', '" . $LeakageLower_array_ort3[$j] . "', '" . $AirPressure_ort . "', '" . $LeakageData_array_ort3[$j] . "', '" . $Temperature_ort . "', '" . $TestResult_ort . "' , '" . $TestDate_ort . "', '" . $TestTime_ort . "')";
            }
            $InsertModifyAirtight_query_ort = mysqli_query($connect_ort, $InsertModifyAirtight_sql_ort) or die("警告 ：" . $ServiceNumber_ort . " : modify_measuredata_airtight數據上傳資料庫失敗");
            $j++;
        }
    }
} else {
                    //刪除modify_measurecontent中Spec沒有的測試項ServiceNumber
    $SelSN_sql_ort   = "SELECT * FROM `modify_measurecontent` WHERE ServiceNumber='" . $ServiceNumber_ort . "' ";
    $SelSN_query_ort = mysqli_query($connect_ort, $SelSN_sql_ort) or die("警告 ： 搜尋modify_measurecontent SN失敗");

    while ($SelSNInfo_ort = mysqli_fetch_assoc($SelSN_query_ort)) {
        $DelSN_sql_ort   = "DELETE FROM `modify_measurecontent` WHERE ServiceNumber='" . $ServiceNumber_ort . "' ";
        $DelSN_query_ort = mysqli_query($connect_ort, $DelSN_sql_ort) or die("警告 ：" . $ServiceNumber_ort . " : modify_measurecontent SN刪除失敗");
    }
}

}//ORT結束

}
echo "【 優化數據上傳資料庫成功 】</br>";
$end_time = date("H")*3600+date("i")*60+date("s");

echo "start_time：".$start_time."秒<br/>";
echo "end_time：".$end_time."秒<br/>";
$time_total = $end_time - $start_time;
echo "執行了：".$time_total."秒<br/>";
