<?php
set_time_limit(0);
session_start();
error_reporting(0);
$start_time = date("H")*3600+date("i")*60+date("s");

require_once '../../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

function nrand($mean, $sd)
{
  $x = mt_rand() / mt_getrandmax();
  $y = mt_rand() / mt_getrandmax();
  return sqrt(-2 * log($x)) * cos(2 * pi() * $y) * $sd + $mean;
}

if (!isset($_SESSION)) {session_start();}
$Ticket_Number_array0 = $Ticket_Number_array1 = $PN_MN_array = array();
$NowDate              = date('Y-m-d');
$d1                   = date('Y-m-d', strtotime($NowDate) - 60 * 60 * 24 * 1) . " 08:00:00";
$d2                   = $NowDate . " 07:59:59";

mysqli_select_db($connect_spec, $database_spec);
mysqli_select_db($connect_stmp, $database_stmp);
mysqli_select_db($connect_plating, $database_plating);
mysqli_select_db($connect_sq, $database_sq);

//Select part Number from Spec db
$PNSearch_sql = "SELECT * FROM modify_spec_welding WHERE 1=1 group by PartNumber,MoldNumber order by PartNumber";

$PNSearch_query = mysqli_query($connect_spec, $PNSearch_sql) or die("警告 ： 搜尋Part Number失敗");
while ($PNSearch = mysqli_fetch_assoc($PNSearch_query)) {
  $ProjectName    = $PNSearch['ProjectName'];
  $PartNumber    = $PNSearch['PartNumber'];
  $MoldNumber    = $PNSearch['MoldNumber'];
  $M_Type    = substr($PartNumber,5,2);

  if($M_Type=='9W'){
    $Search_db_name1='laser_dim_data';
    $Search_db_name2='main_table';
    $Insert_db_name='modify_laser_dim_data';
    $Type='Welding';//鐳射

    $AllTNSearch_sql_check   = "SELECT * FROM $Search_db_name1 WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' GROUP BY Ticket_Number ORDER BY DateTime ";

    $AllTNSearch_query_check  = mysqli_query($connect_plating, $AllTNSearch_sql_check ) or die("警告 ： 搜尋Ticket Number失敗");
    $AllTNSearch_check = mysqli_fetch_assoc($AllTNSearch_query_check);

    //check laser_dim_data表單內是否有數據
    if($AllTNSearch_check){

      $AllTNSearch_sql   = "SELECT * FROM $Search_db_name1 WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' GROUP BY Ticket_Number ORDER BY DateTime ";
      //echo $AllTNSearch_sql."</br>" ;

      $AllTNSearch_query = mysqli_query($connect_plating, $AllTNSearch_sql) or die("警告 ： 搜尋Ticket Number失敗");

      while ($AllTNSearch = mysqli_fetch_assoc($AllTNSearch_query)) {

        $End_DateTime           = $AllTNSearch['DateTime'];
        $End_Time           = substr($AllTNSearch['DateTime'],11,8);
        $Date               = $AllTNSearch['Date'];
        $DateTime               = $AllTNSearch['DateTime'];
        $PartNumber         = $AllTNSearch['PartNumber'];
        $Ticket_Number      = $AllTNSearch['Ticket_Number'];
        $Productline      = $AllTNSearch['Productline'];
        $day_night      = $AllTNSearch['day_night'];
        $Personnel_ID      = $AllTNSearch['Personnel_ID'];
    //$sections_array[]      = $AllTNSearch['sections'];

        //for Insert sq system
        if ($End_Time >= '08:00:00' && $End_Time <= '09:59:59'){
         $PhaseNumber='D-1';
       }
       else if ($End_Time >= '10:00:00' && $End_Time <= '11:59:59'){
         $PhaseNumber='D-2';
       }
       else if ($End_Time >= '12:00:00' && $End_Time <= '13:59:59'){
         $PhaseNumber='D-3';
       }
       else if ($End_Time >= '14:00:00' && $End_Time <= '15:59:59'){
         $PhaseNumber='D-4';
       }
       else if ($End_Time >= '16:00:00' && $End_Time <= '17:59:59'){
         $PhaseNumber='D-5';
       }
       else if ($End_Time >= '18:00:00' && $End_Time <= '19:59:59'){
         $PhaseNumber='D-6';
       }
       else if ($End_Time >= '20:00:00' && $End_Time <= '21:59:59'){
         $PhaseNumber='N-1';
       }
       else if ($End_Time >= '22:00:00' && $End_Time <= '23:59:59'){
         $PhaseNumber='N-2';
       }
       else if ($End_Time >= '00:00:00' && $End_Time <= '01:59:59'){
         $PhaseNumber='N-3';
       }
       else if ($End_Time >= '02:00:00' && $End_Time <= '03:59:59'){
         $PhaseNumber='N-4';
       }
       else if ($End_Time >= '04:00:00' && $End_Time <= '05:59:59'){
         $PhaseNumber='N-5';
       }
       else if ($End_Time >= '06:00:00' && $End_Time <= '07:59:59'){
         $PhaseNumber='N-6';
       }

       $SpecSearch_sql   = "SELECT * FROM modify_spec_welding WHERE PartNumber = '" . $PartNumber . "' ";
         //echo $SpecSearch_sql."</br>";
       $SpecSearch_query = mysqli_query($connect_spec, $SpecSearch_sql) or die("警告 ： 搜尋Modify Spec失敗");
       $id=0;
       while ($AllInfo = mysqli_fetch_array($SpecSearch_query)) {
        $Data_array       = array();
        $Project_Name       = $AllInfo['ProjectName'];
        $PartNumber       = $AllInfo['PartNumber'];
        $Rev              = $AllInfo['Rev'];
        $DimNO            = $AllInfo['DimNO'];
        $Remark           = strtoupper($AllInfo['Remark']);
        $DimSpec          = $AllInfo['DimSpec'];
        $DimUpper         = $AllInfo['DimUpper'];
        $DimLower         = $AllInfo['DimLower'];
        $Expect_UpperData = $AllInfo['Expect_UpperData'];
        $Expect_LowerData = $AllInfo['Expect_LowerData'];
        $Expect_UpperStdv = ($AllInfo['Expect_UpperStdv']);
        $Expect_LowerStdv = ($AllInfo['Expect_LowerStdv']);
        $Expect_Result    = $AllInfo['Expect_Result'];
        $Expect_Data      = rand($Expect_LowerData * 1000, $Expect_UpperData * 1000) / 1000;
        $Expect_Stdv      = rand($Expect_LowerStdv * 1000, $Expect_UpperStdv * 1000) / 1000;
//echo "PartNumber : ".$PartNumber."</br>";
//echo "DimNO : ".$DimNO."</br>";
//echo "Remark : ".$Remark."</br>";

        $A = 0;
        while ($A < 14) {
          $val = round(nrand($Expect_Data, $Expect_Stdv), 4);

          if ($Expect_Result == 'OK') {
            if ($val <= $DimUpper && $val >= $DimLower) {
              $Data_array[] = $val;
              $A++;
            }

          } else {
            $Data_array[] = $val;
            $A++;
          }
        }

//Insert優化數據
        $Measure_Data = $Data_array[0];
        if ($Measure_Data > $DimUpper or $Measure_Data < $DimLow) {
          $Measure_Result = 'NG';
        } else {
          $Measure_Result = 'OK';
        }

    //Insert sq system
        $InsertSQModifyData_sql   = "REPLACE INTO `ipqc_modify_datas` (`MeasureDataTime`,`DateTime`, `PartNumber`, `PartDescription`, `LineNumber`, `PhaseNumber`, `TicketNumber`, `DimNO`, `MoldNumber`, `Type`, `CavNumber`, `Sample1`, `Sample2`, `Sample3`, `Sample4`, `Sample5`, `Sample6`, `Sample7`, `Sample8`, `Sample9`, `Sample10`, `Sample11`, `Sample12`, `Sample13`, `Sample14`) VALUES ('" . $End_DateTime . "','" . $Date . "', '" . $PartNumber . "', '" . $Project_Name . "', '". $Productline ."', '" . $PhaseNumber . "', '" . $Ticket_Number . "', '" . $DimNO . "', 'NA', '" . $Type . "', 'NA', '" . $Data_array[0] . "', '" . $Data_array[1] . "', '" . $Data_array[2] . "', '" . $Data_array[3] . "', '" . $Data_array[4] . "', '" . $Data_array[5] . "', '" . $Data_array[6] . "', '" . $Data_array[7] . "', '" . $Data_array[8] . "', '" . $Data_array[9] . "', '" . $Data_array[10] . "', '" . $Data_array[11] . "', '" . $Data_array[12] . "', '" . $Data_array[13] . "')";
            //echo $InsertSQModifyData_sql;
        $InsertSQModifyData_query = mysqli_query($connect_sq, $InsertSQModifyData_sql) or die("警告 " . $Ticket_Number . " : Welding SQModifydatas數據上傳資料庫失敗");

    //Insert Modify System
        $InsertModifyData_sql   = "REPLACE INTO $Insert_db_name (`id`,`PartNumber`, `Type`, `day_night`, `Productline`, `Ticket_Number`, `Personnel_ID`, `Date`, `DateTime`, `sections`, `FAI_Number`, `spec`, `up_lim`, `down_lim`, `Measure_Value-1`, `Measure_Value-2`, `Measure_Value-3`, `Measure_Value-4`, `Measure_Value-5`, `Measure_Value-6`, `Measure_Value-7`, `Measure_Value-8`, `Measure_Value-9`, `Measure_Value-10`, `Measure_Value-11`, `Measure_Value-12`, `Measure_Value-13`, `Measure_Value-14`) VALUES ('" . $id . "','" . $PartNumber . "', '" . $Type . "', '" . $day_night . "', '" . $Productline . "', '" . $Ticket_Number . "', '" . $Personnel_ID . "', '" . $Date . "', '" . $DateTime . "', '" . $sections . "', '" . $DimNO . "', '" . $DimSpec . "', '" . $DimUpper . "', '" . $DimLower . "', '" . $Data_array[0] . "', '" . $Data_array[1] . "', '" . $Data_array[2] . "', '" . $Data_array[3] . "', '" . $Data_array[4] . "', '" . $Data_array[5] . "', '" . $Data_array[6] . "', '" . $Data_array[7] . "', '" . $Data_array[8] . "', '" . $Data_array[9] . "', '" . $Data_array[10] . "', '" . $Data_array[11] . "', '" . $Data_array[12] . "', '" . $Data_array[13] . "')";
              //echo $InsertModifyData_sql;
        $InsertModifyData_query = mysqli_query($connect_plating, $InsertModifyData_sql) or die("警告 " . $Ticket_Number . " : Modifydatas 鐳射數據上傳資料庫失敗");
        $id++;

      }
    }
    
//Insert modify_laser_visual_data
    $VisualInsert_sql   = "REPLACE INTO modify_laser_visual_data SELECT * from laser_visual_data WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $VisualInsert_sql;
    $VisualInsert_query = mysqli_query($connect_plating, $VisualInsert_sql) or die("警告 ： Insert modify_laser_visual_data失敗");

      //Insert modify_laser_roughness_data
    $RoughnessInsert_sql   = "REPLACE INTO modify_laser_roughness_data SELECT * from laser_roughness_data WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' ";
//echo $RoughnessInsert_sql;
    $RoughnessInsert_query = mysqli_query($connect_plating, $RoughnessInsert_sql) or die("警告 ： Insert modify_laser_roughness_data失敗"); 
  } 

    //laser_dim_data表單內沒有數據>>抓取stamping db數據
  else{

//delect 既有的數據
    $AllCTDelete_sql   = "SELECT Ticket_Number FROM modify_main_table WHERE 1=1 AND Part_Number_V = '$PartNumber' AND Mold_Number = '$MoldNumber' AND concat(End_Date,'-',End_Time) between '$d1' AND '$d2' ORDER BY End_Date,End_Time";
    $AllCTDelete_query = mysqli_query($connect_stmp, $AllCTDelete_sql) or die("警告 ： 搜尋main table數據失敗");
    $DeleteVisuals_sql   = "DELETE FROM modify_visual_inspection WHERE 1=1 AND Part_Number = '$PartNumber' AND Mold_Number = '$MoldNumber' AND concat(Date,'-',Time) between '$d1' AND '$d2' ";
    $DeleteVisuals_query = mysqli_query($connect_stmp, $DeleteVisuals_sql) or die("警告 ： 刪除Visual數據失敗");
    $DeleteSQ_sql   = "DELETE FROM ipqc_modify_datas WHERE 1=1 AND PartNumber = '$PartNumber' AND MoldNumber = '$MoldNumber' AND MeasureDataTime between '$d1' AND '$d2' ";
    $DeleteSQ_query = mysqli_query($connect_sq, $DeleteSQ_sql) or die("警告 ： 刪除SQ數據失敗");

    while ($AllCTDelete = mysqli_fetch_assoc($AllCTDelete_query)) {
      $Ticket_Number = $AllCTDelete['Ticket_Number'];

      $DeleteContents_sql   = "DELETE FROM modify_main_table WHERE 1=1 AND Ticket_Number = '$Ticket_Number'";
      $DeleteContents_query = mysqli_query($connect_stmp, $DeleteContents_sql) or die("警告 ： 刪除main table數據失敗");
      $DeleteDatas_sql      = "DELETE FROM modify_measure_data_1cav WHERE 1=1 AND Ticket_Number = '$Ticket_Number'";
      $DeleteDatas_query    = mysqli_query($connect_stmp, $DeleteDatas_sql) or die("警告 ： 刪除measuredatas數據失敗");
    }
    //Insert modify_visual_inspection
    $VisualInsert_sql   = "REPLACE INTO modify_visual_inspection SELECT * from visual_inspection WHERE 1=1 AND Part_Number = '$PartNumber' AND Mold_Number = '$MoldNumber' AND concat(Date,'-',Time) between '$d1' AND '$d2' ";
    $VisualInsert_query = mysqli_query($connect_stmp, $VisualInsert_sql) or die("警告 ： Insert modify_visual_inspection失敗");
    
    //Insert Measure Datas
    $AllTNSearch_sql   = "SELECT * FROM main_table WHERE 1=1 AND Part_Number_V LIKE '$PartNumber%'  AND Mold_Number = '$MoldNumber' AND concat(End_Date,'-',End_Time) between '$d1' AND '$d2' ORDER BY concat(End_Date,'-',End_Time)";
    //echo $AllTNSearch_sql;
    $AllTNSearch_query = mysqli_query($connect_stmp, $AllTNSearch_sql) or die("警告 ： 搜尋Ticket Number失敗");
    while ($AllTNSearch = mysqli_fetch_assoc($AllTNSearch_query)) {
        //$Project_Name_m           = $AllTNSearch['Project_Name'];
      $Part_Number_V          = $AllTNSearch['Part_Number_V'];
      $Mold_Number            = $AllTNSearch['Mold_Number'];
      $Ticket_Number          = $AllTNSearch['Ticket_Number'];
      $Ticket_Number_array1[] = $VisualSearch['Ticket_Number'];
      $Start_Date             = $AllTNSearch['Start_Date'];
      $Start_Time             = $AllTNSearch['Start_Time'];
      $End_Date               = $AllTNSearch['End_Date'];
      $End_Time               = $AllTNSearch['End_Time'];
      $End_DateTime           = $End_Date . " " . $End_Time;
      $Status                 = $AllTNSearch['Status'];
      $Remark                 = $AllTNSearch['Remark'];
      $Spec_Status            = $AllTNSearch['Spec_Status'];
      $Cavity                 = $AllTNSearch['Cavity'];
      $Re                     = $AllTNSearch['Re'];

        //for Insert sq system
      if ($End_Time >= '00:00:00' && $End_Time <= '07:59:59') {
        $DateTime = date('Y-m-d', strtotime($End_Date) - 60 * 60 * 24);
      } else {
        $DateTime = $End_Date;
      }
      if ($End_Time >= '08:00:00' && $End_Time <= '09:59:59'){
       $PhaseNumber='D-1';
     }
     else if ($End_Time >= '10:00:00' && $End_Time <= '11:59:59'){
       $PhaseNumber='D-2';
     }
     else if ($End_Time >= '12:00:00' && $End_Time <= '13:59:59'){
       $PhaseNumber='D-3';
     }
     else if ($End_Time >= '14:00:00' && $End_Time <= '15:59:59'){
       $PhaseNumber='D-4';
     }
     else if ($End_Time >= '16:00:00' && $End_Time <= '17:59:59'){
       $PhaseNumber='D-5';
     }
     else if ($End_Time >= '18:00:00' && $End_Time <= '19:59:59'){
       $PhaseNumber='D-6';
     }
     else if ($End_Time >= '20:00:00' && $End_Time <= '21:59:59'){
       $PhaseNumber='N-1';
     }
     else if ($End_Time >= '22:00:00' && $End_Time <= '23:59:59'){
       $PhaseNumber='N-2';
     }
     else if ($End_Time >= '00:00:00' && $End_Time <= '01:59:59'){
       $PhaseNumber='N-3';
     }
     else if ($End_Time >= '02:00:00' && $End_Time <= '03:59:59'){
       $PhaseNumber='N-4';
     }
     else if ($End_Time >= '04:00:00' && $End_Time <= '05:59:59'){
       $PhaseNumber='N-5';
     }
     else if ($End_Time >= '06:00:00' && $End_Time <= '07:59:59'){
       $PhaseNumber='N-6';
     }

//Insert Main Table數據
     $Insert_maintable = "REPLACE INTO `modify_main_table` (`Project_Name`, `Part_Number_V`, `Mold_Number`, `Ticket_Number`, `Start_Date`, `Start_Time`, `End_Date`, `End_Time`, `Status`, `Remark`, `Spec_Status`, `Cavity`, `Re`) VALUES ('" . $ProjectName . "', '" . $PartNumber . "', '" . $Mold_Number . "', '" . $Ticket_Number . "', '" . $Start_Date . "', '" . $Start_Time . "', '" . $End_Date . "', '" . $End_Time . "', '" . $Status . "', '" . $Remark . "', '" . $Spec_Status . "', '" . $Cavity . "', '" . $Re . "')";
     $query            = mysqli_query($connect_stmp, $Insert_maintable) or die("警告 " . $Ticket_Number . " : Modify_Main_table上傳資料庫失敗");

     $SpecSearch_sql   = "SELECT * FROM modify_spec_welding WHERE PartNumber = '" . $PartNumber . "' AND MoldNumber = '$MoldNumber' ";
     $SpecSearch_query = mysqli_query($connect_spec, $SpecSearch_sql) or die("警告 ： 搜尋Modify Spec失敗");
     while ($AllInfo = mysqli_fetch_array($SpecSearch_query)) {
      $Data_array       = array();
      $Spec_Project_Name       = $AllInfo['ProjectName'];
      $PartNumber       = $AllInfo['PartNumber'];
      $Rev              = $AllInfo['Rev'];
      $DimNO            = $AllInfo['DimNO'];
      $Remark           = strtoupper($AllInfo['Remark']);
      $DimSpec          = $AllInfo['DimSpec'];
      $DimUpper         = $AllInfo['DimUpper'];
      $DimLower         = $AllInfo['DimLower'];
      $Expect_UpperData = $AllInfo['Expect_UpperData'];
      $Expect_LowerData = $AllInfo['Expect_LowerData'];
      $Expect_UpperStdv = ($AllInfo['Expect_UpperStdv']);
      $Expect_LowerStdv = ($AllInfo['Expect_LowerStdv']);
      $Expect_Result    = $AllInfo['Expect_Result'];
      $Expect_Data      = rand($Expect_LowerData * 1000, $Expect_UpperData * 1000) / 1000;
      $Expect_Stdv      = rand($Expect_LowerStdv * 1000, $Expect_UpperStdv * 1000) / 1000;

      $A = 0;
      while ($A < 6) {
        $val = round(nrand($Expect_Data, $Expect_Stdv), 4);

        if ($Expect_Result == 'OK') {
          if ($val <= $DimUpper && $val >= $DimLower) {
            $Data_array[] = $val;
            $A++;
          }

        } else {
          $Data_array[] = $val;
          $A++;
        }
      }

            //Insert CPK優化數據
      if ($Remark == 'CPK') {
        $B = 0;
        for ($i = 1; $i <= 5; $i++) {
          $DimNO_CPK    = $DimNO . "_CPK" . $i;
          $Measure_Data = $Data_array[$B];
          if ($Measure_Data > $DimUpper or $Measure_Data < $DimLow) {
            $Measure_Result = 'NG';
          } else {
            $Measure_Result = 'OK';
          }

          $InsertModifyData_sql   = "REPLACE INTO `modify_measure_data_1cav` (`Ticket_Number`, `FAI_Number`, `Nominal_Dim`, `Upper_Dim`, `Lower_Dim`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Result`, `Expect_Data`, `Expect_Stdv`, `Measure_Value`, `Measure_Datetime`, `Measure_Result`) VALUES ('" . $Ticket_Number . "', '" . $DimNO_CPK . "', '" . $DimSpec . "', '" . $DimUpper . "', '" . $DimLower . "', '" . $Expect_UpperData . "', '" . $Expect_LowerData . "', '" . $Expect_UpperStdv . "', '" . $Expect_LowerStdv . "', '" . $Expect_Result . "', '" . $Expect_Data . "', '" . $Expect_Stdv . "', '" . $Measure_Data . "', '" . $End_DateTime . "', '" . $Measure_Result . "')";
          $InsertModifyData_query = mysqli_query($connect_stmp, $InsertModifyData_sql) or die("警告 " . $Ticket_Number . " : ModifyCPKdatas數據上傳資料庫失敗");
          $B++;
        }

                //Insert sq system
        $InsertSQModifyData_sql   = "REPLACE INTO `ipqc_modify_datas` (`MeasureDataTime`,`DateTime`, `PartNumber`, `PartDescription`, `LineNumber`, `PhaseNumber`, `TicketNumber`, `DimNO`, `MoldNumber`, `Type`, `CavNumber`, `Sample1`, `Sample2`, `Sample3`, `Sample4`, `Sample5`) VALUES ('" . $End_DateTime . "','" . $DateTime . "', '" . $PartNumber . "', '" . $ProjectName . "', 'NA', '" . $PhaseNumber . "', '" . $Ticket_Number . "', '" . $DimNO . "', '" . $Mold_Number . "', 'Stamping', 'NA', '" . $Data_array[0] . "', '" . $Data_array[1] . "', '" . $Data_array[2] . "', '" . $Data_array[3] . "', '" . $Data_array[4] . "')";
        //echo $InsertSQModifyData_sql;
        $InsertSQModifyData_query = mysqli_query($connect_sq, $InsertSQModifyData_sql) or die("警告 " . $Ticket_Number . " : SQModifyCPKdatas數據上傳資料庫失敗");
      } else {
//Insert優化數據
        $Measure_Data = $Data_array[0];
        if ($Measure_Data > $DimUpper or $Measure_Data < $DimLow) {
          $Measure_Result = 'NG';
        } else {
          $Measure_Result = 'OK';
        }

        $InsertModifyData_sql   = "REPLACE INTO `modify_measure_data_1cav` (`Ticket_Number`, `FAI_Number`, `Nominal_Dim`, `Upper_Dim`, `Lower_Dim`, `Expect_UpperData`, `Expect_LowerData`, `Expect_UpperStdv`, `Expect_LowerStdv`, `Expect_Data`, `Expect_Stdv`, `Measure_Value`, `Measure_Datetime`, `Measure_Result`) VALUES ('" . $Ticket_Number . "', '" . $DimNO . "', '" . $DimSpec . "', '" . $DimUpper . "', '" . $DimLower . "', '" . $Expect_UpperData . "', '" . $Expect_LowerData . "', '" . $Expect_UpperStdv . "', '" . $Expect_LowerStdv . "', '" . $Expect_Data . "', '" . $Expect_Stdv . "', '" . $Measure_Data . "', '" . $End_DateTime . "', '" . $Measure_Result . "')";
        //echo $InsertModifyData_sql;
        $InsertModifyData_query = mysqli_query($connect_stmp, $InsertModifyData_sql) or die("警告 " . $Ticket_Number . " : Modifydatas數據上傳資料庫失敗");

                //Insert sq system
        $InsertSQModifyData_sql   = "REPLACE INTO `ipqc_modify_datas` (`MeasureDataTime`,`DateTime`, `PartNumber`, `PartDescription`, `LineNumber`, `PhaseNumber`, `TicketNumber`, `DimNO`, `MoldNumber`, `Type`, `CavNumber`, `Sample1`, `Sample2`, `Sample3`, `Sample4`, `Sample5`) VALUES ('" . $End_DateTime . "','" . $DateTime . "', '" . $PartNumber . "', '" . $ProjectName_S . "', 'NA', '" . $PhaseNumber . "', '" . $Ticket_Number . "', '" . $DimNO . "', '" . $Mold_Number . "', 'Stamping', 'NA', '" . $Data_array[0] . "', '', '', '', '')";
        $InsertSQModifyData_query = mysqli_query($connect_sq, $InsertSQModifyData_sql) or die("警告 " . $Ticket_Number . " : SQModifyCPKdatas數據上傳資料庫失敗");
      }
    }
  }
}

}
}
echo "【 優化數據上傳資料庫成功 】</br>";
$end_time = date("H")*3600+date("i")*60+date("s");

//echo "start_time：".$start_time."秒<br/>";
//echo "end_time：".$end_time."秒<br/>";
echo "報告生成日期:".$NowDate."<br/>";
$time_total = $end_time - $start_time;
echo "執行了：".$time_total."秒<br/>";
