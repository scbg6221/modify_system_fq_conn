<?php
session_start();
error_reporting(0);
include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';

if (!isset($_SESSION)) {session_start();}
mysqli_select_db($connect_spec, $database_spec);
mysqli_select_db($connect_asm, $database_asm);
mysqli_select_db($connect_mold1, $database_mold1);
mysqli_select_db($connect_mold2, $database_mold2);

if ($_POST['DeptF'] == '組裝') {
    $ProgramName_T = "ModifyData_Upload-2_0.php?A=T";
    $ProgramName_F = "ModifyData_Upload-2_0.php?A=F";
} elseif ($_POST['DeptF'] == '沖壓') {
    $ProgramName_T = "ModifyData_Upload-3_0.php?A=T";
    $ProgramName_F = "ModifyData_Upload-3_0.php?A=F";
} else {
    $ProgramName_T = "ModifyData_Upload-4_0.php?A=T";
    $ProgramName_F = "ModifyData_Upload-4_0.php?A=F";
}
?>

<!DOCTYPE HTML><head>
<meta charset="utf-8">
<title>Untitled Document</title></head>

<script>

function check(){
  var ProgramName_T = '<?php echo $ProgramName_T ?>';
  var ProgramName_F = '<?php echo $ProgramName_F ?>';

  if(confirm ("此時段優化外觀/尺寸已經存在!!是否要複寫?")){
    location.href='<?php echo $ProgramName_T ?>';
 }
  else {
    location.href='<?php echo $ProgramName_F ?>';
  }
}
</script>
<style type="text/css">
</style>
</head>
<body>

<?php
if ($_POST['DeptF'] == '組裝') {
    if (!empty($_POST['DeptF']) && !empty($_POST['PartNumberF']) && !empty($_POST['StartDateF']) && !empty($_POST['EndDateF'])) {

        $Dept                    = $_POST['DeptF'];
        $PartNumber              = $_POST['PartNumberF'];
        $d1                      = $_POST['StartDateF'];
        $d2                      = $_POST['EndDateF'];
        $_SESSION['DeptF']       = $_POST['DeptF'];
        $_SESSION['PartNumberF'] = $_POST['PartNumberF'];
        $_SESSION['StartDateF']  = $_POST['StartDateF'];
        $_SESSION['EndDateF']    = $_POST['EndDateF'];
        //$_SESSION['StartDateF']  = str_replace(" ", "-", $_POST['StartDateF']);
        //$_SESSION['EndDateF']    = str_replace(" ", "-", $_POST['EndDateF']);

        $check       = "SELECT PartNumber FROM visual_measuredata WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' UNION SELECT PartNumber FROM visual_glue_measuredata WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' UNION SELECT PartNumber FROM visual_first_measuredata WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' UNION SELECT PartNumber FROM 3f_visual_measuredata WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' UNION SELECT PartNumber FROM 3f_visual_glue_measuredata WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' UNION SELECT PartNumber FROM 3f_visual_first_measuredata WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' UNION SELECT PartNumber FROM measurecontent WHERE 1=1 AND PartNumber = '$PartNumber' AND MeasureEndTime between '$d1' AND '$d2' UNION SELECT PartNumber FROM 3f_measurecontent WHERE 1=1 AND PartNumber = '$PartNumber' AND MeasureEndTime between '$d1' AND '$d2'";
        $check_query = mysqli_query($connect_asm, $check) or die("警告 ： 搜尋visual & measuredata失敗");
        //echo $check;
        if (mysqli_fetch_array($check_query)) {
            $check_modify = "SELECT PartNumber FROM modify_visual_measuredata WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' UNION SELECT PartNumber FROM modify_visual_glue_measuredata WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' UNION SELECT PartNumber FROM modify_visual_first_measuredata WHERE 1=1 AND PartNumber = '$PartNumber' AND DateTime between '$d1' AND '$d2' UNION SELECT PartNumber FROM modify_measurecontent WHERE 1=1 AND PartNumber = '$PartNumber' AND MeasureEndTime between '$d1' AND '$d2'";
            //echo $check_modify;
            $checkmodify_query      = mysqli_query($connect_asm, $check_modify) or die("警告 ： 搜尋modify visual & measuredata失敗");
            $checkmodify_data1      = mysqli_fetch_array($checkmodify_query);
            $checkmodify_PartNumber = $checkmodify_data1['PartNumber'];

            if ($checkmodify_PartNumber == $PartNumber) {
                echo "<script>check();</script>";
            } else {
                header("Location: ModifyData_Upload-2_1.php");
            }
        } else {echo "此時段無外觀/尺寸量測數據!!";}
    } else {echo "*選項為必選項目!";}

} elseif ($_POST['DeptF'] == '沖壓') {
    if (!empty($_POST['DeptF']) && !empty($_POST['PartNumberF']) && !empty($_POST['StartDateF']) && !empty($_POST['EndDateF'])) {

        $Dept                    = $_POST['DeptF'];
        $PartNumber              = $_POST['PartNumberF'];
        $d1                      = str_replace(" ", "-", $_POST['StartDateF']);
        $d2                      = str_replace(" ", "-", $_POST['EndDateF']);
        $_SESSION['DeptF']       = $_POST['DeptF'];
        $_SESSION['PartNumberF'] = $_POST['PartNumberF'];
        $_SESSION['StartDateF']  = str_replace(" ", "-", $_POST['StartDateF']);
        $_SESSION['EndDateF']    = str_replace(" ", "-", $_POST['EndDateF']);

        $check = "SELECT Part_Number_V,Mold_Number FROM main_table WHERE 1=1 AND Part_Number_V LIKE '$PartNumber%' AND concat(End_Date,'-',End_Time) between '$d1' AND '$d2' UNION SELECT Part_Number,Mold_Number FROM visual_inspection WHERE 1=1 AND Part_Number = '$PartNumber' AND concat(Date,'-',Time) between '$d1' AND '$d2'";
        //echo $check;
        $check_query = mysqli_query($connect_stmp, $check) or die("警告 ： 搜尋visual & measuredata失敗");

        if (mysqli_fetch_array($check_query)) {
            $check_modify = "SELECT Part_Number_V,Mold_Number FROM modify_main_table WHERE 1=1 AND Part_Number_V  = '$PartNumber' AND concat(End_Date,'-',End_Time) between '$d1' AND '$d2' UNION SELECT Part_Number,Mold_Number FROM modify_visual_inspection WHERE 1=1 AND Part_Number = '$PartNumber' AND concat(Date,'-',Time) between '$d1' AND '$d2'";
            //echo $check_modify;
            $checkmodify_query = mysqli_query($connect_stmp, $check_modify) or die("警告 ： 搜尋modify visual & measuredata失敗");
            $checkmodify_data1 = mysqli_fetch_array($checkmodify_query);
            $check_PartNumber1 = $checkmodify_data1['Part_Number_V'];
            $check_PartNumber2 = $checkmodify_data1['Part_Number'];
            $check_MoldNumber  = $checkmodify_data1['Mold_Number'];

            if ($check_PartNumber1 == $PartNumber or $check_PartNumber2 == $PartNumber) {
                echo "<script>check();</script>";
            } else {
                header("Location: ModifyData_Upload-3_1.php");
            }
        } else {echo "此時段無外觀/尺寸量測數據!!";}
    } else {
        echo "*選項為必選項目!";
    }

} else {
    if (!empty($_POST['DeptF']) && !empty($_POST['PartNumberF']) && !empty($_POST['StartDateF']) && !empty($_POST['EndDateF'])) {

        $Dept                    = $_POST['DeptF'];
        $PartNumber              = $_POST['PartNumberF'];
        $d1                      = str_replace(" ", "-", $_POST['StartDateF']);
        $d2                      = str_replace(" ", "-", $_POST['EndDateF']);
        $_SESSION['DeptF']       = $_POST['DeptF'];
        $_SESSION['PartNumberF'] = $_POST['PartNumberF'];
        $_SESSION['StartDateF']  = str_replace(" ", "-", $_POST['StartDateF']);
        $_SESSION['EndDateF']    = str_replace(" ", "-", $_POST['EndDateF']);

        $check1 = "SELECT Part_Number_V,Mold_Number FROM m_main_table WHERE 1=1 AND Part_Number_V  LIKE '$PartNumber%' AND concat(End_Date,'-',End_Time) between '$d1' AND '$d2' UNION SELECT Part_Number,Mold_Number FROM visual_inspection WHERE 1=1 AND Part_Number = '$PartNumber' AND concat(Date,'-',Time) between '$d1' AND '$d2'";
        //echo $check1;
        $check1_query = mysqli_query($connect_mold1, $check1) or die("警告 ： 搜尋PJ visual & measuredata失敗");
        $check2       = "SELECT PartNumber,PartMold FROM measurecontent where 1=1 AND PartNumber  = '$PartNumber' AND Status='9' AND MeasureEndTime between '$d1' AND '$d2'";
        //echo $check2;
        $check2_query = mysqli_query($connect_mold2, $check2) or die("警告 ： 搜尋GV visual & measuredata失敗");

        if (mysqli_fetch_array($check1_query) or mysqli_fetch_array($check2_query)) {
            $check_modify      = "SELECT Part_Number_V,Mold_Number FROM modify_m_main_table WHERE 1=1 AND Part_Number_V  = '$PartNumber' AND concat(End_Date,'-',End_Time) between '$d1' AND '$d2' UNION SELECT Part_Number,Mold_Number FROM modify_visual_inspection WHERE 1=1 AND Part_Number = '$PartNumber' AND concat(Date,'-',Time) between '$d1' AND '$d2'";
            $checkmodify_query = mysqli_query($connect_mold1, $check_modify) or die("警告 ： 搜尋modify visual & measuredata失敗");
            $checkmodify_data1 = mysqli_fetch_array($checkmodify_query);
            $check_PartNumber1 = $checkmodify_data1['Part_Number_V'];
            $check_PartNumber2 = $checkmodify_data1['Part_Number'];
            $check_MoldNumber  = $checkmodify_data1['Mold_Number'];

            if ($check_PartNumber1 == $PartNumber or $check_PartNumber2 == $PartNumber) {
                echo "<script>check();</script>";
            } else {
                header("Location: ModifyData_Upload-4_1.php");
            }
        } else {echo "此時段無外觀/尺寸量測數據!!";}

    } else {echo "*選項為必選項目!";}
}

?>
