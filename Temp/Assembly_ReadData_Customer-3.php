<?php
/*
 * Copyright (C) 2014 Foxlink SCBG CAE Dept. All rights reserved
 * Project: Projector System
 * File Name: Measure Data-2
 * Function: Result of Measure Data
 * Author: Angel Wang
 * --------------------------------------------------
 * Rev: 1.2 Date: PM 04:34 2014/05/29 Modifier: Angel Wang
 * --------------------------------------------------
 */

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
error_reporting(0);
if (!isset($_SESSION)) {session_start();}

$Date       = $_REQUEST['Date1'];
$Phase      = $_REQUEST['Phase'];
$PartName   = $_REQUEST['PartName'];
$LineNumber = $_REQUEST['Line'];
$PartNumber = $_REQUEST['Part'];
$data_table = $_REQUEST['Datatable'];
$d1         = date('Y-m-d', strtotime($Date));
$d2         = date('Y-m-d', strtotime($Date) + 60 * 60 * 24);

if ($PartName) {$qa = "AND modify_measurecontent.PartDescription='$PartName' ";}
if ($LineNumber) {$qb = "AND modify_measurecontent.LineNumber='$LineNumber' ";}
if ($PartNumber) {$qc = "AND modify_measurecontent.PartNumber='$PartNumber' ";}
if ($Phase) {$qd = "AND modify_measurecontent.PhaseNumber='$Phase' ";}
if ($Date) {$qe = "AND MeasureEndTime between '$d1-08:00:00' and '$d2-08:00:00' ";}

mysqli_select_db($connect_asm, $database_asm);
$query_listoutF =
    "SELECT
		modify_measurecontent.LineNumber,
		modify_measurecontent.PartNumber,
		modify_measurecontent.ServiceNumber,
        modify_measurecontent.PartDescription,
        modify_measurecontent.PhaseNumber,
        modify_measurecontent.MeasureEndTime,
		" . $data_table . ".DimNO,
		" . $data_table . ".DimNOOrder,
	    " . $data_table . ".DimSpec,
		" . $data_table . ".DimUpper,
		" . $data_table . ".DimLower,
		" . $data_table . ".Sample1,
		" . $data_table . ".Sample2,
		" . $data_table . ".Sample3,
        " . $data_table . ".Sample4,
        " . $data_table . ".Sample5
    FROM modify_measurecontent, " . $data_table . " WHERE 1=1  " . $qa . " " . $qb . " " . $qc . " " . $qd . " " . $qe . " AND DimNO NOT like 'ORT%' AND " . $data_table . ".ServiceNumber=modify_measurecontent.ServiceNumber ORDER BY MeasureEndTime ASC";

$listoutF = mysqli_query($connect_asm, $query_listoutF);
?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<script src="../../Public/library/Other/Sorttable.js"></script>
<script type="text/javascript">

  function result()
{
   document['form1'].action = "Assembly_ReadData_Customer-4.php";
   document['form1'].target = '_self';
   document['form1'].submit();
}

</script>
<link rel="stylesheet" type="text/css" href="CSS/ALL_CSS.css">
</head>
<body>
<form id="form1" name="form1" method="post" >

<div class="Measure-Data-table-2">
<table id="Measure-Data-table-2" class="sortable">
<thead>
<input type='button' name='submitA' id='submitA'  value='下載報告' class='ReadData-BT' onClick='result();'/>
<th><div align="center">線別號</div></th>
<th><div align="center">料號</div></th>
<th><div align="center">FAI號</div></th>
<th><div align="center">尺寸</div></th>
<th><div align="center">上限</div></th>
<th><div align="center">下限</div></th>
<th><div align="center">量測值-1</div></th>
<th><div align="center">量測值-2</div></th>
<th><div align="center">量測值-3</div></th>
<th><div align="center">量測值-4</div></th>
<th><div align="center">量測值-5</div></th>
<th><div align="center">量測時間</div></th>
<th><div align="center">判定</div></th>
</thead>
  <div align="center"></div>
<tbody>

<?php

while ($listout = mysqli_fetch_assoc($listoutF)) {
    $ServiceNumber  = $listout['ServiceNumber'];
    $DimUpper       = $listout['DimUpper'];
    $DimLower       = $listout['DimLower'];
    $MeasureResult1 = $MeasureResult2 = $MeasureResult3 = $MeasureResult4 = $MeasureResult5 = '';

    if ($listout['Sample1'] > $DimUpper or $listout['Sample1'] < $DimLower) {
        $MeasureResult1 = 'NG';
    }

    if ($listout['Sample2'] > $DimUpper or $listout['Sample2'] < $DimLower) {
        $MeasureResult2 = 'NG';
    }

    if ($listout['Sample3'] > $DimUpper or $listout['Sample3'] < $DimLower) {
        $MeasureResult3 = 'NG';

    }

    if ($listout['Sample4'] > $DimUpper or $listout['Sample4'] < $DimLower) {
        $MeasureResult4 = 'NG';

    }

    if ($listout['Sample5'] > $DimUpper or $listout['Sample5'] < $DimLower) {
        $MeasureResult5 = 'NG';
    }

    if ($MeasureResult1 == 'NG' or $MeasureResult2 == 'NG' or $MeasureResult3 == 'NG' or $MeasureResult4 == 'NG' or $MeasureResult5 == 'NG') {
        $color         = "<font color='#EE0000'>";
        $MeasureResult = 'NG';
    } else {
        $color         = "";
        $MeasureResult = 'OK';
    }

    echo "<tr>";
    echo "<td>" . $color . $listout['LineNumber'] . "</td>";
    echo "<td>" . $color . $listout['PartNumber'] . "</td>";
    echo "<td>" . $color . $listout['DimNO'] . "</td>";
    echo "<td>" . $color . $listout['DimSpec'] . "</td>";
    echo "<td>" . $color . $DimUpper . "</td>";
    echo "<td>" . $color . $DimLower . "</td>";
    echo "<td>" . $color . $listout['Sample1'] . "</td>";
    echo "<td>" . $color . $listout['Sample2'] . "</td>";
    echo "<td>" . $color . $listout['Sample3'] . "</td>";
    echo "<td>" . $color . $listout['Sample4'] . "</td>";
    echo "<td>" . $color . $listout['Sample5'] . "</td>";
    echo "<td>" . $color . $listout['MeasureEndTime'] . "</td>";
    echo "<td>" . $color . $MeasureResult . "</td>";
    echo "</tr>";

}
echo "
   <div style='float:left'>
    <input type='hidden' name='ServiceNumber' id='ServiceNumber'  value='" . $ServiceNumber . "' class='ReadData-2'>
   </div>"
?>
</tbody>
</table>
</form>
</body>
</html>


