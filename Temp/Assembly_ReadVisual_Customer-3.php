<?php
/*
 * Copyright (C) 2014 Foxlink SCBG CAE Dept. All rights reserved
 * Project: Projector System
 * File Name: Measure Data-2
 * Function: Result of Measure Data
 * Author: Angel Wang
 * --------------------------------------------------
 * Rev: 1.2 Date: PM 04:34 2014/05/29 Modifier: Angel Wang
 * --------------------------------------------------
 */

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
//error_reporting(0);
if (!isset($_SESSION)) {session_start();}

$Date                   = $_REQUEST['Date'];
$Phase                  = $_REQUEST['Phase'];
$Rev                    = $_REQUEST['Rev'];
$PartNumber             = $_REQUEST['PartNumber'];
$day_night              = $_REQUEST['day_night'];
$plin                   = $_REQUEST['plin'];
$_SESSION['Date']       = $_REQUEST['Date'];
$_SESSION['Phase']      = $_REQUEST['Phase'];
$_SESSION['Rev']        = $_REQUEST['Rev'];
$_SESSION['PartNumber'] = $_REQUEST['PartNumber'];
$_SESSION['day_night']  = $_REQUEST['day_night'];
$_SESSION['plin']       = $_REQUEST['plin'];

if ($Phase < '08:00:00') {$Date = date('Y-m-d', strtotime($Date) + 60 * 60 * 24);}
$DateTime = $Date . ' ' . $Phase;

if ($PartNumber) {$qc = "AND PartNumber='$PartNumber' ";}
if ($Rev) {$qd = "AND Rev='$Rev' ";}
if ($plin) {$qp = "AND Productline='$plin' ";}
if ($DateTime) {$qe = "AND DateTime='$DateTime' ";}
if ($day_night) {$qb = "AND day_night='$day_night' ";}

mysqli_select_db($connect_asm, $database_asm);
$query_listoutF = "SELECT * FROM `modify_visual_measuredata` WHERE 1=1  " . $qb . " " . $qc . " " . $qd . " " . $qp . " " . $qe . "  ORDER BY id ASC";
$listoutF       = mysqli_query($connect_asm, $query_listoutF);
//echo $query_listoutF;

$query_listoutT = "SELECT * FROM `modify_visual_measuredata` WHERE 1=1  " . $qb . " " . $qc . " " . $qd . " " . $qp . " " . $qe . "  ORDER BY id ASC";
$listoutT       = mysqli_query($connect_asm, $query_listoutT) or die(mysqli_error());
$listoutT       = mysqli_fetch_assoc($listoutT);
$Ticket         = $listoutT['Ticket'];

if ($Ticket) {$qf = "AND Ticket='$Ticket' ";}

$query_listoutF2 = "SELECT * FROM `modify_visual_glue_measuredata` WHERE 1=1  " . $qb . " " . $qc . " " . $qd . " " . $qp . " " . $qf . " ORDER BY id ASC";
$listoutF2       = mysqli_query($connect_asm, $query_listoutF2);
//echo $query_listoutF2;
$num2 = mysqli_num_rows($listoutF2);

//echo $num2;
?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<script src="../../Public/library/Other/Sorttable.js"></script>
<script type="text/javascript">

  function result()
{
   document['form1'].action = "Assembly_ReadVisual_Customer-4.php";
   document['form1'].target = '_self';
   document['form1'].submit();
}

</script>
<link rel="stylesheet" type="text/css" href="CSS/ALL_CSS.css">
</head>
<body>
<form id="form1" name="form1" method="post" >

<div class="Measure-Data-table-2">
<table id="Measure-Data-table-2" class="sortable">
<thead>
<input type='button' name='submitA' id='submitA'  value='下載報告' class='ReadData-BT' onClick='result();'/>
<th><div align="center">檢查工站</div></th>
<th><div align="center">物料</div></th>
<th><div align="center">物料料號</div></th>
<th><div align="center">檢測項目</div></th>
<th><div align="center">上限</div></th>
<th><div align="center">下限</div></th>
<th><div align="center">節次一</div></th>
<th><div align="center">節次二</div></th>
<th><div align="center">節次三</div></th>
<th><div align="center">節次四</div></th>
<th><div align="center">節次五</div></th>
<th><div align="center">判定</div></th>
<th><div align="center">備註</div></th>
</thead>
  <div align="center"></div>
<tbody>

<?php
//checkstop sort  Part_Number_V  checkitem up_lim  down_lim  Measure_Value-1  Measure_Value-2  Measure_Value-3 Measure_Value-4  Measure_Value-5 判定 Remark
$color         = "";
$ServiceNumber = "";

while ($listout = mysqli_fetch_assoc($listoutF)) {
    if ($listout['up_lim'] != '') {
        $DimUpper       = $listout['up_lim'];
        $DimLow         = $listout['down_lim'];
        $MeasureResult1 = $MeasureResult2 = $MeasureResult3 = $MeasureResult4 = $MeasureResult5 = '';

        if ($listout['Measure_Value-1'] > $DimUpper or $listout['Measure_Value-1'] < $DimLow) {
            if (!empty($listout['Measure_Value-1'])) {
                $MeasureResult1 = 'NG';}
        }

        if ($listout['Measure_Value-2'] > $DimUpper or $listout['Measure_Value-2'] < $DimLow) {
            if (!empty($listout['Measure_Value-2'])) {
                $MeasureResult2 = 'NG';}
        }

        if ($listout['Measure_Value-3'] > $DimUpper or $listout['Measure_Value-3'] < $DimLow) {
            if (!empty($listout['Measure_Value-3'])) {
                $MeasureResult3 = 'NG';}

        }

        if ($listout['Measure_Value-4'] > $DimUpper or $listout['Measure_Value-4'] < $DimLow) {
            if (!empty($listout['Measure_Value-4'])) {
                $MeasureResult4 = 'NG';}

        }

        if ($listout['Measure_Value-5'] > $DimUpper or $listout['Measure_Value-5'] < $DimLow) {
            if (!empty($listout['Measure_Value-5'])) {
                $MeasureResult5 = 'NG';}
        }

        if (!empty($listout['Measure_Value-1']) or !empty($listout['Measure_Value-2']) or !empty($listout['Measure_Value-3']) or !empty($listout['Measure_Value-4']) or !empty($listout['Measure_Value-5'])) {

            if ($MeasureResult1 == 'NG' or $MeasureResult2 == 'NG' or $MeasureResult3 == 'NG' or $MeasureResult4 == 'NG' or $MeasureResult5 == 'NG') {
                $color         = "<font color='#EE0000'>";
                $MeasureResult = 'NG';
            } else {
                $color         = "";
                $MeasureResult = 'OK';
            }
        } else { $MeasureResult = '';}

        echo "<tr>";
        echo "<td>" . $color . $listout['checkstop'] . "</td>";
        echo "<td>" . $color . $listout['sort'] . "</td>";
        echo "<td>" . $color . $listout['Part_Number_V'] . "</td>";
        echo "<td>" . $color . $listout['checkitem'] . "</td>";
        echo "<td>" . $color . $listout['up_lim'] . "</td>";
        echo "<td>" . $color . $listout['down_lim'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-1'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-2'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-3'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-4'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-5'] . "</td>";
        echo "<td>" . $color . $MeasureResult . "</td>";
        echo "<td>" . $color . $listout['Remark'] . "</td>";
        echo "</tr>";

        $color = "";
    } else {

        $MeasureResult1 = $MeasureResult2 = $MeasureResult3 = $MeasureResult4 = $MeasureResult5 = '';

        if ($listout['Measure_Value-1'] != 'OK' && !empty($listout['Measure_Value-1'])) {
            $MeasureResult1 = 'NG';
        }

        if ($listout['Measure_Value-2'] != 'OK' && !empty($listout['Measure_Value-2'])) {
            $MeasureResult2 = 'NG';
        }

        if ($listout['Measure_Value-3'] != 'OK' && !empty($listout['Measure_Value-3'])) {
            $MeasureResult3 = 'NG';

        }

        if ($listout['Measure_Value-4'] != 'OK' && !empty($listout['Measure_Value-4'])) {
            $MeasureResult4 = 'NG';

        }

        if ($listout['Measure_Value-5'] != 'OK' && !empty($listout['Measure_Value-5'])) {
            $MeasureResult5 = 'NG';
        }

        if (!empty($listout['Measure_Value-1']) or !empty($listout['Measure_Value-2']) or !empty($listout['Measure_Value-3']) or !empty($listout['Measure_Value-4']) or !empty($listout['Measure_Value-5'])) {
            if ($MeasureResult1 == 'NG' or $MeasureResult2 == 'NG' or $MeasureResult3 == 'NG' or $MeasureResult4 == 'NG' or $MeasureResult5 == 'NG') {
                $color         = "<font color='#EE0000'>";
                $MeasureResult = 'NG';
            } else {
                $color         = "";
                $MeasureResult = 'OK';
            }

        } else { $MeasureResult = '';}

        echo "<tr>";
        echo "<td>" . $color . $listout['checkstop'] . "</td>";
        echo "<td>" . $color . $listout['sort'] . "</td>";
        echo "<td>" . $color . $listout['Part_Number_V'] . "</td>";
        echo "<td>" . $color . $listout['checkitem'] . "</td>";
        echo "<td>" . $color . $listout['up_lim'] . "</td>";
        echo "<td>" . $color . $listout['down_lim'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-1'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-2'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-3'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-4'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-5'] . "</td>";
        echo "<td>" . $color . $MeasureResult . "</td>";
        echo "<td>" . $color . $listout['Remark'] . "</td>";
        echo "</tr>";

        $color = "";
    }

}
echo "
   <div style='float:left'>
    <input type='hidden' name='ServiceNumber' id='ServiceNumber'  value='" . $ServiceNumber . "' class='ReadData-2'>
   </div>"
?>
</tbody>
</table>
<br>
膠量:<?php if ($num2 == 0) {echo '膠量尚未量測';}?><br>
<div class="Measure-Data-table-2">
<table id="Measure-Data-table-2" class="sortable">
<thead>
<th><div align="center">檢查工站</div></th>
<th><div align="center">上限</div></th>
<th><div align="center">下限</div></th>
<th colspan=5><div align="center">機台一</div></th>
<th colspan=5><div align="center">機台二</div></th>
<th><div align="center">判定</div></th>
</thead>
  <div align="center"></div>
<tbody>

<?php
//checkstop sort  Part_Number_V  checkitem up_lim  down_lim  Measure_Value-1  Measure_Value-2  Measure_Value-3 Measure_Value-4  Measure_Value-5 判定 Remark
$color = "";

while ($listout2 = mysqli_fetch_assoc($listoutF2)) {

    $DimUpper       = $listout2['up_lim'];
    $DimLow         = $listout2['down_lim'];
    $MeasureResult1 = $MeasureResult2 = $MeasureResult3 = $MeasureResult4 = $MeasureResult5 = $MeasureResult6 = $MeasureResult7 = $MeasureResult8 = $MeasureResult9 = $MeasureResult10 = '';

    if ($listout2['Measure_Value-1'] > $DimUpper or $listout2['Measure_Value-1'] < $DimLow) {
        if (!empty($listout2['Measure_Value-1'])) {
            $MeasureResult1 = 'NG';}
    }

    if ($listout2['Measure_Value-2'] > $DimUpper or $listout2['Measure_Value-2'] < $DimLow) {
        if (!empty($listout2['Measure_Value-2'])) {
            $MeasureResult2 = 'NG';}
    }

    if ($listout2['Measure_Value-3'] > $DimUpper or $listout2['Measure_Value-3'] < $DimLow) {
        if (!empty($listout2['Measure_Value-3'])) {
            $MeasureResult3 = 'NG';}

    }

    if ($listout2['Measure_Value-4'] > $DimUpper or $listout2['Measure_Value-4'] < $DimLow) {
        if (!empty($listout2['Measure_Value-4'])) {
            $MeasureResult4 = 'NG';}

    }

    if ($listout2['Measure_Value-5'] > $DimUpper or $listout2['Measure_Value-5'] < $DimLow) {
        if (!empty($listout2['Measure_Value-5'])) {
            $MeasureResult5 = 'NG';}
    }

    if ($listout2['Measure_Value-6'] > $DimUpper or $listout2['Measure_Value-6'] < $DimLow) {
        if (!empty($listout2['Measure_Value-6'])) {
            $MeasureResult6 = 'NG';}
    }

    if ($listout2['Measure_Value-7'] > $DimUpper or $listout2['Measure_Value-7'] < $DimLow) {
        if (!empty($listout2['Measure_Value-7'])) {
            $MeasureResult7 = 'NG';}
    }

    if ($listout2['Measure_Value-8'] > $DimUpper or $listout2['Measure_Value-8'] < $DimLow) {
        if (!empty($listout2['Measure_Value-8'])) {
            $MeasureResult8 = 'NG';}

    }

    if ($listout2['Measure_Value-9'] > $DimUpper or $listout2['Measure_Value-9'] < $DimLow) {
        if (!empty($listout2['Measure_Value-9'])) {
            $MeasureResult9 = 'NG';}

    }

    if ($listout2['Measure_Value-10'] > $DimUpper or $listout2['Measure_Value-10'] < $DimLow) {
        if (!empty($listout2['Measure_Value-10'])) {
            $MeasureResult10 = 'NG';}
    }
    if (!empty($listout2['Measure_Value-1']) or !empty($listout2['Measure_Value-2']) or !empty($listout2['Measure_Value-3']) or !empty($listout2['Measure_Value-4']) or !empty($listout2['Measure_Value-5']) or !empty($listout2['Measure_Value-6']) or !empty($listout2['Measure_Value-7']) or !empty($listout2['Measure_Value-8']) or !empty($listout2['Measure_Value-9']) or !empty($listout2['Measure_Value-10'])) {
        if ($MeasureResult1 == 'NG' or $MeasureResult2 == 'NG' or $MeasureResult3 == 'NG' or $MeasureResult4 == 'NG' or $MeasureResult5 == 'NG' or $MeasureResult6 == 'NG' or $MeasureResult7 == 'NG' or $MeasureResult8 == 'NG' or $MeasureResult9 == 'NG' or $MeasureResult10 == 'NG') {
            $color         = "<font color='#EE0000'>";
            $MeasureResult = 'NG';
        } else {
            $color         = "";
            $MeasureResult = 'OK';
        }
    } else { $MeasureResult = '';}

    echo "<tr>";
    echo "<td>" . $color . $listout2['checkstop'] . "</td>";
    echo "<td>" . $color . $listout2['up_lim'] . "</td>";
    echo "<td>" . $color . $listout2['down_lim'] . "</td>";
    echo "<td>" . $color . $listout2['Measure_Value-1'] . "</td>";
    echo "<td>" . $color . $listout2['Measure_Value-2'] . "</td>";
    echo "<td>" . $color . $listout2['Measure_Value-3'] . "</td>";
    echo "<td>" . $color . $listout2['Measure_Value-4'] . "</td>";
    echo "<td>" . $color . $listout2['Measure_Value-5'] . "</td>";
    echo "<td>" . $color . $listout2['Measure_Value-6'] . "</td>";
    echo "<td>" . $color . $listout2['Measure_Value-7'] . "</td>";
    echo "<td>" . $color . $listout2['Measure_Value-8'] . "</td>";
    echo "<td>" . $color . $listout2['Measure_Value-9'] . "</td>";
    echo "<td>" . $color . $listout2['Measure_Value-10'] . "</td>";
    echo "<td>" . $color . $MeasureResult . "</td>";
    echo "</tr>";

    $color = "";
}
?>
</tbody>
</table>
</form>
</body>
</html>


