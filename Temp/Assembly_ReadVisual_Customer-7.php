<?php
session_start();
//error_reporting(0);

date_default_timezone_set('Asia/Taipei');
//$DateTime      = date("Y-m-d", strtotime("-1 day"));

$Date       = $_SESSION['Date'];
$Phase      = $_SESSION['Phase'];
$Rev        = $_SESSION['Rev'];
$PartNumber = $_SESSION['PartNumber'];
$DateTime   = $_SESSION['DateTime'];
$day_night  = $_SESSION['day_night'];
$plin       = $_SESSION['plin'];

if ($Phase < '08:00:00') {$Date = date('Y-m-d', strtotime($Date) + 60 * 60 * 24);}
$DateTime = $Date . ' ' . $Phase;

$qf = "AND DateTime='$DateTime' ";
$qc = "AND PartNumber='$PartNumber' ";
$qd = "AND Rev='$Rev' ";
$qe = "AND Date='$Date' ";
$qb = "AND day_night='$day_night' ";
$qp = "AND Productline='$plin' ";

require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../Public/library/pclzip/pclzip-2-8-2/pclzip.lib.php';

$query_listoutT = "SELECT * FROM `modify_visual_first_measuredata` WHERE 1=1  " . $qb . " " . $qc . " " . $qd . " " . $qe . "" . $qf . " " . $qp . "  GROUP BY cave1";
$listoutT       = mysqli_query($connect_asm, $query_listoutT) or die(mysqli_error());
$query_listoutD = "SELECT * FROM `modify_visual_first_measuredata` WHERE 1=1  " . $qb . " " . $qc . " " . $qd . " " . $qe . "" . $qf . " " . $qp . "  ORDER BY id ASC";
$listoutD       = mysqli_query($connect_asm, $query_listoutD) or die(mysqli_error());

$listoutT = mysqli_fetch_assoc($listoutT);

$filename1 = $listoutT['PartNumber'] . '_' . $listoutT['Rev'] . '_' . $listoutT['day_night'] . '_' . $listoutT['Date'];
$filename  = mb_convert_encoding($filename1, "big5", "utf8");

$filename_xlsx = $filename . ".xlsx";

$xls1 = PHPExcel_IOFactory::load('Report_Template/Assembly_QVGV/Conn_IPQC_first.xlsx');

$xls1->setActiveSheetIndexByName('#temp');
$xls_sheet1 = $xls1->getActiveSheet();

$objStyleA1 = $xls_sheet1->getStyle('A13');

//设置字体
//$objFontA1 = $objStyleA1->getFont();
//$objFontA1->setName('Courier New');
//$objFontA1->setSize(10);
//$objFontA1->setBold(true);
//$objFontA1->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);

//设置对齐方式
$objAlignA1 = $objStyleA1->getAlignment();
$objAlignA1->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objAlignA1->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

//设置边框
$objBorderA1 = $objStyleA1->getBorders();
$objBorderA1->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objBorderA1->getTop()->getColor()->setARGB('000000'); // color
$objBorderA1->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objBorderA1->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objBorderA1->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

//设置填充颜色
//$objFillA1 = $objStyleA1->getFill();
//$objFillA1->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
//$objFillA1->getStartColor()->setARGB('FFEEEEEE');

//从指定的单元格复制样式信息.
$xls_sheet1->duplicateStyle($objStyleA1, 'A14:M' . (mysqli_num_rows($listoutD) + 12));

$objWriter1 = PHPExcel_IOFactory::createWriter($xls1, 'Excel2007');
$objWriter1->save('../../Report/Modify_System/FQ_Conn/IPQC_Report/From/Temp/' . $filename . '.xlsx');

$xls = PHPExcel_IOFactory::load('../../Report/Modify_System/FQ_Conn/IPQC_Report/From/Temp/' . $filename . '.xlsx');

$xls->setActiveSheetIndexByName('#temp');
$xls->getActiveSheet()->setTitle('Data');
$xls->setActiveSheetIndexByName('Data');
$xls_sheet = $xls->getActiveSheet();

$xls_sheet->setCellValue('D5', $listoutT['PartNumber']);
//$xls_sheet->setCellValue('D6', $listoutT['Rev']);
$xls_sheet->setCellValue('D7', $listoutT['Status']);
$xls_sheet->setCellValue('D8', $listoutT['Productline']);
$xls_sheet->setCellValue('D9', $listoutT['follow']);
$xls_sheet->setCellValue('D10', $listoutT['checkresult']);
$xls_sheet->setCellValue('K5', $listoutT['Personnel_ID']);
$xls_sheet->setCellValue('K6', $listoutT['Date']);
$xls_sheet->setCellValue('K7', $listoutT['day_night']);
$xls_sheet->setCellValue('K8', $listoutT['sample_num']);
$xls_sheet->setCellValue('K9', $listoutT['line_host']);
//$xls_sheet->setCellValue('K10', $listoutT['Date']);

$xls_sheet->setCellValue('E12', $listoutT['cave1']);
$xls_sheet->setCellValue('F12', $listoutT['cave2']);
$xls_sheet->setCellValue('G12', $listoutT['cave3']);
$xls_sheet->setCellValue('H12', $listoutT['cave4']);
$xls_sheet->setCellValue('I12', $listoutT['cave5']);
$xls_sheet->setCellValue('J12', $listoutT['cave6']);
$xls_sheet->setCellValue('K12', $listoutT['cave7']);
$xls_sheet->setCellValue('L12', $listoutT['cave8']);

$N = 13;
while ($listout = mysqli_fetch_assoc($listoutD)) {
    if ($listout['up_lim'] != '') {

        $xls_sheet->setCellValue('A' . $N, $listout['checkstop']);
        $xls_sheet->setCellValue('B' . $N, $listout['checkitem']);
        $xls_sheet->setCellValue('C' . $N, $listout['up_lim']);
        $xls_sheet->setCellValue('D' . $N, $listout['down_lim']);
        $xls_sheet->setCellValue('E' . $N, $listout['Measure_Value-1']);
        $xls_sheet->setCellValue('F' . $N, $listout['Measure_Value-2']);
        $xls_sheet->setCellValue('G' . $N, $listout['Measure_Value-3']);
        $xls_sheet->setCellValue('H' . $N, $listout['Measure_Value-4']);
        $xls_sheet->setCellValue('I' . $N, $listout['Measure_Value-5']);
        $xls_sheet->setCellValue('J' . $N, $listout['Measure_Value-6']);
        $xls_sheet->setCellValue('K' . $N, $listout['Measure_Value-7']);
        $xls_sheet->setCellValue('L' . $N, $listout['Measure_Value-8']);

        $DimUpper       = $listout['up_lim'];
        $DimLow         = $listout['down_lim'];
        $MeasureResult1 = $MeasureResult2 = $MeasureResult3 = $MeasureResult4 = $MeasureResult5 = $MeasureResult6 = $MeasureResult7 = $MeasureResult8 = '';

        if ($listout['Measure_Value-1'] != '') {
            if ($listout['Measure_Value-1'] > $DimUpper or $listout['Measure_Value-1'] < $DimLow) {

                $MeasureResult1 = 'NG';
                $xls_sheet->getStyle('E' . $N)->getFont()->getColor()->setARGB('FF0000');
                $xls_sheet->getStyle('E' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));
            }
        }

        if ($listout['Measure_Value-2'] != '') {
            if ($listout['Measure_Value-2'] > $DimUpper or $listout['Measure_Value-2'] < $DimLow) {
                $MeasureResult2 = 'NG';
                $xls_sheet->getStyle('F' . $N)->getFont()->getColor()->setARGB('FF0000');
                $xls_sheet->getStyle('F' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));
            }
        }

        if ($listout['Measure_Value-3'] != '') {
            if ($listout['Measure_Value-3'] > $DimUpper or $listout['Measure_Value-3'] < $DimLow) {
                $MeasureResult3 = 'NG';

                $xls_sheet->getStyle('G' . $N)->getFont()->getColor()->setARGB('FF0000');
                $xls_sheet->getStyle('G' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));
            }
        }

        if ($listout['Measure_Value-4'] != '') {
            if ($listout['Measure_Value-4'] > $DimUpper or $listout['Measure_Value-4'] < $DimLow) {
                $MeasureResult4 = 'NG';
                $xls_sheet->getStyle('H' . $N)->getFont()->getColor()->setARGB('FF0000');
                $xls_sheet->getStyle('H' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));
            }
        }

        if ($listout['Measure_Value-5'] != '') {
            if ($listout['Measure_Value-5'] > $DimUpper or $listout['Measure_Value-5'] < $DimLow) {
                $MeasureResult5 = 'NG';
                $xls_sheet->getStyle('I' . $N)->getFont()->getColor()->setARGB('FF0000');
                $xls_sheet->getStyle('I' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));
            }
        }
        if ($listout['Measure_Value-6'] != '') {
            if ($listout['Measure_Value-6'] > $DimUpper or $listout['Measure_Value-6'] < $DimLow) {
                $MeasureResult5 = 'NG';
                $xls_sheet->getStyle('J' . $N)->getFont()->getColor()->setARGB('FF0000');
                $xls_sheet->getStyle('J' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));
            }
        }

        if ($listout['Measure_Value-7'] != '') {
            if ($listout['Measure_Value-7'] > $DimUpper or $listout['Measure_Value-7'] < $DimLow) {
                $MeasureResult5 = 'NG';
                $xls_sheet->getStyle('K' . $N)->getFont()->getColor()->setARGB('FF0000');
                $xls_sheet->getStyle('K' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));
            }
        }

        if ($listout['Measure_Value-8'] != '') {
            if ($listout['Measure_Value-8'] > $DimUpper or $listout['Measure_Value-8'] < $DimLow) {
                $MeasureResult8 = 'NG';
                $xls_sheet->getStyle('L' . $N)->getFont()->getColor()->setARGB('FF0000');
                $xls_sheet->getStyle('L' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));
            }
        }

        if ($MeasureResult1 == 'NG' or $MeasureResult2 == 'NG' or $MeasureResult3 == 'NG' or $MeasureResult4 == 'NG' or $MeasureResult5 == 'NG' or $MeasureResult6 == 'NG' or $MeasureResult7 == 'NG' or $MeasureResult8 == 'NG') {
            $xls_sheet->setCellValue('M' . $N, 'NG');
            $xls_sheet->getStyle('M' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FF0000')));
        } else {
            $xls_sheet->setCellValue('M' . $N, 'OK');
        }

        $xls_sheet->getRowDimension($N)->setRowHeight(25);
        $N++;
    } else {
        $xls_sheet->setCellValue('A' . $N, $listout['checkstop']);
        $xls_sheet->setCellValue('M' . $N, $listout['Measure_Value-1']);
        if ($listout['Measure_Value-1'] == 'NG') {$xls_sheet->getStyle('M' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FF0000')));}
        $N++;
    }

}

header('Content-Disposition: attachment;filename=' . $filename_xlsx . '');
header('content-transfer-encoding: binary');
$objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
$objWriter->setPreCalculateFormulas(false);
$objWriter->save('php://output');

function between($min, $max, $value)
{
    //處理成陣列
    if (is_array($value)) {
        $limit = $value;
    } else {
        $limit = explode(",", $value);
    }
    //合併成多個數值
    $value   = array_merge($limit, $limit);
    $limit[] = $max;
    $limit[] = $min;

    //使用max及min函數判斷是否在區間內
    if ((max($limit) == $max && min($limit) == $min) || (max($value) == $max && min($value) == $min)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Data_CreateReportByDay-3</title>

</head>
</html>
