<?php
/*
 * Copyright (C) 2014 Foxlink SCBG CAE Dept. All rights reserved
 * Project: Projector System
 * File Name: Measure-Data-1
 * Function: GUI of Measure-Data
 * Author: Angel Wang
 * --------------------------------------------------
 * Rev: 1.2 Date: PM 04:34 2015/12/23 Modifier: Angel Wang
 * --------------------------------------------------
 */
if (!isset($_SESSION)) {
    session_start();
}
?>
<?php
error_reporting(0);
include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';

require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/library/Other/Fork.php';
/////////////////////////////////////////////////////////////////////////////////////////////////////
mysqli_select_db($connect_ort, $database_ort);
if ($_POST['End_DateF']) {
    $c_Request_MachineF = $_POST['Request_MachineF'];
    $c_Request_ItemF    = $_POST['Request_ItemF'];
    $c_Part_Number_VF   = $_POST['Part_Number_VF'];
    $c_Line_NumberF     = $_POST['Line_NumberF'];

    $c_Start_DateF = $_POST['Start_DateF'];
    $c_End_DateF   = $_POST['End_DateF'];
    $d1            = date('Y-m-d', strtotime($_POST['End_DateF']) + 60 * 60 * 24 * 1);
    $d2            = $_POST['Start_DateF'];

} else {
    $c_End_DateF   = date("Y-m-d");
    $c_Start_DateF = date('Y-m-d', strtotime($c_End_DateF) - 60 * 60 * 24 * 10);
    $d1            = date('Y-m-d', strtotime($c_End_DateF) + 60 * 60 * 24 * 1);
    $d2            = date('Y-m-d', strtotime($c_End_DateF) - 60 * 60 * 24 * 10);
}

if ($_POST['Request_MachineF']) {
    $qa  = "AND MachineName='$c_Request_MachineF'";
    $qaa = urlencode($qa);}

if ($_POST['Request_ItemF']) {
    $qb  = "AND RequestTestItem='$c_Request_ItemF'";
    $qbb = urlencode($qb);}

if ($_POST['Line_NumberF']) {
    $qc  = "AND LineNumber='$c_Line_NumberF'";
    $qcc = urlencode($qc);}

if ($_POST['Part_Number_VF']) {
    $qd  = "AND PartNumber='$c_Part_Number_VF'";
    $qdd = urlencode($qd);}

$qe  = "AND RequestDate BETWEEN '$d2' AND '$d1'";
$qee = urlencode($qe);

$fork   = new Fork;
$output =
$fork
    ->add("http://localhost/MainWebsite/Modify_System/FQ_Conn/Assembly_ReadORTData_Customer-A.php?t=0&qaa=" . $qaa . "&qbb=" . $qbb . "&qcsc=" . $qcc . "&qdd=" . $qdd . "&qee=" . $qee)
    ->add("http://localhost/MainWebsite/Modify_System/FQ_Conn/Assembly_ReadORTData_Customer-A.php?t=1&qaa=" . $qaa . "&qbb=" . $qbb . "&qcc=" . $qcc . "&qdd=" . $qdd . "&qee=" . $qee)
    ->add("http://localhost/MainWebsite/Modify_System/FQ_Conn/Assembly_ReadORTData_Customer-A.php?t=2&qaa=" . $qaa . "&qbb=" . $qbb . "&qcc=" . $qcc . "&qdd=" . $qdd . "&qee=" . $qee)
    ->add("http://localhost/MainWebsite/Modify_System/FQ_Conn/Assembly_ReadORTData_Customer-A.php?t=3&qaa=" . $qaa . "&qbb=" . $qbb . "&qcc=" . $qcc . "&qdd=" . $qdd . "&qee=" . $qee)
    ->run();
?>


<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>
<link rel="stylesheet" type="text/css" href="CSS/ALL_CSS.css?id='sssa'">
<script type="text/javascript">
$(function() {
  $( "#Start_DateF").datepicker({
  dateFormat:"yy-mm-dd",
  yearRange:"2014:2033",
  buttonImageOnly: true
  });
  $( "#End_DateF").datepicker({
  dateFormat:"yy-mm-dd",
  yearRange:"2014:2033",
  buttonImageOnly: true
  });


});
function timecheck(){
  document['form1'].action = "Assembly_ReadORTData_Customer-1.php";
  document['form1'].target = 'Index_Search';
  document['form1'].submit();
}
function drop()
{
  document['form1'].action = "Assembly_ReadORTData_Customer-1.php";
  document['form1'].target = 'Index_Search';
  document['form1'].submit();
}
function result()
{
  document['form1'].action = "Assembly_ReadORTData_Customer-2.php";
  document['form1'].target = 'Index_Content';
  document['form1'].submit();
}
</script>

</head>
<body background="Images/loginb.png">
<form id="form1" name="form1" method="post" >
<div style="line-height:40px">
<VisualL>起始日:(*)</VisualL>
<input type="text" name="Start_DateF" id="Start_DateF" value="<?php echo $c_Start_DateF ?>" onChange="timecheck()" class="ReadData-1">
<VisualL>結束日:(*)</VisualL>
<input type="text" name="End_DateF" id="End_DateF" class="ReadData-1" value="<?php echo $c_End_DateF ?>" onChange="timecheck()">

<!-------------------------------------Request MachineF------------------------------------------->
</div>
<div style="line-height:40px">
<VisualL>測試機台:(*)</VisualL>
<select name="Request_MachineF" id="Request_MachineF" class="ReadData-2"  onChange="drop();">
<?php
echo "<option></option>";
foreach ($output[0] as $e) {
    foreach ($e as $c) {
        echo "<option value='" . $c . "'" . ($c == $c_Request_MachineF ? "selected" : "") . ">" . $c . "</option>";
    }
}
?>
</select>
<!-------------------------------------Request ItemF------------------------------------------->
<?php
if ($c_Request_MachineF != '週期性送測') {

    echo "
<VisualL>測試項目:(*)</VisualL>
<select name='Request_ItemF' id='Request_ItemF' class='ReadData-2'  onChange='drop()';>
<option></option>";

    foreach ($output[1] as $e) {
        foreach ($e as $c) {
            echo "<option value='" . $c . "'" . ($c == $c_Request_ItemF ? "selected" : "") . ">" . $c . "</option>";
        }
    }
}
?>
</select>
<!-------------------------------------Part_Number_VF------------------------------------------->
<VisualL>料號:(*)</VisualL>
<select name="Part_Number_VF" id="Part_Number_VF" class="ReadData-2" onChange="drop();">
<?php
echo "<option></option>";
foreach ($output[2] as $e) {
    foreach ($e as $c) {
        echo "<option value='" . $c . "'" . ($c == $c_Part_Number_VF ? "selected" : "") . ">" . $c . "</option>";
    }

}
?>
</select>
<!-------------------------------------Line_NumberF------------------------------------------->
<VisualL>線別號:(*)</VisualL>
<select name="Line_NumberF" id="Line_NumberF" class="ReadData-2" onChange="drop(); ">
<?php
echo "<option></option>";
foreach ($output[3] as $e) {
    foreach ($e as $c) {
        echo "<option value='" . $c . "'" . ($c == $c_Line_NumberF ? "selected" : "") . ">" . $c . "</option>";
    }

}
?>
</select>

</div>
</BR>

<div style="float:left">
<input type="button" name="submitA" id="submitA"  value="檢閱" class="ReadData-BT" onClick="result()"/>
</form>
<?php //echo "*若有尺寸數據才需選擇版次"; ?>
</div>
</div>
</form>
</body>
</html>

