<?php
/*
 * Copyright (C) 2014 Foxlink SCBG CAE Dept. All rights reserved
 * Project: Projector System
 * File Name: Measure Data-2
 * Function: Result of Measure Data
 * Author: Angel Wang
 * --------------------------------------------------
 * Rev: 1.2 Date: PM 04:34 2014/05/29 Modifier: Angel Wang
 * --------------------------------------------------
 */

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
//error_reporting(0);
if (!isset($_SESSION)) {session_start();}

$Date                   = $_REQUEST['Date'];
$Phase                  = $_REQUEST['Phase'];
$Rev                    = $_REQUEST['Rev'];
$PartNumber             = $_REQUEST['PartNumber'];
$day_night              = $_REQUEST['day_night'];
$plin                   = $_REQUEST['plin'];
$_SESSION['Date']       = $_REQUEST['Date'];
$_SESSION['Phase']      = $_REQUEST['Phase'];
$_SESSION['Rev']        = $_REQUEST['Rev'];
$_SESSION['PartNumber'] = $_REQUEST['PartNumber'];
$_SESSION['DateTime']   = $_REQUEST['DateTime'];
$_SESSION['day_night']  = $_REQUEST['day_night'];
$_SESSION['plin']       = $_REQUEST['plin'];

if ($Phase < '08:00:00') {$Date = date('Y-m-d', strtotime($Date) + 60 * 60 * 24);}
$DateTime = $Date . ' ' . $Phase;

if ($PartNumber) {$qc = "AND PartNumber='$PartNumber' ";}
if ($Rev) {$qd = "AND Rev='$Rev' ";}
if ($Date) {$qe = "AND Date='$Date' ";}
if ($day_night) {$qb = "AND day_night='$day_night' ";}
if ($DateTime) {$qf = "AND DateTime='$DateTime' ";}
if ($plin) {$qp = "AND Productline='$plin' ";}

mysqli_select_db($connect_asm, $database_asm);
$query_listoutF2 = "SELECT * FROM `modify_visual_first_measuredata` WHERE 1=1  " . $qb . " " . $qc . " " . $qd . " " . $qp . " " . $qe . "" . $qf . "  GROUP BY cave1";
$listoutF2       = mysqli_query($connect_asm, $query_listoutF2);

mysqli_select_db($connect, $database);
$query_listoutF = "SELECT * FROM `modify_visual_first_measuredata` WHERE 1=1  " . $qb . " " . $qc . " " . $qd . " " . $qp . " " . $qe . "" . $qf . "  ORDER BY id ASC";
$listoutF       = mysqli_query($connect_asm, $query_listoutF);

?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<script src="../../Public/library/Other/Sorttable.js"></script>
<script type="text/javascript">

  function result()
{
   document['form1'].action = "Assembly_ReadVisual_Customer-7.php";
   document['form1'].target = '_self';
   document['form1'].submit();
}

</script>
<link rel="stylesheet" type="text/css" href="CSS/ALL_CSS.css">
</head>
<body>
<form id="form1" name="form1" method="post" >

<div class="Measure-Data-table-2">
<table id="Measure-Data-table-2" class="sortable">
<thead>
<input type='button' name='submitA' id='submitA'  value='下載報告' class='ReadData-BT' onClick='result();'/>
<th><div align="center">日期</div></th>
<th><div align="center">送單時間</div></th>
<th><div align="center">線別</div></th>
<th><div align="center">線長</div></th>
<th><div align="center">工令</div></th>
<th><div align="center">料號</div></th>
</thead>

<tbody>

<?php

while ($listout2 = mysqli_fetch_assoc($listoutF2)) {
    $Time  = substr($listout2['DateTime'], 11, 8);
    $cave1 = $listout2['cave1'];
    $cave2 = $listout2['cave2'];
    $cave3 = $listout2['cave3'];
    $cave4 = $listout2['cave4'];
    $cave5 = $listout2['cave5'];
    $cave6 = $listout2['cave6'];
    $cave7 = $listout2['cave7'];
    $cave8 = $listout2['cave8'];

    echo "<tr>";
    echo "<td>" . $listout2['Date'] . "</td>";
    echo "<td>" . $Time . "</td>";
    echo "<td>" . $listout2['Productline'] . "</td>";
    echo "<td>" . $listout2['line_host'] . "</td>";
    echo "<td>" . $listout2['Ticket_Number'] . "</td>";
    echo "<td>" . $listout2['PartNumber'] . "</td>";
    echo "</tr>";
    ?>
</tbody>

<thead>
<th><div align="center">班別</div></th>
<th><div align="center">樣本數</div></th>
<th><div align="center">工作狀態</div></th>
<th><div align="center">IPQC人員</div></th>
<th><div align="center">檢驗依據</div></th>
<th><div align="center">檢驗結果</div></th>
</thead>
<tbody>
<?php
echo "<tr>";
    echo "<td>" . $listout2['day_night'] . "</td>";
    echo "<td>" . $listout2['sample_num'] . "</td>";
    echo "<td>" . $listout2['Status'] . "</td>";
    echo "<td>" . $listout2['Personnel_ID'] . "</td>";
    echo "<td>" . $listout2['follow'] . "</td>";
    echo "<td>" . $listout2['checkresult'] . "</td>";
    echo "</tr>";

    $sample_num = substr($listout2['sample_num'], 0, 1);
    //echo $sample_num;
}
?>

</tbody>
</table>
<br>
<table id="Measure-Data-table-2" class="sortable">

<?php

echo "<thead>
<th><div align='center'>檢測項目</div></th>
<th><div align='center'>規格</div></th>
<th><div align='center'>上限</div></th>
<th><div align='center'>下限</div></th>
<th><div align='center'>" . $cave1 . "</div></th>
<th><div align='center'>" . $cave2 . "</div></th>
<th><div align='center'>" . $cave3 . "</div></th>
<th><div align='center'>" . $cave4 . "</div></th>
<th><div align='center'>" . $cave5 . "</div></th>
<th><div align='center'>" . $cave6 . "</div></th>
<th><div align='center'>" . $cave7 . "</div></th>
<th><div align='center'>" . $cave8 . "</div></th>
<th><div align='center'>判定</div></th>
</thead>
<tbody>";

while ($listout = mysqli_fetch_assoc($listoutF)) {
    if ($listout['up_lim'] != '') {
        $DimUpper       = $listout['up_lim'];
        $DimLow         = $listout['down_lim'];
        $MeasureResult1 = $MeasureResult2 = $MeasureResult3 = $MeasureResult4 = $MeasureResult5 = $MeasureResult6 = $MeasureResult7 = $MeasureResult8 = '';

        if ($listout['Measure_Value-1'] > $DimUpper or $listout['Measure_Value-1'] < $DimLow) {
            if (!empty($listout['Measure_Value-1'])) {
                $MeasureResult1 = 'NG';}
        }

        if ($listout['Measure_Value-2'] > $DimUpper or $listout['Measure_Value-2'] < $DimLow) {
            if (!empty($listout['Measure_Value-2'])) {
                $MeasureResult2 = 'NG';}
        }

        if ($listout['Measure_Value-3'] > $DimUpper or $listout['Measure_Value-3'] < $DimLow) {
            if (!empty($listout['Measure_Value-3'])) {
                $MeasureResult3 = 'NG';}

        }

        if ($listout['Measure_Value-4'] > $DimUpper or $listout['Measure_Value-4'] < $DimLow) {
            if (!empty($listout['Measure_Value-4'])) {
                $MeasureResult4 = 'NG';}

        }

        if ($listout['Measure_Value-5'] > $DimUpper or $listout['Measure_Value-5'] < $DimLow) {
            if (!empty($listout['Measure_Value-5'])) {
                $MeasureResult5 = 'NG';}
        }

        if ($listout['Measure_Value-6'] != '') {
            if (!empty($listout['Measure_Value-6'])) {
                if ($listout['Measure_Value-6'] > $DimUpper or $listout['Measure_Value-6'] < $DimLow) {
                    $MeasureResult5 = 'NG';}
            }
        }

        if ($listout['Measure_Value-7'] != '') {
            if ($listout['Measure_Value-7'] > $DimUpper or $listout['Measure_Value-7'] < $DimLow) {
                if (!empty($listout['Measure_Value-7'])) {
                    $MeasureResult5 = 'NG';}
            }
        }

        if ($listout['Measure_Value-8'] != '') {
            if (!empty($listout['Measure_Value-8'])) {
                if ($listout['Measure_Value-8'] > $DimUpper or $listout['Measure_Value-8'] < $DimLow) {
                    $MeasureResult8 = 'NG';}
            }
        }

        if (!empty($listout['Measure_Value-1']) or !empty($listout['Measure_Value-2']) or !empty($listout['Measure_Value-3']) or !empty($listout['Measure_Value-4']) or !empty($listout['Measure_Value-5']) or !empty($listout['Measure_Value-6']) or !empty($listout['Measure_Value-7']) or !empty($listout['Measure_Value-8'])) {
            if ($MeasureResult1 == 'NG' or $MeasureResult2 == 'NG' or $MeasureResult3 == 'NG' or $MeasureResult4 == 'NG' or $MeasureResult5 == 'NG' or $MeasureResult6 == 'NG' or $MeasureResult7 == 'NG' or $MeasureResult8 == 'NG') {
                $color         = "<font color='#EE0000'>";
                $MeasureResult = 'NG';
            } else {
                $color         = "";
                $MeasureResult = 'OK';
            }
        } else {
            $MeasureResult = '';
            $color         = "";}

        echo "<tr>";
        echo "<td>" . $color . $listout['checkstop'] . "</td>";
        echo "<td>" . $color . $listout['checkitem'] . "</td>";
        echo "<td>" . $color . $listout['up_lim'] . "</td>";
        echo "<td>" . $color . $listout['down_lim'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-1'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-2'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-3'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-4'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-5'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-6'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-7'] . "</td>";
        echo "<td>" . $color . $listout['Measure_Value-8'] . "</td>";
        echo "<td>" . $color . "$MeasureResult</td>";
        echo "</tr>";
    } else {
        echo "<tr>";
        echo "<td>" . $listout['checkstop'] . "</td>";
        if ($listout['Measure_Value-1'] == 'OK') {$color = "";} else { $color = "<font color='#EE0000'>";}

        echo "<td colspan=11></td>";
        echo "<td>" . $color . $listout['Measure_Value-1'] . "</td>";
        echo "</tr>";
    }
}

?>
</tbody>
</table>


</table>
</form>
</body>
</html>


