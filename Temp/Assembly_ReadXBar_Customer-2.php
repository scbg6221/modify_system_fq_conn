<?php
session_start();
error_reporting(0);
include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/library/PHPExcel/php-excel-reader-2.21/simplexlsx.class.php';

//ini_set("memory_limit", "-1");
ini_set("max_execution_time", '0');
$ServiceNumber = $_SESSION['ServiceNumber'];

if (!empty($_POST['PartNumberF']) && !empty($_POST['LineNumberF']) && !empty($_POST['DimNOF']) && !empty($_POST['StartDateF']) && !empty($_POST['EndDateF'])) {

    $Factory                   = $_SESSION['Factory'];
    $User                      = $_SESSION["user"];
    $Department                = "IPQC";
    $_SESSION['Department']    = $Department;
    $PartDescription           = $_POST['PartDescriptionF'];
    $PartNumber                = $_POST['PartNumberF'];
    $LineNumber                = $_POST['LineNumberF'];
    $Sampling_Rate             = "5";
    $_SESSION['Sampling_Rate'] = $Sampling_Rate;
    $DimNOF                    = $_POST['DimNOF'];
    $_SESSION['DimNOF']        = $DimNOF;
    $StartDate                 = $_POST['StartDateF'];
    $EndDate                   = $_POST['EndDateF'];
    $d2                        = date('Y-m-d', strtotime($EndDate) + 60 * 60 * 24) . "-08:00:00";
    $d1                        = $StartDate . "-08:00:00";
    $CpkTarget                 = "1.33";
    $Piece                     = "5";
    $_SESSION['Piece']         = $Piece;
    $submit                    = $_POST['submitA'];
    $FAI                       = $DimNOF . '1';

    mysqli_select_db($connect, $database);

    $Dim = array();
    $Dim = array('Date', 'Time');
    for ($r = 1; $r <= $Piece; $r++) {
        array_push($Dim, 'FAI-' . $r);
    }

    array_push($Dim, "Sum", "X_Bar", "R");
    $t = 1;
    foreach ($Dim as $v) {
        ${'Tital' . $t} = array($v);
        $t++;
    }

    $AllServiceNumber = $AllMeasureData = $MeasureData_1 = $MeasureData_2 = $MeasureData_3 = $MeasureData_4 = $MeasureData_5 = $Nominal_Dim = $Upper_Dim = $Lower_Dim = $Total_Samples = $Group_Nums = $Date1 = $Time1 = $sum1 = $avg1 = $R1 = $sum = $avg = $R = $CLxx = $UCLxx = $LCLxx = $CLrr = $UCLrr = $LCLrr = array();

    mysqli_select_db($connect_asm, $database_asm);
    $Select_AllServiceNumber = "SELECT ServiceNumber FROM modify_measurecontent WHERE 1=1 AND PartNumber = '" . $PartNumber . "' AND    LineNumber = '" . $LineNumber . "' AND MeasureEndTime BETWEEN '" . $StartDate . " 07:59:59' and '" . $EndDate . " 19:59:59' group by ServiceNumber";
    $query_AllServiceNumber  = mysqli_query($connect_asm, $Select_AllServiceNumber);
    $a                       = 0;
    while ($AllServiceNumberF = mysqli_fetch_array($query_AllServiceNumber)) {

        $AllServiceNumber[] = $AllServiceNumberF['ServiceNumber'];
        $a++;
    }

    for ($b = 0; $b < $a; $b++) {
        $Select_AllData = "SELECT a.*, b.MeasureEndTime FROM modify_measuredata a inner join modify_measurecontent b on a.ServiceNumber=b.ServiceNumber WHERE 1=1 AND a.`ServiceNumber` = '" . $AllServiceNumber[$b] . "' AND DimNO = '" . $DimNOF . "' ";
        $query_AllData  = mysqli_query($connect_asm, $Select_AllData);

        while ($AllData = mysqli_fetch_array($query_AllData)) {

            $MeasureData[$b] = array($AllData["Sample1"], $AllData["Sample2"], $AllData["Sample3"], $AllData["Sample4"], $AllData["Sample5"]);
            array_push($Nominal_Dim, $AllData["DimSpec"]);
            array_push($Upper_Dim, $AllData["DimUpper"]);
            array_push($Lower_Dim, $AllData["DimLower"]);
            array_push($Date1, substr($AllData["MeasureEndTime"], 0, 10));
            array_push($Time1, substr($AllData["MeasureEndTime"], 10));
            array_push($MeasureData_1, $AllData["Sample1"]);
            array_push($MeasureData_2, $AllData["Sample2"]);
            array_push($MeasureData_3, $AllData["Sample3"]);
            array_push($MeasureData_4, $AllData["Sample4"]);
            array_push($MeasureData_5, $AllData["Sample5"]);
            $ggroupsum[$b] = array_sum($MeasureData[$b]);
            $ggroupR[$b]   = round(max($MeasureData[$b]) - min($MeasureData[$b]), 4);
            $ggroupavg[$b] = round($ggroupsum[$b] / $Piece, 4);
        }
    }
    $i          = 1;
    $d          = 0;
    $Group_Nums = $a;

    $a6 = 1;
    while ($a6 <= $Group_Nums) {
        $a7               = $a6 - 1;
        $Group_array[$a7] = $a6;
        $a6++;
    }

    $_SESSION['Nominal_Dim']   = $Nominal_Dim[0];
    $_SESSION['Upper_Dim']     = $Upper_Dim[0];
    $_SESSION['Lower_Dim']     = $Lower_Dim[0];
    $_SESSION['Group_Nums']    = $Group_Nums;
    $_SESSION['Group_array']   = $Group_array;
    $_SESSION['Date_array']    = $Date1;
    $_SESSION['Time_array']    = $Time1;
    $_SESSION['MeasureData_1'] = $MeasureData_1;
    $_SESSION['MeasureData_2'] = $MeasureData_2;
    $_SESSION['MeasureData_3'] = $MeasureData_3;
    $_SESSION['MeasureData_4'] = $MeasureData_4;
    $_SESSION['MeasureData_5'] = $MeasureData_5;
    $_SESSION['sum1']          = $ggroupsum;
    $_SESSION['R1']            = $ggroupR;
    $_SESSION['avg1']          = $ggroupavg;
    $A2                        = 0.577;
    $d2                        = 2.326;
    $D3                        = 0.000;
    $D4                        = 2.114;

///運算Cpk,Cp...各參數///
    $avg_sum = array_sum($ggroupavg);
    $CLx1    = $avg_sum / $Group_Nums;

    $CLx             = round($CLx1, 3);
    $_SESSION['CLx'] = $CLx;

    $R_sum           = array_sum($ggroupR);
    $CLr1            = $R_sum / $Group_Nums;
    $CLr             = round($CLr1, 3);
    $_SESSION['CLr'] = $CLr;

    $UCLx1            = $CLx1 + $A2 * $CLr1;
    $UCLx             = round($UCLx1, 3);
    $LCLx1            = $CLx1 - $A2 * $CLr1;
    $LCLx             = round($LCLx1, 3);
    $_SESSION['UCLx'] = $UCLx;
    $_SESSION['LCLx'] = $LCLx;

    $UCLr1            = $D4 * $CLr1;
    $UCLr             = round($UCLr1, 3);
    $LCLr1            = $D3 * $CLr1;
    $LCLr             = round($LCLr1, 3);
    $_SESSION['UCLr'] = $UCLr;
    $_SESSION['LCLr'] = $LCLr;

    $C1 = $CLr1 / $d2;
    $C  = round($C1, 4);
//echo $C1;
    $USL = $Upper_Dim[0];
    $LSL = $Lower_Dim[0];

    $Cp1            = ($USL - $LSL) / (6 * $C1);
    $Cp             = round($Cp1, 2);
    $_SESSION['Cp'] = $Cp;

    $CpU1            = ($USL - $CLx1) / (3 * $C1);
    $CpU             = round($CpU1, 2);
    $_SESSION['CpU'] = $CpU;

    $CpL1            = ($CLx1 - $LSL) / (3 * $C1);
    $CpL             = round($CpL1, 2);
    $_SESSION['CpL'] = $CpL;

    $Ca1            = abs((($USL + $LSL) / 2) - $CLx1);
    $Ca2            = $Ca1 / (($USL - $LSL) / 2);
    $Ca3            = $Ca2 * 100;
    $Ca             = round($Ca3, 2);
    $_SESSION['Ca'] = $Ca;

    $Cpk1            = (1 - $Ca2) * $Cp1;
    $Cpk             = round($Cpk1, 2);
    $_SESSION['Cpk'] = $Cpk;

    $j6 = 0;
    while ($j6 < $Group_Nums) {
        $CLrr[$j6]  = $CLr;
        $UCLrr[$j6] = $UCLr;
        $LCLrr[$j6] = $LCLr;
        $CLxx[$j6]  = $CLx;
        $UCLxx[$j6] = $UCLx;
        $LCLxx[$j6] = $LCLx;
        $j6++;
    }

    $_SESSION['CLxx']  = $CLxx;
    $_SESSION['UCLxx'] = $UCLxx;
    $_SESSION['LCLxx'] = $LCLxx;
    $_SESSION['CLrr']  = $CLrr;
    $_SESSION['UCLrr'] = $UCLrr;
    $_SESSION['LCLrr'] = $LCLrr;
    ?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>無標題文件</title>
<link rel="stylesheet" type="text/css" href="CSS/ALL_CSS.css?id='12'">
</head>

<body>

<table width="800" border="2"><VisualZ>Information</VisualZ>
  <tr>
    <td width="133" align="center" valign="middle" nowrap="nowrap"><VisualM>Factory</VisualM></td>
    <td width="133" align="center" valign="middle" nowrap="nowrap"><VisualM>Department</VisualM></td>
    <td width="178" align="center" valign="middle" nowrap="nowrap"><VisualM>User</VisualM></td>
    <td align="center" valign="middle" nowrap="nowrap"><VisualM>Line Number</VisualM></td>
    <td align="center" valign="middle" nowrap="nowrap"><VisualM>Part Description</VisualM></td>
  </tr>

  <tr>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Factory; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Department; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $User; ?></td>
    <td width="183" align="center" valign="middle" nowrap="nowrap"><?php echo $LineNumber; ?></td>
    <td width="142" align="center" valign="middle" nowrap="nowrap"><?php echo $PartDescription; ?></td>
  </tr>

  <tr>
    <td align="center" valign="middle" nowrap="nowrap"><VisualM>Part Number</VisualM></td>
    <td width="97" align="center" valign="middle" nowrap="nowrap"><VisualM>Cpk Target</VisualM></td>
    <td align="center" valign="middle" nowrap="nowrap"><VisualM>Group Numbers</VisualM></td>
    <td align="center" valign="middle" nowrap="nowrap"><VisualM>Sampling Sizes (Pcs</VisualM>)</td>
    <td align="center" valign="middle" nowrap="nowrap"><VisualM>Sampling Rates (Hrs)</VisualM></td>

  <tr>
    <td width="149" align="center" valign="middle" nowrap="nowrap"><?php echo $PartNumber; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $CpkTarget; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Group_Nums; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Piece; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Sampling_Rate; ?></td>
  </tr>
  <tr>
    <td align="center" valign="middle" nowrap="nowrap"><VisualM>Start Date</VisualM></td>
    <td align="center" valign="middle" nowrap="nowrap"><VisualM>End Date</VisualM></td>
    <td width="142" align="center" valign="middle" nowrap="nowrap"><VisualM>Dimension(mm)</VisualM></td>
    <td align="center" valign="middle" nowrap="nowrap"><VisualM>USL</VisualM></td>
    <td align="center" valign="middle" nowrap="nowrap"><VisualM>LSL</VisualM></td>
  </tr>
  <tr>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $StartDate; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $EndDate; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Nominal_Dim[0]; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Upper_Dim[0]; ?></td>
    <td align="center" valign="middle" nowrap="nowrap"><?php echo $Lower_Dim[0]; ?></td>
  </tr>

<table width="750" border="2"><VisualZ>Result</VisualZ>
  <tr>
    <td width="50" align="center" valign="middle"><VisualM>CLx</VisualM></td>
    <td width="50" align="center" valign="middle"><VisualM>UCLx</VisualM></td>
    <td width="50" align="center" valign="middle"><VisualM>LCLx</VisualM></td>
    <td width="50" align="center" valign="middle"><VisualM>CLr</VisualM></td>
    <td width="50" align="center" valign="middle"><VisualM>UCLr</VisualM></td>
    <td width="50" align="center" valign="middle"><VisualM>LCLr</VisualM></td>
    <td width="50" align="center" valign="middle"><VisualM>CpU</VisualM></td>
    <td width="50" align="center" valign="middle"><VisualM>CpL</VisualM></td>
    <td width="50" align="center" valign="middle"><VisualM>Cp</VisualM></td>
    <td width="50" align="center" valign="middle"><VisualM>Ca</VisualM></td>
    <td width="50" align="center" valign="middle"><VisualM>Cpk</VisualM></td>
    <td colspan='2' width="200" align="center" valign="middle"><VisualM>Graph Plot</VisualM></td>

  </tr>
  <tr>
    <td width="50" align="center" valign="middle"><?php echo $CLx; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $UCLx; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $LCLx; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $CLr; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $UCLr; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $LCLr; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $CpU; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $CpL; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $Cp; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $Ca . '%'; ?></td>
    <td width="50" align="center" valign="middle"><?php echo $Cpk; ?></td>
    <td width="100" align="center" valign="middle">
<?php
echo "<form name='1' id='1' method='post' action='Assembly_ReadXBar_Customer-3.php' target='_blank'>";
    $string1 = serialize($ggroupR);
    $string2 = serialize($CLrr);
    $string3 = serialize($UCLrr);
    $string4 = serialize($LCLrr);

    echo "<input type='hidden' name=string1  value=" . $string1 . ">";
    echo "<input type='hidden' name=string2  value=" . $string2 . ">";
    echo "<input type='hidden' name=string3  value=" . $string3 . ">";
    echo "<input type='hidden' name=string4  value=" . $string4 . ">";
    echo "<input type='submit' name='submit1' id='submit1' value='Range(R Chart)'>";
    echo "</form>";
    ?></td>
    <td width="100" align="center" valign="middle">
<?php
echo "<form name='2' id='2' method='post' action='Assembly_ReadXBar_Customer-4.php' target='_blank'>";
    $string5 = serialize($ggroupavg);
    $string6 = serialize($CLxx);
    $string7 = serialize($UCLxx);
    $string8 = serialize($LCLxx);

    echo "<input type='hidden' name=string5  value=" . $string5 . ">";
    echo "<input type='hidden' name=string6  value=" . $string6 . ">";
    echo "<input type='hidden' name=string7  value=" . $string7 . ">";
    echo "<input type='hidden' name=string8  value=" . $string8 . ">";
    echo "<input type='submit' name='submit2' id='submit2' value='Average(X_Bar Chart)'>";
    echo "</form>";
    ?>
</td>
</tr>
</table>

<div class="Measure-Data-table-2">
<table id="Measure-Data-table-2">
  <thead>
<th>Group NO.</th>

<?php
$c = 1;

    while ($c <= $Group_Nums) {
        echo "<th>" . $c . "</th>";
        $c++;
    }

    ?>

</thead>
<tbody>

<?php
echo "</tr>";
    echo "<td align=center>Date</td>";
    for ($d = 0; $d <= $Group_Nums; $d++) {
        echo "<td align=center>" . $Date1[$d] . "</td>";
    }

    echo "</tr>";
    echo "<td align=center>Time</td>";
    for ($d = 0; $d <= $Group_Nums; $d++) {
        echo "<td align=center>" . $Time1[$d] . "</td>";
    }

    for ($i = 1; $i <= $Piece; $i++) {
        echo "</tr>";
        echo "<td align=center>Sample-$i</td>";
        for ($d = 0; $d <= $Group_Nums; $d++) {
            echo "<td align=center>" . ${"MeasureData_" . $i}[$d] . "</td>";
        }

    }

    echo "</tr>";
    echo "<td align=center>Sum</td>";
    for ($d = 0; $d <= $Group_Nums; $d++) {
        echo "<td align=center>" . $ggroupsum[$d] . "</td>";
    }

    echo "</tr>";
    echo "<td align=center>X_bar</td>";
    for ($d = 0; $d <= $Group_Nums; $d++) {
        echo "<td align=center>" . $ggroupavg[$d] . "</td>";
    }

    echo "</tr>";
    echo "<td align=center>R</td>";
    for ($d = 0; $d <= $Group_Nums; $d++) {
        echo "<td align=center>" . $ggroupR[$d] . "</td>";

    }
} else {echo ' 每一個(*)為必選欄位,請選擇! ';}
?>

</tbody>
</table>


</body>
</html>