<?php
session_start();
//error_reporting(0);

date_default_timezone_set('Asia/Taipei');
$DateTime      = date("Y-m-d", strtotime("-1 day"));
$ServiceNumber = $_POST['ServiceNumber'];

//include '../../Public/MainWebUI/User_Count.php';
//include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../Public/library/pclzip/pclzip-2-8-2/pclzip.lib.php';

$CallC = "";
//$AllSN=array();
/*
mysqli_select_db($connect,$database);
$Search = "SELECT ServiceNumber FROM modify_measurecontent WHERE DateTime = '".$DateTime."' ORDER BY ServiceNumber ASC";
$Search_Report = mysqli_query($connect,$Search);

if($Search_Report){

while($row=mysqli_fetch_array($Search_Report)){
array_push($AllSN,$row['ServiceNumber']);
}

foreach($AllSN as $key => $value) {
 */
$query_listoutT = "SELECT * FROM modify_measurecontent WHERE ServiceNumber = '" . $ServiceNumber . "'";
$listoutT       = mysqli_query($connect_asm, $query_listoutT) or die(mysqli_error());
$query_listoutD = "SELECT * FROM modify_measuredata WHERE ServiceNumber = '" . $ServiceNumber . "' ORDER BY DimNOOrder ASC";
$listoutD       = mysqli_query($connect_asm, $query_listoutD) or die(mysqli_error());

$listoutT = mysqli_fetch_assoc($listoutT);

$filename = mb_convert_encoding(substr($listoutT['RawFileName'], 15, strrpos($listoutT['RawFileName'], '.') - 15), "big5", "utf8");

$filename_xlsx = $filename . ".xlsx";
$Year          = substr($listoutT['DateTime'], 0, 4);
$MonthDay      = substr($listoutT['DateTime'], 5, 2) . substr($listoutT['DateTime'], 8, 2);

$xls1 = PHPExcel_IOFactory::load('Report_Template/Assembly_QVGV/Conn_IPQC_T.xlsx');

$xls1->setActiveSheetIndexByName('#temp');
$xls_sheet1 = $xls1->getActiveSheet();

$objStyleA1 = $xls_sheet1->getStyle('A10');

//设置字体
//$objFontA1 = $objStyleA1->getFont();
//$objFontA1->setName('Courier New');
//$objFontA1->setSize(10);
//$objFontA1->setBold(true);
//$objFontA1->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);

//设置对齐方式
$objAlignA1 = $objStyleA1->getAlignment();
$objAlignA1->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objAlignA1->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

//设置边框
$objBorderA1 = $objStyleA1->getBorders();
$objBorderA1->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objBorderA1->getTop()->getColor()->setARGB('000000'); // color
$objBorderA1->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objBorderA1->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objBorderA1->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

//设置填充颜色
//$objFillA1 = $objStyleA1->getFill();
//$objFillA1->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
//$objFillA1->getStartColor()->setARGB('FFEEEEEE');

//从指定的单元格复制样式信息.
$xls_sheet1->duplicateStyle($objStyleA1, 'A11:N' . (mysqli_num_rows($listoutD) + 9));

$objWriter1 = PHPExcel_IOFactory::createWriter($xls1, 'Excel2007');
$objWriter1->save('../../Report/QV_System/FQ_Conn/IPQC_Report/From/Temp/' . $filename . '.xlsx');

$xls = PHPExcel_IOFactory::load('../../Report/QV_System/FQ_Conn/IPQC_Report/From/Temp/' . $filename . '.xlsx');

$xls->setActiveSheetIndexByName('#temp');
$xls->getActiveSheet()->setTitle('Data');
$xls->setActiveSheetIndexByName('Data');
$xls_sheet = $xls->getActiveSheet();

$xls_sheet->setCellValue('D5', $listoutT['PartNumber']);
$xls_sheet->setCellValue('D6', $listoutT['PartDescription']);
$xls_sheet->setCellValue('D7', $listoutT['LineNumber']);
$xls_sheet->setCellValue('L5', $listoutT['QV_Inspector']);
$xls_sheet->setCellValue('L6', $listoutT['PhaseNumber']);
$xls_sheet->setCellValue('L7', $listoutT['DateTime']);

$N = 10;
while ($listout = mysqli_fetch_assoc($listoutD)) {
    $xls_sheet->setCellValue('A' . $N, $listout['DimNO']);
    $xls_sheet->setCellValue('B' . $N, $listout['DimSpec']);
    $xls_sheet->setCellValue('C' . $N, $listout['DimUpper']);
    $xls_sheet->setCellValue('D' . $N, $listout['DimLower']);
    $xls_sheet->setCellValue('E' . $N, $listout['Sample1']);
    $xls_sheet->setCellValue('E' . $N, $listout['Sample1']);
    $xls_sheet->setCellValue('F' . $N, $listout['Sample2']);
    $xls_sheet->setCellValue('G' . $N, $listout['Sample3']);
    $xls_sheet->setCellValue('H' . $N, $listout['Sample4']);
    $xls_sheet->setCellValue('I' . $N, $listout['Sample5']);
    $xls_sheet->setCellValue('J' . $N, "=MIN(E" . $N . ":I" . $N . ")");
    $xls_sheet->setCellValue('K' . $N, "=MAX(E" . $N . ":I" . $N . ")");
    $xls_sheet->setCellValue('L' . $N, "=AVERAGE(E" . $N . ":I" . $N . ")");
    if (between($listout['DimLower'], $listout['DimUpper'], array($listout['Sample1'], $listout['Sample2'], $listout['Sample3'], $listout['Sample4'], $listout['Sample5']))) {
        $xls_sheet->setCellValue('M' . $N, 'OK');
    } else {
        $xls_sheet->setCellValue('M' . $N, 'NG');
        $xls_sheet->getStyle('M' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FF0000')));
        //$xls_sheet->setCellValue('N' . $N, $listout['SCAR']);
    }

    if (between($listout['DimLower'], $listout['DimUpper'], $listout['Sample1'])) {
    } else {
        $xls_sheet->getStyle('E' . $N)->getFont()->getColor()->setARGB('FF0000');
        $xls_sheet->getStyle('E' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));
    }
    if (between($listout['DimLower'], $listout['DimUpper'], $listout['Sample2'])) {
    } else {
        $xls_sheet->getStyle('F' . $N)->getFont()->getColor()->setARGB('FF0000');
        $xls_sheet->getStyle('F' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));
    }
    if (between($listout['DimLower'], $listout['DimUpper'], $listout['Sample3'])) {
    } else {
        $xls_sheet->getStyle('G' . $N)->getFont()->getColor()->setARGB('FF0000');
        $xls_sheet->getStyle('G' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));
    }
    if (between($listout['DimLower'], $listout['DimUpper'], $listout['Sample4'])) {
    } else {
        $xls_sheet->getStyle('H' . $N)->getFont()->getColor()->setARGB('FF0000');
        $xls_sheet->getStyle('H' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));
    }
    if (between($listout['DimLower'], $listout['DimUpper'], $listout['Sample5'])) {
    } else {
        $xls_sheet->getStyle('I' . $N)->getFont()->getColor()->setARGB('FF0000');
        $xls_sheet->getStyle('I' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));
    }
    $xls_sheet->getRowDimension($N)->setRowHeight(25);
    $N = $N + 1;
}

header('Content-Disposition: attachment;filename=' . $filename_xlsx . '');
header('content-transfer-encoding: binary');
$objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
$objWriter->setPreCalculateFormulas(false);
$objWriter->save('php://output');

function between($min, $max, $value)
{
    //處理成陣列
    if (is_array($value)) {
        $limit = $value;
    } else {
        $limit = explode(",", $value);
    }
    //合併成多個數值
    $value   = array_merge($limit, $limit);
    $limit[] = $max;
    $limit[] = $min;

    //使用max及min函數判斷是否在區間內
    if ((max($limit) == $max && min($limit) == $min) || (max($value) == $max && min($value) == $min)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Data_CreateReportByDay-3</title>

</head>
</html>
