<?php
//error_reporting(0);
//as
require_once '../../Public/Connections/modify_system_fq_icbu.php';

mysqli_select_db($connect_asm, $database_asm);

if (isset($_REQUEST['qcc'])) {
    $qcc = stripslashes($_REQUEST['qcc']);
} else {
    $qcc = '';
}

if (isset($_REQUEST['qpp'])) {
    $qpp = stripslashes($_REQUEST['qpp']);
} else {
    $qpp = '';
}

if (isset($_REQUEST['qdd'])) {
    $qdd = stripslashes($_REQUEST['qdd']);
} else {
    $qdd = '';
}

if ($_REQUEST['Rev'] == '巡查檢驗') {

    $t = filter_input(INPUT_GET, 't', FILTER_SANITIZE_STRING);

    switch ($t) {
        case 0:
            {
                $query_Part_NumberF = "SELECT DISTINCT(PartNumber) FROM modify_visual_measuredata WHERE 1=1 " . $qcc . " " . $qpp . " " . $qdd . "  ORDER BY PartNumber ASC ";
                $PartNumberF        = mysqli_query($connect_asm, $query_Part_NumberF);
                while ($rows = mysqli_fetch_assoc($PartNumberF)) {
                    $data[] = array($rows['PartNumber']);
                }
                //$data[]=array($query_Mold_NumberF);
                echo json_encode($data);
                break;
            }

        case 1:
            {
                $query_Part_NumberF = "SELECT DISTINCT(Productline) FROM modify_visual_measuredata WHERE 1=1 " . $qcc . " " . $qpp . " " . $qdd . "  ORDER BY Productline ASC ";
                $PartNumberF        = mysqli_query($connect_asm, $query_Part_NumberF);
                while ($rows = mysqli_fetch_assoc($PartNumberF)) {
                    $data[] = array($rows['Productline']);
                }
                //$data[]=array($query_Mold_NumberF);
                echo json_encode($data);
                break;
            }
    }
}

if ($_REQUEST['Rev'] == '初件檢驗') {

    $t = filter_input(INPUT_GET, 't', FILTER_SANITIZE_STRING);

    switch ($t) {
        case 0:
            {
                $query_Part_NumberF = "SELECT DISTINCT(PartNumber) FROM modify_visual_first_measuredata WHERE 1=1 " . $qcc . " " . $qpp . " " . $qdd . "  ORDER BY PartNumber ASC ";
                $PartNumberF        = mysqli_query($connect_asm, $query_Part_NumberF);
                while ($rows = mysqli_fetch_assoc($PartNumberF)) {
                    $data[] = array($rows['PartNumber']);
                }
                //$data[]=array($query_Mold_NumberF);
                echo json_encode($data);
                break;
            }
        case 1:
            {
                $query_Part_NumberF = "SELECT DISTINCT(Productline) FROM modify_visual_first_measuredata WHERE 1=1 " . $qcc . " " . $qpp . " " . $qdd . "  ORDER BY Productline ASC ";
                $PartNumberF        = mysqli_query($connect_asm, $query_Part_NumberF);
                while ($rows = mysqli_fetch_assoc($PartNumberF)) {
                    $data[] = array($rows['Productline']);
                }
                //$data[]=array($query_Mold_NumberF);
                echo json_encode($data);
                break;
            }
    }
}
