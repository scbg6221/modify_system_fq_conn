<?php
session_start();
//error_reporting(0);

date_default_timezone_set('Asia/Taipei');
//$DateTime      = date("Y-m-d", strtotime("-1 day"));
$RequestDate   = $_REQUEST['Date1'];
$RequesMachine = $_REQUEST['RequesMachine'];
$RequesItem    = $_REQUEST['RequesItem'];
$LineNumber    = $_REQUEST['Line'];
$PartNumber    = $_REQUEST['Part'];
$Datatable     = $_REQUEST['Datatable'];
//$d1            = $Date . " 08:00:00";
//$d2            = date('Y-m-d', strtotime($Date) + 60 * 60 * 24 * 1) . " 07:59:59";

if ($RequesMachine) {$qa = "AND MachineName='$RequesMachine' ";}
if ($RequesItem) {$qb = "AND RequestTestItem='$RequesItem' ";}
if ($PartNumber) {$qc = "AND PartNumber='$PartNumber' ";}
if ($LineNumber) {$qd = "AND LineNumber='$LineNumber' ";}
if ($RequestDate) {$qe = "AND RequestDate='$RequestDate' ";}

require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../Public/library/pclzip/pclzip-2-8-2/pclzip.lib.php';

mysqli_select_db($connect_ort, $database_ort);
$select_Info = "SELECT * FROM `modify_measurecontent` WHERE 1=1  " . $qa . " " . $qb . " " . $qc . " " . $qd . " " . $qe . " ORDER BY MachineName,RequestTestItem,Phase";

$query_Info = mysqli_query($connect_ort, $select_Info) or die(mysqli_error());
$i          = 0;
while ($listoutInfos = mysqli_fetch_assoc($query_Info)) {
    $ServiceNumber[] = $listoutInfos['ServiceNumber'];
    $Phase0          = $listoutInfos['Phase'];
    $RequestStatus0  = $listoutInfos['RequestStatus'];
    $PartNumber      = $listoutInfos['PartNumber'];
    $LineNumber      = $listoutInfos['LineNumber'];
    $MachineName     = $listoutInfos['MachineName'];
    $Test_Inspector  = $listoutInfos['Test_Inspector'];
    $RequestTestItem = $listoutInfos['RequestTestItem'];
    $MeasureEndDate1 = substr($listoutInfos['RequestDate'], 0, 10);
    $MeasureEndDate2 = substr($listoutInfos['RequestDate'], 0, 4) . substr($listoutInfos['RequestDate'], 5, 2) . substr($listoutInfos['RequestDate'], 8, 2);

    $select_Data = "SELECT * FROM " . $Datatable . " WHERE 1=1  AND ServiceNumber=" . $listoutInfos['ServiceNumber'] . " ORDER BY DimNOOrder";
    $query_Data  = mysqli_query($connect_ort, $select_Data) or die(mysqli_error());

    while ($listoutData = mysqli_fetch_assoc($query_Data)) {
        $Phase[]         = $Phase0;
        $RequestStatus[] = $RequestStatus0;
        $DimNO[]         = $listoutData['DimNO'];
        $UpperSpec[]     = $listoutData['UpperSpec'];
        $LowerSpec[]     = $listoutData['LowerSpec'];
        $Leakage[]       = $listoutData['Leakage'];
        $AirPressure[]   = $listoutData['AirPressure'];
        $Temperature[]   = $listoutData['Temperature'];
        $i++;
    }
}

$filename1     = $MeasureEndDate2 . '_' . $PartNumber . '_' . $LineNumber . '_' . $MachineName . '_' . $RequestTestItem;
$filename      = mb_convert_encoding($filename1, "big5", "utf8");
$filename_xlsx = $filename . ".xlsx";

$xls1 = PHPExcel_IOFactory::load('Report_Template/Assembly_ORT/Conn_IPQC_airtight.xlsx');
$xls1->setActiveSheetIndexByName('#temp');
$xls_sheet1 = $xls1->getActiveSheet();

$objStyleA1 = $xls_sheet1->getStyle('A10');

//设置字体
//$objFontA1 = $objStyleA1->getFont();
//$objFontA1->setName('Courier New');
//$objFontA1->setSize(10);
//$objFontA1->setBold(true);
//$objFontA1->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);

//设置对齐方式
$objAlignA1 = $objStyleA1->getAlignment();
$objAlignA1->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objAlignA1->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

//设置边框
$objBorderA1 = $objStyleA1->getBorders();
$objBorderA1->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objBorderA1->getTop()->getColor()->setARGB('000000'); // color
$objBorderA1->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objBorderA1->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objBorderA1->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

//设置填充颜色
//$objFillA1 = $objStyleA1->getFill();
//$objFillA1->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
//$objFillA1->getStartColor()->setARGB('FFEEEEEE');

//从指定的单元格复制样式信息.
$xls_sheet1->duplicateStyle($objStyleA1, 'A10:I' . ($i + 9));

$objWriter1 = PHPExcel_IOFactory::createWriter($xls1, 'Excel2007');
$objWriter1->save('../../Report/Modify_System/FQ_Conn/ORT_Report/' . $filename . '.xlsx');

$xls = PHPExcel_IOFactory::load('../../Report/Modify_System/FQ_Conn/ORT_Report/' . $filename . '.xlsx');

$xls->setActiveSheetIndexByName('#temp');
$xls->getActiveSheet()->setTitle('氣密機');
$xls->setActiveSheetIndexByName('氣密機');
$xls_sheet = $xls->getActiveSheet();

$xls_sheet->setCellValue('C5', $PartNumber);
$xls_sheet->setCellValue('G5', $Test_Inspector);
$xls_sheet->setCellValue('C6', '氣密機');
$xls_sheet->setCellValue('G6', $RequestTestItem);
$xls_sheet->setCellValue('C7', $LineNumber);
$xls_sheet->setCellValue('G7', $MeasureEndDate1);

$N = 10;
for ($j = 0; $j < $i; $j++) {

    $xls_sheet->setCellValue('A' . $N, $Phase[$j]);
    $xls_sheet->setCellValue('B' . $N, $RequestStatus[$j]);
    $xls_sheet->setCellValue('C' . $N, $DimNO[$j]);
    $xls_sheet->setCellValue('D' . $N, $UpperSpec[$j]);
    $xls_sheet->setCellValue('E' . $N, $LowerSpec[$j]);
    $xls_sheet->setCellValue('F' . $N, $Leakage[$j]);
    $xls_sheet->setCellValue('G' . $N, $AirPressure[$j]);
    $xls_sheet->setCellValue('H' . $N, $Temperature[$j]);

    if ($Leakage[$j] > $UpperSpec[$j]) {
        $xls_sheet->getStyle('F' . $N)->getFont()->getColor()->setARGB('FF0000');
        $xls_sheet->getStyle('F' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));

        $xls_sheet->setCellValue('I' . $N, 'NG');
        $xls_sheet->getStyle('I' . $N)->getFont()->getColor()->setARGB('FF0000');
        $xls_sheet->getStyle('I' . $N)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => 'FFFF00')));
    } else {
        $xls_sheet->setCellValue('I' . $N, 'OK');
    }

    $xls_sheet->getRowDimension($N)->setRowHeight(25);
    $N = $N + 1;
}

// Save Excel 2007 file
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=' . $filename_xlsx . '');
header('content-transfer-encoding: binary');
$objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
$objWriter->setIncludeCharts(true);
$objWriter->setPreCalculateFormulas(false);
$objWriter->save('php://output');

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Data_CreateReportByDay-3</title>

</head>
</html>
