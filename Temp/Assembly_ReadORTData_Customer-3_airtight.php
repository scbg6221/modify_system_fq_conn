<?php
/*
 * Copyright (C) 2014 Foxlink SCBG CAE Dept. All rights reserved
 * Project: Projector System
 * File Name: Measure Data-2
 * Function: Result of Measure Data
 * Author: Angel Wang
 * --------------------------------------------------
 * Rev: 1.2 Date: PM 04:34 2014/05/29 Modifier: Angel Wang
 * --------------------------------------------------
 */

include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
error_reporting(0);
if (!isset($_SESSION)) {session_start();}

$RequestDate   = $_REQUEST['Date1'];
$RequestStatus = $_REQUEST['RequestStatus'];
$RequesMachine = $_REQUEST['RequesMachine'];
$RequesItem    = $_REQUEST['RequesItem'];
$LineNumber    = $_REQUEST['Line'];
$PartNumber    = $_REQUEST['Part'];
$Phase         = $_REQUEST['Phase'];
$data_table    = $_REQUEST['Datatable'];
//$d1            = date('Y-m-d', strtotime($Date));
//$d2            = date('Y-m-d', strtotime($Date) + 60 * 60 * 24);

if ($RequestStatus) {$qa = "AND modify_measurecontent.RequestStatus='$RequestStatus' ";}
if ($RequesMachine) {$qb = "AND modify_measurecontent.MachineName='$RequesMachine' ";}
if ($RequesItem) {$qv = "AND modify_measurecontent.RequestTestItem='$RequesItem' ";}
if ($LineNumber) {$qd = "AND modify_measurecontent.LineNumber='$LineNumber' ";}
if ($PartNumber) {$qe = "AND modify_measurecontent.PartNumber='$PartNumber' ";}
if ($Phase) {$qf = "AND modify_measurecontent.Phase='$Phase' ";}
if ($RequestDate) {$qg = "AND modify_measurecontent.RequestDate ='$RequestDate' ";}
mysqli_select_db($connect_ort, $database_ort);

$query_listoutF =
    "SELECT
        modify_measurecontent.LineNumber,
        modify_measurecontent.PartNumber,
        modify_measurecontent.ServiceNumber,
        modify_measurecontent.MachineName,
        modify_measurecontent.RequestStatus,
        modify_measurecontent.RequestTestItem,
        modify_measurecontent.RequestDate,
        modify_measurecontent.Phase,
        " . $data_table . ".DimNO,
        " . $data_table . ".DimNOOrder,
        " . $data_table . ".UpperSpec,
        " . $data_table . ".LowerSpec,
        " . $data_table . ".AirPressure,
        " . $data_table . ".Leakage,
        " . $data_table . ".Temperature,
        " . $data_table . ".TestResult,
        " . $data_table . ".TestDate,
        " . $data_table . ".TestTime
    FROM modify_measurecontent, " . $data_table . " WHERE 1=1  " . $qa . " " . $qb . " " . $qc . " " . $qd . " " . $qe . " " . $qf . " " . $qg . "  AND " . $data_table . ".ServiceNumber=modify_measurecontent.ServiceNumber ORDER BY RequestDate,Phase ASC";
//echo $query_listoutF;
$listoutF = mysqli_query($connect_ort, $query_listoutF);
?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<script src="../../Public/library/Other/Sorttable.js"></script>
<script type="text/javascript">

</script>
<link rel="stylesheet" type="text/css" href="CSS/ALL_CSS.css">
</head>
<body>
<form id="form1" name="form1" method="post" >

<div class="Measure-Data-table-2">
<table id="Measure-Data-table-2" class="sortable">
<thead>

<?php
//<input type='button' name='submitA' id='submitA'  value='下載報告' class='ReadData-BT' onClick='result();'/>
echo "<th><div align='center'>料號</div></th>";
echo "<th><div align='center'>線別號</div></th>";
echo "<th><div align='center'>測試機台</div></th>";
echo "<th><div align='center'>測試項目</div></th>";
echo "<th><div align='center'>測試階段</div></th>";
echo "<th><div align='center'>測試節次</div></th>";
echo "<th><div align='center'>FAI號</div></th>";
echo "<th><div align='center'>洩漏量上限</div></th>";
echo "<th><div align='center'>洩漏量下限</div></th>";
echo "<th><div align='center'>氣壓值</div></th>";
echo "<th><div align='center'>洩漏量</div></th>";
echo "<th><div align='center'>溫度</div></th>";
echo "<th><div align='center'>量測時間</div></th>";
echo "<th><div align='center'>判定</div></th>";
echo "</thead>";
echo "<div align='center'></div>";
echo "<tbody>";

while ($listout = mysqli_fetch_assoc($listoutF)) {

    $ServiceNumber = $listout['ServiceNumber'];
    $UpperSpec     = $listout['UpperSpec'];

    $color = $MeasureResult = '';

    if ($listout['Leakage'] > $UpperSpec) {
        $color         = "<font color='#EE0000'>";
        $MeasureResult = 'NG';
    } else {
        $color         = "";
        $MeasureResult = 'OK';
    }

    echo "<tr>";
    echo "<td>" . $color . $listout['PartNumber'] . "</td>";
    echo "<td>" . $color . $listout['LineNumber'] . "</td>";
    echo "<td>" . $color . $listout['MachineName'] . "</td>";
    echo "<td>" . $color . $listout['RequestTestItem'] . "</td>";
    echo "<td>" . $color . $listout['RequestStatus'] . "</td>";
    echo "<td>" . $color . $listout['Phase'] . "</td>";
    echo "<td>" . $color . $listout['DimNO'] . "</td>";
    echo "<td>" . $color . $listout['UpperSpec'] . "</td>";
    echo "<td>" . $color . $listout['LowerSpec'] . "</td>";
    echo "<td>" . $color . $listout['AirPressure'] . "</td>";
    echo "<td>" . $color . $listout['Leakage'] . "</td>";
    echo "<td>" . $color . $listout['Temperature'] . "</td>";
    echo "<td>" . $color . $listout['RequestDate'] . " 節次:" . $listout['Phase'] . "</td>";
    echo "<td>" . $color . $listout['TestResult'] . "</td>";
    echo "</tr>";
}

echo "
   <div style='float:left'>
    <input type='hidden' name='ServiceNumber' id='ServiceNumber'  value='" . $ServiceNumber . "' class='ReadData-2'>
   </div>"
?>
</tbody>
</table>
</form>
</body>
</html>


