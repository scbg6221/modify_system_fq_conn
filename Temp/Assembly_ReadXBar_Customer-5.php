<?php
//Some variable is session from xbart-2 not all of it are post
session_start();
error_reporting(E_ERROR | E_PARSE);?>
<?php
require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';
ini_set("memory_limit", "-1");
ini_set("max_execution_time", '0');

$sum_array = $avg_array = $R_array = $R_array1 = $sum_array = $avg_array = $Date_array = $Time_array = $dataseriesLabels = $MeasureData1_array = $MeasureData2_array = $MeasureData3_array = $MeasureData4_array = $MeasureData5_array = $CLr_array = $UCLr_array = $LCLr_array = $CLx_array = $UCLx_array = $LCLx_array = array();

$Piece              = $_SESSION['Piece'];
$Factory            = $_SESSION['Factory'];
$Department         = $_SESSION['Department'];
$User               = $_SESSION['user'];
$Start_Date         = $_POST['StartDateF'];
$End_Date           = $_POST['EndDateF'];
$Part_Description   = $_POST['PartDescriptionF'];
$Cpk_Target         = '1.33';
$Part_Number_V      = $_POST['PartNumberF'];
$FAI_Number         = $_POST['DimNOF'];
$Line_Number        = $_POST['LineNumberF'];
$Date1              = date("Y-m-d");
$Nominal_Dim        = $_SESSION['Nominal_Dim'];
$Upper_Dim          = $_SESSION['Upper_Dim'];
$Lower_Dim          = $_SESSION['Lower_Dim'];
$MeasureData1_array = $_SESSION['MeasureData_1'];
$MeasureData2_array = $_SESSION['MeasureData_2'];
$MeasureData3_array = $_SESSION['MeasureData_3'];
$MeasureData4_array = $_SESSION['MeasureData_4'];
$MeasureData5_array = $_SESSION['MeasureData_5'];
$Date_array         = $_SESSION['Date'];
$Time_array         = $_SESSION['Time'];
$Sampling_Rate      = $_SESSION['Sampling_Rate'];
$Total_Samples      = $_SESSION['Total_Samples'];
$FAI_Nums1          = $_SESSION['DimNOF'];
$Group_Nums         = $_SESSION['Group_Nums'];
$Group_array        = $_SESSION['Group_array'];
$CLx                = $_SESSION['CLx'];
$UCLx               = $_SESSION['UCLx'];
$LCLx               = $_SESSION['LCLx'];
$CLr                = $_SESSION['CLr'];
$UCLr               = $_SESSION['UCLr'];
$LCLr               = $_SESSION['LCLr'];
$CpU                = $_SESSION['CpU'];
$CpL                = $_SESSION['CpL'];
$Cp                 = $_SESSION['Cp'];
$Ca                 = $_SESSION['Ca'];
$Cpk                = $_SESSION['Cpk'];
$CLr_array          = $_SESSION['CLrr'];
$UCLr_array         = $_SESSION['UCLrr'];
$LCLr_array         = $_SESSION['LCLrr'];
$CLx_array          = $_SESSION['CLxx'];
$UCLx_array         = $_SESSION['UCLxx'];
$LCLx_array         = $_SESSION['LCLxx'];
$Date_array         = $_SESSION['Date_array'];
$Time_array         = $_SESSION['Time_array'];

$sum_array = $_SESSION['sum1'];
$R_array1  = $_SESSION['R1'];
$avg_array = $_SESSION['avg1'];

$R_count = count($R_array1);

$filename = "X_bar Chart_" . $Part_Number_V . "(FAI_" . $FAI_Number . ").xlsx";

$objPHPExcel = PHPExcel_IOFactory::load("Report_Template/Assembly_QVGV/X_bar_Chart_Temp.xlsx");

$objPHPExcel->getActiveSheet(0)->setTitle('sheet1');

$objWorksheet = $objPHPExcel->getActiveSheet(0);

$objWorksheet->setCellValue('A3', $Factory);
$objWorksheet->setCellValue('A5', $Part_Number_V);
$objWorksheet->setCellValue('E3', $Department);
$objWorksheet->setCellValue('E5', $Part_Description);
$objWorksheet->setCellValue('I3', $User);
$objWorksheet->setCellValue('I5', $Date1);
$objWorksheet->setCellValue('M3', $Start_Date);
$objWorksheet->setCellValue('P3', $End_Date);
$objWorksheet->setCellValue('M5', $Nominal_Dim);
$objWorksheet->setCellValue('O5', $Upper_Dim);
$objWorksheet->setCellValue('Q5', $Lower_Dim);
$objWorksheet->setCellValue('R3', $Line_Number);
$objWorksheet->setCellValue('R5', $FAI_Number);
$objWorksheet->setCellValue('U5', $Sampling_Rate);
$objWorksheet->setCellValue('W3', $Cpk_Target);
$objWorksheet->setCellValue('W5', $Group_Nums);
$objWorksheet->setCellValue('A24', $CLx);
$objWorksheet->setCellValue('B24', $UCLx);
$objWorksheet->setCellValue('C24', $LCLx);
$objWorksheet->setCellValue('D24', $CLr);
$objWorksheet->setCellValue('E24', $UCLr);
$objWorksheet->setCellValue('F24', $LCLr);
$objWorksheet->setCellValue('G24', $CpU);
$objWorksheet->setCellValue('H24', $CpL);
$objWorksheet->setCellValue('I24', $Cp);
$objWorksheet->setCellValue('J24', $Ca);
$objWorksheet->setCellValue('K24', $Cpk);

$objWorksheet->fromArray($Group_array, null, 'C7', true);
$objWorksheet->fromArray($Group_array, null, 'C68', true);
$objWorksheet->fromArray($Date_array, null, 'C8', true);
$objWorksheet->fromArray($Time_array, null, 'C9', true);

for ($t = 1; $t <= $Piece; $t++) {
    $q = $t + 9;
    $objWorksheet->fromArray(${'MeasureData' . $t . '_array'}, null, 'C' . $q, true);
}

$objWorksheet->fromArray($sum_array, null, 'C20', true);
$objWorksheet->fromArray($avg_array, null, 'C21', true);
$objWorksheet->fromArray($R_array1, null, 'C22', true);
$objWorksheet->fromArray($avg_array, null, 'C69', true);
$objWorksheet->fromArray($CLx_array, null, 'C70', true);
$objWorksheet->fromArray($UCLx_array, null, 'C71', true);
$objWorksheet->fromArray($LCLx_array, null, 'C72', true);
$objWorksheet->fromArray($R_array1, null, 'C73', true);
$objWorksheet->fromArray($CLr_array, null, 'C74', true);
$objWorksheet->fromArray($UCLr_array, null, 'C75', true);
$objWorksheet->fromArray($LCLr_array, null, 'C76', true);

///定義Excel欄位名稱///
$Width1 = array('', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB');
if ($Group_Nums < 26) {
    $Group_Nums1 = $Group_Nums + 2;
    $Width6      = $Width1[$Group_Nums1];
}

if ($Group_Nums >= 26) {
    $Group_Nums1 = $Group_Nums + 2;
    $Width3      = $Group_Nums1 / 26;
    $Width4      = floor($Width3);
    $Width5      = $Group_Nums1 - 26 * $Width4;
    $Width6      = $Width1[$Width4] . $Width1[$Width5];
}

////畫框線///
$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '000000'),
        ),
    ),
);

$objWorksheet->getStyle('A7:' . $Width6 . '22')->applyFromArray($styleArray);
$objWorksheet->getStyle('B68:' . $Width6 . '76')->applyFromArray($styleArray);

// 设置每一个data series 数据系列的名称
$dataseriesLabels = array(
    new PHPExcel_Chart_DataSeriesValues('String', 'sheet1!B69', null, 1), //  X_Bar
    new PHPExcel_Chart_DataSeriesValues('String', 'sheet1!B70', null, 1), //  CLx
    new PHPExcel_Chart_DataSeriesValues('String', 'sheet1!B71', null, 1), //  UClx
    new PHPExcel_Chart_DataSeriesValues('String', 'sheet1!B72', null, 1), //  LCLx
);
// 设置X轴Tick数据(X轴每一个刻度值)
$xAxisTickValues = array(
    new PHPExcel_Chart_DataSeriesValues('String', 'sheet1!C68:' . $Width6 . '68', null, $Group_Nums),
);
//  设置作图区域数据
$dataSeriesValues = array(
    new PHPExcel_Chart_DataSeriesValues('Number', 'sheet1!$C$69:' . $Width6 . '69', null, $Group_Nums),
    new PHPExcel_Chart_DataSeriesValues('Number', 'sheet1!$C$70:' . $Width6 . '70', null, $Group_Nums),
    new PHPExcel_Chart_DataSeriesValues('Number', 'sheet1!$C$71:' . $Width6 . '71', null, $Group_Nums),
    new PHPExcel_Chart_DataSeriesValues('Number', 'sheet1!$C$72:' . $Width6 . '72', null, $Group_Nums),
);

//  构建数据系列 dataseries
$series = new PHPExcel_Chart_DataSeries(
    PHPExcel_Chart_DataSeries::TYPE_LINECHART, // plotType
    PHPExcel_Chart_DataSeries::GROUPING_STANDARD, // plotGrouping
    //PHPExcel_Chart_DataSeries::GROUPING_STACKED,  // plotGrouping
    range(0, count($dataSeriesValues) - 1), // plotOrder
    $dataseriesLabels, // plotLabel
    $xAxisTickValues, // plotCategory
    $dataSeriesValues // plotValues
);

// 给数据系列分配一个做图区域
$plotarea = new PHPExcel_Chart_PlotArea(null, array($series));
// Set the chart legend
$legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_TOPRIGHT, null, false);
// 设置图形标题
$title = new PHPExcel_Chart_Title('AVERAGE (X_Bar Chart)');
// 设置Y轴标签
$yAxisLabel = new PHPExcel_Chart_Title('Value');

// 创建图形
$chart = new PHPExcel_Chart(
    'chart1', // name
    $title, // title
    $legend, // legend
    $plotarea, // plotArea
    true, // plotVisibleOnly
    0, // displayBlanksAs
    null, // xAxisLabel
    $yAxisLabel // yAxisLabel
);

// 设置图形绘制区域
$chart->setTopLeftPosition('A26');
$chart->setBottomRightPosition('' . $Width6 . '42');

// 将图形添加到当前工作表
$objWorksheet->addChart($chart);

// 设置每一个data series 数据系列的名称
$dataseriesLabels2 = array(
    new PHPExcel_Chart_DataSeriesValues('String', 'sheet1!B73', null, 1), //  R
    new PHPExcel_Chart_DataSeriesValues('String', 'sheet1!B74', null, 1), //  CLr
    new PHPExcel_Chart_DataSeriesValues('String', 'sheet1!B75', null, 1), //  UClr
    new PHPExcel_Chart_DataSeriesValues('String', 'sheet1!B76', null, 1), //  LCLr

);
//  设置X轴Tick数据(X轴每一个刻度值)
$xAxisTickValues2 = array(
    new PHPExcel_Chart_DataSeriesValues('String', 'sheet1!C68:' . $Width6 . '68', null, $Group_Nums),
);
//  设置作图区域数据
$dataSeriesValues2 = array(
    new PHPExcel_Chart_DataSeriesValues('Number', 'sheet1!C$73:' . $Width6 . '73', null, $Group_Nums),
    new PHPExcel_Chart_DataSeriesValues('Number', 'sheet1!C$74:' . $Width6 . '74', null, $Group_Nums),
    new PHPExcel_Chart_DataSeriesValues('Number', 'sheet1!C$75:' . $Width6 . '75', null, $Group_Nums),
    new PHPExcel_Chart_DataSeriesValues('Number', 'sheet1!C$76:' . $Width6 . '76', null, $Group_Nums),
);

//  构建数据系列 dataseries
$series2 = new PHPExcel_Chart_DataSeries(
    PHPExcel_Chart_DataSeries::TYPE_LINECHART, // plotType
    PHPExcel_Chart_DataSeries::GROUPING_STANDARD, // plotGrouping
    //PHPExcel_Chart_DataSeries::GROUPING_STACKED,    // plotGrouping
    range(0, count($dataSeriesValues2) - 1), // plotOrder
    $dataseriesLabels2, // plotLabel
    $xAxisTickValues2, // plotCategory
    $dataSeriesValues2 // plotValues
);

// 给数据系列分配一个做图区域
$plotarea2 = new PHPExcel_Chart_PlotArea(null, array($series2));
// Set the chart legend
$legend2 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_TOPRIGHT, null, false);
// 设置图形标题
$title2 = new PHPExcel_Chart_Title('RANGE (R Chart)');
// 设置Y轴标签
$yAxisLabel2 = new PHPExcel_Chart_Title('Value');

// 创建图形
$chart2 = new PHPExcel_Chart(
    'chart2', // name
    $title2, // title
    $legend2, // legend
    $plotarea2, // plotArea
    true, // plotVisibleOnly
    0, // displayBlanksAs
    null, // xAxisLabel
    $yAxisLabel2 // yAxisLabel
);

// 设置图形绘制区域
$chart2->setTopLeftPosition('A43');
$chart2->setBottomRightPosition('' . $Width6 . '57');

// 将图形添加到当前工作表
$objWorksheet->addChart($chart2);

// Save Excel 2007 file
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=' . $filename . '');
header('content-transfer-encoding: binary');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->setIncludeCharts(true);
$objWriter->setPreCalculateFormulas(false);
$objWriter->save('php://output');
