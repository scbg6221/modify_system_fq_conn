<?php
//error_reporting(0);
require_once '../../Public/Connections/modify_system_fq_icbu.php';
mysqli_select_db($connect_asm, $database_asm);

if (isset($_REQUEST['qaa'])) {
    $qaa = stripslashes($_REQUEST['qaa']);
} else {
    $qaa = '';
}

if (isset($_REQUEST['qbb'])) {
    $qbb = stripslashes($_REQUEST['qbb']);
} else {
    $qbb = '';
}

if (isset($_REQUEST['qcc'])) {
    $qcc = stripslashes($_REQUEST['qcc']);
} else {
    $qcc = '';
}

if (isset($_REQUEST['qdd'])) {
    $qdd = stripslashes($_REQUEST['qdd']);
} else {
    $qdd = '';
}

if (isset($_REQUEST['floor'])) {
    $floor = stripslashes($_REQUEST['floor']);
} else {
    $floor = '';
}

$t = filter_input(INPUT_GET, 't', FILTER_SANITIZE_STRING);

switch ($t) {

    case 0:
        {
            $query_PartDescriptionF = "SELECT DISTINCT(PartDescription) FROM modify_measurecontent WHERE 1=1 " . $qaa . " " . $qbb . " " . $qcc . " " . $qdd . "  GROUP BY PartDescription ORDER BY PartDescription ASC ";
            $PartDescriptionF       = mysqli_query($connect_asm, $query_PartDescriptionF);
            while ($rows = mysqli_fetch_assoc($PartDescriptionF)) {
                $data[] = array($rows['PartDescription']);
            }
            //$data[] = array($query_PartDescriptionF);
            echo json_encode($data);
            break;
        }

    case 1:
        {
            $query_PartNumberF = "SELECT DISTINCT(PartNumber) FROM modify_measurecontent WHERE 1=1 " . $qaa . " " . $qbb . " " . $qcc . " " . $qdd . " GROUP BY PartNumber ORDER BY PartNumber ASC";
            $PartNumberF       = mysqli_query($connect_asm, $query_PartNumberF);
            while ($rows = mysqli_fetch_assoc($PartNumberF)) {
                $data[] = array($rows['PartNumber']);
            }
//$data[] = array($query_PartNumberF);
            echo json_encode($data);
        }
        break;

    case 2:
        {
            $query_LineNumberF = "SELECT DISTINCT(LineNumber) FROM modify_measurecontent WHERE 1=1 " . $qaa . " " . $qbb . " " . $qcc . " " . $qdd . " GROUP BY LineNumber ORDER BY LineNumber ASC ";
            $LineNumberF       = mysqli_query($connect_asm, $query_LineNumberF);
            while ($rows = mysqli_fetch_assoc($LineNumberF)) {
                $data[] = array($rows['LineNumber']);
            }
//$data[] = array($query_LineNumberF);
            echo json_encode($data);

        }
        break;

    case 3:
        {
            if ($qbb != '' && $qcc != '') {
                $query_ServiceNumberF = "SELECT DISTINCT(ServiceNumber) FROM modify_measurecontent WHERE 1=1 " . $qaa . " " . $qbb . " " . $qcc . " ORDER BY ServiceNumber";
                $ServiceNumberF       = mysqli_query($connect_asm, $query_ServiceNumberF);
                while ($rows = mysqli_fetch_assoc($ServiceNumberF)) {
                    $data[] = array($rows['ServiceNumber']);
                }
            }

//$data[] = array($query_ServiceNumberF);
            echo json_encode($data);
        }
        break;

}
