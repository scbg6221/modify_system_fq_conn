<?php
//error_reporting(0);
//as
require_once '../../Public/Connections/modify_system_fq_icbu.php';
mysqli_select_db($connect_asm, $database_asm);

if (isset($_REQUEST['qaa'])) {
    $qaa = stripslashes($_REQUEST['qaa']);
} else {
    $qaa = '';
}

if (isset($_REQUEST['qbb'])) {
    $qbb = stripslashes($_REQUEST['qbb']);
} else {
    $qbb = '';
}

if (isset($_REQUEST['qcc'])) {
    $qcc = stripslashes($_REQUEST['qcc']);
} else {
    $qcc = '';
}

if (isset($_REQUEST['qdd'])) {
    $qdd = stripslashes($_REQUEST['qdd']);
} else {
    $qdd = '';
}

$t = filter_input(INPUT_GET, 't', FILTER_SANITIZE_STRING);

switch ($t) {

    case 0:
        {
            $query_Project_NameF = "SELECT DISTINCT(PartDescription) FROM modify_measurecontent WHERE 1=1 " . $qaa . " " . $qbb . " " . $qcc . " " . $qdd . "  ORDER BY PartDescription ASC ";
            $ProjectNameF        = mysqli_query($connect_asm, $query_Project_NameF);
            while ($rows = mysqli_fetch_assoc($ProjectNameF)) {
                $data[] = array($rows['PartDescription']);
            }
            //$data[]=array($query_Project_NameF);
            echo json_encode($data);
            //echo $query_Project_NameF;
            break;
        }
    case 1:
        {
            $query_Part_NumberF = "SELECT DISTINCT(PartNumber) FROM modify_measurecontent WHERE 1=1 " . $qaa . " " . $qbb . " " . $qcc . " " . $qdd . "  ORDER BY PartNumber ASC ";
            $PartNumberF        = mysqli_query($connect_asm, $query_Part_NumberF);
            while ($rows = mysqli_fetch_assoc($PartNumberF)) {
                $data[] = array($rows['PartNumber']);
            }
            //$data[]=array($query_Mold_NumberF);
            echo json_encode($data);
            break;
        }
    case 2:
        {
            $query_LineNumber = "SELECT DISTINCT(LineNumber) FROM modify_measurecontent WHERE 1=1 " . $qaa . " " . $qbb . " " . $qcc . " " . $qdd . "  ORDER BY LineNumber ASC";
            $LineNumberF      = mysqli_query($connect_asm, $query_LineNumber);
            while ($rows = mysqli_fetch_assoc($LineNumberF)) {
                $data[] = array($rows['LineNumber']);
            }
            echo json_encode($data);
            break;
        }
}
