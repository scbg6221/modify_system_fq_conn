<?php
/*
 * Copyright (C) 2015 Foxlink SCBG CAE Dept. All rights reserved
 * Project: Wanhao System
 * File Name: Spec Search-2
 * Function: Part Number List
 * Author: Bruce Huang
 * --------------------------------------------------
 * Rev: 1.2 Date: PM 04:34 2015/01/28 Modifier: Bruce Huang
 * --------------------------------------------------
 */
session_start();
include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
error_reporting(0);

$a = $_SESSION['Part_Number_VF'];

mysqli_select_db($connect_spec, $database_spec);

$query_listout_S = "SELECT * FROM modify_visual_spec_assembly WHERE PartNumber = '$a' AND Rev='初件檢驗' order by `id`";
$listout_S       = mysqli_query($connect_spec, $query_listout_S) or die(mysqli_error());
$num             = mysqli_num_rows($listout_S);

$query_listout_S1 = "SELECT * FROM modify_visual_spec_assembly WHERE PartNumber = '$a' AND Rev='巡查檢驗' order by `id`";
$listout_S1       = mysqli_query($connect_spec, $query_listout_S1) or die(mysqli_error());
$num1             = mysqli_num_rows($listout_S1);
?>

<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>

<style type="text/css">
body {
  font: normal medium/1.4 sans-serif;
}
table {
  border-collapse: collapse;
  width: 900px;
}
th{
  padding: 0.25rem;
  text-align: center;
  border: 1px solid #ccc;
  background: #888888;
  font-size:15px;

}
td {
  padding: 0.25rem;
  text-align: center;
  border: 1px solid #ccc;
  font-size:13px;

}
B{
	font-family:"Arial Black", Gadget, sans-serif;
	color:#00000;
	}
tbody tr:nth-child(odd) {
  background: #eee;
}

.ifile {
	position:absolute;opacity:0;filter:alpha(opacity=0);

}
</style>

</head>
<body>
   <tr>
      <td >
      <input type="submit"  value="返回查詢已上傳料號"  OnClick="window.open('ModifyVisualSpec_Search-3.php','Index_Content')">
      </td>
   </tr>

<br>
初件檢驗SPEC:<?php if ($num == 0) {echo '此料號初件檢驗SPEC未上傳';}?><br>
<div class="Measure-Data-table-2">
<table id="Measure-Data-table-2" class="sortable">
<thead>
<th width='50px'><div align="center">檢查工站</div></th>
<th width='50px'><div align="center">檢查項目</div></th>
<th width='35px'><div align="center">上限</div></th>
<th width='35px'><div align="center">下限</div></th>
<th width='35px'><div align="center">備註</div></th>
</thead>
  <div align="center"></div>
<tbody>

<?php
while ($listout = mysqli_fetch_assoc($listout_S)) {

    echo "<tr>";
    echo "<td>" . $listout['checkstop'] . "</td>";
    echo "<td>" . $listout['checkitem'] . "</td>";
    echo "<td>" . $listout['up_lim'] . "</td>";
    echo "<td>" . $listout['down_lim'] . "</td>";
    echo "<td>" . $listout['Remark'] . "</td>";
    echo "</tr>";

    $color = "";
}
?>
</tbody>
</table>


<br>
巡查檢驗SPEC:<?php if ($num1 == 0) {echo '此料號巡查檢驗SPEC未上傳';}?><br>
<div class="Measure-Data-table-2">
<table id="Measure-Data-table-2" class="sortable">
<thead>
<th width='50px'><div align="center">檢查工站</div></th>
<th width='50px'><div align="center">物料</div></th>
<th width='50px'><div align="center">物料料號</div></th>
<th width='50px'><div align="center">檢查項目</div></th>
<th width='35px'><div align="center">上限</div></th>
<th width='35px'><div align="center">下限</div></th>
<th width='35px'><div align="center">備註</div></th>
</thead>
  <div align="center"></div>
<tbody>

<?php
while ($listout1 = mysqli_fetch_assoc($listout_S1)) {

    echo "<tr>";
    echo "<td>" . $listout1['checkstop'] . "</td>";
    echo "<td>" . $listout1['sort'] . "</td>";
    echo "<td>" . $listout1['Part_Number_V'] . "</td>";
    echo "<td>" . $listout1['checkitem'] . "</td>";
    echo "<td>" . $listout1['up_lim'] . "</td>";
    echo "<td>" . $listout1['down_lim'] . "</td>";
    echo "<td>" . $listout1['Remark'] . "</td>";
    echo "</tr>";

}
?>
</tbody>
</table>





</body>
</html>


