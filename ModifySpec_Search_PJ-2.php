<?php
/*
 * Copyright (C) 2015 Foxlink SCBG CAE Dept. All rights reserved
 * Project: Wanhao System
 * File Name: Spec Search-3
 * Function: Display all SPEC in data base
 * Author: Bruce Huang
 * --------------------------------------------------
 * Rev: 1.2 Date: PM 04:34 2015/01/28 Modifier: Bruce Huang
 * --------------------------------------------------
 */
session_start();
include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/library/PHPExcel/php-excel-reader-2.21/simplexlsx.class.php';
error_reporting(0);

//搜尋Assembly DB內SPEC專案數量
mysqli_select_db($connect2, $database2);
$query_listout_U = "SELECT ProjectName FROM modify_spec_assembly UNION SELECT ProjectName FROM modify_spec_stamping UNION SELECT ProjectName FROM modify_spec_molding UNION SELECT ProjectName FROM modify_spec_plating UNION SELECT ProjectName FROM modify_spec_welding UNION SELECT ProjectName FROM modify_spec_blasting UNION SELECT ProjectName FROM modify_spec_iqc group by ProjectName ";
$listout_U       = mysqli_query($connect_spec, $query_listout_U) or die(mysqli_error());
$total_num       = mysqli_num_rows($listout_U);
$total_row1      = $total_num / 6;

//重新排列SPEC專案顯示(一排顯示六個SPEC專案)
if (is_int($total_row1)) {
    $i = 0;
    $j = 6;

    for ($k = 1; $k <= $total_row1; $k++) {
        ${'A' . $k} = "SELECT ProjectName FROM modify_spec_assembly UNION SELECT ProjectName FROM modify_spec_stamping UNION SELECT ProjectName FROM modify_spec_molding UNION SELECT ProjectName FROM modify_spec_plating UNION SELECT ProjectName FROM modify_spec_welding UNION SELECT ProjectName FROM modify_spec_blasting UNION SELECT ProjectName FROM modify_spec_iqc group by ProjectName order by ProjectName limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

            ${'data_PJName' . $k}[] = ${'C' . $k}['ProjectName'];
        }
        $i += $j;
    }
    $total_row = $total_row1;
} else {
    $total_row2 = floor($total_row1);
    $total_row3 = ($total_row2) + 1;
    $i               = 0;
    $j               = 6;

    for ($k = 1; $k <= $total_row2; $k++) {
        ${'A' . $k} = "SELECT ProjectName FROM modify_spec_assembly UNION SELECT ProjectName FROM modify_spec_stamping UNION SELECT ProjectName FROM modify_spec_molding UNION SELECT ProjectName FROM modify_spec_plating UNION SELECT ProjectName FROM modify_spec_welding UNION SELECT ProjectName FROM modify_spec_blasting UNION SELECT ProjectName FROM modify_spec_iqc group by ProjectName order by ProjectName limit $i,$j";
        ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
        while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

            ${'data_PJName' . $k}[] = ${'C' . $k}['ProjectName'];
        }
        $i += $j;
    }

    $i          = ($total_row2) * 6;
    $j          = 6;
    $k          = $total_row3;
    ${'A' . $k} = "SELECT ProjectName FROM modify_spec_assembly UNION SELECT ProjectName FROM modify_spec_stamping UNION SELECT ProjectName FROM modify_spec_molding UNION SELECT ProjectName FROM modify_spec_plating UNION SELECT ProjectName FROM modify_spec_welding UNION SELECT ProjectName FROM modify_spec_blasting UNION SELECT ProjectName FROM modify_spec_iqc group by ProjectName order by ProjectName limit $i,$j";
    ${'B' . $k} = mysqli_query($connect_spec, ${'A' . $k}) or die(mysqli_error());
    while (${'C' . $k} = mysqli_fetch_assoc(${'B' . $k})) {

        ${'data_PJName' . $k}[] = ${'C' . $k}['ProjectName'];
    }
    $total_row = $total_row3;
}
?>

<!DOCTYPE HTML><head>
    <meta charset="utf-8">
    <title>Untitled Document</title></head>

    <style type="text/css">
        body {
          font: normal medium/1.4 sans-serif;
      }
      table {
          border-collapse: collapse;
      }
      th{
          padding: 0.25rem;
          text-align: center;
          border: 1px solid #ccc;
          background: #888888;
          font-size:15px;

      }
      td {
          padding: 0.25rem;
          text-align: center;
          border: 1px solid #ccc;
          font-size:13px;

      }
      B{
       font-family:"Arial Black", Gadget, sans-serif;
       color:#00000;
   }
   tbody tr:nth-child(odd) {
      background: #eee;
  }

  .ifile {
   position:absolute;opacity:0;filter:alpha(opacity=0);

}
</style>
</head>

<body background="Images/loginb.png">
    <table width='1400' method="post" style="table-layout:fixed">
        <thead>
            <tr>
                <th width='100' colspan=6>已上傳專案名稱</th>
            </tr>
        </thead>

        <tbody>
            <?php
            echo "<tr>";
            for ($k = 1; $k <= $total_row; $k++) {
                for ($a = 0; $a < 6; $a++) {
                    echo "<td width=100% align=right>" . ${'data_PJName' . $k}[$a] . "</td>";
                }
                echo "</tr>";
            }

            ?>
        </tbody>
    </table>
</body>
</html>


