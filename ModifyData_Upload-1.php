<?php
if (!isset($_SESSION)) {
    session_start();
}
?>
<?php
error_reporting(0);
include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';

require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/library/Other/Fork.php';
/////////////////////////////////////////////////////////////////////////////////////////////////////
mysqli_select_db($connect_stmp, $database_stmp);
//$Dept_array = array('組裝', '沖壓', '成型');
$Dept_array = array('沖壓', '成型');//刪除"組裝"選項(因組裝目前是要依據當日生產機種生成數據,不是和跑多天數據生成)

if ($_POST['EndDateF']) {
    $c_PartNumber = $_POST['PartNumberF'];
    $c_StartDate  = $_POST['StartDateF'];
    $c_EndDate    = $_POST['EndDateF'];
} else {
    $c_EndDate   = date('Y-m-d') . " 07:59:59";
    $c_StartDate = date('Y-m-d', strtotime($c_EndDate) - 60 * 60 * 24 * 10) . " 08:00:00";
}

if ($_POST['DeptF']) {
    $c_Dept = $_POST['DeptF'];
//echo $c_Dept;

    if ($c_Dept == '組裝') {
        $SpecTable = "modify_spec_assembly";
        $ConnItem  = "connect_asm";
    } elseif ($c_Dept == '沖壓') {
        $SpecTable = "modify_spec_stamping";
        $ConnItem  = "connect_stmp";
    } elseif ($c_Dept == '成型') {
        $SpecTable = "modify_spec_molding";
        $ConnItem  = "connect_mold1";
    }
}

if ($_POST['PartNumberF']) {
    $qa  = "AND PartNumber='$c_PartNumber'";
    $qaa = urlencode($qa);}

$fork   = new Fork;
$output =
$fork
    ->add("http://localhost/MainWebsite/Modify_System/FQ_Conn/ModifyData_Upload-A.php?t=0&Dept=" . $c_Dept . "&SpecTable=" . $SpecTable . "&ConnItem=" . $ConnItem . "&qaa=" . $qaa)
    ->run();
?>


<!DOCTYPE HTML>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>

<script type="text/javascript" src="../../Public/library/JQuery/jquery-1.11.3/jquery-1.11.3.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/datetimepicker/datepicker.js"></script>
<script type="text/javascript" src="../../Public/library/JQuery/datetimepicker/jquery-ui-timepicker-addon.css"></script>
<script type="text/javascript" src="../../Public/library/JQuery/datetimepicker/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="../../Public/library/JQuery/jquery-ui-1.11.4.custom/jquery-ui.min.css"/>
<link rel="stylesheet" type="text/css" href="CSS/ALL_CSS.css?id='ssaa'">
<script type="text/javascript">
$(function() {
  $( "#StartDateF").datetimepicker({
    dateFormat: 'yy-mm-dd',
    showSecond: true,
    timeFormat: 'HH:mm:ss',
    showOn : "focus",
    controlType:"select"

  });
  $( "#EndDateF").datetimepicker({
    dateFormat: 'yy-mm-dd',
    showSecond: true,
    timeFormat: 'HH:mm:ss',
    showOn : "focus",
    controlType:"select"
  });


});
function timecheck(){
  document['form1'].action = "ModifyData_Upload-1.php";
  document['form1'].target = 'Index_Search';
  document['form1'].submit();
}
function drop()
{
  document['form1'].action = "ModifyData_Upload-1.php";
  document['form1'].target = 'Index_Search';
	document['form1'].submit();
}
function dataupload()
{
  //var ProgramName = '<?php echo $ProgramName ?>';
  document['form1'].action = "ModifyData_Upload-Check.php";
  document['form1'].target = 'Index_Content';
	document['form1'].submit();
}
</script>

</head>
<body background="Images/loginb.png">
<form id="form1" name="form1" method="post" >
<div style="line-height:40px">
<VisualL>起始日:</VisualL>
<input type="text" name="StartDateF" id="StartDateF" value="<?php echo $c_StartDate ?>" onChange="timecheck()" class="DataUpload-2">
<VisualL>結束日:</VisualL>
<input type="text" name="EndDateF" id="EndDateF" class="DataUpload-2" value="<?php echo $c_EndDate ?>" onChange="timecheck()">
</div>

<!-------------------------------------Dept------------------------------------------->
<div style="line-height:40px">
<VisualL>類別:(*)</VisualL>
  <select name="DeptF" id="DeptF" class="DataUpload-1" onChange="drop();">

  <?php
echo "<option></option>";
foreach ($Dept_array as $c) {
    echo "<option value='" . $c . "'" . ($c == $c_Dept ? "selected" : "") . ">" . $c . "</option>";
}
?>
  </select>
<!-------------------------------------Part_Number_VF------------------------------------------->
<?php
echo "
<VisualL>料號:(*)</VisualL>
<select name='PartNumberF' id='PartNumberF' class='DataUpload-2' onChange='drop();''>
<option></option>";

foreach ($output[0] as $e) {
    foreach ($e as $c) {
        echo "<option value='" . $c . "'" . ($c == $c_PartNumber ? "selected" : "") . ">" . $c . "</option>";
    }
}

?>
</select>
</div>
</BR>

<div style="float:left">
<input type="button" name="submitA" id="submitA"  value="生成數據" class="DataUpload-BT" onClick="dataupload()"/>
</form>
</div>
</div>
</form>
</body>
</html>

