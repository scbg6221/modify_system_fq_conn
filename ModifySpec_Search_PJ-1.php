<?php
if (!isset($_SESSION)) {
    session_start();
} //ob_start();
// session_destroy();
include '../../Public/MainWebUI/User_Count.php';
include '../../Public/MainWebUI/Login_Control.php';
require_once '../../Public/Connections/modify_system_fq_icbu.php';
require_once '../../Public/library/Other/Fork.php';

require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/IOFactory.php';
require_once'../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel/Writer/Excel2007.php';
require_once '../../Public/library/PHPExcel/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

//ob_end_clean();
error_reporting(0);

if (isset($_POST['ProjectName'])) {
    $_SESSION['ProjectName'] = $_POST['ProjectName'];
}

//執行轉跳
if (isset($_POST['Submit_Search'])) {
    header('Location: ModifySpec_Search_PJ-3.php');
}

if (isset($_POST['Submit_Download'])) {

    if ($_POST['ProjectName'] != "") {

        $ProjectName=$_POST['ProjectName'];
        $select_searchFN     = "SELECT fileName from modify_spec_assembly where ProjectName='$ProjectName' UNION SELECT fileName from modify_spec_molding where ProjectName='$ProjectName'  UNION SELECT fileName from modify_spec_stamping where ProjectName='$ProjectName'  UNION SELECT fileName from modify_spec_plating where ProjectName='$ProjectName'  UNION SELECT fileName from modify_spec_welding where ProjectName='$ProjectName'  UNION SELECT fileName from modify_spec_blasting where ProjectName='$ProjectName'  UNION SELECT fileName from modify_spec_iqc where ProjectName='$ProjectName' group by fileName order by fileName";
        //echo $select_searchFN;

        $query_searchFN = mysqli_query($connect_spec, $select_searchFN) or die(mysqli_error());
        $FileName_array=$fileroute_array=array();

        while ($searchFN = mysqli_fetch_assoc($query_searchFN)) {

            //這是要打包的檔案地址陣列
            $fileroute_array[]="../../Spec\Modify_System\FQ_Conn\DimSpec/" .$searchFN['fileName'];
        }

        //這裡需要注意該目錄是否存在，並且有建立的許可權
        $zipname = './'.$ProjectName.'.zip'; 
        $zip = new ZipArchive();
        $res = $zip->open($zipname, ZipArchive::CREATE);
        if ($res === TRUE) {
            foreach ($fileroute_array as $file) {
 //這裡直接用原檔案的名字進行打包，也可以直接命名，需要注意如果檔名字一樣會導致後面檔案覆蓋前面的檔案，所以建議重新命名
             $new_filename = substr($file, strrpos($file, '/') + 1);
             $zip->addFile($file, $new_filename);
         }
           //關閉檔案
         $zip->close();

           //這裡是下載zip檔案
         header("Content-Type: application/zip");
         header("Content-Transfer-Encoding: Binary");
         header("Content-Length: " . filesize($zipname));
         header("Content-Disposition: attachment; filename=\"" . basename($zipname) . "\"");
         readfile($zipname);
         unlink($zipname);  
         exit;
     }    
     header('Location: ModifySpec_Search_PJ-2.php');

 } else {
    echo "<script> alert('請輸入欲下載的料號');self.location.href='ModifySpec_Search_PJ-2.php'; </script>";
}

}

?>

<!DOCTYPE HTML>
<head>
    <meta charset="utf-8">
    <title>Untitled Document</title>
    <link rel="stylesheet" type="text/css" href="CSS/ALL_CSS.css?id='ssaaa'">
    <script>
var $j = jQuery.noConflict(); //解決不同版本問題

$j(document).ready(function(){
    $("#Part_Number_VF").autocomplete("ModifySpec_Search_PJ-3.php", {
        selectFirst: true
    });
})

</script>

<script type="text/javascript">
    function result()
    {
        document['form1'].action = "ModifySpec_Search_PJ-1.php";
        document['form1'].target = 'Index_Content';
    }

    function specdelete()
    {
        document['form1'].action = "ModifySpec_Search_PJ-1.php";
        document['form1'].target = 'Index_Content';
    }

    function specdownload(PN)
    {
        document['form1'].action = "ModifySpec_Search_PJ-1.php";
        document['form1'].target = 'Index_Content';
    }
</script>

<style>
</style>
</head>

<body background="Images/loginb.png">
  <form name="form1" enctype="multipart/form-data" method="post">
      <table width="1000" cellpadding="5" cellspacing="5" frame="void" rules="groups" align="left">
          <tr>
           <td width=1000 height=30 align=left colspan="2">
              <VisualL>尺寸優化規格書搜尋</VisualL></td>
          </tr>

          <tr>
              <td width=70 height=30 align=left>
                  <VisualL>專案名稱(*):</VisualL></td>

                  <td width=125 height=30 align=left>
                    <input type="text" name="ProjectName" id="ProjectName" class="SpecSearch-1" value="<?php echo $_POST['ProjectName'] ?>"></td>


                    <td width=100 height=30 align=left>
                        <input type="Submit" name="Submit_Search" id="Submit_Search" value="搜尋" class="SpecSearch-BT" onclick="result()"></td>


                        <td width=100 height=30 align=left>

                          <input type="Submit" name="Submit_Download" id="Submit_Download" value="下載" class="SpecSearch-BT" onclick="specdownload()"></td>

                      </tr>

                  </table>
              </form>
          </body>
          </html>

